<?php

use studentsportal\assets\StudentsPortalAsset;
use kartik\sidenav\SideNav;

StudentsPortalAsset::register($this);

$url = Yii::$app->getUrlManager();
$item = Yii::$app->controller->id . "/" . Yii::$app->controller->action->id;
?>

<?php $this->beginBlock('appName'); ?>
<?php $this->endBlock() ?>
<?php $this->beginBlock('menu') ?>
<?php if (!Yii::$app->user->isGuest && isset($_SESSION['update']) && empty($_SESSION['update'])): ?>
    <?=

    SideNav::widget([
        'type' => SideNav::TYPE_DEFAULT,
        'heading' => '',
        'items' => [
            ['url' => $url->createUrl(['/site/index']), 'label' => 'My Profile', 'icon' => 'user', 'active' => ($item == 'site/index')],
            ['url' => $url->createUrl(['/registration/index']), 'label' => 'Registration', 'icon' => 'file', 'active' => ($item == 'registration/index')],
            ['url' => $url->createUrl(['/renewal']), 'label' => 'Renewal', 'icon' => 'refresh'],
            [
                'label' => 'Exemptions',
                'icon' => 'eject',
                'items' => [
                    ['label' => 'Kasneb Graduate', 'icon' => 'eject', 'url' => $url->createUrl(['/exemption/index']), 'active' => ($item == 'exemption/index')],
                    ['label' => 'Other Qualifications', 'icon' => 'eject', 'url' => $url->createUrl(['/exemption/others']), 'active' => ($item == 'exemption/others')],
                ],
            ],
            [
                'label' => 'Examination',
                'icon' => 'folder-close',
                'items' => [
                    ['label' => 'Examination Booking', 'icon' => 'pencil', 'url' => $url->createUrl(['/examination/index']), 'active' => ($item == 'examination/index')],
                    ['label' => 'Examination Center Booking', 'icon' => 'ok', 'url' => $url->createUrl(['/examination/center-booking']), 'active' => ($item == 'examination/center-booking')],
                ],
            ],
            ['url' => $url->createUrl(['/invoices/index']), 'label' => 'Invoices', 'icon' => 'book', 'active' => ($item == 'invoices/index')],
            ['url' => $url->createUrl(['/transactions/index']), 'label' => 'My Transactions', 'icon' => 'th', 'active' => ($item == 'transactions/index')],
            [
                'label' => 'Resources',
                'icon' => 'folder-open',
                'items' => [
                    ['label' => 'Examination Timetable', 'icon' => 'file', 'url' => $url->createUrl(['/site/timetable']), 'active' => ($item == 'site/timetable')],
                    ['label' => 'Exemption Letter', 'icon' => 'file', 'url' => $url->createUrl(['/site/exemption-letter']), 'active' => ($item == 'site/exemption-letter')],
                    ['label' => 'E-Learning', 'icon' => 'file', 'url' => 'http://library.kasneb.or.ke/', 'template' => '<a href="{url}" target="_blank">{icon}{label}</a>'],
               ],
            ],
            [
                'label' => 'Payments',
                'icon' => 'eject',
                'items' => [
                    ['label' => 'Wallet Balance', 'icon' => 'credit-card', 'url' => $url->createUrl(['/site/balance']), 'active' => ($item == 'site/balance')],
                    //['label' => 'Wallet Statement', 'icon' => 'credit-card', 'url' => $url->createUrl(['/site/wallet-statement']), 'active' => ($item == 'site/wallet-statement')],
                    ['label' => 'Top up', 'icon' => 'credit-card', 'url' => $url->createUrl(['/site/topup']), 'active' => ($item == 'site/topup')],
                    ['label' => 'Pay Invoice', 'icon' => 'credit-card', 'url' => $url->createUrl(['/invoices/index']), 'active' => ($item == 'invoices/index')],
                    ['label' => 'Refund Request', 'icon' => 'credit-card', 'url' => $url->createUrl(['/site/request-refund']), 'active' => ($item == 'site/request-refund')],
                ],
            ], 
            [
                'label' => 'FAQs',
                'icon' => 'list',
                'url' => '#'
            ], 
        ],
    ]);
    ?>
<?php endif; ?>
<?php $this->endBlock() ?>

<?= $this->render('@common/views/layouts/main', ['content' => $content])?>