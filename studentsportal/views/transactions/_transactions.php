<?php

use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\Html;
?>
<?=

GridView::widget([

    'dataProvider' => new ArrayDataProvider([
        'allModels' => $datas,
            ]),
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute' => 'totalAmount',
            'value' => function($data) {
                return "KSH ".number_format($data->totalAmount);
            }
        ],
        'channel',
        [
            'attribute' => 'paymentTimestamp',
            'value' => function($data) {
                return $data->paymentTimestamp ;
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{receipt}&nbsp;&nbsp;',
            'buttons' => [
                'receipt' => function($url, $model) {
                    $url = Url::toRoute(['transactions/print', 'id' => $model->id]);
                    return Html::a('<span class="glyphicon glyphicon-print"></span> download', $url, ['target'=>'_blank','title' => Yii::t('yii', 'receipt')]);
                },
            ],
        ],
        ],
    ]);
?>