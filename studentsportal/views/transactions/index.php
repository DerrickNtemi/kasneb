<?php
$this->title = Yii::t('app', 'My Transactions');
?>
<h1 class='header-h1'><?= $this->title ?></h1>
<div class="row header-underline"></div>
<h1></h1>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-8 personal-details-div"><?= $this->title ?></div>
                    <div class="col-md-4 text-right">
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <?= $this->render('@common/views/_filterTransanctions',['model'=>$filter,'revenueStreams'=>$revenueStream]) ?>
                <?= $this->render('_transactions', ['datas'=>$datas]) ?>
            </div>
        </div>
    </div>
</div>