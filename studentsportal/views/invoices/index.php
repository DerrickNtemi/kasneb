<?php
$this->title = Yii::t('app', 'My Invoice(s)');
?>
<h1 class='header-h1'><?= $this->title ?></h1>
<div class="row header-underline"></div>
<h1></h1>
<?php if (Yii::$app->session->hasFlash('success-message')): ?>
    <div class="alert alert-success"><?= Yii::$app->session->getFlash('success-message') ?></div>
<?php endif; ?>

<?php if (Yii::$app->session->hasFlash('error-message')): ?>
    <div class="alert alert-danger"><?= Yii::$app->session->getFlash('error-message') ?></div>
<?php endif; ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-8 personal-details-div"><?= $this->title ?></div>
                    <div class="col-md-4 text-right">
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <?= $this->render('_invoices', ['invoices'=>$invoices]) ?>
            </div>
        </div>
    </div>
</div>