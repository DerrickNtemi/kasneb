<?php

use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\Html;
?>
<?=

GridView::widget([

    'dataProvider' => new ArrayDataProvider([
        'allModels' => $invoices,
            ]),
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'label' => 'Invoice No.',
            'value' => function($data) {
                return $data->reference;
            }
        ],
        [
            'label' => 'Amount',
            'value' => function($data) {
                return $data->localCurrency." ".number_format($data->localAmount);
            }
        ],
        [
            'label' => 'Timestamp',
            'value' => function($data) {
                return $data->dateGenerated;
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view}&nbsp;&nbsp;{invoice}&nbsp;&nbsp;',
            'buttons' => [
                    'view' => function($url, $model) {
                        $url = Url::toRoute(['invoices/pay', 'id' => $model->id]);
                        return Html::a('view', $url, ['title' => 'View Details']);
                    },
                'invoice' => function($url, $model) {
                    $url = Url::toRoute(['invoices/print', 'id' => $model->id]);
                    return Html::a('download', $url, ['target'=>'_blank','title' => Yii::t('yii', 'invoice')]);
                },
                ],
            ],
        ],
    ]);
?>