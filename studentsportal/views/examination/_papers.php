<?php
use yii\widgets\ActiveForm;
$form = ActiveForm::begin([
            'id' => 'examination-paper-form',
            'action' => Yii::$app->urlManager->createUrl(['examination/create-sitting-paper']),
            'options' => ['enctype' => 'multipart/form-data',],
            'fieldConfig' => [
                'template' => "{label}{input}\n{hint}\n{error}",
            ],
        ]);
$name = "";
$paperDatas = [];
if ($courseTypeCode == 100) {
    $name = $papersData['eligiblePart']['name'];
    $papersDatas = $papersData['eligiblePart']['sections'];
} else {
    $name = $papersData['eligibleLevels'][0]['name'];
    $papersDatas = $papersData['eligibleLevels'];
}
//print_r($papersDatas);exit;
?>
<div class="panel-group mt10">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="row form-rows">
                        <p style="margin-left: 10px;"><b><?= $name ?></b></p>
                        <?php $i = 1;
                        foreach ($papersDatas as $paperDt) : ?>                            
                            <p style="margin-left: 20px;">
                                <input type="checkbox" id="sectionId<?= $i ?>" value="<?= $paperDt['name'] ?>" <?= empty($paperDt['optional']) ? 'checked disabled="disabled"' : ""  ?>><b><?= " ".$paperDt['name'] ?></b>
                            </p>
                            <?php $i++; ?>
    <?php foreach ($paperDt['papers'] as $paperDt1) : ?>
                                <p style="margin-left: 30px;"><b><?= $paperDt1['code'] ?> : </b><?= $paperDt1['name'] ?> </p>
    <?php endforeach; ?>
<?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>            
<?php ActiveForm::end(); ?>