<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$form = ActiveForm::begin([
            'id' => 'examination-sitting-form',
            'action' => Yii::$app->urlManager->createUrl(['examination/create-sitting']),
            'options' => ['enctype' => 'multipart/form-data',],
            'fieldConfig' => [
                'template' => "{label}{input}\n{hint}\n{error}",
            ],
        ]);
?>
<?= Html::activeHiddenInput($model, 'id') ?>
<?= Html::activeHiddenInput($model, 'studentId') ?>
<?= Html::activeHiddenInput($model, 'courseId') ?>
<div class="panel-group mt10">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="row form-row-below">                        
                        <?= $form->field($model, 'month', [
                                'options' => ['class' => 'col-md-4'],
                                'inputOptions' => ['class' => 'form-control  form-control-custom'],
                            ])->dropDownList($sittingArray, ['prompt'=>'Choose sitting...']) ?>                        
                            
                       
                    </div>
                    <div class="row form-rows">                          
                            <?=
                                $form->field($model, 'year', [
                                    'options' => ['class' => 'col-md-4'],
                                    'inputOptions' => ['class' => 'form-control  form-control-custom capitalize-input','readonly'=>true],
                                ])?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>            
<?php ActiveForm::end(); ?>