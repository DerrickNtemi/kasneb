<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;

$this->title = Yii::t('app', 'Examination');
?>
<h1 class='header-h1'><?= $this->title ?></h1>
<div class="row header-underline"></div>
<?= $this->render('@common/views/_needHelp') ?>

<!-- BASIC WIZARD -->
<div id="examinationwizard"  class="basic-wizard" >
    <div class="row nav-tab-row">
        <div class="col-md-12">
            <ul class="nav nav-pills nav-justified nav-list-container" id="myTab">
        <li>
            <a href="#tab1" data-toggle="tab" style="padding:0px;">
                <div class="row">
                    <div class="col-md-3 nav-step"><span>1</span> </div>
                    <div class="col-md-9 nav-div">Sitting</div>
                </div>
            </a>
        </li>
        <li>
            <a href="#tab2" data-toggle="tab" style="padding:0px;">
                <div class="row">
                    <div class="col-md-3 nav-step"><span>2</span> </div>
                    <div class="col-md-9 nav-div">Papers</div>
                </div>
            </a>
        </li>
        <li>
            <a href="#tab3" data-toggle="tab" style="padding:0px;">
                <div class="row">
                    <div class="col-md-3 nav-step"><span>3</span> </div>
                    <div class="col-md-9 nav-div">Bill Presentment</div>
                </div>
            </a>
        </li>
        <li>
            <a href="#tab4" data-toggle="tab" style="padding:0px;">
                <div class="row">
                    <div class="col-md-3 nav-step"><span>4</span> </div>
                    <div class="col-md-9 nav-div">Centers</div>
                </div>
            </a>
        </li>
    </ul>
        </div>
    </div>
    <h2></h2>
    <div id="bar" class="progress">
        <div class="bar progress-bar progress-bar-striped active" id="wizard-progress-bar"></div>
    </div>
    <div class="tab-content">
        <div class="tab-pane" id="tab1">
            <h2></h2><h2></h2>
            <div class="alert alert-danger"  id='sitting-error-summary' style="display: none;"> </div>
            <div>
                <?= $this->render('_sitting', ['model' => $sittingModel, 'sittingArray' => $sittingArray]) ?>
            </div>
        </div>

        <div class="tab-pane" id="tab2">
            <h2></h2><h2></h2>      
            <div class="alert alert-danger"  id='papers-error-summary' role="alert" style="display: none;"> </div>
            <div>
                <?= $this->render('_papers', ['courseTypeCode'=>$courseTypeCode,'model' => $paperModel, 'papersData' => $papersData]) ?>
            </div>
        </div>

        <div class="tab-pane" id="tab3"> 
            <h2></h2><h2></h2>       
            <div class="alert alert-danger"  id="bill-error-summary" role="alert" style="display: none;"></div>
            <div id="invoice-presentment">
            </div>
        </div>

        <div class="tab-pane" id="tab4">
            <h2></h2><h2></h2>      
            <div class="alert alert-danger"  id='centers-error-summary' role="alert" style="display: none;"> </div>
            <div>
                <?= $this->render('_centers', ['model' => $centerModel, 'centersArray' => $centersArray, 'zones' => $zones]) ?>
            </div>
        </div>        
    </div>

    <ul class="pager wizard">
        <li class="previous"><a href="javascript:;">Previous</a></li>
        <li class="next"><a href="javascript:;">Next</a></li>
        <li class="next finish" id="exam-finish" style="display:none;"><a href="javascript:;">Finish</a></li>
    </ul>
</div>
<div id="loadingDiv"></div>

<!-- Exemption Modal -->
<?php
Modal::begin([
    'id' => 'ExemptionPaperModal',
    'header' => '<h4 class="modal-title" id="expense-account-form-header">Exemption Paper</h4>',
    'footer' =>
    Html::button('Cancel', ['class' => 'btn btn-default', 'data-dismiss' => 'modal'])
    . PHP_EOL .
    Html::button('Add', ['id' => 'exemption-btn', 'class' => 'btn btn-primary btn-modal-save']),
]);
?>
<?php Modal::end() ?>