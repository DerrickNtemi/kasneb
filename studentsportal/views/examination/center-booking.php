<?php
$this->title = Yii::t('app', 'Examination Center Booking');
?>
<h1 class='header-h1'><?= $this->title ?></h1>
<div class="row header-underline"></div>
<?= $this->render('@common/views/_needHelp') ?>
<div class="alert alert-danger"  id='centers-error-summary' role="alert" style="display: none;"> </div>
<div>
    <?= $this->render('_centers', ['model' => $centerModel, 'centersArray' => $centersArray, 'zones' => $zones, 'visible' => true]) ?>
</div>

