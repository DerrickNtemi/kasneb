<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

$form = ActiveForm::begin([
            'id' => 'centers-form',
            'action' => Yii::$app->urlManager->createUrl(['examination/exam-center']),
            'options' => ['enctype' => 'multipart/form-data',],
            'fieldConfig' => [
                'template' => "{label}{input}\n{hint}\n{error}",
            ],
        ]);
?>
<?= Html::activeHiddenInput($model, 'id') ?>
<div class="panel-group mt10">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <?=
                        $form->field($model, 'zone', [
                            'options' => ['class' => 'col-md-5'],
                            'inputOptions' => ['class' => 'form-control'],
                        ])->dropDownList($zones)
                        ?>
                    </div> 
                    <div class="row">
                        <?=
                        $form->field($model, 'examCenter', [
                            'options' => ['class' => 'col-md-5'],
                            'inputOptions' => ['class' => 'form-control'],
                        ])->dropDownList($centersArray, ['prompt' => 'Select Center...'])
                        ?>
                    </div>   
                </div>
            </div>

            <?php
            if (isset($visible)) {
                ?><p class="btn btn-sm btn-primary next finish" id="exam-finish-btn">Save</p>    <?php
            }
            ?>
        </div>
    </div>
</div>  
<?php ActiveForm::end(); ?>