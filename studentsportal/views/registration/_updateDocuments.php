<?php

use kartik\form\ActiveForm;
use yii\helpers\Html;

$form = ActiveForm::begin([
            'id' => 'documents-update-form',
            'action' => Yii::$app->urlManager->createUrl(['registration/update-documents']),
            'options' => ['enctype' => 'multipart/form-data',],
            'fieldConfig' => [
                'template' => "{label}{input}\n{hint}\n{error}",
            ],
        ]);

if ($model->hasErrors()) {
    echo $form->errorSummary($model, ['header' => '']);
}
?>
<?= Html::activeHiddenInput($model, 'studentId') ?>
<?= Html::activeHiddenInput($model, 'courseId') ?>

<div class="row">
    <div class="col-md-12">
        <div class="row form-row-below">        
            <div class="col-md-6"> 
                <?php
                echo $form->field($model, 'document[]', [
                    'inputOptions' => ['class' => 'form-control form-control-custom'],
                ])->fileInput(['multiple' => true]);
                ?>
            </div> 
        </div>
        <div class="row form-row-below">        
            <div class="col-md-6">
                <button class="btn btn-sm btn-primary" >Save Uploads</button>
            </div>
        </div>
    </div> 
</div>
<?php ActiveForm::end(); ?>