<?php

use kartik\form\ActiveForm;
use yii\helpers\Html;

$learn["Career talks"] = "Career talks";
$learn["Media"] = "Media";
$learn["Student"] = "Student";
$learn["Sponsor"] = "Sponsor";
$learn["Guardian"] = "Guardian";
$learn["Parent"] = "Parent";
$learn["Friend"] = "Friend";
$learn["Others"] = "Others";
$form = ActiveForm::begin([
            'id' => 'more-details-form',
            'action' => Yii::$app->urlManager->createUrl(['registration/more-details']),
            'options' => ['enctype' => 'multipart/form-data',],
            'fieldConfig' => [
                'template' => "{label}{input}\n{hint}\n{error}",
            ],
        ]);
?>
<?= Html::activeHiddenInput($model, 'id') ?>
<?= Html::activeHiddenInput($model, 'selection') ?>
<div class="panel-group mt10">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">                    
                    <?php
                    $i = 1;
                    foreach ($declarations as $declaration) {
                        $class = "form-control";
                        ?>

                        <div class="row">
                            <div class="col-md-5"><?= $i . " " . $declaration['description'] ?></div>
                                <div class="col-md-3">
                                    <?php if ($declaration['description'] != "How did you learn about Kasneb?") { ?>
                                <?= Html::input('radio', 'declarationId' . $declaration['id'], "Y" . $declaration['id'], ['onclick' => "declarationSpecification(this);"]) ?>Yes
                                    <?= Html::input('radio', 'declarationId' . $declaration['id'], "N" . $declaration['id'], ['onclick' => "declarationSpecification(this);"]) ?>No

                                    <?php } else { ?>
                                    <select class="form-control  form-control-custom" style="margin-top:5px;" name="declarationId<?=$declaration['id']?>" id="learn_kasneb">
                                        <?php
                                        foreach ($learn as $key => $value) {
                                            echo "<option val=" . $value . ">$value</option>";
                                        }
                                        ?>
                                    </select>
                                <?php } ?>
                                </div>
                            <div class="col-md-4">
                        <?= Html::textInput('declaration' . $declaration['id'], "", ['class' => 'form-control declarations-specify', 'style' => 'margin-top:5px;', 'placeholder' => 'Specify', 'id' => 'declarationId' . $declaration['id']]) ?>
                            </div>                        
                        </div>
    <?php $i++;
} ?>
                </div>
            </div>
            <div class="row" style="margin-top:10px;">
                <div class="col-md-5">
                    <?= $i ?>. Choose your first examination sitting
                </div>
                <div class="col-md-3">
                    <?=
                    $form->field($model, 'month', [
                        'addon' => ['prepend' => ['content' => '<i class="fa fa-calendar"></i>']],
                        'inputOptions' => ['class' => 'form-control  form-control-custom'],
                    ])->dropDownList($sittingArray, ['prompt' => 'Choose sitting...'])->label(FALSE)
                    ?>
                </div>
                <div class="col-md-4">
                    <?=
                    $form->field($model, 'year', [
                        'options' => ['class' => 'col-md-8'],
                        'addon' => ['prepend' => ['content' => '<i class="fa fa-calendar-o"></i>']],
                        'inputOptions' => ['class' => 'form-control  form-control-custom capitalize-input', 'readonly' => true],
                    ])->label(FALSE)
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>          
<?php ActiveForm::end(); ?>
<script>
    function declarationSpecification(element) {
        var originalString = element.value;
        var val = originalString.substring(1, originalString.length);
        var option = originalString.substring(0, 1);
        if (option === "Y") {
            $('#declarationId' + val).show();
        } else {
            $('#declarationId' + val).hide();
        }
    }
</script>