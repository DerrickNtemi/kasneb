<?php

use kartik\form\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Html;
use kartik\date\DatePicker;

$identity[1] = "National ID";
$identity[2] = "Birth Certificate";
$identity[3] = "Passport";
$elligibleYear = 473040000;
$elligibleDate = date("d-m-Y",(time() - $elligibleYear));
$form = ActiveForm::begin([
            'id' => 'account-details-form',
            'action' => Yii::$app->urlManager->createUrl(['registration/create-profile']),
            'options' => ['enctype' => 'multipart/form-data',],
            'fieldConfig' => [
                'template' => "{label}{input}\n{hint}\n{error}",
            ],
        ]);
?>
<?= Html::activeHiddenInput($model, 'id') ?>
<?= Html::activeHiddenInput($model, 'phoneNumber') ?>
<?= Html::activeHiddenInput($model, 'email') ?>
<div class="panel-group mt10">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="row form-rows">                          
                        <?=
                        $form->field($model, 'firstName', [
                            'addon' => ['prepend' => ['content' => '<i class="fa fa-user"></i>']],
                            'options' => ['class' => 'col-md-4'],
                            'inputOptions' => ['class' => 'form-control  form-control-custom capitalize-input', 'disabled' => true],
                        ])
                        ?>

                        <?=
                        $form->field($model, 'middleName', [
                            'addon' => ['prepend' => ['content' => '<i class="fa fa-user"></i>']],
                            'options' => ['class' => 'col-md-4'],
                            'inputOptions' => ['class' => 'form-control  form-control-custom capitalize-input', 'disabled' => true],
                        ])
                        ?>

                        <?=
                        $form->field($model, 'lastName', [
                            'addon' => ['prepend' => ['content' => '<i class="fa fa-user"></i>']],
                            'options' => ['class' => 'col-md-4'],
                            'inputOptions' => ['class' => 'form-control  form-control-custom capitalize-input', 'disabled' => true],
                        ])
                        ?>
                    </div>
                    <div class="row form-row-below">  
                        <div class="col-md-3">                            
<?=
$form->field($model, 'nationality', [
    'addon' => ['prepend' => ['content' => '<i class="fa fa-flag"></i>']],
    'inputOptions' => ['class' => 'form-control  form-control-custom', 'readonly' => empty($model->nationality) ? false : true],
])->dropDownList($nationalities, ['prompt' => 'Choose ...'])
?>
                        </div>
                        <div class="col-md-3"> 
                            <?=
                            $form->field($model, 'documentType', [
                                'addon' => ['prepend' => ['content' => '<i class="fa fa-file"></i>']],
                                'inputOptions' => ['class' => 'form-control  form-control-custom', 'readonly' => empty($model->documentType) ? false : true],
                            ])->dropDownList($identity)
                            ?>
                        </div>
                        <div class="col-md-3"> 
                            <?=
                            $form->field($model, 'documentNo', [
                                'addon' => ['prepend' => ['content' => '<i class="fa fa-file-o"></i>']],
                                'inputOptions' => ['class' => 'form-control  form-control-custom', 'readonly' => empty($model->documentNo) ? false : true],
                            ])
                            ?> 
                        </div>
                        <div class="col-md-3"> <?=
                            $form->field($model, 'documentScan')->widget(FileInput::classname(), [
                                'options' => ['accept' => 'image/*', 'disabled' => empty($model->documentScan) ? false : true], 'pluginOptions' => [
                                    'showPreview' => false,
                                    'showCaption' => true,
                                    'captionLabel' => "adad",
                                    'showRemove' => false,
                                    'showUpload' => false,
                                    'browseLabel' => '',
                                    'removeLabel' => '',
                                    'browseClass' => 'btn btn-primary btn-block',
                                ]
                            ])
                            ?>
                        </div>
                    </div>
                    <div class="row form-row-below">   
                        <div class="col-md-4"> 
                            <?=
                            $form->field($model, 'passportPhoto')->widget(FileInput::classname(), [
                                'options' => ['accept' => 'image/*', 'disabled' => empty($model->passportPhoto) ? false : true], 'pluginOptions' => [
                                    'showPreview' => false,
                                    'showCaption' => true,
                                    'captionLabel' => "adad",
                                    'showRemove' => false,
                                    'showUpload' => false,
                                    'browseLabel' => '',
                                    'removeLabel' => '',
                                    'browseClass' => 'btn btn-primary btn-block',
                                ]
                            ])
                            ?>
                        </div> 
                        <div class="col-md-4"> 

<?=
$form->field($model, 'dob', [
    'inputOptions' => ['class' => 'form-control  form-control-custom'],
])->widget(DatePicker::classname(), [
    'value' => date("d-m-Y"),
    'pluginOptions' => [
        'autoclose' => true,
        'format' => 'dd-mm-yyyy',
        'endDate' => $elligibleDate,
    ]
])
?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'gender')->radioList(['1' => 'Male', '2' => 'Female'], ['inline' => true]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>            
<?php ActiveForm::end(); ?>