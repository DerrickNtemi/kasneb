<?php
use kartik\form\ActiveForm;
        
$form = ActiveForm::begin([
            'id' => 'contact-details-form',
            'action' => Yii::$app->urlManager->createUrl(['registration/contact-details']),
            'options' => ['enctype' => 'multipart/form-data',],
            'fieldConfig' => [
                'template' => "{label}{input}\n{hint}\n{error}",
            ],
        ]);
?>
<div class="panel-group mt10">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="row form-row-below">  
                            <?=
                                $form->field($model, 'postalAddress', [
                                    'options' => ['class' => 'col-md-4'],
                                    'addon' => ['prepend' => ['content' => '<i class="fa fa-envelope-o"></i>']],
                                    'inputOptions' => ['class' => 'form-control  form-control-custom'],
                                ])?> 
                        
                            <?=
                                $form->field($model, 'postalCode', [
                                    'options' => ['class' => 'col-md-4'],
                                    'addon' => ['prepend' => ['content' => '<i class="fa fa-envelope-o"></i>']],
                                    'inputOptions' => ['class' => 'form-control  form-control-custom'],
                                ])?>  
                            <?=
                                $form->field($model, 'town', [
                                    'options' => ['class' => 'col-md-4'],
                                    'addon' => ['prepend' => ['content' => '<i class="fa fa-map-marker"></i>']],
                                    'inputOptions' => ['class' => 'form-control  form-control-custom'],
                                ])?>
                    </div>
                    <div class="row form-row-below">
                        
                            <?= $form->field($model, 'countryId', [
                                'options' => ['class' => 'col-md-4'],
                                    'addon' => ['prepend' => ['content' => '<i class="fa fa-flag"></i>']],
                                'inputOptions' => ['class' => 'form-control  form-control-custom'],
                            ])->dropDownList($countries) ?>     
                        <?php if($model->countryId == "1") : ?>
                            <?=
                                $form->field($model, 'countyId', [
                                    'options' => ['class' => 'col-md-4'],
                                    'addon' => ['prepend' => ['content' => '<i class="fa fa-map-marker"></i>']],
                                    'inputOptions' => ['class' => 'form-control  form-control-custom'],
                                ])->dropDownList($counties, ['prompt'=>'Select county of residence..'])?>  
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>            
<?php ActiveForm::end(); ?>