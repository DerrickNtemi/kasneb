<?php

use kartik\form\ActiveForm;
use yii\helpers\Html;

$form = ActiveForm::begin([
            'id' => 'course-application-form',
            'action' => Yii::$app->urlManager->createUrl(['registration/course-application']),
            'options' => ['enctype' => 'multipart/form-data',],
            'fieldConfig' => [
                'template' => "{label}{input}\n{hint}\n{error}",
            ],
        ]);
?>
<?= Html::activeHiddenInput($model, 'documentNo') ?>
<div class="panel-group">
    <div class="panel panel-default">
        <div class="panel-body">
            <ul class="nav nav-tabs">
                <li class="active course-type-proff" id="100">
                    <a data-toggle="tab" href="#professional" class=" text-center">
                        <h4 class="glyphicon glyphicon-book"></h4><br/>PROFESSIONAL COURSE
                    </a>
                </li>
                <li class="course-type-dip" id="200">
                    <a data-toggle="tab" href="#diploma" class=" text-center">
                        <h4 class="glyphicon glyphicon-bookmark"></h4><br/>DIPLOMA COURSE
                    </a>
                </li>
            </ul>

            <div class="tab-content">
                <div id="professional" class="tab-pane fade in active">
                    <h1></h1>
                    <h1></h1>
                    <div class="row form-row-below">
                        <div class="col-md-6 professional-list">
                            <?=
                            $form->field($model, 'profCourseId', [
                                'addon' => ['prepend' => ['content' => '<i class="fa fa-graduation-cap"></i>']],
                                'inputOptions' => ['class' => 'form-control  form-control-custom'],
                            ])->dropDownList($professionalCourses, ['prompt' => 'Choose course...'])->label()
                            ?>
                        </div>
                    </div>
                    <h1></h1>
                    <div class="row">
                        <div class="col-md-12">
                            <strong>ENTRY REQUIREMENTS</strong>
                            <p style="font-size: 0.8em; color:red;"><sup>*</sup>You must tick/check at least one qualification</p>
                        </div>
                    </div>
                    <div class="professional-exams">
                        <div class="row">
                            <div class="col-md-12">
                                <p><strong>Professional Course</strong></p>
                                <p>A person seeking to be registered as a student for any of the professional examinations must show evidence of being a holder of one of the following minimum qualifications. Check the qualification(s) you meet:</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped table-hover responsive">
                                            <?php foreach ($professionalRequirements as $req) : ?>
                                        <tr>
                                            <td style="width:45%;">
                                                <?=
                                                $form->field($model, 'profReq', [
                                                    'inputOptions' => ['class' => 'form-control '],
                                                ])->checkboxlist([$req["id"] => $req['description']], ['class' => $req["type"]])->label(FALSE)
                                                ?>
                                            </td>
                                            <td style="width:30%;">
                                                <label style="font-size:0.8em;"><?= $req['documentName'] . " " . ($req['id'] == 3 ? "e.g NAC/12345" : "") ?>.</label><input class="document-number form-control" id="professional<?= $req["id"] ?>" />
                                            </td>
                                            <td style="width:25%;">
                                                <label style="font-size:0.8em;"><?= $req['institutionType'] ?></label><input class="document-number form-control" id="institution-proffesional<?= $req["id"] ?>" value="<?= ($req['id'] == 3 ? "KASNEB" : "") ?>" />
                                            </td>
                                        </tr>
<?php endforeach; ?>
                                </table>
                            </div>
                        </div>
                        <div class="row form-rows professinal-document">
                            <div class="col-md-6">
                                <?=
                                $form->field($model, 'profDocument[]', [
                                    'options' => ['class' => 'col-md-12'],
                                    'inputOptions' => ['class' => 'form-control form-control-custom'],
                                ])->fileInput(['multiple' => true])
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="diploma" class="tab-pane fade">
                    <h1></h1>
                    <h1></h1>
                    <div class="row">
                        <div class="col-md-6">
                            <?=
                            $form->field($model, 'dipCourseId', [
                                'addon' => ['prepend' => ['content' => '<i class="fa fa-graduation-cap"></i>']],
                                'inputOptions' => ['class' => 'form-control  form-control-custom'],
                            ])->dropDownList($diplomaCourses, ['prompt' => 'Choose course...'])->label()
                            ?>
                        </div>
                    </div>
                    <h1></h1>
                    <div class="row">
                        <div class="col-md-12">
                            <strong>ENTRY REQUIREMENTS</strong>
                            <p style="font-size: 0.8em; color:red;"><sup>*</sup>You must tick/check at least one qualification</p>
                        </div>
                    </div>
                    <div class="diploma-exams">
                        <div class="row">
                            <div class="col-md-12">
                                <p><strong>Diploma Examinations</strong></p>
                                <p>A person seeking to be registered as a student for any of the diploma examinations must show evidence of being a holder of one of the following minimum qualifications. Check the qualification you meet:</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped table-hover responsive">
                                            <?php foreach ($diplomaRequirements as $req) : ?>
                                        <tr>
                                            <td style="width:45%;">
                                                <?=
                                                $form->field($model, 'dipReq', [
                                                    'inputOptions' => ['class' => 'form-control $req["type"]'],
                                                ])->checkboxlist([$req["id"] => $req['description']])->label(FALSE)
                                                ?>
                                            </td>
                                            <td style="width:30%;">
                                                <label  style="font-size:0.8em;" ><?= $req['documentName'] . " " . ($req['id'] == 3 ? "e.g NAC/12345" : "") ?>.</label><input class="document-number form-control" id="diploma<?= $req["id"] ?>" />
                                            </td>
                                            <td style="width:25%;">
                                                <label  style="font-size:0.8em;" >Institution</label><input class="document-number form-control" id="institution-diploma<?= $req["id"] ?>" value="<?= ($req['id'] == 3 ? "KASNEB" : "") ?>" />
                                            </td>
                                        </tr>
<?php endforeach; ?>
                                </table>
                            </div>
                        </div>
                        <div class="row form-rows diploma-document">
                            <div class="col-md-6"><?=
                                $form->field($model, 'dipDocument[]', [
                                    'options' => ['class' => 'col-md-12'],
                                    'inputOptions' => ['class' => 'form-control form-control-custom'],
                                ])->fileInput(['multiple' => true])
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>  
<?php ActiveForm::end(); ?>