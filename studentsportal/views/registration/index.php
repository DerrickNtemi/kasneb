<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;

$this->title = Yii::t('app', 'Student Registration');
?>
<h1 class='header-h1'><?= $this->title ?></h1>
<div class="row header-underline"></div>
<?= $this->render('@common/views/_needHelp') ?>

<!-- BASIC WIZARD -->
<div id="rootwizard"  class="basic-wizard" >
    <div class="row nav-tab-row">
        <div class="col-md-12">
            <ul class="nav nav-pills nav-justified nav-list-container" id="myTab">
                <li>
                    <a href="#tab1" data-toggle="tab" style="padding:0px;">
                        <div class="row">
                            <div class="col-md-3 nav-step"><span>1</span> </div>
                            <div class="col-md-9 nav-div">About You</div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#tab2" data-toggle="tab" style="padding:0px;">
                        <div class="row">
                            <div class="col-md-3 nav-step"><span>2</span> </div>
                            <div class="col-md-9 nav-div">Contacts</div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#tab3" data-toggle="tab" style="padding:0px;">
                        <div class="row">
                            <div class="col-md-3 nav-step"><span>3</span> </div>
                            <div class="col-md-9 nav-div">Your Application</div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#tab4" data-toggle="tab" style="padding:0px;">
                        <div class="row">
                            <div class="col-md-3 nav-step"><span>4</span> </div>
                            <div class="col-md-9 nav-div">More Details</div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#tab5" data-toggle="tab" style="padding:0px;">
                        <div class="row">
                            <div class="col-md-3 nav-step"><span>5</span> </div>
                            <div class="col-md-9 nav-div">Bill Presentment</div>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <h2></h2>
    <div id="bar" class="progress">
        <div class="bar progress-bar progress-bar-striped active" id="wizard-progress-bar"></div>
    </div>
    <div class="tab-content">
        <div class="tab-pane" id="tab1">
            <h2></h2><h2></h2>
            <div class="alert alert-danger"  id='profile-error-summary' style="display: none;"> </div>
            <div>
                <?= $this->render('_accountDetails', ['model' => $studentModel, 'nationalities' => $nationalities]) ?>
            </div>
        </div>

        <div class="tab-pane" id="tab2">
            <h2></h2><h2></h2>      
            <div class="alert alert-danger"  id='contact-error-summary' role="alert" style="display: none;"> </div>
            <div>
                <?= $this->render('_contactDetails', ['model' => $contact, 'countries' => $countries, 'counties' => $counties]) ?>
            </div>
        </div>

        <div class="tab-pane" id="tab3"> 
            <h2></h2><h2></h2>       
            <div class="alert alert-danger"  id="application-error-summary" role="alert" style="display: none;"></div>
            <div>
                <?= $this->render('_courseApplication', ['model' => $courseApplication, 'diplomaCourses' => $diploma, 'professionalCourses' => $professional, 'diplomaRequirements' => $diplomaRequirements, 'professionalRequirements' => $professionalRequirements]) ?>
            </div>
        </div>

        <div class="tab-pane" id="tab4"> 
            <h2></h2><h2></h2>       
            <div class="alert alert-danger"  id="declaration-error-summary" role="alert" style="display: none;"></div>
            <div>
                <?= $this->render('_moreDetails', ['model' => $declarationModel, 'declarations' => $declarationData, 'sittingArray' => $sittingArray]) ?>
            </div>
        </div>

        <div class="tab-pane" id="tab5"> 
        </div>

    </div>

    <ul class="pager wizard">
        <li class="previous"><a href="javascript:;">Previous</a></li>
        <li class="next"><a href="javascript:;">Next</a></li>
        <li class="next finish" style="display:none;"><a href="javascript:;">Finish</a></li>
    </ul>
</div>
<div id="loadingDiv"></div>

<!-- Exemption Modal -->
<?php
Modal::begin([
    'id' => 'ExemptionPaperModal',
    'header' => '<h4 class="modal-title" id="expense-account-form-header">Exemption Paper</h4>',
    'footer' =>
    Html::button('Cancel', ['class' => 'btn btn-default', 'data-dismiss' => 'modal'])
    . PHP_EOL .
    Html::button('Add', ['id' => 'exemption-btn', 'class' => 'btn btn-primary btn-modal-save']),
]);
?>
<?php Modal::end() ?>