<?php
$this->title = Yii::t('app', 'Update Documents');
?>
<h1 class='header-h1'><?= $this->title ?></h1>
<div class="row header-underline"></div>
<?= $this->render('@common/views/_needHelp') ?>

<!-- BASIC WIZARD -->


<h2></h2><h2></h2>      
<div class="alert alert-danger"  id='contact-error-summary' role="alert" style="display: none;"> </div>
<div class="panel-group mt10">
    <div class="panel panel-default">
        <div class="panel-body">
            <?= $this->render('_updateDocuments', ['model'=>$courseDocuments]) ?>
        </div>
    </div>
</div>
<div id="loadingDiv"></div>