<?php

use kartik\file\FileInput;
use kartik\form\ActiveForm;
use yii\helpers\Html;

$identity[1] = "National ID";
$identity[2] = "Birth Certificate";
$identity[3] = "Passport";

$elligibleYear = 473040000;
$elligibleDate = date("d-m-Y", (time() - $elligibleYear));
$form = ActiveForm::begin([
            'id' => 'account-details-update-form',
            'action' => Yii::$app->urlManager->createUrl(['registration/update-profile-data']),
            'options' => ['enctype' => 'multipart/form-data',],
            'fieldConfig' => [
                'template' => "{label}{input}\n{hint}\n{error}",
            ],
        ]);
?>
<?= Html::activeHiddenInput($model, 'id') ?>
<div class="row">
    <div class="col-md-12">
        <div class="row form-rows">   
            <div class="col-md-4">
                <?=
                $form->field($model, 'phoneNumber', [
                    'addon' => ['prepend' => ['content' => '<i class="fa fa-phone"></i>']],
                    'inputOptions' => ['class' => 'form-control  form-control-custom capitalize-input'],
                ])
                ?>
            </div>
            <div class="col-md-4"> 
                <?=
                $form->field($model, 'passportPhoto', [
                    'options' => ['class' => 'col-md-12'],
                    'inputOptions' => ['class' => 'form-control form-control-custom', 'disabled' => empty($model->passportPhoto) ? false : true],
                ])->fileInput()
                ?>
            </div>
        </div>

        <div class="row form-row-below">  
            <div class="col-md-4"> 
                <?=
                $form->field($model, 'documentType', [
                    'addon' => ['prepend' => ['content' => '<i class="fa fa-file"></i>']],
                    'inputOptions' => ['class' => 'form-control  form-control-custom', 'readonly' => $updateNationalIdField],
                ])->dropDownList($identity)
                ?>
            </div>
            <div class="col-md-4"> 
                <?=
                $form->field($model, 'documentNo', [
                    'addon' => ['prepend' => ['content' => '<i class="fa fa-file-o"></i>']],
                    'inputOptions' => ['class' => 'form-control  form-control-custom', 'readonly' => $updateNationalIdField],
                ])
                ?> 
            </div>
            <div class="col-md-4"> 
                <?=
                $form->field($model, 'documentScan', [
                    'options' => ['class' => 'col-md-12' ],
                    'inputOptions' => ['class' => 'form-control form-control-custom', 'disabled' => $updateNationalIdField ],
                ])->fileInput()
                ?>
            </div>
        </div>
        <?php $model->contact = $contact ?>
        <div class="row form-row-below">  
            <?=
            $form->field($model->contact, 'postalAddress', [
                'options' => ['class' => 'col-md-4'],
                'addon' => ['prepend' => ['content' => '<i class="fa fa-envelope-o"></i>']],
                'inputOptions' => ['class' => 'form-control  form-control-custom'],
            ])
            ?> 

            <?=
            $form->field($model->contact, 'postalCode', [
                'options' => ['class' => 'col-md-4'],
                'addon' => ['prepend' => ['content' => '<i class="fa fa-envelope-o"></i>']],
                'inputOptions' => ['class' => 'form-control  form-control-custom'],
            ])
            ?>  
            <?=
            $form->field($model->contact, 'town', [
                'options' => ['class' => 'col-md-4'],
                'addon' => ['prepend' => ['content' => '<i class="fa fa-map-marker"></i>']],
                'inputOptions' => ['class' => 'form-control  form-control-custom'],
            ])
            ?>
        </div>
        <div class="row form-row-below">

            <?=
            $form->field($model->contact, 'countryId', [
                'options' => ['class' => 'col-md-4'],
                'addon' => ['prepend' => ['content' => '<i class="fa fa-flag"></i>']],
                'inputOptions' => ['class' => 'form-control  form-control-custom'],
            ])->dropDownList($countries)
            ?>     
            <?php if ($model->countryId['code'] == "1") : ?>
                <?=
                $form->field($model->contact, 'countyId', [
                    'options' => ['class' => 'col-md-4'],
                    'addon' => ['prepend' => ['content' => '<i class="fa fa-map-marker"></i>']],
                    'inputOptions' => ['class' => 'form-control  form-control-custom'],
                ])->dropDownList($counties, ['prompt' => 'Select County...'])
                ?>  
            <?php endif; ?>
        </div>
    </div>
</div> 
<p class="text-center"><button class="btn btn-sm btn-primary" >Save Changes</button></p>
<?php ActiveForm::end(); ?>