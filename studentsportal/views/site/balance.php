<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Wallet Balance';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <div class="row header-label-row" >
        <div class="col-md-12">
            Wallet Balance
        </div>
    </div>

    <div class="row signup-main-row">
        <div class="col-lg-1"></div>
        <div class="col-lg-5">
            <?php if (Yii::$app->session->hasFlash('fail-message')): ?>
                <div class="alert alert-danger"><?= Yii::$app->session->getFlash('fail-message') ?></div>
            <?php endif; ?>
            <?php if (Yii::$app->session->hasFlash('success-message')): ?>
                <div class="alert alert-success"><?= Yii::$app->session->getFlash('success-message') ?></div>
            <?php endif; ?>
            <?php
            $form = ActiveForm::begin(['id' => 'balance-form']);
            if ($model->hasErrors()) {
                echo $form->errorSummary($model, ['header' => '']);
            }
            ?>
            <?= $form->field($model, 'phoneNumber')->textInput(['autofocus' => true]) ?>
            <?= $form->field($model, 'jpPin')->passwordInput() ?>

            <div class="form-group">
                <?= Html::submitButton('Get Balance', ['class' => 'btn btn-primary', 'style' => 'border-radius:0px;']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
