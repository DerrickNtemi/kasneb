<?php
$this->title = Yii::t('app', "Code of Ethics");

use yii\helpers\Html;

$url = Yii::$app->getUrlManager();
?>
<h1 class='header-h1'><?= $this->title ?> :: <?= Html::a("Back Home", $url->createUrl(['/site/login']), []) ?></h1>
<div class="row header-underline"></div>
<h1></h1>
<div class="row">
    <div class="col-md-12">
        <h3 style="text-decoration: underline;">CODE OF CONDUCT AND ETHICS FOR KASNEB STUDENTS</h3>
        <h4>Introduction</h4>
        <p>
            This code of conduct and ethics (hereinafter referred to as “the Code”) is intended to establish the
            minimum standards of ethical conduct and behaviour of KASNEB students in different environments
            and circumstances. The Code is expected to provide guidance to students for purposes of adhering to
            the fundamental principles of law, national values and ethics, while safeguarding the integrity and
            dignity of the student and KASNEB.
        </p>
        <p>
            The Code further aims at ensuring that registered KASNEB students not only uphold the core values
            of KASNEB which include integrity, professionalism, equity, teamwork and innovativeness, but are
            also seen to actively promote the values.
        </p>
        <ol type="1">
            <h3>PART I – PRELIMINARY</h3>
            <h4>Citation</h4>
            <li>This Code may be cited as the Code of Conduct and Ethics for KASNEB Students.</li>
            <h4>Interpretation</h4>
            <li>In this Code, unless the context otherwise requires –
                “Board” means the Board of KASNEB.<br/>
                “Chairman” means the Chairman of the Board of KASNEB.<br/>
                “Chief Executive Officer” means the Chief Executive Officer of KASNEB.<br/>
                “Country” means the country in which a student is resident.<br/>
                “Student” means a person duly registered by KASNEB to undertake any of its examinations.<br/>
                “Trainer” means a person engaged by a training institution to offer tuition for any of the
                examinations administered by KASNEB.<br/>
                “Training institution” means an institution so registered to offer tuition for KASNEB examinations
            </li>
            <h4>Application and scope of the Code</h4>
            <li>This Code shall apply to all registered KASNEB students. The students will be required to
                read, understand and sign an undertaking to abide by the Code.</li>


            <h3>PART II – REQUIREMENTS</h3>
            <h4>General conduct</h4>
            <li>A student shall observe the principle of integrity in his/her conduct and interactions with other
                persons. Upholding integrity requires the student to be honest, trustworthy and generally
                conduct himself/herself in a manner that upholds the dignity of the student, the examination,
                the relevant profession(s) and KASNEB.</li>
            <li>A student shall observe the laws of the country in which he/she is resident.</li>
            <h4>Submission of documents and other information</h4>
            <li>A student shall ensure that all documents and information submitted to KASNEB including
                copies of certificates, transcripts, identity documents, passport photos, testimonials, receipts
                and other documents for purposes of registration, exemption or any other purpose are genuine
                and accurate. </li>
            <li>A student shall ensure that any document or other information that he/she submits purporting
                to be issued by KASNEB; to either a university or other institution of higher learning,
                employer/potential employer, government agency or any other institution for whatever purpose,
                is genuine and originates from KASNEB.</li>
            <li>A student shall ensure that any person certifying documents to be submitted to KASNEB is
                duly authorised to do so under the guidelines issued by KASNEB for certification of
                documents</li>
            <h4>Conduct in a training institution</h4>
            <li>A student attending tuition in a training institution shall abide by the rules and regulations
                issued by the training institution to govern the conduct of students.</li>
            <li>Notwithstanding Clause 9 above, a student shall treat fellow students, trainers and
                management of a training institution with courtesy and respect and shall not engage in any
                activity that is likely to bring himself/herself, the training institution or KASNEB into disrepute.</li>
            <h4>Examination rules and regulations</h4>
            <li>A student shall abide by the KASNEB examination rules and regulations at all times whether or
                not the student is sitting for the examination.</li>
            <li>The examination rules and regulations are set out in the Appendix to this Code and shall form
                part of this Code</li>
            <h4>Posting and sharing of information/comments on social and other media</h4>
            <li>
                A student shall observe the general rules of integrity and decorum in posting information or
                making comments on social and other media. In particular:
                <ol type="a">
                    <li>A student shall not post or circulate information purported to be from, or relating to
                        KASNEB on social or any other media before reliably confirming the accuracy and
                        completeness of the information.</li>
                    <li>
                        A student shall not make derogatory, abusive or otherwise offensive comments against
                        KASNEB, other students, trainers or any other person or institution.</li>
                    <li>No student shall create any website, blog or social media page purporting to be that of
                        KASNEB for whatever purpose. </li>
                    <li>A student shall not hold himself or herself as representing KASNEB in any capacity unless
                        with the express authority of the Chief Executive Officer which shall be in writing.</li>
                </ol>
            </li>
            <h3>PART III: BREACH OF THE CODE</h3>
            <h4>Reporting breaches of the Code</h4>
            <li>All suspected cases of breach of the Code should be reported in writing to the Chief Executive
Officer. The suspected breach should be reported within reasonable time to facilitate efficient
and effective investigations.</li>
            <li>Any available evidence should be attached when reporting cases of breach of the Code</li>
            <li>All information received shall be treated in strict confidentiality.</li>
            <h4>Action for breach of the Code</h4>
            <li>All reported cases of breach of the Code shall be referred to the Examinations Committee, or
any other Committee that the Board may decide based on circumstances of each case.</li>
            <li>Action shall be taken in confirmed cases of breach of the Code in accordance with the laid
down rules and regulations of KASNEB, the provisions of Section 42 of the Accountants Act.
No. 15 of 2008 and/or any other applicable law.</li>
            <li>
                Action for breach of the Code shall include but not limited to:
                <ol type="a">
                    <li>Deregistration as a student.</li>
                    <li>Cancellation of examination results.</li>
                    <li>Prosecution as provided for under the Accountants Act. No. 15 of 2008 and/or any
other applicable law.</li>
                    <li>A student shall not hold himself or herself as representing KASNEB in any capacity unless
                        with the express authority of the Chief Executive Officer which shall be in writing.</li>
                </ol>
            </li>
            <h4>Appeals</h4>
            <li>All cases of appeal shall be forwarded in writing within 30 days of notification of action for
breach to the Chief Executive Officer.</li>
            <h4>Enquiries and clarifications</h4>
            <li>All enquiries or requests for clarification shall be directed to the Chief Executive Officer.</li>
            <h4>Revision of the Code</h4>
            <li>KASNEB shall review the Code from time to time as appropriate for relevance and validity.</li>
            <li>The review of the Code shall be undertaken through a consultative and participatory process.</li>
        </ol>
        <h4 style="text-decoration: underline; margin-top: 10px;">7 January 2016</h4>
    </div>
</div>