<?php
use yii\helpers\Html;
use yii\bootstrap\Modal;
$this->title = Yii::t('app', 'Students Refund Request(s)');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row members-index" style="margin-top: 20px;">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading"><?= $this->title ?></div>
            <div class="panel-body">
                <?php if (Yii::$app->session->hasFlash('fail-message')): ?>
                    <div class="alert alert-danger"><?= Yii::$app->session->getFlash('fail-message') ?></div>
                <?php endif; ?>
                <?php if (Yii::$app->session->hasFlash('success-message')): ?>
                    <div class="alert alert-success"><?= Yii::$app->session->getFlash('success-message') ?></div>
                <?php endif; ?>
                    <p class="text-right"><?= Html::button('<span class="glyphicon glyphicon-plus" ></span> Request Refund', ['class' => 'btn btn-primary btn-sm', 'data-toggle' => "modal", 'data-whatever' => '', 'data-target' => "#RequestRefundModal"]) ?></p>
                <?= $this->render('_refundrequests', ['refundRequests' => $refundRequests]) ?>
            </div>
        </div>
    </div>
</div>
<!-- Beneficiary create modal -->
<?php
Modal::begin([
    'id' => 'RequestRefundModal',
    'header' => '<h4 class="modal-title" id="beneficiary-modal-header" >Refund Request</h4>',
    'footer' =>
    Html::button('Close', ['class' => 'btn btn-default', 'data-dismiss' => 'modal'])
    . PHP_EOL .
    Html::button('Request', ['id' => 'add-refund-request', 'class' => 'btn btn-primary btn-modal-save']),
    'options' => [
        'role' => 'dialog',
        'aria-labelledby' => 'category-modal-label',
        'aria-hidden' => 'true',
    ],
]);
?>
<?=
$this->render('_refundrequestform', [
    'model' => $refundRequest,
    'transactionTypeAccounts' => $transactionTypeAccounts,
    'refundGrounds' => $refundGrounds
])
?>
<?php Modal::end() ?>