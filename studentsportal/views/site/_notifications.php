<?php

use yii\data\ArrayDataProvider;
use yii\grid\GridView;
?>
<?=

GridView::widget([

    'dataProvider' => new ArrayDataProvider([
        'allModels' => $datas,
            ]),
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'details',
        ],
    ]);
?>