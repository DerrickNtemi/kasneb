<?php

use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
?>
<?=
GridView::widget([
    'dataProvider' => new ArrayDataProvider(['allModels' => $refundRequests]),
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'label' => 'Reg No.',
            'value' => function($data) {
                return $data->registrationNo;
            }
        ],
        [
            'label' => 'Timestamp',
            'value' => function($data) {
                return $data->createdAt;
            }
        ],
        [
            'attribute' => 'amount',
            'value' => function($data) {
                return number_format($data->amount, 2);
            },
            'contentOptions' => ['style' => 'text-align:right;'],
            'headerOptions' => ['style' => 'text-align:right;'],
        ],
        [
            'label' => 'Grounds',
            'value' => function($data) {
                return $data->refundReasonsId['description'];
            }
        ],
        [
            'label' => 'Status',
            'value' => function($data) {
                return $data->requestStatusId['name'];
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view}&nbsp;&nbsp;{delete}&nbsp;&nbsp;',
            'buttons' => [
                'view' => function($url, $model) {
                    $url = Url::toRoute(['site/refund-request-view', 'id' => $model->id]);
                    return Html::a('view', $url);
                },
                'delete' => function($url, $model) {
                    $url = Url::toRoute(['site/refund-request-delete', 'id' => $model->id]);
                    return Html::a('cancel', $url, ['data-whatever' => $url, 'data-toggle' => "modal", 'data-target' => "#DeleteRedirectModal", 'title' => Yii::t('yii', 'Delete'), 'class'=>($model->requestStatusId['id'] == 4 ? "" : "disable-link")]);
                },
            ],
        ],
    ],
]);
?>
<!-- Activate / Deactivate Redirect Modal -->
<?php
Modal::begin([
    'id' => 'AuthorizeRefundRequestModal',
    'header' => '<h4 class="modal-title" >Authorize Record</h4>',
    'footer' =>
    Html::button('Cancel', ['class' => 'btn btn-default', 'data-dismiss' => 'modal'])
    . PHP_EOL .
    Html::button('Process ', ['id' => 'verify-redirect-btn', 'class' => 'btn btn-primary btn-modal-save']),
]);
?>
<?= $this->render('@common/views/confirmauthorizationrefundrequest') ?>
<?php Modal::end() ?>


<!-- Delete Redirect Modal -->
<?php
Modal::begin([
    'id' => 'DeleteRedirectModal',
    'header' => '<h4 class="modal-title" >Cancel Record</h4>',
    'footer' =>
    Html::button('Cancel', ['class' => 'btn btn-default', 'data-dismiss' => 'modal'])
    . PHP_EOL .
    Html::button('Delete', ['id' => 'delete-redirect-btn', 'class' => 'btn btn-primary btn-modal-save']),
]);
?>
<?= $this->render('@common/views/_cancel') ?>
<?php Modal::end() ?>