<?php 
    use yii\widgets\ActiveForm;
    use yii\helpers\Html;

    $form = ActiveForm::begin([
        'id' => 'create-refund-request-form',           
        'enableClientValidation'=> true,
        'action' => Yii::$app->urlManager->createUrl(['site/create-refund-request']),
        'fieldConfig' => [
            'template' => "{label}<div class=\"clearfix\"></div>\n{input}\n{hint}\n{error}",
        ],
    ]);  
?>     
<?= Html::activeHiddenInput($model,'id') ?>
<div class="row">
    <div class="col-md-6" >
        <?= $form->field($model, 'amount', [
                       'inputOptions' => ['placeholder' => $model->getAttributeLabel('amount'),'class'=>'form-control chosen-select'],
            ])?> 
    </div>
    <div class="col-md-6" >
        <?= $form->field($model, 'transactionAccountTypeId', [
                       'inputOptions' => ['data-placeholder' => $model->getAttributeLabel('transactionAccountTypeId'),'class'=>'form-control chosen-select'],
            ])->dropDownList($transactionTypeAccounts) ?> 
    </div>
</div>
<div class="row">
    <div class="col-md-6" >
        <?= $form->field($model, 'refundReasonsId', [
                       'inputOptions' => ['data-placeholder' => $model->getAttributeLabel('refundReasonsId'),'class'=>'form-control chosen-select'],
            ])->dropDownList($refundGrounds) ?> 
    </div>
</div>
<div class="row" style="display: none;" id="refund-request-description">
    <div class="col-md-12" >
        <?= $form->field($model, 'description', [
                'inputOptions' => ['placeholder' => $model->getAttributeLabel('description'),'class'=>'form-control'],
            ])->textarea()?>
    </div>
</div>
<?php ActiveForm::end() ?>
<h2></h2>
<div class="alert alert-danger"  id="error-summary" style="display: none;"></div>
<h2></h2>
<div id="progress-spinner" class="text-center" style="display: none;">
    <?= Html::img('@web/images/ajax-loader.gif', ['alt' => 'progress spinner']) ?>
</div>
<h2></h2>