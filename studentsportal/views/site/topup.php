<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Wallet Top Up';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <div class="row header-label-row" >
        <div class="col-md-12">
            Wallet Top Up
        </div>
    </div>

    <div class="row signup-main-row">
        <div class="col-lg-1"></div>
        <div class="col-lg-5">
            <?php if (Yii::$app->session->hasFlash('fail-message')): ?>
                <div class="alert alert-danger"><?= Yii::$app->session->getFlash('fail-message') ?></div>
            <?php endif; ?>
            <?php if (Yii::$app->session->hasFlash('success-message')): ?>
                <div class="alert alert-success"><?= Yii::$app->session->getFlash('success-message') ?></div>
            <?php endif; ?>
            <?php
            $form = ActiveForm::begin(['id' => 'topup-form']);
            if ($model->hasErrors()) {
                echo $form->errorSummary($model, ['header' => '']);
            }
            ?>
            <?= $form->field($model, 'phoneNumber')->textInput(['autofocus' => true]) ?>
            <?= $form->field($model, 'amount')->textInput() ?>

            <div class="form-group">
                <?= Html::submitButton('Top Up', ['class' => 'btn btn-primary', 'style' => 'border-radius:0px;']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
