<?php
use yii\helpers\Url;
use yii\helpers\Html;
use common\models\Service;
$service = new Service();

$this->title = Yii::t('app', 'Exemption Letters');
?>
<h1 class='header-h1'><?= $this->title ?></h1>
<div class="row header-underline"></div>
<h1></h1>
<?php if (Yii::$app->session->hasFlash('success-message')): ?>
    <div class="alert alert-success"><?= Yii::$app->session->getFlash('success-message') ?></div>
<?php endif; ?>

<?php if (Yii::$app->session->hasFlash('error-message')): ?>
    <div class="alert alert-danger"><?= Yii::$app->session->getFlash('error-message') ?></div>
<?php endif; ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-8 personal-details-div"><?= $this->title ?></div>
                    <div class="col-md-4 text-right">
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped">
                    <tr>
                        <th></th>
                        <th>Reference</th>
                        <th>Timestamp</th>
                        <th></th>
                    </tr>
                    <?php 
                        $i=0;
                        foreach($documents as $document){
                            $url = Url::toRoute(['site/download-exemptionletter', 'id' => $document['id']]);
                            echo "<tr><td>".++$i."</td><td>".$document['reference']."</td><td>".$service->timestampToSting1($document['date'])."</td><td>".Html::a('download', $url, ['target'=>'_blank','title' => Yii::t('yii', 'timetable')])."</td></tr>";
                        }
                    ?>
                </table>
            </div>
        </div>
    </div>
</div>