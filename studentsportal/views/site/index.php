<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\helpers\Url;

$url = Url::toRoute(['site/wallet-balance']);
$profileUrl = Url::toRoute(['registration/update-profile']);

$this->title = Yii::t('app', 'Student Profile');
$passport = empty($model->passportPhoto) ? "placeholder.jpg" : $model->passportPhoto;
$gender = empty($model->gender) ? "" : ($model->gender == 1 ? "Male" : "Female");
$documentType = empty($model->documentType) ? "ID No." : ($model->documentType == 1 ? "ID No." : ($model->documentType == 2 ? "Birth Cert. No." : "Passport No."));
?>
<h1 class='header-h1'><?= $this->title ?></h1>
<div class="row header-underline"></div>
<h1></h1>
<?php if (Yii::$app->session->hasFlash('success-message')): ?>
    <div class="alert alert-success"><?= Yii::$app->session->getFlash('success-message') ?></div>
<?php endif; ?>

<?php if (Yii::$app->session->hasFlash('error-message')): ?>
    <div class="alert alert-danger"><?= Yii::$app->session->getFlash('error-message') ?></div>
<?php endif; ?>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-5">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-5 personal-details-div">Personal Details</div>
                            <div class="col-md-4 text-right">
                                <?= Html::a('<span class="fa fa-bank"></span> Wallet Balance', "", ['style' => 'margin-right:10px;', 'class' => 'btn btn-sm btn-success', 'data-toggle' => "modal", 'data-whatever' => $url, 'data-target' => "#WalletBalanceModal", 'title' => Yii::t('yii', 'Wallet Balance'),]); ?>
                            </div>
                            <div class="col-md-2 text-right">
                                <?= Html::a('<span class="fa fa-edit"></span> Update', $profileUrl, ['style' => 'margin-right:10px;', 'class' => 'btn btn-sm btn-primary', 'title' => Yii::t('yii', 'Update Profile'),]); ?>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-md-5">
                                <?= Html::img('@web/uploads/students/documents/' . $passport, ['alt' => 'passport-photo']) ?>
                            </div>
                            <div class="col-md-4"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row profile-info-row">  
                                    <div class="col-md-4"><strong><?= $model->getAttributeLabel("firstName") ?></strong></div>
                                    <div class="col-md-1"><strong>:</strong></div>
                                    <div class="col-md-7"><?= $model->firstName ?></div>
                                </div>
                                <div class="row profile-info-row">  
                                    <div class="col-md-4"><strong><?= $model->getAttributeLabel("middleName") ?></strong></div>
                                    <div class="col-md-1"><strong>:</strong></div>
                                    <div class="col-md-7"><?= $model->middleName ?></div>
                                </div>
                                <div class="row profile-info-row">  
                                    <div class="col-md-4"><strong><?= $model->getAttributeLabel("lastName") ?></strong></div>
                                    <div class="col-md-1"><strong>:</strong></div>
                                    <div class="col-md-7"><?= $model->lastName ?></div>
                                </div>
                                <div class="row profile-info-row">  
                                    <div class="col-md-4"><strong><?= $model->getAttributeLabel("nationality") ?></strong></div>
                                    <div class="col-md-1"><strong>:</strong></div>
                                    <div class="col-md-7"><?= $model->nationality['nationality'] ?></div>
                                </div>
                                <div class="row profile-info-row">  
                                    <div class="col-md-4"><strong>ID No.</strong></div>
                                    <div class="col-md-1"><strong>:</strong></div>
                                    <div class="col-md-7"><?= $model->documentNo ?></div>
                                </div>
                                <div class="row profile-info-row">  
                                    <div class="col-md-4"><strong><?= $model->getAttributeLabel("dob") ?></strong></div>
                                    <div class="col-md-1"><strong>:</strong></div>
                                    <div class="col-md-7"><?= $model->dob ?></div>
                                </div>
                                <div class="row profile-info-row">  
                                    <div class="col-md-4"><strong><?= $model->getAttributeLabel("gender") ?></strong></div>
                                    <div class="col-md-1"><strong>:</strong></div>
                                    <div class="col-md-7"><?= $gender ?></div>
                                </div>
                                <div class="row profile-info-row">  
                                    <div class="col-md-4"><strong><?= $model->getAttributeLabel("email") ?></strong></div>
                                    <div class="col-md-1"><strong>:</strong></div>
                                    <div class="col-md-7"><?= $model->email ?></div>
                                </div>
                                <div class="row profile-info-row">  
                                    <div class="col-md-4"><strong>Phone No.</strong></div>
                                    <div class="col-md-1"><strong>:</strong></div>
                                    <div class="col-md-7"><?= $model->phoneNumber ?></div>
                                </div>
                                <div class="row profile-info-row">  
                                    <div class="col-md-4"><strong><?= $model->getAttributeLabel("contact") ?></strong></div>
                                    <div class="col-md-1"><strong>:</strong></div>
                                    <div class="col-md-7"><?= $model->contact['postalAddress'] . " - " . $model->contact['postalCode'] . " " . $model->contact['town'] ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>Examinations</strong>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-bordered">
                            <tr>
                                <th>#</th>
                                <th>Examination</th>
                                <th>Reg. No.</th>
                                <th>Renewal Status</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                            <?php
                            $i = 0;
                            foreach ($model->studentCourses as $course) : $i++;
                               // print_r($model->studentCourses); exit;
                                ?>
                                <tr>
                                    <td><?= $i ?></td>
                                    <td><?= $course['course']['name'] ?></td>
                                    <td><?= empty($course['fullRegistrationNumber']) ? "-" : $course['fullRegistrationNumber'] ?></td>
                                    <td><?= $course['renewalStatus'] ?></td>
                                    <td><?= $course['courseStatus'] == "PENDING" ? "Pending Approval" : $course['courseStatus'] ?></td>
                                    <td>
                                        <?php
                                        if ($course['courseStatus'] == "REJECTED" && ($course['rejectCode'] == "REJ_2" || $course['rejectCode'] == "REJ_4")) {
                                            $url = Url::toRoute(['registration/update-documents', 'courseTypeCode' => $course['course']['courseTypeCode'], 'courseId' => $course['id'], 'studentId' => $model->id]);
                                            echo Html::a('re-apply', $url, ['title' => Yii::t('yii', 'Re-apply')]);
                                        }
                                        ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?> 
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>       

<!-- Wallet Balance Query Modal -->
<?php
Modal::begin([
    'id' => 'WalletBalanceModal',
    'header' => '<h4 class="modal-title" >Wallet Balance</h4>',
    'footer' =>
    Html::button('Cancel', ['class' => 'btn btn-default', 'data-dismiss' => 'modal'])
    . PHP_EOL .
    Html::button('Get Balance', ['id' => 'wallet-balance-btn', 'class' => 'btn btn-primary btn-modal-save']),
]);
?>
<?= $this->render('@common/views/_wallet') ?>
<?php Modal::end() ?>

<!-- Topup Modal -->
<?php
Modal::begin([
    'id' => 'TopupModal',
    'header' => '<h4 class="modal-title" >Top up instructions</h4>',
    'footer' => Html::button('Close', ['class' => 'btn btn-default', 'data-dismiss' => 'modal']),
]);
?>
<?= $this->render('@common/views/_topup') ?>
<?php Modal::end() ?>