<?php
$this->title = Yii::t('app', 'Students Refund Request - View');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row members-index" style="margin-top: 20px;">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading"><?= $this->title ?></div>
            <div class="panel-body">
                <?php if (Yii::$app->session->hasFlash('fail-message')): ?>
                    <div class="alert alert-danger"><?= Yii::$app->session->getFlash('fail-message') ?></div>
                <?php endif; ?>
                <?php if (Yii::$app->session->hasFlash('success-message')): ?>
                    <div class="alert alert-success"><?= Yii::$app->session->getFlash('success-message') ?></div>
                <?php endif; ?>
            </div>
            <div class="row">
                <div class="col-md-1"></div>    
                <div class="col-md-8">                    
                    <table class="table table-striped">
                        <tr>
                            <th>Reg. No.</th>
                            <th>:</th>
                            <td><?= $refundRequest->registrationNo ?></td>
                        </tr>
                        <tr>
                            <th>Timestamp</th>
                            <th>:</th>
                            <td><?= $refundRequest->createdAt ?></td>
                        </tr>
                        <tr>
                            <th>Amount</th>
                            <th>:</th>
                            <td><?= $refundRequest->amount ?></td>
                        </tr>
                        <tr>
                            <th>Ground(s)</th>
                            <th>:</th>
                            <td><?= $refundRequest->refundReasonsId['description'] ?></td>
                        </tr>
                        <tr>
                            <th>Description</th>
                            <th>:</th>
                            <td><?= $refundRequest->description ?></td>
                        </tr>
                        <tr>
                            <th>Status</th>
                            <th>:</th>
                            <td><?= $refundRequest->requestStatusId['name'] ?></td>
                        </tr>
                        <tr>
                            <th>Remarks</th>
                            <th>:</th>
                            <td><?= $refundRequest->remarks?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>