<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;

$this->title = Yii::t('app', 'Exemptions : Other Graduate');
?>
<h1 class='header-h1'><?= $this->title ?></h1>
<div class="row header-underline"></div>
<div class="row extra-links">
    <div class="col-md-8">
    </div>
    <div class="col-md-4 text-right">
        <h1 class="btn btn-sm btn-success"><a href="http://www.kasneb.or.ke/images/stories/downloads/exam/exemption_policy.pdf" target="_blank"   style="color:white;">Exemption Policy</a></h1>
        <h1 class="btn btn-sm btn-primary">Need Help ?</h1>
    </div>
</div>

<!-- BASIC WIZARD -->
<div id="exemptionwizardothers"  class="basic-wizard" >
    <div class="row nav-tab-row">
        <div class="col-md-12">
            <ul class="nav nav-pills nav-justified nav-list-container" id="myTab">
                <li>
                    <a href="#tab1" data-toggle="tab" style="padding:0px;">
                        <div class="row">
                            <div class="col-md-3 nav-step"><span>1</span> </div>
                            <div class="col-md-9 nav-div">Exemption Papers</div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#tab2" data-toggle="tab" style="padding:0px;">
                        <div class="row">
                            <div class="col-md-3 nav-step"><span>2</span> </div>
                            <div class="col-md-9 nav-div">Bill Presentment</div>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <h2></h2>
    <div id="bar" class="progress">
        <div class="bar progress-bar progress-bar-striped active" id="wizard-progress-bar"></div>
    </div>
    <div class="tab-content">
        <div class="tab-pane" id="tab1">
            <h2></h2><h2></h2>
            <div class="alert alert-danger"  id='exemption-grounds-error-summary' role="alert" style="display: none;"> </div>
            <h2></h2>
            <div>
                <?= $this->render('_examOthers', ['model' => $model, 'institutions' => $institutions, 'qualificationType' => $qualificationTypes, 'courses' => $courses]) ?>
            </div>
        </div>
        <div class="tab-pane" id="tab2"> 
        </div>       
    </div>

    <ul class="pager wizard">
        <li class="previous"><a href="javascript:;">Previous</a></li>
        <li class="next"><a href="javascript:;">Next</a></li>
        <li class="next finish" id="exemption-finish" style="display:none;"><a href="javascript:;">Finish</a></li>
    </ul>
</div>
<div id="loadingDiv"></div>

<!-- Exemption Modal -->
<?php
Modal::begin([
    'id' => 'ExemptionPaperModal',
    'header' => '<h4 class="modal-title" id="expense-account-form-header">Exemption Paper</h4>',
    'footer' =>
    Html::button('Cancel', ['class' => 'btn btn-default', 'data-dismiss' => 'modal'])
    . PHP_EOL .
    Html::button('Add', ['id' => 'exemption-btn', 'class' => 'btn btn-primary btn-modal-save']),
]);
?>
<?php Modal::end() ?>