<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$form = ActiveForm::begin([
            'id' => 'exemptions-registration-form',
            'action' => Yii::$app->urlManager->createUrl(['exemption/grounds']),
            'options' => ['enctype' => 'multipart/form-data',],
            'fieldConfig' => [
                'template' => "{label}{input}\n{hint}\n{error}",
            ],
        ]);
?>
<?= Html::activeHiddenInput($model, 'id') ?>
<?= Html::activeHiddenInput($model, 'paperId') ?>
<div class="panel-group mt10">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-7" id="exemption-papers-div">
                    <?php foreach ($exemptionPapers as $paper) : ?>
                    <p><input type="checkbox" checked="true" class="exemptionpapers" name="exemption-papers" value="<?= $paper['code'] ?>"><?= $paper['name'] ?></p>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>  
</div>
<?php ActiveForm::end(); ?>