<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$section[1] = "PART I Section 1";
$section[2] = "PART I Section 2";
$section[3] = "PART II Section 3";
$section[4] = "PART II Section 4";
$section[5] = "PART III Section 5";
$section[6] = "PART III Section 6";

$codes[1] = "COD5454";
$codes[2] = "COD5455";
$codes[3] = "COD5456";
$codes[4] = "COD5457";

$form = ActiveForm::begin([
            'id' => 'examination-registration-form',
            'action' => Yii::$app->urlManager->createUrl(['examination/register']),
            'options' => ['enctype' => 'multipart/form-data',],
            'fieldConfig' => [
                'template' => "{label}{input}\n{hint}\n{error}",
            ],
        ]);
?>
<?= Html::activeHiddenInput($model, 'id') ?>
<div class="row">
    <div class="col-md-12">
    <?= $form->field($model, 'cpaqa', [
            'options' => ['class'=>'field-label'],
            'inputOptions' => ['class' => 'form-control'],
    ])->checkbox(['label'=>"Quality Analysis - CPAQA001"]); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
    <?= $form->field($model, 'cpaqa1', [
            'options' => ['class'=>'field-label'],
            'inputOptions' => ['class' => 'form-control'],
    ])->checkbox(['label'=>"Quality Analysis - CPAQA002"]); ?>
    </div>
</div>   
<div class="row">
    <div class="col-md-12">
    <?= $form->field($model, 'cpaqa1', [
            'options' => ['class'=>'field-label'],
            'inputOptions' => ['class' => 'form-control'],
    ])->checkbox(['label'=>"Quality Analysis - CPAQA003"]); ?>
    </div>
</div>   
<div class="row">
    <div class="col-md-12">
    <?= $form->field($model, 'cpaqa1', [
            'options' => ['class'=>'field-label'],
            'inputOptions' => ['class' => 'form-control'],
    ])->checkbox(['label'=>"Quality Analysis - CPAQA004"]); ?>
    </div>
</div>   
<div class="row">
    <div class="col-md-12">
    <?= $form->field($model, 'cpaqa1', [
            'options' => ['class'=>'field-label'],
            'inputOptions' => ['class' => 'form-control'],
    ])->checkbox(['label'=>"Quality Analysis - CPAQA005"]); ?>
    </div>
</div>   
<?php ActiveForm::end(); ?>