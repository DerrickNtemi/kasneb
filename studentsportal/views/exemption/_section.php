<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ArrayDataProvider;
use yii\bootstrap\Modal;
?>


<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class='col-md-4'>                 
                        <p>
                            <?=Html::a('<span class="fa fa-plus"></span> Add Paper', "", [ 'class'=>'btn btn-primary', 'data-toggle'=>"modal", 'data-whatever'=>'' ,'data-target'=>"#ExemptionPaperModal", 'title' => Yii::t('yii', 'Add Paper'),]);?>  
                        </p>
                    </div>
                    <div class="col-md-3">                            
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?= GridView::widget([
                            'dataProvider' => new ArrayDataProvider([
                                'allModels' => $model,
                            ]),
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                'section',
                                'paperCode',
                                'paperTitle',
                            ],
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Exemption Modal -->
<?php
    Modal::begin([
        'id' => 'ExemptionPaperModal',
        'header' => '<h4 class="modal-title" id="expense-account-form-header">Exemption Paper(s)</h4>',
        'footer' =>
            Html::button('Cancel', ['class' => 'btn btn-default', 'data-dismiss' => 'modal'])
            . PHP_EOL .
            Html::button('Add', ['id' => 'exemption-btn', 'class' => 'btn btn-primary btn-modal-save']),
    ]);
?>
<?= $this->render('_form', [ 'model' => $section]) ?>
<?php Modal::end() ?>
