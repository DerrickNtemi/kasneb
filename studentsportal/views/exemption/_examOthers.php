<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$form = ActiveForm::begin([
            'id' => 'exemptions-registration-form',
            'action' => Yii::$app->urlManager->createUrl(['exemption/grounds']),
            'options' => ['enctype' => 'multipart/form-data',],
            'fieldConfig' => [
                'template' => "{label}{input}\n{hint}\n{error}",
            ],
        ]);
?>
<?= Html::activeHiddenInput($model, 'id') ?>
<?= Html::activeHiddenInput($model, 'paperId') ?>
<div class="panel-group mt10">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="row">
                        <?=
                        $form->field($model, 'qualificationTypesId', [
                            'options' => ['class'=>'col-md-12'],
                            'inputOptions' => ['class' => 'form-control'],
                        ])->dropDownList($qualificationType)->label()
                        ?>
                    </div>
                    <div class="row" id="institution-id">
                        <?=
                        $form->field($model, 'institutionId', [
                            'options' => ['class'=>'col-md-12'],
                            'inputOptions' => ['class' => 'form-control'],
                        ])->dropDownList($institutions, ['prompt' => 'Choose Institution'])->label()
                        ?>
                    </div>
                    <div class="row"  id="institution-name" style="display: none;">
                    <?=
                                $form->field($model, 'institutionName', [
                                    'options' => ['class' => 'col-md-12'],
                                    'inputOptions' => ['class' => 'form-control  form-control-custom'],
                                ])?>
                    </div>
                    <div class="row" id="course-id">
                        <?=
                        $form->field($model, 'courseId', [
                            'options' => ['class'=>'col-md-12'],
                            'inputOptions' => ['class' => 'form-control'],
                        ])->dropDownList($courses, ['prompt' => 'Choose Course'])->label()
                        ?>
                    </div>    
                    <div class="row" id="course-name" style="display: none;">
                    <?=
                                $form->field($model, 'courseName', [
                                    'options' => ['class' => 'col-md-12'],
                                    'inputOptions' => ['class' => 'form-control  form-control-custom'],
                                ])?>
                    </div>                

                    <div class="row">
                        <?=
                        $form->field($model, 'document[]', [
                            'options' => ['class'=>'col-md-12'],
                            'inputOptions' => ['class' => 'form-control form-control-custom'],
                        ])->fileInput(['multiple' => true])
                        ?>
                        <span style="margin-left: 15px; font-size: 0.9em;">(*Use control key to attach multiple document)</span>
                    </div>
                    <div class="row" style="margin-top:10px;">
                        <div class="col-md-12">
                            <p class="btn btn-primary" id="add-qualification">Add Qualification</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-7">
                    <div class="row">
                        <div class="col-md-12"  id="exemption-papers-div"></div>
                    </div>
                    <div class="row" style="margin-top: 30px;">
                        <div class="col-md-12"  id="exemption-papers-final"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>  
</div>
<?php ActiveForm::end(); ?>