<?php

namespace studentsportal\assets;

use yii\web\AssetBundle;

/**
 * Main student's portal application asset bundle.
 */
class StudentsPortalAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/custom.css',
        'css/font-awesome/css/font-awesome.min.css',
    ];
    public $js = [
        'js/ccv.js',
        'js/face.js',
        'js/jquery.facedetection.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
