var errorMessage = 'An error occurred while processing your request, please try again or contact system administrator for assistance..';
var errorNotFound = "The requested resource was not found";
var errorInternalServer = "Internal server error, please try again or contact system administrator for assistance..";

$(document).ready(function () {

    $(window).load(function () {
        var pageHeight = $(document).height();
        var mainHeaderHeight = $('.main-header-div').height();
        var contentHeight = pageHeight - mainHeaderHeight - 60;
        $('.menu-div').css('min-height', contentHeight);
    });

    $('#loadingDiv').hide();
    $('#rootwizard').bootstrapWizard({
        onNext: function (tab, navigation, index) {
            var total = (navigation.find('li').length) - 1;
            if (index === 1) {
                var form = $('#account-details-form');
                var url = form.attr('action');
                var data = new FormData($(form)[0]);
                silentWizardCommunication(url, data, "#rootwizard", index, total, "#profile-error-summary");
                return false;
            } else if (index === 2) {
                var form = $('#contact-details-form');
                var url = form.attr('action');
                var data = new FormData($(form)[0]);
                silentWizardCommunication(url, data, "#rootwizard", index, total, "#contact-error-summary");
                return false;
            } else if (index === 3) {
                var proff = $(".course-type-proff");
                var proffClass = proff.attr('class');
                var selectedCourse = proffClass.indexOf("active");
                if (selectedCourse !== -1) {
                    courseType = 100;
                } else {
                    courseType = 200;
                }
                var docNumber = new Array();
                var i = 0;
                if (courseType === 200) {
                    $("input[name='CourseApplication[dipReq][]']:checkbox").each(function () {
                        if (this.checked) {
                            var lbl = "#diploma" + $(this).val();
                            var docNo = $(lbl).val();
                            if ($(this).val() !== "") {
                                docNumber[i] = [$(this).val(), docNo];
                            }
                            i++;
                        }
                    });
                } else {
                    $("input[name='CourseApplication[profReq][]']:checkbox").each(function () {
                        if (this.checked) {
                            var lbl = "#professional" + $(this).val();
                            var docNo = $(lbl).val();
                            if ($(this).val() !== "") {
                                docNumber[i] = [$(this).val(), docNo];
                            }
                            i++;
                        }
                    });
                }
                var form = $('#course-application-form');
                var url = form.attr('action') + "?courseType=" + courseType;
                $('#courseapplication-documentno').val(docNumber);
                var data = new FormData($(form)[0]);
                silentWizardCommunication(url, data, "#rootwizard", index, total, "#application-error-summary");
                return false;
            } else if (index === 4) {
                var form = $('#more-details-form');
                var declarations = new Array();
                var i = 0;
                var declarationValue = "";
                $("input[name='Declarations[declarationId][]']:checkbox").each(function () {
                    var declarationVal = (this.checked ? true : false);
                    if (declarationValue === "10") {
                        declarationValue = declarationVal;
                    } else {
                        declarations[i] = [$(this).val(), declarationVal];
                    }
                    i++;
                });
                declarations[i] = [$("#learn_kasneb").val(), declarationValue];
                var url = form.attr('action');
                $('#declarations-selection').val(declarations);
                var data = new FormData($(form)[0]);
                silentWizardCommunication(url, data, "#rootwizard", index, total, "#declaration-error-summary");
                return false;
            }
        },
        onTabShow: function (tab, navigation, index) {
            var total = (navigation.find('li').length);
            var percent = ((index + 1) / total) * 100;
            $('#rootwizard').find('.bar').css({width: percent + '%'});
            $('#rootwizard').find('.bar').text(Math.round(percent) + '% Complete');
            // If it's the last tab then hide the last button and show the finish instead
            if ((index + 1) >= total) {
                $('#rootwizard').find('.pager .next').hide();
                $('#rootwizard').find('.pager .finish').show();
                $('#rootwizard').find('.pager .finish').removeClass('disabled');
            } else {
                $('#rootwizard').find('.pager .next').show();
                $('#rootwizard').find('.pager .finish').hide();
            }
        },
        onTabClick: function (tab, navigation, index) {
            return false;
        }
    });

    $('#examinationwizard').bootstrapWizard({
        onNext: function (tab, navigation, index) {
            var total = (navigation.find('li').length) - 1;
            if (index === 1) {
                var form = $('#examination-sitting-form');
                var url = form.attr('action');
                var data = new FormData($(form)[0]);
                silentWizardCommunication(url, data, "#examinationwizard", index, total, "#sitting-error-summary");
                return false;
            } else if (index === 2) {
                var form = $('#examination-paper-form');
                var url = form.attr('action');
                var data = new FormData($(form)[0]);
                var value = "";
                if ($("#sectionId2").is(':checked')) {
                    value = "2";
                }
                data = {upper: value};
                silentWizardCommunicationWithoutCache(url, data, "#examinationwizard", index, total, "#papers-error-summary");
                return false;
            } else if (index === 3) {
                return false;
            } else if (index === 4) {
                var form = $('#centers-form');
                var data = new FormData(form[0]);
                var url = form.attr('action');
                silentWizardRedirectComm(url, data, "#centers-error-summary");
                return false;
            }
        },
        onTabShow: function (tab, navigation, index) {
            var total = (navigation.find('li').length);
            var percent = ((index + 1) / total) * 100;
            $('#examinationwizard').find('.bar').css({width: percent + '%'});
            $('#examinationwizard').find('.bar').text(Math.round(percent) + '% Complete');
            // If it's the last tab then hide the last button and show the finish instead
            if ((index + 1) >= total) {
                $('#examinationwizard').find('.pager .next').hide();
                $('#examinationwizard').find('.pager .previous').addClass('disabled');
                $('#examinationwizard').find('.pager .finish').show();
                $('#examinationwizard').find('.pager .finish').removeClass('disabled');
            } else {
                $('#examinationwizard').find('.pager .next').show();
                $('#examinationwizard').find('.pager .finish').hide();
            }
        },
        onTabClick: function (tab, navigation, index) {
            return false;
        }
    });

    $("#exam-finish-btn").click(function () {
        var form = $('#centers-form');
        var data = new FormData(form[0]);
        var url = form.attr('action');
        silentWizardRedirectComm(url, data, "#centers-error-summary");
    });

    $("#exemption-finish").click(function () {
        var form = $('#exemptions-registration-form');
        var url = form.attr('action') + "-others-finish";
        var data = "";
        silentWizardRedirectComm(url, data, "#bill-error-summary");
    });

    $('#exemptionwizard').bootstrapWizard({
        onNext: function (tab, navigation, index) {
            var total = (navigation.find('li').length) - 1;
            if (index === 1) {
                var form = $('#exemptions-registration-form');
                var papers = new Array();
                var i = 0;
                $("input[name='exemption-papers']:checkbox").each(function () {
                    if (this.checked) {
                        papers[i] = $(this).val();
                    }
                    i++;
                });
                $('#exemptiongroundsothers-paperid').val(papers);
                var url = form.attr('action') + "-others";
                var data = new FormData($(form)[0]);
                console.log(data);
                if ($("#exemptiongroundsothers-qualificationtypesid").val() === 1000) {
                    console.log("Others");
                    //silentWizardRedirectComm(url, data, "#exemption-grounds-error-summary");
                } else {
                    console.log("Present Bill");
                }
                return false;
            } else if (index === 2) {
                return false;
            }
        },
        onTabShow: function (tab, navigation, index) {
            var total = (navigation.find('li').length);
            var percent = ((index + 1) / total) * 100;
            $('#exemptionwizard').find('.bar').css({width: percent + '%'});
            $('#exemptionwizard').find('.bar').text(Math.round(percent) + '% Complete');
            // If it's the last tab then hide the last button and show the finish instead
            if ((index + 1) >= total) {
                $('#exemptionwizard').find('.pager .next').hide();
                $('#exemptionwizard').find('.pager .finish').show();
                $('#exemptionwizard').find('.pager .finish').removeClass('disabled');
            } else {
                $('#exemptionwizard').find('.pager .next').show();
                $('#exemptionwizard').find('.pager .finish').hide();
            }
        },
        onTabClick: function (tab, navigation, index) {
            return false;
        }
    });

    $('#exemptionwizardothers').bootstrapWizard({
        onNext: function (tab, navigation, index) {
            var total = (navigation.find('li').length) - 1;
            if (index === 1) {
                var form = $('#exemptions-registration-form');
                var papers = new Array();
                var i = 0;
                $("input[name='exemption-papers']:checkbox").each(function () {
                    if (this.checked) {
                        papers[i] = $(this).val();
                    }
                    i++;
                });
                $('#exemptiongroundsothers-paperid').val(papers);
                var url = form.attr('action') + "-others";
                var data = new FormData($(form)[0]);
                silentWizardCommunication(url, data, "#exemptionwizardothers", index, total, "#exemption-grounds-error-summary");
                return false;
            } else if (index === 2) {
                return false;
            }
        },
        onTabShow: function (tab, navigation, index) {
            var total = (navigation.find('li').length);
            var percent = ((index + 1) / total) * 100;
            $('#exemptionwizardothers').find('.bar').css({width: percent + '%'});
            $('#exemptionwizardothers').find('.bar').text(Math.round(percent) + '% Complete');
            // If it's the last tab then hide the last button and show the finish instead
            if ((index + 1) >= total) {
            } else {
                $('#exemptionwizardothers').find('.pager .next').show();
                $('#exemptionwizardothers').find('.pager .finish').hide();
            }
        },
        onTabClick: function (tab, navigation, index) {
            return false;
        }
    });

    $("#save-student-updates").click(function () {
        var form = $('#account-details-update-form');
        var url = form.attr('action');
        var data = new FormData($(form)[0]);
        silentWizardRedirectComm(url, data, "#contact-error-summary");
    });

    $("input[name='CourseApplication[profReq][]']:checkbox").click(function () {
        if ($(this).val() === "3") {
            $(".professinal-document").hide();
        } else {
            $(".professinal-document").show();
        }
    });

    $("input[name='CourseApplication[dipReq][]']:checkbox").click(function () {
        if ($(this).val() === "3") {
            $(".diploma-document").hide();
        } else {
            $(".diploma-document").show();
        }
    });


    $("input[name='Student[studentStatus]']:radio").click(function () {
        signUpDisplayer($(this).val());
    });

    $(window).load(function () {
        var statusVal = "";
        $("input[name='Student[studentStatus]']:radio").each(function () {
            if (this.checked) {
                statusVal = $(this).val();
            }
        });
        signUpDisplayer(statusVal);
    });

    function signUpDisplayer(statusId) {
        if (statusId === "1") {
            $("#signup-div").show();
            signupNewFields();
        } else if (statusId === "2") {
            $("#signup-div").show();
            signupExistingFields();
        }
    }

    function signupExistingFields() {
        $(".for-new-students").hide();
        $(".for-existing-students").show();
    }

    function signupNewFields() {
        $(".for-new-students").show();
        $(".for-existing-students").hide();
    }

    $('#WalletBalanceModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var modal = $(this);
        var url = button.data('whatever');
        modal.find('.modal-body #url').text(url);
        modal.find('.modal-body #error-summary').hide();
        modal.find('.modal-body #progress-spinner').hide();
    });

    $('#wallet-balance-btn').on('click', function () {
        var deleteModal = $('#WalletBalanceModal');
        var url = deleteModal.find('.modal-body #url').text();
        var pin = deleteModal.find('.modal-body #walletpin').val();
        $.ajax({
            url: url,
            type: 'POST',
            data: {pin: pin},
            beforeSend: function () {
                deleteModal.find('.modal-body #progress-spinner').show();
                deleteModal.find('.modal-body #error-summary').hide();
            },
            complete: function () {
                deleteModal.find('.modal-body #progress-spinner').hide();
            },
            success: function (data) {
                if (typeof data.success !== 'undefined') {
                    deleteModal.modal('hide');
                    deleteModal.removeData('modal');
                    window.location = data.success;
                } else {
                    deleteModal.find('.modal-body #error-summary').show().html(data.error);
                }
            },
            error: function () {
                deleteModal.find('.modal-body #error-summary').show().html(errorMessage);
            },
            statusCode: {
                404: function () {
                    deleteModal.find('.modal-body #error-summary').show().html(errorNotFound);
                },
                500: function () {
                    deleteModal.find('.modal-body #error-summary').show().html(errorInternalServer);
                }
            }
        });
    });

    function silentWizardCommunication(url, data, wizard, index, total, errorId) {
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $('#loadingDiv').show();
                $(errorId).hide();
            },
            complete: function () {
                $('#loadingDiv').hide();
            },
            success: function (data) {
                if (typeof data.success !== 'undefined') {
                    if (index === total) {
                        $('#tab' + (index + 1)).html(data.success);
                    }
                    if (wizard === "#examinationwizard" && index === 2) {
                        $('#tab' + (index + 1)).html(data.success);
                    }
                    if (wizard === "#exemptionwizardothers" && index === 1) {
                        $('#tab' + (index + 1)).html(data.success.view);
                    }
                    $(wizard).bootstrapWizard('show', index);
                    $(errorId).hide();
                    if (wizard === "#exemptionwizardothers" && index === 1) {
                        if (data.success.type === "OTHERS") {
                            $(wizard).find('.pager .next').hide();
                            $(wizard).find('.pager .previous').addClass('disabled');
                            $(wizard).find('.pager .finish').show();
                            $(wizard).find('.pager .finish').removeClass('disabled');
                        } else {
                            $(wizard).find('.pager .next').hide();
                            $(wizard).find('.pager .previous').removeClass('disabled');
                            $(wizard).find('.pager .finish').show();
                            $(wizard).find('.pager .finish').addClass('disabled');
                        }
                    }
                } else {
                    $(errorId).show().html(data.error);
                }
            },
            error: function () {
                $(errorId).show().html(errorMessage);
            },
            statusCode: {
                404: function () {
                    $(errorId).show().html(errorNotFound);
                },
                500: function () {
                    $(errorId).show().html(errorInternalServer);
                }
            }
        });
    }


    function silentWizardCommunicationWithoutCache(url, data, wizard, index, total, errorId) {
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            beforeSend: function () {
                $('#loadingDiv').show();
                $(errorId).hide();
            },
            complete: function () {
                $('#loadingDiv').hide();
            },
            success: function (data) {
                if (typeof data.success !== 'undefined') {
                    if (index === total) {
                        $('#tab' + (index + 1)).html(data.success);
                    }
                    if (wizard === "#examinationwizard" && index === 2) {
                        $('#tab' + (index + 1)).html(data.success);
                    }
                    $(wizard).bootstrapWizard('show', index);
                    $(errorId).hide();
                } else {
                    $(errorId).show().html(data.error);
                }
            },
            error: function () {
                $(errorId).show().html(errorMessage);
            },
            statusCode: {
                404: function () {
                    $(errorId).show().html(errorNotFound);
                },
                500: function () {
                    $(errorId).show().html(errorInternalServer);
                }
            }
        });
    }

    function silentWizardRedirectComm(url, data, errorId) {
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $('#loadingDiv').show();
                $(errorId).hide();
            },
            complete: function () {
                $('#loadingDiv').hide();
            },
            success: function (data) {
                if (typeof data.success !== 'undefined') {
                    window.location = data.success.url;
                } else {
                    $(errorId).show().html(data.error);
                }
            },
            error: function () {
                $(errorId).show().html(errorMessage);
            },
            statusCode: {
                404: function () {
                    $(errorId).show().html(errorNotFound);
                },
                500: function () {
                    $(errorId).show().html(errorInternalServer);
                }
            }
        });
    }

    function silentCommReturnVal(url, data, errorId, inputId) {
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $('#loadingDiv').show();
                $(errorId).hide();
            },
            complete: function () {
                $('#loadingDiv').hide();
            },
            success: function (data) {
                if (typeof data.success !== 'undefined') {
                    $(inputId).val(data.success);
                } else {
                    $(errorId).show().html(data.error);
                }
            },
            error: function () {
                $(errorId).show().html(errorMessage);
            },
            statusCode: {
                404: function () {
                    $(errorId).show().html(errorNotFound);
                },
                500: function () {
                    $(errorId).show().html(errorInternalServer);
                }
            }
        });
    }

    function silentCommReturnDropdownList(url, data, errorId, inputId) {
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $('#loadingDiv').show();
                $(errorId).hide();
            },
            complete: function () {
                $('#loadingDiv').hide();
            },
            success: function (data) {
                if (typeof data.success !== 'undefined') {
                    $(inputId).find('option').remove().end();
                    $.each(data.success, function (i, value) {
                        $(inputId).append($('<option>').text(value).attr('value', i));
                    });

                } else {
                    $(errorId).show().html(data.error);
                }
            },
            error: function () {
                $(errorId).show().html(errorMessage);
            },
            statusCode: {
                404: function () {
                    $(errorId).show().html(errorNotFound);
                },
                500: function () {
                    $(errorId).show().html(errorInternalServer);
                }
            }
        });
    }


    function silentCommReturnDropdownListDisplay(url, data, errorId, inputId, checkboxName) {
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $('#loadingDiv').show();
                $(errorId).hide();
            },
            complete: function () {
                $('#loadingDiv').hide();
            },
            success: function (data) {
                if (typeof data.success !== 'undefined') {
                    $(inputId).find('p').remove().end();
                    $.each(data.success, function (i, value) {
                        $(inputId).append($("<p>").text(" " + value).prepend($("<input>").attr({'type': 'checkbox', 'name': checkboxName}).val(i).prop('checked', true)));
                    });
                } else {
                    $(errorId).show().html(data.error);
                }
            },
            error: function () {
                $(errorId).show().html(errorMessage);
            },
            statusCode: {
                404: function () {
                    $(errorId).show().html(errorNotFound);
                },
                500: function () {
                    $(errorId).show().html(errorInternalServer);
                }
            }
        });
    }

    function silentCommReturnTableDisplay(url, data, errorId, inputId, checkboxName) {
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $('#loadingDiv').show();
                $(errorId).hide();
            },
            complete: function () {
                $('#loadingDiv').hide();
            },
            success: function (data) {
                if (typeof data.success !== 'undefined') {
                    var content = "<table class='table table-striped'>";
                    $("#exemption-papers-final").find('p').remove().end();
                    $(inputId).find('table').remove().end();
                    $(inputId).find('p').remove().end();
                    $.each(data.success, function (i, value) {
                        //$(inputId).append($("<p>").text(" " + value).prepend($("<input>").attr({'type': 'checkbox', 'name': checkboxName}).val(i).prop('checked', true)));
                        content += '<tr><td>' + ++i + '</td><td>' + value.institutionName + '</td><td>' + value.courseName + '</td><td style="color:skyblue; cursor:pointer;" class="remove-qualification" id=' + value.courseId + '>remove</td></tr>';
                    });
                    content += "</table><p class='btn btn-primary get-qualification-papers text-right' >Get Papers</p>";
                    $(inputId).append(content);
                } else {
                    $(errorId).show().html(data.error);
                }
            },
            error: function () {
                $(errorId).show().html(errorMessage);
            },
            statusCode: {
                404: function () {
                    $(errorId).show().html(errorNotFound);
                },
                500: function () {
                    $(errorId).show().html(errorInternalServer);
                }
            }
        });
    }

    function silentCommunicationWithRedirect(activeModalId, submitForm, button) {
        var activeModal = $(activeModalId);
        var form = $(submitForm);
        var formData = new FormData(form[0]);
        $.ajax({
            url: form.attr('action'),
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
                activeModal.find('.modal-body #progress-spinner').show();
                activeModal.find('.modal-body #error-summary').hide();
                button.attr('disabled', true);
            },
            complete: function () {
                activeModal.find('.modal-body #progress-spinner').hide();
            },
            success: function (data) {
                if (typeof data.success !== 'undefined') {
                    activeModal.modal('hide');
                    activeModal.removeData('modal');
                    window.location = data.success;
                    button.attr('disabled', false);
                } else {
                    activeModal.find('.modal-body #error-summary').show().html(data.error);
                    button.attr('disabled', false);
                }
            },
            error: function () {
                activeModal.find('.modal-body #error-summary').show().html(errorMessage);
                button.attr('disabled', false);
            },
            statusCode: {
                404: function () {
                    activeModal.find('.modal-body #error-summary').show().html(errorNotFound);
                    button.attr('disabled', false);
                },
                500: function () {
                    activeModal.find('.modal-body #error-summary').show().html(errorInternalServer);
                    button.attr('disabled', false);
                }
            }
        });
    }

    $("#verify-payment-btn").click(function () {
        var paymentModel = $('#VerifyPayment');
        var payid = paymentModel.find('.modal-body #payid').text();
        var feecode = paymentModel.find('.modal-body #feecode').text();
        var invoiceid = paymentModel.find('.modal-body #invoiceid').text();
        var wizard = paymentModel.find('.modal-body #wizard').text();
        var tabindex = paymentModel.find('.modal-body #tabindex').text();
        var pin = paymentModel.find('.modal-body #pin').text();
        var phone = paymentModel.find('.modal-body #phone').text();
        var url = paymentModel.find('.modal-body #url').text();

        if (payid === "examination-pay") {
            var data = {pin: pin, feeCode: feecode, phone: phone};
        } else if (payid === "jambopay-pay") {
            var data = {pin: pin, feeCode: feecode, phone: phone};
        } else {
            var data = {pin: pin, invoiceId: invoiceid, phone: phone};
        }

        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            beforeSend: function () {
                paymentModel.find('.modal-body #progress-spinner').show();
                paymentModel.find('.modal-body #error-summary').hide();
            },
            complete: function () {
                paymentModel.find('.modal-body #progress-spinner').hide();
            },
            success: function (data) {
                if (typeof data.success !== 'undefined') {
                    if (payid === "examination-pay") {
                        $(wizard).bootstrapWizard('show', tabindex);
                    } else {
                        window.location = data.success.url;
                    }
                } else {
                    paymentModel.find('.modal-body #error-summary').show().html(data.error);
                }
            },
            error: function () {
                paymentModel.find('.modal-body #error-summary').show().html(errorMessage);
            },
            statusCode: {
                404: function () {
                    paymentModel.find('.modal-body #error-summary').show().html(errorNotFound);
                },
                500: function () {
                    paymentModel.find('.modal-body #error-summary').show().html(errorInternalServer);
                }
            }
        });
    });

    $('#VerifyPayment').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var modal = $(this);

        var payid = button.data('whatever');
        var feecode = button.data('feecode');
        var invoiceid = button.data('invoiceid');
        var wizard = button.data('wizard');
        var tabindex = button.data('tabindex');

        var pin = $("#wallet-pin-pay").val();
        var phone = $("#phone-number-pay").val();
        var form = $('#bill-details-form');
        var url = form.attr('action');

        modal.find('.modal-body #url').text(url);
        modal.find('.modal-body #tabindex').text(tabindex);
        modal.find('.modal-body #feecode').text(feecode);
        modal.find('.modal-body #wizard').text(wizard);
        modal.find('.modal-body #pin').text(pin);
        modal.find('.modal-body #phone').text(phone);
        modal.find('.modal-body #payid').text(payid);
        modal.find('.modal-body #invoiceid').text(invoiceid);
        modal.find('.modal-body #error-summary').hide();
        modal.find('.modal-body #progress-spinner').hide();
    });

    $("#learn_kasneb").change(function () {
        if ($(this).val() === "Others") {
            $("#declarationId10").show();
        } else {
            $("#declarationId10").hide();
        }
    });

    $("#contact-countryid").change(function () {
        if ($(this).val() === "1") {
            $(".field-contact-countyid").show();
        } else {
            $(".field-contact-countyid").hide();
        }
    });

    $("#centers-zone").change(function () {
        var form = $('#centers-form');
        var data = "";
        var url = form.attr('action') + "-filter?zone=" + $(this).val();
        silentCommReturnDropdownList(url, data, "#centers-error-summary", '#centers-examcenter');
    });

    $("#exemptiongroundsothers-qualificationtypesid").change(function () {
        $("#exemption-papers-div").text("");
        var form = $('#exemptions-registration-form');
        var data = "";
        if ($(this).val() === "1000") {
            $('#institution-id').hide();
            $("#institution-name").show();
            $("#course-id").hide();
            $("#course-name").show();
            var url = form.attr('action') + "-get-papers?id=1000&codeType=1";
            silentCommReturnDropdownListDisplay(url, data, "#exemption-grounds-error-summary", '#exemption-papers-div', 'exemption-papers');
        } else {
            $('#institution-id').show();
            $("#institution-name").hide();
            $("#course-id").show();
            $("#course-name").hide();
            var url = form.attr('action') + "-get-institution?id=" + $(this).val();
            silentCommReturnDropdownList(url, data, "#exemption-grounds-error-summary", '#exemptiongroundsothers-institutionid');
        }
    });

    $("#exemptiongroundsothers-institutionid").change(function () {
        $("#exemption-papers-div").text("");
        var form = $('#exemptions-registration-form');
        var data = "";
        if ($(this).val() === "1000") {
            $("#institution-name").show();
            $("#course-id").hide();
            $("#course-name").show();
            var url = form.attr('action') + "-get-papers?id=1000&codeType=2";
            silentCommReturnDropdownListDisplay(url, data, "#exemption-grounds-error-summary", '#exemption-papers-div', 'exemption-papers');
        } else {
            $("#course-id").show();
            $("#course-name").hide();
            $("#institution-name").hide();
            var url = form.attr('action') + "-get-course-others?id=" + $(this).val();
            silentCommReturnDropdownList(url, data, "#exemption-grounds-error-summary", '#exemptiongroundsothers-courseid');
        }
    });
    $("#add-qualification").click(function () {
        var form = $('#exemptions-registration-form');
        var url = form.attr('action') + "-add-qualifications";
        $("#exemptiongroundsothers-institutionname").val($("#exemptiongroundsothers-institutionid option:selected").text());
        $("#exemptiongroundsothers-coursename").val($("#exemptiongroundsothers-courseid option:selected").text());
        var data = new FormData($(form)[0]);
        silentCommReturnTableDisplay(url, data, "#exemption-grounds-error-summary", '#exemption-papers-div', 'exemption-papers');
    });

    $(document).on('click', '.remove-qualification', function () {
        var form = $('#exemptions-registration-form');
        var value = $(this).attr('id');
        var url = form.attr('action') + "-remove-qualifications?id=" + value;
        var data = "";
        silentCommReturnTableDisplay(url, data, "#exemption-grounds-error-summary", '#exemption-papers-div', 'exemption-papers');
    });

    $(document).on('click', '.get-qualification-papers', function () {
        var form = $('#exemptions-registration-form');
        var url = form.attr('action') + "-get-qualification-papers";
        var data = "";
        silentCommReturnDropdownListDisplay(url, data, "#exemption-grounds-error-summary", '#exemption-papers-final', 'exemption-papers');
    });

    /*$("#exemptiongroundsothers-courseid").change(function () {
     if ($(this).val() === "1000") {
     $("#course-name").show();
     } else {
     $("#course-name").hide();
     }
     var form = $('#exemptions-registration-form');
     var url = form.attr('action') + "-get-papers?id=" + $(this).val() + "&codeType=3";
     var data = "";
     silentCommReturnDropdownListDisplay(url, data, "#exemption-grounds-error-summary", '#exemption-papers-div', 'exemption-papers');
     
     });*/

    $("#exemptiongrounds-qualificationtypesid").change(function () {
        var form = $('#exemptions-registration-form');
        var url = form.attr('action') + "-get-courses?id=" + $(this).val();
        var data = "";
        silentCommReturnDropdownList(url, data, "#exemption-grounds-error-summary", '#exemptiongrounds-courseid');
    });

    $("#exemptiongrounds-courseid").change(function () {
        var form = $('#exemptions-registration-form');
        var url = form.attr('action') + "-get-papers?id=" + $(this).val();
        var data = "";
        silentCommReturnDropdownListDisplay(url, data, "#exemption-grounds-error-summary", '#exemption-papers-div', 'exemption-papers');
    });

    $("#sitting-month").change(function () {
        var form = $('#examination-sitting-form');
        var url = form.attr('action') + "-get-year?month=" + $(this).val();
        var data = "";
        silentCommReturnVal(url, data, "#sitting-error-summary", '#sitting-year');
    });

    $("#declarations-month").change(function () {
        var form = $('#more-details-form');
        var url = form.attr('action') + "-get-year?month=" + $(this).val();
        var data = "";
        silentCommReturnVal(url, data, "#declaration-error-summary", '#declarations-year');
    });



    $('#filter-stream').on('change', function () {
        var form = $('#filter-transactions-form');
        var url = form.attr('action');
        var stream = $("#filter-stream").val();
        window.location = url + '?k=' + stream;
    });

    $(document).on('change', "#bill-methods", function () {
        if ($(this).val() === "1") {
            $('.jp').show();
            $('.paybill').hide();
            $('.bankbill').hide();
        } else if ($(this).val() === "2") {
            $('.jp').hide();
            $('.paybill').show();
            $('.bankbill').hide();
        } else if ($(this).val() === "3") {
            $('.jp').hide();
            $('.paybill').hide();
            $('.bankbill').show();
        }
    });



    $("#refundrequest-refundreasonsid").change(function () {
        if ($(this).val() === "1") {
            $("#refund-request-description").show();
        } else {
            $("#refund-request-description").hide();
        }
    });


    $("#add-refund-request").click(function () {
        var activeModalId = "#RequestRefundModal";
        var submitForm = "#create-refund-request-form";
        silentCommunicationWithRedirect(activeModalId, submitForm, $(this));
    });

    $('#DeleteRedirectModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var modal = $(this);
        var url = button.data('whatever');
        modal.find('.modal-body #url').text(url);
        modal.find('.modal-body #error-summary').hide();
        modal.find('.modal-body #progress-spinner').hide();
    });

    $('#delete-redirect-btn').on('click', function () {
        var deleteModal = $('#DeleteRedirectModal');
        var url = deleteModal.find('.modal-body #url').text();
        var password = deleteModal.find('.modal-body #password').val();
        $.ajax({
            url: url,
            type: 'POST',
            data: {password: password},
            beforeSend: function () {
                deleteModal.find('.modal-body #progress-spinner').show();
                deleteModal.find('.modal-body #error-summary').hide();
            },
            complete: function () {
                deleteModal.find('.modal-body #progress-spinner').hide();
            },
            success: function (data) {
                if (typeof data.success !== 'undefined') {
                    deleteModal.modal('hide');
                    deleteModal.removeData('modal');
                    window.location = data.success;
                } else {
                    deleteModal.find('.modal-body #error-summary').show().html(data.error);
                }
            },
            error: function () {
                deleteModal.find('.modal-body #error-summary').show().html(errorMessage);
            },
            statusCode: {
                404: function () {
                    deleteModal.find('.modal-body #error-summary').show().html(errorNotFound);
                },
                500: function () {
                    deleteModal.find('.modal-body #error-summary').show().html(errorInternalServer);
                }
            }
        });
    });

});