<?php

namespace studentsportal\controllers;

use Yii;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\ResetPassword;
use common\models\ResetPasswordRequest;
use common\models\Student;
use common\models\Service;
use common\models\Notifications;
use yii\helpers\Html;
use yii\web\Response;
use yii\helpers\Url;
use common\models\RefundRequest;
use common\models\WalletBalance;
use common\models\TopUp;

/**
 * Site controller
 */
class SiteController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'error', 'forgot-password', 'reset-password', 'expired-password', 'signup', 'info', 'activate-account', 'code-of-ethics'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex() {
        $service = new Service();
        $studentModel = new Student();
        $identity = Yii::$app->user->getIdentity();
        $studentModel->setAttributes($service->getStudentData($identity->student['id']));
        //print_r($studentModel->studentCourses);exit;
        return $this->render('index', ['model' => $studentModel,]);
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionNotifications() {
        $identity = Yii::$app->user->getIdentity();
        $service = new Service();
        $notifications = [];
        $notificationsData = $service->getNotifications($identity->student['id']);
        foreach ($notificationsData as $not) {
            $model = new Notifications();
            $model->setAttributes($not);
            $notifications[] = $model;
        }
        return $this->render('notifications', ['notificationsData' => $notifications]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin() {
        $portalName = "STUDENT'S PORTAL";
        $portalUser = "STUDENT";
        $portalAction = "Login";

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $studentGuide = [];
        $studentGuideArray = [];
        foreach ($studentGuide as $data) {
            $guide = new GUIDE();
            $guide->setAttributes($data);
            $studentGuideArray[] = $guide;
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $i = 0;
            $service = new Service();
            $_SESSION['update'] = "";
            $studentModel = new Student();
            $identity = Yii::$app->user->getIdentity();
            $studentModel->setAttributes($service->getStudentData($identity->student['id']));
            
           $studentModel->studentCourses = empty($studentModel->studentCourses) ? [] : $studentModel->studentCourses;
            foreach ($studentModel->studentCourses as $courses) {
                if ($courses['courseStatus'] == "FAILED_IDENTIFICATION") {
                    $i++;
                }
            }
            if (empty($studentModel->documentScan) || empty($studentModel->passportPhoto) || empty($studentModel->documentNo) || empty($studentModel->contact['postalAddress']) || empty($studentModel->contact['postalCode']) || empty($studentModel->contact['town'])) {
                $i++;
            }
            if ($i > 0) {
                $_SESSION['update'] = "update";
                $url = Url::toRoute(['registration/update-profile']);
                return $this->redirect($url);
            }
            return $this->goBack();
        }
        return $this->renderPartial('@common/views/accountAccess', [
                    'model' => $model,
                    'portalName' => $portalName,
                    'portalUser' => $portalUser,
                    'portalAction' => $portalAction,
                    'studentGuide' => $studentGuideArray,
                        ], true, false);
    }

    public function actionSignup() {
        $portalName = "STUDENT'S PORTAL";
        $portalUser = "STUDENT";
        $portalAction = "Sign Up";

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $service = new Service();
        $studentGuide = []; //$service->getStudentGuide();
        $studentGuideArray = [];
        foreach ($studentGuide as $data) {
            $guide = new GUIDE();
            $guide->setAttributes($data);
            $studentGuideArray[] = $guide;
        }
        $model = new Student(['scenario' => 'PARTIAL_REGISTRATION']);
        $country = $service->getCountries();
        $courses = $service->getAllCourses();
        $countries = $courseData = [];
        foreach ($country as $cntry) {
            $countries[$cntry['code']] = $cntry['name'];
        }
        $courseData[] = "Choose Examination";
        foreach ($courses as $c) {
            $courseData[$c['id']] = $c['name'];
        }
        if ($model->load(Yii::$app->request->post())) {
            if ($model->studentStatus == 2) {
                $model = new Student(['scenario' => 'EXISTING_SIGNUP']);
            }
        }
        $model->countryId = 1;
        $model->documentType = 1;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->loginId = ['email' => $model->email, 'password' => $model->password, 'lastLogin' => '',];
            $model->nationality = [];
            $model->countryId = ['code' => $model->countryId];
            //$model->dob = date("Y-m-d", strtotime($model->dob));
            $response = (Object) $service->signUp($model);
            if ($response->status['code'] == 1) {
                $backUrl = Html::a("Login", ['/site/login'], ['class' => 'btn btn-small btn-success']);
                \Yii::$app->getSession()->setFlash('success-message', "Account successfully created a verification link has been sent to the email address. Click on the link to verify Account. &nbsp; &nbsp; &nbsp;" . $backUrl);
                $this->redirect(['info']);
                return;
            }
            $model->addError('Signup', $response->status['message']);
        }
        return $this->renderPartial('@common/views/accountAccess', [
                    'model' => $model,
                    'countries' => $countries,
                    'portalName' => $portalName,
                    'portalUser' => $portalUser,
                    'portalAction' => $portalAction,
                    'courseData' => $courseData,
                    'studentGuide' => $studentGuideArray,
                        ], true, false);
    }

    public function actionActivateAccount($token) {

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $service = new Service();
        $response = (Object) $service->activateAccount($token);
        if ($response->status['code'] == 1) {
            $backUrl = Html::a("Login", ['/site/login'], ['class' => 'btn btn-small btn-success']);
            \Yii::$app->getSession()->setFlash('success-message', "Account successfully activated. Login to access and use the portal service. &nbsp; &nbsp; &nbsp;" . $backUrl);
        } else {
            \Yii::$app->getSession()->setFlash('error-message', $response->status['message'] . ". Please conduct system administrator for further assistance");
        }
        $this->redirect(['info']);
        return;
    }

    public function actionForgotPassword() {
        $model = new ResetPasswordRequest();
        if ($model->load(Yii::$app->request->post())) {
            $service = new Service();
            $data = [
                'email' => $model->username
            ];
            $response = (Object) $service->requestPasswordReset($data);
            if ($response->status['code'] == 1) {
                \Yii::$app->getSession()->setFlash('success-message', "Password reset request was successful. Check your email address for reset instructions.");
                return $this->redirect(['info']);
            } else {
                $model->addError("", $response->status['message']);
            }
        }
        return $this->render('requestPasswordReset', [
                    'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token = "") {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new ResetPassword();
        if ($model->load(Yii::$app->request->post())) {
            $service = new Service();
            $data = [
                'password' => $model->password,
                'resetToken' => $model->token,
            ];
            $response = (Object) $service->resetStudentPassword($data);
            if ($response->status['code'] == 1) {
                \Yii::$app->getSession()->setFlash('success-message', "Password reset was successful.");
                return $this->redirect('login');
            } else {
                $model->addError("", $response->status['message']);
            }
        }
        $model->token = $token;
        return $this->render('resetPassword', [
                    'model' => $model,
        ]);
    }

    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionTopup() {
        $model = new TopUp();
        $identity = Yii::$app->user->getIdentity();
        $model->phoneNumber = $identity->student['phoneNumber'];
        if ($model->load(Yii::$app->request->post())) {
            $service = new Service();
            $data = [
                'amount' => $model->amount,
                'phoneNumber' => $model->phoneNumber,
            ];
            $response = (Object) $service->topUp($data);
            if ($response->status['code'] == 1) {
                \Yii::$app->getSession()->setFlash('success-message', $response->status['message']);
                return $this->redirect('balance');
            } else {
                $model->addError("", $response->status['message']);
            }
        }
        return $this->render('topup', [
                    'model' => $model,
        ]);
    }

    public function actionWalletStatement() {
        return $this->render('@common/views/_wallet_statement');
    }

    public function actionInfo() {
        return $this->render('@common/views/message');
    }

    public function actionCodeOfEthics() {
        return $this->render('codeofethics');
    }

    public function actionBillPayment() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $service = new Service();
        $studentModel = new Student();
        $identity = Yii::$app->user->getIdentity();
        $studentModel->setAttributes($service->getStudentData($identity->student['id']));
        $error = 0;
        if (empty($_POST['pin'])) {
            $error = 1;
            $studentModel->addError('', 'Wallet pin cannot be blank');
        }
        if (empty($_POST['phone'])) {
            $error = 1;
            $studentModel->addError('', 'Phone number cannot be blank');
        }
        $pin = $_POST['pin'];
        $phone = $_POST['phone'];
        $feeCode = $_POST['feeCode'];
        $data = [];
        //print_r($studentModel->invoices); exit;
        foreach ($studentModel->invoices as $invoice) {
            //$studentModel->phoneNumber
            //$_POST['pin']
            if ($invoice['feeCode']['code'] == $feeCode && $invoice['status']['status'] == "PENDING") {
                $data = ['phoneNumber' => $phone, 'pin' => $pin, 'amount' => $invoice['kesTotal'], 'currency' => 'KES', 'invoice' => ['id' => $invoice['id']]];
            }
        }
        if ($error == 0) {
            //print_r($data);  exit;
            $response = (object) $service->makePayments($data);

            if ($response->status['code'] == 1) {
                Yii::$app->session->setFlash('success-message', 'Your Registration application has been received .kindly await registration verification.');
                if ($feeCode == "EXEMPTION_FEE") {
                    unset($_SESSION['othersQualifications']);
                    Yii::$app->session->setFlash('success-message', 'Your examination exemption application has been received.');
                }if ($feeCode == "REGISTRATION_RENEWAL_FEE") {
                    Yii::$app->session->setFlash('success-message', 'Your renewal payment was successful and your status is now upto date.');
                }
                $url = Url::toRoute(['site/index']);
                return ['success' => ['url' => $url]];
            } else {
                //$studentModel->addError("", "ERROR CODE 500 :: Internal Server Error");
                $studentModel->addError("", $response->status['message']);
            }
        }

        $errorsArry = '';
        foreach ($studentModel->getErrors() as $error) {
            $errorsArry .= $error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

    public function actionCommonPayment() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $service = new Service();
        $studentModel = new Student();
        $error = 0;
        if (empty($_POST['pin'])) {
            $error = 1;
            $studentModel->addError('', 'Wallet pin cannot be blank');
        }
        if (empty($_POST['phone'])) {
            $error = 1;
            $studentModel->addError('', 'Phone number cannot be blank');
        }
        $invoice = $service->getInvoice($_POST['invoiceId']);
        $pin = $_POST['pin'];
        $phone = $_POST['phone'];
        $data = ['phoneNumber' => $phone, 'pin' => $pin, 'amount' => $invoice['kesTotal'], 'currency' => 'KES', 'invoice' => ['id' => $invoice['id']]];
        //print_r($data);
        //exit;
        if ($error == 0) {
            $response = (object) $service->makePayments($data);

            if ($response->status['code'] == 1) {
                Yii::$app->session->setFlash('success-message', 'Invoice successfully paid.');
                $url = Url::toRoute(['invoices/index']);
                return ['success' => ['url' => $url]];
            } else {
                $studentModel->addError("", $response->status['message']);
            }
        }

        $errorsArry = '';
        foreach ($studentModel->getErrors() as $error) {
            $errorsArry .= $error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

    public function actionWalletBalance() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new Student();
        $service = new Service();
        $identity = Yii::$app->user->getIdentity();
        $pin = $_POST['pin'];
        if (!empty($pin)) {
            $data = [
                'phoneNumber' => $identity->student['phoneNumber'],
                'jpPin' => $pin,
            ];
            $response = (Object) $service->getWalletBalance($data);
            if ($response->status['code'] == 1) {
                $url = Url::toRoute(['index']);
                $topupUrl = Html::a("Top up instructions", Url::toRoute(['site/topup']), ['class' => 'btn btn-sm btn-success', 'title' => 'Top up instructions']);
                \Yii::$app->getSession()->setFlash('success-message', $response->status['message'] . " ..... " . $topupUrl);
                return ['success' => $url];
            } else {
                $model->addError("", $response->status['message']);
            }
        } else {
            $model->addError("", "Wallet pin cannot be blank");
        }
        $errorsArry = '';
        foreach ($model->getErrors() as $error) {
            $errorsArry .= $error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

    public function actionBalance() {
        $model = new WalletBalance();
        $identity = Yii::$app->user->getIdentity();
        $model->phoneNumber = $identity->student['phoneNumber'];
        if ($model->load(Yii::$app->request->post())) {
            $service = new Service();
            $data = [
                'jpPin' => $model->jpPin,
                'phoneNumber' => $model->phoneNumber,
            ];
            $response = (Object) $service->getWalletBalance($data);
            if ($response->status['code'] == 1) {
                \Yii::$app->getSession()->setFlash('success-message', $response->status['message']);
                return $this->redirect('balance');
            } else {
                $model->addError("", $response->status['message']);
            }
        }
        return $this->render('balance', [
                    'model' => $model,
        ]);
    }

    public function actionRequestRefund() {
        $services = new Service();
        $studentModel = new Student();
        $identity = Yii::$app->user->getIdentity();
        $refundRequest = new RefundRequest();
        $requestData = $services->getStudentRequest($identity->student['id']);
        $reasonGroundArray = $services->getRefundReasons();
        $refundRequests = $refundGrounds = $transactionTypeAccounts = [];
        foreach ($reasonGroundArray as $reason) {
            $refundGrounds[$reason['id']] = $reason['description'];
        }

        foreach ($requestData as $request) {
            $refundRequestModel = new RefundRequest();
            $refundRequestModel->setAttributes($request);
            $refundRequestModel->createdAt = $services->timestampToSting1($refundRequestModel->createdAt);
            $refundRequests[] = $refundRequestModel;
        }
        $transactionAccountTypesArray = $services->getTransactionAccountTypes(1);
        foreach ($transactionAccountTypesArray as $tata) {
            $transactionTypeAccounts[$tata['id']] = $tata['name'];
        }
        return $this->render('refund-request', ['refundGrounds' => $refundGrounds, 'refundRequest' => $refundRequest, 'refundRequests' => $refundRequests, 'transactionTypeAccounts' => $transactionTypeAccounts]);
    }

    public function actionCreateRefundRequest() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $service = new Service();
        $studentModel = new Student();
        $identity = Yii::$app->user->getIdentity();
        $studentModel->setAttributes($service->getStudentData($identity->student['id']));
        $model = new RefundRequest();
        $model->load(Yii::$app->request->post());
        $model->studentId = $studentModel->id;
        $model->phoneNumber = $studentModel->phoneNumber;
        $model->name = $studentModel->firstName . " " . $studentModel->middleName . " " . $studentModel->lastName;
        if (!empty($studentModel->currentCourse)) {
            $model->course = $studentModel->currentCourse['course']['name'];
            $model->registrationNo = $studentModel->currentCourse['fullRegistrationNumber'];
        }
        $model->currency = "KES";
        $model->transactionTypeId = 1;
        if ($model->validate()) {
            $response = (Object) $service->createRefundRequest($model);
            if ($response->status['code'] == 100) {
                $url = Url::toRoute(['refund-request-view', 'id' => $response->item]);
                \Yii::$app->getSession()->setFlash('success-message', "Refund request successfully proccessed. Kindly await refund verification");
                return ['success' => $url];
            } else {
                $model->addError("", "Error Code " . $response->status['code'] . " :: " . $response->status['message']);
            }
        }
        $errorsArry = '';
        foreach ($model->getErrors() as $error) {
            $errorsArry .= $error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

    public function actionRefundRequestDelete($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new RefundRequest();
        $service = new Service();
        $password = $_POST['password'];
        if (!empty($password)) {
            $studentModel = new Student();
            $identity = Yii::$app->user->getIdentity();
            $studentModel->setAttributes($service->getStudentData($identity->student['id']));
            $data = [
                'id' => $id,
                'studentId' => $studentModel->id
            ];
            $response = (Object) $service->deleteRefundRequest($data);
            if ($response->status['code'] == 100) {
                $url = Url::toRoute(['request-refund']);
                \Yii::$app->getSession()->setFlash('success-message', "Refund request successfully cancelled.");
                return ['success' => $url];
            } else {
                $model->addError("", "Error Code " . $response->status['code'] . " :: " . $response->status['message']);
            }
        } else {
            $model->addError("", "Password cannot be blank");
        }
        $errorsArry = '';
        foreach ($model->getErrors() as $error) {
            $errorsArry .= $error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

    public function actionRefundRequestView($id) {
        $services = new Service();
        $refundRequest = new RefundRequest();
        $requestData = $services->getRefundRequest($id);
        $refundRequest->setAttributes($requestData);
        $refundRequest->createdAt = $services->timestampToSting1($refundRequest->createdAt);
        return $this->render('refund-request-view', ['refundRequest' => $refundRequest]);
    }

    public function actionTimetable() {
        $identity = Yii::$app->user->getIdentity();
        $services = new Service();
        $documents = $services->getStudentTimetable($identity->student['id']);
        return $this->render('document-timetable', ['documents' => $documents]);
    }

    public function actionDownloadTimetable($id) {
        $service = new Service();
        $path = $service->getTimetable($id);
        header('Content-type: application/pdf');
        header('Content-Disposition: attachment; filename=timetable_' . $id . '.pdf');
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');
        readfile($path);
    }

    public function actionExemptionLetter() {
        $identity = Yii::$app->user->getIdentity();
        $services = new Service();
        $documents = $services->getStudentExemptionLetter($identity->student['id']);
        return $this->render('document-exemptionletter', ['documents' => $documents]);
    }

    public function actionDownloadExemptionletter($id) {
        $service = new Service();
        $path = $service->getExemptionLetter($id);
        header('Content-type: application/pdf');
        header('Content-Disposition: attachment; filename=exemptionletter_' . $id . '.pdf');
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');
        readfile($path);
    }

}
