<?php

namespace studentsportal\controllers;

use common\models\Bill;
use common\models\Invoice;
use common\models\Service;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\Student;

/**
 * Site controller
 */
class InvoicesController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                        [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex() {
        $service = new Service();
        $identity = Yii::$app->user->getIdentity();
        $studentModel = new Student();
        $studentModel->setAttributes($service->getStudentData($identity->student['id']));
        $studentModel->studentCourses = empty($studentModel->studentCourses) ? [] : $studentModel->studentCourses;
        foreach ($studentModel->studentCourses as $courses) {
            if ($courses['courseStatus'] == "REJECTED" && ($courses['remarks'] == "REJ_2" || $courses['remarks'] == "REJ_4")) {
                \Yii::$app->getSession()->setFlash('error-message', "");
                $this->redirect(['registration/update-documents', 'courseTypeCode' => $courses['course']['courseTypeCode'], 'courseId' => $courses['id'], 'studentId' => $identity->student['id']]);
                return;
            }
            if ($courses['courseStatus'] == "PENDING") {
                \Yii::$app->getSession()->setFlash('error-message', "Student course awaiting verification");
                $this->redirect(['info']);
                return;
            }
            if ($courses['courseStatus'] == "PENDING_IDENTIFICATION" || $courses['courseStatus'] == "FAILED_IDENTIFICATION") {
                \Yii::$app->getSession()->setFlash('error-message', "Student awaiting identification verification");
                $this->redirect(['info']);
                return;
            }
        }
        
        $pendingInvoices = $service->getStudentData($identity->student['id']);
        //print_r($pendingInvoices['pendingInvoices']);exit;
        $invoices = [];
        foreach ($pendingInvoices['pendingInvoices'] as $pendingInvoices) {
            $model = new Invoice();
            $model->setAttributes($pendingInvoices);
            $invoices[] = $model;
        }
        return $this->render('index', [
                    'invoices' => $invoices,
        ]);
    }

    public function actionInfo() {
        return $this->render('@common/views/message');
    }

    public function actionPay($id) {
        $service = new Service();
        $bill = new Bill();
        $invoice = $service->getInvoice($id);
        $bill->items = [];
        $bill->invoiceDate = $invoice['dateGenerated'];
        $bill->dueDate = $invoice['dueDate'];
        $bill->invoiceNo = $invoice['reference'];
        $bill->localAmount = $invoice['localAmount'];
        $bill->localCurrency = $invoice['localCurrency'];
        $bill->items = $invoice['invoiceDetails'];
        $bill->id = $invoice['id'];
        return $this->render("@common/views/_commonbill", ['model' => $bill]);
    }

    public function actionPrint($id) {
        $service = new Service();
        $path = $service->getInvoicePdf($id);
        header('Content-type: application/pdf');
        header('Content-Disposition: attachment; filename=invoice_' . $id . '.pdf');
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');
        readfile($path);
    }

}
