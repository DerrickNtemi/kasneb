<?php

namespace studentsportal\controllers;

use common\models\Service;
use common\models\Transactions;
use common\models\Filter;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * Site controller
 */
class TransactionsController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex($k="") {
        $service = new Service();
        $filter = new Filter();
        $filter->stream = $k;
        $identity = Yii::$app->user->getIdentity();
        $transactions = $service->getStudentTransactions($identity->student['id'],$k);
        //print_r($transactions); exit;
        $transactionsArray = [];
        foreach($transactions as $transaction){
            $model = new Transactions();
            $model->setAttributes($transaction);
            $transactionsArray[] = $model;
        }        
        $revenueStream = $service->revenueStream();
        return $this->render('index', ['datas' => $transactionsArray,'filter'=>$filter,'revenueStream'=>$revenueStream]);
    }
    
    public function actionPrint($id) {
        $service = new Service();
        $path = $service->getReceiptPdf($id);
        header('Content-type: application/pdf');
        header('Content-Disposition: attachment; filename=receipt.pdf');
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');
        readfile($path);
    }

}
