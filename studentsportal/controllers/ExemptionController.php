<?php

namespace studentsportal\controllers;

use common\models\Bill;
use common\models\ExemptionGrounds;
use common\models\ExemptionGroundsOthers;
use common\models\Service;
use common\models\Student;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;

/**
 * Site controller
 */
class ExemptionController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionOthers() {
        $service = new Service();
        $studentModel = new Student();
        $identity = Yii::$app->user->getIdentity();
        $studentModel->setAttributes($service->getStudentData($identity->student['id']));
        if (empty($studentModel->currentCourse)) {
            \Yii::$app->getSession()->setFlash('error-message', "Student does not have an active course at the moment");
            $this->redirect(['info']);
            return;
        }
        foreach ($studentModel->studentCourses as $courses) {
            if ($courses['courseStatus'] == "REJECTED" && ($courses['remarks'] == "REJ_2" || $courses['remarks'] == "REJ_4")) {
                \Yii::$app->getSession()->setFlash('error-message', "");
                $this->redirect(['registration/update-documents', 'courseTypeCode' => $courses['course']['courseTypeCode'], 'courseId' => $courses['id'], 'studentId' => $identity->student['id']]);
                return;
            }
            if ($courses['courseStatus'] == "PENDING") {
                \Yii::$app->getSession()->setFlash('error-message', "Student course awaiting verification");
                $this->redirect(['info']);
                return;
            }
            if ($courses['courseStatus'] == "PENDING_IDENTIFICATION" || $courses['courseStatus'] == "FAILED_IDENTIFICATION") {
                \Yii::$app->getSession()->setFlash('error-message', "Student awaiting identification verification");
                $this->redirect(['info']);
                return;
            }
        }
        
        $model = new ExemptionGroundsOthers();
        $currentCourse = $studentModel->currentCourse;
        $model->registrationNumber = $currentCourse['registrationNumber'];
        $model->id = $currentCourse['id'];

        $courses = $qualificationTypes = $institutions = [];
        $qualificationType = $service->getCourseTypeOthers();
        $courseTypeCode = $institutionCode = $courseCode = "";
        $i = 0;
        foreach ($qualificationType as $data) {
            $qualificationTypes[$data['code']] = $data['name'];
            if ($i == 0) {
                $courseTypeCode = $data['code'];
            }
            $i++;
        }

        $institution = $service->getInstitutions($courseTypeCode);
        $j = 0;
        foreach ($institution as $data) {
            $institutions[$data['id']] = $data['name'];
            if ($j == 0) {
                $institutionCode = $data['id'];
            }
            $j++;
        }

        $course = $service->getCoursesOthers($institutionCode);
        $j = 0;
        foreach ($course as $data) {
            $courses[$data['id']] = $data['name'];
            if ($j == 0) {
                $courseCode = $data['id'];
            }
            $j++;
        }
        return $this->render('others', [
                    'model' => $model,
                    'institutions' => $institutions,
                    'qualificationTypes' => $qualificationTypes,
                    'courses' => $courses,
        ]);
    }

    public function actionInfo() {
        return $this->render('@common/views/message');
    }

    public function actionIndex() {
        $service = new Service();
        $studentModel = new Student();
        $identity = Yii::$app->user->getIdentity();
        $studentModel->setAttributes($service->getStudentData($identity->student['id']));
        if (empty($studentModel->currentCourse)) {
            \Yii::$app->getSession()->setFlash('error-message', "Student does not have an active course at the moment");
            $this->redirect(['info']);
            return;
        }
        $model = new ExemptionGrounds();
        $course = $studentModel->currentCourse;
        $exemptionPapers = $course['eligibleExemptions'];
        if (empty($exemptionPapers)) {
            \Yii::$app->getSession()->setFlash('error-message', "Please note, currently you don't qualify for exemption");
            $this->redirect(['info']);
            return;
        }
        $model->registrationNumber = $course['registrationNumber'];
        $model->id = $course['id'];
        return $this->render('index', [
                    'model' => $model,
                    'exemptionPapers' => $exemptionPapers
        ]);
    }

    public function actionGroundsAddQualifications() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new ExemptionGroundsOthers();
        $model->load(Yii::$app->request->post());
        $documents = $documentPath = [];
        $i = 0;
        foreach ($_FILES['ExemptionGroundsOthers']['type']['document'] as $type) {
            $lastIndex = strripos($type, "/");
            $extension = substr($type, $lastIndex + 1, strlen($type));
            $rand = rand(1, 9999999);
            $name = $model->id . "-" . $rand . '.' . $extension;
            $path = Yii::getAlias('@webroot') . '/uploads/students/exemptions/' . $name;
            $documentPath[$i] = $path;
            $documents[] = ['name' => $name];
            $i++;
        }
        $j = 0;
        foreach ($_FILES['ExemptionGroundsOthers']['tmp_name']['document'] as $temp) {
            move_uploaded_file($temp, $documentPath[$j]);
            $j++;
        }

        $qualificationData = [
            "id" => $model->id,
            "institutionId" => $model->institutionId,
            "institutionName" => $model->institutionName,
            "qualificationTypesId" => $model->qualificationTypesId,
            "courseId" => $model->courseId,
            "courseName" => $model->courseName,
            "documents" => $documents
        ];
        $_SESSION['othersQualifications'][] = $qualificationData;
        return ['success' => $_SESSION['othersQualifications']];
    }

    public function actionGroundsGetQualificationPapers() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $data = [];
        foreach ($_SESSION['othersQualifications'] as $session) {
            $data[] = $session['courseId'];
        }
        $service = new Service();
        $studentModel = new Student();
        $identity = Yii::$app->user->getIdentity();
        $studentModel->setAttributes($service->getStudentData($identity->student['id']));
        $paper = $service->getMultipleExemptionPapers($data, $studentModel->currentCourse['id']);
        $papers = [];
        foreach ($paper as $data) {
            $papers[$data['code']] = $data['name'];
        }
        return ['success' => $papers];
    }

    public function actionGroundsRemoveQualifications($id = "") {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $data = $_SESSION['othersQualifications'];
        $_SESSION['othersQualifications'] = [];
        foreach ($data as $session) {
            if ($session['courseId'] == $id) {
                continue;
            }
            $_SESSION['othersQualifications'][] = $session;
        }
        return ['success' => $_SESSION['othersQualifications']];
    }

    public function actionGrounds() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $service = new Service();
        $model = new ExemptionGrounds();
        $model->load(Yii::$app->request->post());
        $papersId = [];
        $papers = explode(',', $model->paperId);
        for ($i = 0; $i < count($papers); $i++) {
            if (!empty($papers[$i])) {
                $papersId[] = ['paper' => ['code' => $papers[$i]]];
            }
        }
        $model->paperId = $papersId;
        if ($model->validate()) {
            $data = [
                'studentCourse' => ['id' => $model->id],
                'type' => "KASNEB",
                'papers' => $model->paperId,
            ];
            $response = (object) $service->createExemptionGrounds($data);
            if ($response->status['code'] == 1) {
                $bill = new Bill();
                $studentModel = new Student();
                $identity = Yii::$app->user->getIdentity();
                $studentModel->setAttributes($service->getStudentData($identity->student['id']));
                $bill->items = [];
                foreach ($studentModel->invoices as $invoice) {
                    if ($invoice['feeCode']['code'] == "EXEMPTION_FEE" && $invoice['status']['status'] == "PENDING") {
                        $bill->invoiceDate = $invoice['dateGenerated'];
                        $bill->dueDate = $invoice['dueDate'];
                        $bill->invoiceNo = $invoice['reference'];
                        $bill->localAmount = $invoice['localAmount'];
                        $bill->localCurrency = $invoice['localCurrency'];
                        $bill->id = $invoice['id'];
                        $bill->items = $invoice['invoiceDetails'];
                    }
                }
                $payId = "jambopay-pay";
                $nextTab = "3";
                $feeCode = "EXEMPTION_FEE";
                return ['success' => $this->renderPartial("@common/views/_bill", ['model' => $bill, 'payId' => $payId, 'nextTabIndex' => $nextTab, 'feeCode' => $feeCode])];
            } else {
                $model->addError("", $response->status['message']);
            }
        }
        $errorsArry = '';
        foreach ($model->getErrors() as $error) {
            $errorsArry .= $error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

    public function actionGroundsOthers() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $service = new Service();
        $model = new ExemptionGroundsOthers();
        $model->load(Yii::$app->request->post());
        $documents = $documentPath = $qualifications = $papersId = [];
        $papers = explode(',', $model->paperId);
        for ($i = 0; $i < count($papers); $i++) {
            if (!empty($papers[$i])) {
                $papersId[] = ['paper' => ['code' => $papers[$i]]];
            }
        }
        $model->paperId = $papersId;
        $codeType = 3;
        if ($model->qualificationTypesId == 1000) {
            $codeType = 1;
            $model->courseId = $model->qualificationTypesId;
            $qualifications[]= ["id"=>1000];
            $institutionId = null;
            $i = 0;

            foreach ($_FILES['ExemptionGroundsOthers']['type']['document'] as $type) {
                $lastIndex = strripos($type, "/");
                $extension = substr($type, $lastIndex + 1, strlen($type));
                $rand = rand(1, 9999999);
                $name = $model->id . "-" . $rand . '.' . $extension;
                $path = Yii::getAlias('@webroot') . '/uploads/students/exemptions/' . $name;
                $documentPath[$i] = $path;
                $documents[] = ['name' => $name];
                $i++;
            }
            $j = 0;
            foreach ($_FILES['ExemptionGroundsOthers']['tmp_name']['document'] as $temp) {
                move_uploaded_file($temp, $documentPath[$j]);
                $j++;
            }
        } else {
            $institutionId = [
                'id' => $model->institutionId,
            ];
            foreach ($_SESSION['othersQualifications'] as $session) {
                $qualifications[] = [
                    'id' => $session['courseId']
                ];
                foreach($session['documents'] as $doc){
                    $documents[] = $doc;
                }
            }
            $model->courseName = "";
            $model->institutionName = "";
        }
        if ($model->institutionId == 1000) {
            $codeType = 2;
        }
        if ($model->validate()) {
            $data = [
                'studentCourse' => [
                    'id' => $model->id,
                ],
                'qualifications' => $qualifications,
                'institutionName' => $model->institutionName,
                'courseName' => $model->courseName,
                'type' => 'OTHER',
                'documents' => $documents,
                'papers' => $model->paperId,
            ];
            $response = (object) $service->createExemptionGrounds($data);

            if ($response->status['code'] == 1) {
                if ($model->qualificationTypesId == 1000) {
                    //$backUrl = Html::a("Login", ['/site/login'], ['class' => 'btn btn-small btn-success']);
                    Yii::$app->session->setFlash('success-message', 'Exemption application was successful..');
                    unset($_SESSION['othersQualifications']);
                    return ['success' => ['view' => $this->renderPartial("@common/views/message"), 'type' => 'OTHERS']];
                } else {
                    $bill = new Bill();
                    $studentModel = new Student();
                    $identity = Yii::$app->user->getIdentity();
                    $studentModel->setAttributes($service->getStudentData($identity->student['id']));
                    $bill->items = [];
                    foreach ($studentModel->invoices as $invoice) {
                        if ($invoice['feeCode']['code'] == "EXEMPTION_FEE" && $invoice['status']['status'] == "PENDING") {
                            $bill->invoiceDate = $invoice['dateGenerated'];
                            $bill->dueDate = $invoice['dueDate'];
                            $bill->invoiceNo = $invoice['reference'];
                            $bill->localAmount = $invoice['localAmount'];
                            $bill->localCurrency = $invoice['localCurrency'];
                            $bill->id = $invoice['id'];
                            $bill->items = $invoice['invoiceDetails'];
                        }
                    }
                    $payId = "jambopay-pay";
                    $nextTab = "3";
                    $feeCode = "EXEMPTION_FEE";
                    return ['success' => ['view' => $this->renderPartial("@common/views/_bill", ['model' => $bill, 'payId' => $payId, 'nextTabIndex' => $nextTab, 'feeCode' => $feeCode]), 'type' => 'OTHERS-FILLED']];
                }
            } else {
                $model->addError("", $response->status['message']);
            }
        }
        $errorsArry = '';
        foreach ($model->getErrors() as $error) {
            $errorsArry .= $error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

    public function actionGroundsOthersFinish() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        Yii::$app->session->setFlash('success-message', 'Your examination exemption application has been received.');
        $url = Url::toRoute(['site/index']);
        return ['success' => ['url' => $url]];
    }

    public function actionGroundsGetInstitution($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $service = new Service();
        $institution = $service->getInstitutions($id);
        $institutions[] = "Choose Institution";
        foreach ($institution as $data) {
            $institutions[$data['id']] = $data['name'];
        }
        return ['success' => $institutions];
    }

    public function actionGroundsGetCourses($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $service = new Service();
        $courses = [];
        $exemptionCourse = $service->getKasnebCourses($id);
        foreach ($exemptionCourse as $data) {
            $courses[$data['id']] = $data['name'];
        }
        return ['success' => $courses];
    }

    public function actionGroundsGetCourseOthers($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $service = new Service();
        $course = $service->getCoursesOthers($id);
        $courses[] = "Choose Course";
        foreach ($course as $data) {
            $courses[$data['id']] = $data['name'];
        }
        return ['success' => $courses];
    }

    public function actionGroundsGetPapers($id, $codeType) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $service = new Service();
        $studentModel = new Student();
        $identity = Yii::$app->user->getIdentity();
        $studentModel->setAttributes($service->getStudentData($identity->student['id']));
        $paper = $service->getExemptionPapers($studentModel->currentCourse['id'], $id, $codeType);
        $papers = [];
        foreach ($paper as $data) {
            $papers[$data['code']] = $data['name'];
        }
        return ['success' => $papers];
    }

    public function actionGroundsBill() {
        $service = new Service();
        $bill = new Bill();
        $studentModel = new Student();
        $identity = Yii::$app->user->getIdentity();
        $studentModel->setAttributes($service->getStudentData($identity->student['id']));
        $bill->items = [];
        foreach ($studentModel->invoices as $invoice) {
            if ($invoice['feeCode']['code'] == "EXEMPTION_FEE" && $invoice['status']['status'] == "PENDING") {
                $bill->invoiceDate = $invoice['dateGenerated'];
                $bill->dueDate = $invoice['dueDate'];
                $bill->invoiceNo = $invoice['reference'];
                $bill->localAmount = $invoice['localAmount'];
                $bill->localCurrency = $invoice['localCurrency'];
                $bill->id = $invoice['id'];
                $bill->items = $invoice['invoiceDetails'];
            }
        }
        $payId = "jambopay-pay";
        $nextTab = "3";
        $feeCode = "EXEMPTION_FEE";
        return ['success' => $this->renderPartial("@common/views/_bill", ['model' => $bill, 'payId' => $payId, 'nextTabIndex' => $nextTab, 'feeCode' => $feeCode])];
    }

    public function groundsBill() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $service = new Service();
        $bill = new Bill();
        $studentModel = new Student();
        $identity = Yii::$app->user->getIdentity();
        $studentModel->setAttributes($service->getStudentData($identity->student['id']));
        $bill->items = [];
        foreach ($studentModel->invoices as $invoice) {
            if ($invoice['feeCode']['code'] == "EXEMPTION_FEE" && $invoice['status']['status'] == "PENDING") {
                $bill->invoiceDate = $invoice['dateGenerated'];
                $bill->dueDate = $invoice['dueDate'];
                $bill->invoiceNo = $invoice['reference'];
                $bill->localAmount = $invoice['localAmount'];
                $bill->localCurrency = $invoice['localCurrency'];
                $bill->id = $invoice['id'];
                $bill->items = $invoice['invoiceDetails'];
            }
        }
        $payId = "jambopay-pay";
        $nextTab = "3";
        $feeCode = "EXEMPTION_FEE";
        return ['success' => $this->renderPartial("@common/views/_bill", ['model' => $bill, 'payId' => $payId, 'nextTabIndex' => $nextTab, 'feeCode' => $feeCode])];
    }

}
