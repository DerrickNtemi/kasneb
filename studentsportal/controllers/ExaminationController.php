<?php

namespace studentsportal\controllers;

use yii\web\Controller;
use common\models\Student;
use common\models\Sitting;
use common\models\Service;
use common\models\Bill;
use common\models\Papers;
use yii\web\Response;
use yii\filters\AccessControl;
use yii\helpers\Url;
use common\models\Centers;
use Yii;

/**
 * Site controller
 */
class ExaminationController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionInfo() {
        return $this->render('@common/views/message');
    }

    public function actionIndex() {
        $service = new Service();
        $studentModel = new Student();
        $sittingModel = new Sitting();
        $identity = Yii::$app->user->getIdentity();
        $studentModel->setAttributes($service->getStudentData($identity->student['id']));
        //print_r($studentModel);exit;
        $sittingModel->studentId = $studentModel->id;
        $studentModel->studentCourses = empty($studentModel->studentCourses) ? [] : $studentModel->studentCourses;
        $paperModel = new Papers();

        $papers = [];
        $courseId = 0;
        if (empty($studentModel->currentCourse)) {
            \Yii::$app->getSession()->setFlash('error-message', "Student does not have an active course at the moment");
            $this->redirect(['info']);
            return;
        }
        foreach ($studentModel->studentCourses as $courses) {
            if ($courses['courseStatus'] == "REJECTED" && ($courses['remarks'] == "REJ_2" || $courses['remarks'] == "REJ_4")) {
                \Yii::$app->getSession()->setFlash('error-message', "");
                $this->redirect(['registration/update-documents', 'courseTypeCode' => $courses['course']['courseTypeCode'], 'courseId' => $courses['id'], 'studentId' => $identity->student['id']]);
                return;
            }
            if ($courses['courseStatus'] == "PENDING") {
                \Yii::$app->getSession()->setFlash('error-message', "Student course awaiting verification");
                $this->redirect(['info']);
                return;
            }
            if ($courses['courseStatus'] == "PENDING_IDENTIFICATION" || $courses['courseStatus'] == "FAILED_IDENTIFICATION") {
                \Yii::$app->getSession()->setFlash('error-message', "Student awaiting identification verification");
                $this->redirect(['info']);
                return;
            }
        }
        $courseTypeCode = "";
        foreach ($studentModel->studentCourses as $courses) {
            if ($courses['active'] == 1) {
                $courseTypeCode = $courses['course']['courseTypeCode'];
                $courseId = $courses['id'];
                if (!empty($courses['studentCourseSittings'])) {
                    //print_r($courses['studentCourseSittings']);exit;
                    foreach ($courses['studentCourseSittings'] as $sit) {
                        //print_r($sit);exit;
                        if ($sit['status'] == "PENDING") {
                            //print_r($sit);exit;
                            $sittingModel->month = $sit['sitting']['id'];
                            $sittingModel->year = $sit['sitting']['sittingYear'];
                        }
                    }
                }
                $sittingModel->registrationNumber = $courses['registrationNumber'];
                $sittingModel->courseId = $courses['course']['id'];
            }
        }
        $papersData = $service->getEligiblePapers($courseId);
        //print_r($papersData);exit;
        $sittingArray = [];
        $sittings = $service->getExaminationSittings();
        foreach ($sittings as $sitting) {
            $sittingArray[$sitting['id']] = $sitting['sittingPeriod']."-".$sitting['sittingYear'];
        }

        $centers = new Centers();
        $zoneData = $service->getZones($identity->student['id']);
        $zones = $centersArray = [];
        $centers->zone = empty($centers->zone) ? "D047" : $centers->zone;
        foreach ($zoneData as $dt) {
            $zones[$dt['id']] = $dt['name'];
        }
        $sittingCenters = $service->getCenters($centers->zone, $identity->student['id']);
        foreach ($sittingCenters as $center) {
            $centersArray[$center['code']] = $center['name'];
        }
        return $this->render('index', [
                    'sittingModel' => $sittingModel,
                    'sittingArray' => $sittingArray,
                    'papersData' => $papersData,
                    'paperModel' => $paperModel,
                    'centerModel' => $centers,
                    'centersArray' => $centersArray,
                    'courseTypeCode' => $courseTypeCode,
                    'zones' => $zones,
        ]);
    }

    public function actionExamCenterFilter($zone) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $service = new Service();
        $identity = Yii::$app->user->getIdentity();
        $centersArray = [];

        $sittingCenters = $service->getCenters($zone, $identity->student['id']);
        foreach ($sittingCenters as $center) {
            $centersArray[$center['code']] = $center['name'];
        }
        return ['success' => $centersArray];
    }

    public function actionCreateSittingGetYear($month) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $service = new Service();
        $sittings = $service->getExaminationSittings();
        $sittingYear = "";
        foreach ($sittings as $sitting) {
            if ($sitting['id'] == $month) {
                $sittingYear = $sitting['sittingYear'];
            }
        }
        return ['success' => $sittingYear];
    }

    public function actionCreateSitting() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $service = new Service();
        $model = new Sitting();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $studentModel = new Student();
            $studentModel->setAttributes($service->getStudentData($model->studentId));
            //print_r($studentModel);exit;
            $studentCourseId = $id = 0;
            foreach ($studentModel->studentCourses as $courses) {
                if ($courses['active'] == 1) {
                    $studentCourseId = $courses['id'];
                    if (!empty($courses['studentCourseSittings'])) {
                        //print_r($courses['studentCourseSittings']);exit;
                        foreach ($courses['studentCourseSittings'] as $sit) {
                            $id = $sit['id'];
                        }
                    }
                }
            }


            $data = [
                'id' => $id,
                "sitting" => [
                    'id' => $model->month,
                ],
                "studentCourse" => [
                    'id' => $studentCourseId,
                ],
                'sittingCentre' => []
            ];
            $response = (object) $service->createSitting($data, $id);
            if ($response->status['code'] == 1) {
                return ['success' => 'success'];
            } else {
                $model->addError("", $response->status['message']);
            }
        }
        $errorsArry = '';
        foreach ($model->getErrors() as $error) {
            $errorsArry .= $error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

    public function actionCreateSittingPaper() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $lower = $_POST['upper'];
        $service = new Service();
        $identity = Yii::$app->user->getIdentity();
        $studentModel = new Student();
        $studentModel->setAttributes($service->getStudentData($identity->student['id']));
        $sittingId = $courseId = 0;
        $courseTypeCode = "";
        foreach ($studentModel->studentCourses as $courses) {
            if ($courses['active'] == 1) {
                $courseId = $courses['id'];
                $courseTypeCode = $courses['course']['courseTypeCode'];
                if (!empty($courses['studentCourseSittings'])) {
                    foreach ($courses['studentCourseSittings'] as $sit) {
                        if ($sit['status'] == "PENDING") {
                            $sittingId = $sit['id'];
                        }
                    }
                }
            }
        }

        $papersData = $service->getEligiblePapers($courseId);
        $papers = [];
        if ($courseTypeCode == 100) {

            foreach ($papersData['eligiblePart']['sections'] as $sections) {
                if (empty($sections['optional'])) {
                    foreach ($sections['papers'] as $paperDt1) {
                        $papers[] = ['paperCode' => $paperDt1['code']];
                    }
                }
                if (!empty($sections['optional']) && !empty($lower)) {
                    foreach ($sections['papers'] as $paperDt1) {
                        $papers[] = ['paperCode' => $paperDt1['code']];
                    }
                }
            }
        } else {

            foreach ($papersData['eligibleLevels'] as $level) {
                if (empty($level['optional'])) {
                    foreach ($level['papers'] as $paperDt1) {
                        $papers[] = ['paperCode' => $paperDt1['code']];
                    }
                }
                if (!empty($level['optional']) && !empty($lower)) {
                    foreach ($level['papers'] as $paperDt1) {
                        $papers[] = ['paperCode' => $paperDt1['code']];
                    }
                }
            }
        }
        $data = [
            'id' => $sittingId,
            'papers' => $papers,
        ];
        $response = (object) $service->createSitting($data, $sittingId);
        if ($response->status['code'] == 1) {
            $bill = new Bill();
            $studentModel = new Student();
            $identity = Yii::$app->user->getIdentity();
            $studentModel->setAttributes($service->getStudentData($identity->student['id']));
            $bill->items = [];
            foreach ($studentModel->invoices as $invoice) {
                if ($invoice['feeCode']['code'] == "EXAM_ENTRY_FEE" && $invoice['status']['status'] == "PENDING") {
                    $bill->invoiceDate = $invoice['dateGenerated'];
                    $bill->dueDate = $invoice['dueDate'];
                    $bill->invoiceNo = $invoice['reference'];
                    $bill->localAmount = $invoice['localAmount'];
                    $bill->localCurrency = $invoice['localCurrency'];
                    $bill->id = $invoice['id'];
                    $bill->items = $invoice['invoiceDetails'];
                }
            }
            $payId = "examination-pay";
            $nextTab = "3";
            $feeCode = "EXAM_ENTRY_FEE";
            return ['success' => $this->renderPartial("@common/views/_bill", ['model' => $bill, 'payId' => $payId, 'nextTabIndex' => $nextTab, 'feeCode' => $feeCode])];
        } else {
            $studentModel->addError("", $response->status['message']);
        }
        $errorsArry = '';
        foreach ($studentModel->getErrors() as $error) {
            $errorsArry .= $error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

    public function actionExamCenter() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new Centers();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $service = new Service();
            $identity = Yii::$app->user->getIdentity();
            $studentModel = new Student();
            $studentModel->setAttributes($service->getStudentData($identity->student['id']));
            $sittingId = 0;
            foreach ($studentModel->studentCourses as $courses) {
                if ($courses['active'] == 1) {
                    if (!empty($courses['studentCourseSittings'])) {
                        foreach ($courses['studentCourseSittings'] as $sit) {
                            if ($sit['status'] == "PAID") {
                                $sittingId = $sit['id'];
                            }
                        }
                    }
                }
            }
            $sitting = ['code' => $model->examCenter];
            $data = [
                'id' => $sittingId,
                'sittingCentre' => $sitting,
            ];
            $response = (object) $service->createCenter($data);
            if ($response->status['code'] == 1) {
                Yii::$app->session->setFlash('success-message', 'Examination booking was successful. Please check your email to access the timetable');
                $url = Url::toRoute(['site/index']);
                return ['success' => ['url' => $url]];
            } else {
                $model->addError("", $response->status['message']);
            }
        }
        $errorsArry = '';
        foreach ($model->getErrors() as $error) {
            $errorsArry .= $error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

    //examination center selection
    public function actionCenterBooking() {
        $service = new Service();
        $studentModel = new Student();
        $sittingModel = new Sitting();
        $identity = Yii::$app->user->getIdentity();
        $studentModel->setAttributes($service->getStudentData($identity->student['id']));
        //print_r($studentModel);exit;
        $sittingModel->studentId = $studentModel->id;
        $studentModel->studentCourses = empty($studentModel->studentCourses) ? [] : $studentModel->studentCourses;
        $paperModel = new Papers();
        $papers = [];
        $courseId = 0;
        if (empty($studentModel->currentCourse)) {
            \Yii::$app->getSession()->setFlash('error-message', "Student does not have an active course at the moment");
            $this->redirect(['info']);
            return;
        }
        $courseTypeCode = "";
        foreach ($studentModel->studentCourses as $courses) {
            if ($courses['active'] == 1) {
                $courseTypeCode = $courses['course']['courseTypeCode'];
                $courseId = $courses['id'];
                if (!empty($courses['studentCourseSittings'])) {
                    foreach ($courses['studentCourseSittings'] as $sit) {
                        if (!$sit['hasBooking']) {
                            \Yii::$app->getSession()->setFlash('error-message', "Student does not have an pending examination  center booking");
                            $this->redirect(['info']);
                            return;
                        }
                    }
                } else {
                    \Yii::$app->getSession()->setFlash('error-message', "Student does not have an pending examination  center booking");
                    $this->redirect(['info']);
                    return;
                }
                $sittingModel->registrationNumber = $courses['registrationNumber'];
                $sittingModel->courseId = $courses['course']['id'];
            }
        }
        $papersData = $service->getEligiblePapers($courseId);
        $sittingArray = [];
        $sittings = $service->getExaminationSittings();
        foreach ($sittings as $sitting) {
            $sittingArray[$sitting['id']] = $sitting['sittingPeriod'];
        }

        $centers = new Centers();
        $zoneData = $service->getZones($identity->student['id']);
        $zones = $centersArray = [];
        $centers->zone = empty($centers->zone) ? "D047" : $centers->zone;
        foreach ($zoneData as $dt) {
            $zones[$dt['id']] = $dt['name'];
        }
        $sittingCenters = $service->getCenters($centers->zone, $identity->student['id']);
        foreach ($sittingCenters as $center) {
            $centersArray[$center['code']] = $center['name'];
        }
        return $this->render('center-booking', [
                    'centerModel' => $centers,
                    'centersArray' => $centersArray,
                    'courseTypeCode' => $courseTypeCode,
                    'zones' => $zones,
        ]);
    }

}
