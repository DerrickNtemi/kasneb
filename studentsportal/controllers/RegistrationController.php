<?php

namespace studentsportal\controllers;

use yii\web\Controller;
use common\models\Student;
use common\models\Service;
use common\models\Contact;
use common\models\Course;
use common\models\Requirements;
use common\models\Declarations;
use common\models\Bill;
use yii\web\Response;
use common\models\CourseApplication;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\helpers\Url;
use common\models\SimpleImage;
use common\models\CourseDocuments;
use Yii;

/**
 * Site controller
 */
class RegistrationController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionInfo() {
        return $this->render('@common/views/message');
    }

    public function actionIndex() {
        $service = new Service();
        $studentModel = new Student();
        $identity = Yii::$app->user->getIdentity();
        $studentModel->setAttributes($service->getStudentData($identity->student['id']));
        $studentModel->studentCourses = empty($studentModel->studentCourses) ? [] : $studentModel->studentCourses;
        $contact = new Contact();
        $contact->setAttributes($identity->student['contact']);
        $studentModel->nationality = empty($studentModel->nationality) ? 1 : $studentModel->nationality;
        $contact->countryId = empty($contact->countryId) ? 1 : $contact->countryId['code'];
        $courseApplication = new CourseApplication();
        $declarationModel = new Declarations();
        foreach ($studentModel->studentCourses as $courses) {
            if ($courses['courseStatus'] == "REJECTED" && ($courses['remarks'] == "REJ_2" || $courses['remarks'] == "REJ_4")) {
                \Yii::$app->getSession()->setFlash('error-message', "");
                $this->redirect(['update-documents', 'courseTypeCode' => $courses['course']['courseTypeCode'], 'courseId' => $courses['id'], 'studentId' => $identity->student['id']]);
                return;
            }
            if ($courses['courseStatus'] == "ACTIVE") {
                \Yii::$app->getSession()->setFlash('error-message', "Student has an active course at the moment");
                $this->redirect(['info']);
                return;
            }
            if ($courses['courseStatus'] == "PENDING") {
                \Yii::$app->getSession()->setFlash('error-message', "Student course awaiting verification");
                $this->redirect(['info']);
                return;
            }
            if ($courses['courseStatus'] == "PENDING_IDENTIFICATION" || $courses['courseStatus'] == "FAILED_IDENTIFICATION") {
                \Yii::$app->getSession()->setFlash('error-message', "Student awaiting identification verification");
                $this->redirect(['info']);
                return;
            }
            $data = [];
            for ($j = 0; $j < count($courses['studentRequirements']); $j++) {
                $data[] = $courses['studentRequirements'][$j]['id'];
            }
            if (!$courses['verified']) {
                $courseApplication->studentId = $studentModel->id;
                $courseDetails = $service->getCourse($courses['id']);
                if ($courseDetails['courseTypeCode'] == 100) {
                    $courseApplication->profCourseId = $courses['id'];
                    $courseApplication->profDocument = $courses['documents'];
                    $courseApplication->profReq = $data;
                } else {
                    $courseApplication->dipCourseId = $courses['id'];
                    $courseApplication->dipDocument = $courses['documents'];
                    $courseApplication->dipReq = $data;
                }
            }
        }
        $country = $service->getCountries();
        $county = $service->getCounties();
        $requirements = $service->getCourseType();
        $declarationData = $service->getDeclarations();
        $diploma = $professional = $countries = $nationalities = $counties = $diplomaRequirements = $professionalRequirements = [];

        foreach ($requirements as $requirement) {
            foreach ($requirement['courseCollection'] as $course) {
                $courseModel = new Course();
                $courseModel->setAttributes($course);
                if ($requirement['code'] == 100) {
                    $professional[$courseModel->id] = $courseModel->name;
                }
                if ($requirement['code'] == 200) {
                    $diploma[$courseModel->id] = $courseModel->name;
                }
            }
            foreach ($requirement['courseRequirements'] as $req) {
                $requirementModel = new Requirements();
                $requirementModel->setAttributes($req);
                if ($requirement['code'] == 100) {
                    $professionalRequirements[] = ["id" => $requirementModel->id, "description" => $requirementModel->description, 'institutionType' => $req['institutionType'], 'documentName' => $requirementModel->documentName, 'type' => $requirementModel['type']];
                }
                if ($requirement['code'] == 200) {
                    $diplomaRequirements[] = ["id" => $requirementModel->id, "description" => $requirementModel->description, 'institutionType' => $req['institutionType'], 'documentName' => $requirementModel->documentName, 'type' => $requirementModel['type']];
                }
            }
        }

        foreach ($country as $cntry) {
            $countries[$cntry['code']] = $cntry['name'];
            $nationalities[$cntry['code']] = $cntry['nationality'];
        }
        foreach ($county as $cty) {
            $counties[$cty['id']] = $cty['name'];
        }
        $sittingArray = [];
        $sittings = $service->getExaminationSittings();
        foreach ($sittings as $sitting) {
            $sittingArray[$sitting['id']] = $sitting['sittingPeriod'] . "-" . $sitting['sittingYear'];
        }

        return $this->render('index', [
                    'declarationModel' => $declarationModel,
                    'declarationData' => $declarationData,
                    'diplomaRequirements' => $diplomaRequirements,
                    'professionalRequirements' => $professionalRequirements,
                    'courseApplication' => $courseApplication,
                    'diploma' => $diploma,
                    'professional' => $professional,
                    'contact' => $contact,
                    'studentModel' => $studentModel,
                    'countries' => $countries,
                    'counties' => $counties,
                    'nationalities' => $nationalities,
                    'sittingArray' => $sittingArray,
        ]);
    }

    public function actionUpdateProfile() {
        $service = new Service();
        $studentModel = new Student();
        $identity = Yii::$app->user->getIdentity();
        $studentModel->setAttributes($service->getStudentData($identity->student['id']));
        $studentModel->studentCourses = empty($studentModel->studentCourses) ? [] : $studentModel->studentCourses;
        $contact = new Contact();
        $contact->setAttributes($identity->student['contact']);
        $studentModel->nationality = empty($studentModel->nationality) ? 1 : $studentModel->nationality;
        $contact->countryId = empty($contact->countryId) ? 1 : $contact->countryId['code'];
        $country = $service->getCountries();
        $county = $service->getCounties();

        foreach ($country as $cntry) {
            $countries[$cntry['code']] = $cntry['name'];
            $nationalities[$cntry['code']] = $cntry['nationality'];
        }
        foreach ($county as $cty) {
            $counties[$cty['id']] = $cty['name'];
        }

        $updateNationalIdField = true;
        if (empty($studentModel->documentNo) || empty($studentModel->documentScan)) {
            $updateNationalIdField = false;
        }
        foreach ($studentModel->studentCourses as $courses) {
            if ($courses['courseStatus'] == "PENDING_IDENTIFICATION" || $courses['courseStatus'] == "FAILED_IDENTIFICATION") {
                $updateNationalIdField = false;
            }
        }

        return $this->render('update', [
                    'contact' => $contact,
                    'studentModel' => $studentModel,
                    'countries' => $countries,
                    'counties' => $counties,
                    'nationalities' => $nationalities,
                    'updateNationalIdField' => $updateNationalIdField,
        ]);
    }

    public function actionUpdateDocuments($courseTypeCode = "", $courseId = "", $studentId = "") {
        $service = new Service();
        $model = new CourseDocuments();
        $model->studentId = $studentId;
        $model->courseTypeCode = $courseTypeCode;
        $model->courseId = $courseId;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $documents = $documentPath = [];
            $i = 0;
            foreach ($_FILES['CourseDocuments']['type']['document'] as $type) {
                $lastIndex = strripos($type, "/");
                $extension = substr($type, $lastIndex + 1, strlen($type));
                $name = $model->studentId . "_" . $model->courseId . "_" . $i . '.' . $extension;
                $path = Yii::getAlias('@webroot') . '/uploads/students/qualifications/' . $name;
                $documentPath[$i] = $path;
                $documents[] = ['name' => $name];
                $i++;
            }
            $j = 0;
            foreach ($_FILES['CourseDocuments']['tmp_name']['document'] as $temp) {
                move_uploaded_file($temp, $documentPath[$j]);
                $j++;
            }
            $data = [
                'id' => $model->courseId,
                'documents' => $documents
            ];
            $response = (object) $service->updateDocuments($data, $model->courseId);
            if ($response->status['code'] == 1) {
                $url = Url::toRoute(['site/index']);
                \Yii::$app->getSession()->setFlash('success-message', "Documents successfully uploaded. Kindly await verification");
                return $this->redirect($url);
            } else {
                $model->addError("", $response->status['message']);
            }
        }
        return $this->render('updatedocument', [
                    'courseDocuments' => $model
        ]);
    }

    public function actionCreateProfile() {
        $identity = Yii::$app->user->getIdentity();
        Yii::$app->response->format = Response::FORMAT_JSON;
        $service = new Service();
        $model = new Student(['scenario' => 'REGISTRATION']);
        $studentModel = new Student();
        $model->load(Yii::$app->request->post());
        $documentScan = UploadedFile::getInstance($model, 'documentScan');
        if ($documentScan) {
            $name = str_ireplace("/", "", $model->documentNo) . '.' . $documentScan->extension;
            $path = Yii::getAlias('@webroot') . '/uploads/students/documents/' . $name;
            $documentScan->saveAs($path);
            $model->documentScan = $name;
        }

        $passportPhotoScan = UploadedFile::getInstance($model, 'passportPhoto');
        if ($passportPhotoScan) {
            $name = str_ireplace("/", "", $model->documentNo) . '_passport.' . $passportPhotoScan->extension;
            $path = Yii::getAlias('@webroot') . '/uploads/students/documents/' . $name;
            $image = new SimpleImage();
            $image->load($_FILES['Student']['tmp_name']['passportPhoto']);
            $image->resize(150, 150);
            $image->save($path);
            $model->passportPhoto = $name;
        }

        $studentData = $service->getStudentData($model->id);
        $studentModel->setAttributes($studentData);
        $model->firstName = $studentModel->firstName;
        $model->email = $studentModel->email;
        $model->phoneNumber = $studentModel->phoneNumber;
        $model->lastName = $studentModel->lastName;
        $model->middleName = $studentModel->middleName;

        if (empty($model->documentScan)) {
            $model->documentScan = $studentModel->documentScan;
        }
        if (empty($model->passportPhoto)) {
            $model->passportPhoto = $studentModel->passportPhoto;
        }
        if ($model->validate()) {
            $model->countryId = empty($model->countryId) ? $identity->student['countryId']['code'] : $model->countryId;
            $model->loginId = [];
            $model->nationality = ['code' => $model->nationality];
            $model->countryId = ['code' => $model->countryId];
            $model->documentType = ['id' => $model->documentType];
            $response = (object) $service->createStudent($model);
            if ($response->status['code'] == 1) {
                return ['success' => 'success'];
            } else {
                $model->addError("", $response->status['message']);
            }
        }
        $errorsArry = '';
        foreach ($model->getErrors() as $error) {
            $errorsArry .= $error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

    public function actionContactDetails() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $service = new Service();
        $model = new Contact();
        $model->load(Yii::$app->request->post());
        if ($model->validate()) {
            $identity = Yii::$app->user->getIdentity();
            $studentData = $service->getStudentData($identity->student['id']);
            $model->countryId = ['code' => $model->countryId];
            $model->countyId = ['id' => $model->countyId];
            $studentData['contact'] = $model;
            $studentData['countryId'] = ['code' => $identity->student['countryId']['code']];
            $student = new Student(['scenario' => 'REGISTRATION']);
            $student->setAttributes($studentData);
            $response = (object) $service->createStudent($student);
            if ($response->status['code'] == 1) {
                $url = Url::toRoute(['site/index']);
                \Yii::$app->getSession()->setFlash('success-message', "Profile successfully updated");
                return ['success' => ['url' => $url]];
            } else {
                $model->addError("", $response->status['message']);
            }
        }
        $errorsArry = '';
        foreach ($model->getErrors() as $error) {
            $errorsArry .= $error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

    public function actionUpdateProfileData() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $identity = Yii::$app->user->getIdentity();
        $service = new Service();
        $studentData = $service->getStudentData($identity->student['id']);
        $studentModel = new Student(['scenario' => 'REGISTRATION']);
        $studentModel->setAttributes($studentData);
        $model = new Student(['scenario' => 'REGISTRATION']);
        $model->load(Yii::$app->request->post());
        $contact = new Contact();
        $contact->load(Yii::$app->request->post());
        $contact->countryId = ['code' => $contact->countryId];
        $contact->countyId = ['id' => $contact->countyId];
        $documentScan = UploadedFile::getInstance($model, 'documentScan');
        if ($documentScan) {
            $name = str_ireplace("/", "", $model->documentNo) . '.' . $documentScan->extension;
            $path = Yii::getAlias('@webroot') . '/uploads/students/documents/' . $name;
            $documentScan->saveAs($path);
            $model->documentScan = $name;
        }

        $passportScan = UploadedFile::getInstance($model, 'passportPhoto');
        if ($passportScan) {
            $name = str_ireplace("/", "", $model->documentNo) . '_passport.' . $passportScan->extension;
            $path = Yii::getAlias('@webroot') . '/uploads/students/documents/' . $name;
            $image = new SimpleImage();
            $image->load($_FILES['Student']['tmp_name']['passportPhoto']);
            $image->resize(150, 150);
            $image->save($path);
            $model->passportPhoto = $name;
        }
        $model->firstName = $studentModel->firstName;
        $model->email = $studentModel->email;
        $model->lastName = $studentModel->lastName;
        $model->middleName = $studentModel->middleName;
        $model->gender = $studentModel->gender;
        $model->dob = $studentModel->dob;
        $model->nationality = $contact->countryId;
        $model->documentType = ['id' => $model->documentType];
        $model->contact = $contact;
        //print_r($model->contact);exit;
        if (empty($model->documentScan)) {
            $model->documentScan = $studentModel->documentScan;
        }
        if (empty($model->passportPhoto)) {
            $model->passportPhoto = $studentModel->passportPhoto;
        }
        if ($model->validate()) {
            $response = (object) $service->createStudent($model);
            if ($response->status['code'] == 1) {
                $_SESSION['update'] = "";
                $url = Url::toRoute(['site/index']);
                \Yii::$app->getSession()->setFlash('success-message', "Profile successfully updated");
                //return ['success' => ['url' => $url]];
                return $this->redirect($url);
            } else {
                $model->addError("", $response->status['message']);
            }
        }
        $errorsArry = '';
        foreach ($model->getErrors() as $error) {
            $errorsArry .= $error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

    public function actionCourseApplication($courseType) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $service = new Service();
        $studentModel = new Student();
        $identity = Yii::$app->user->getIdentity();
        $studentModel->setAttributes($service->getStudentData($identity->student['id']));
        $courseId = null;
        if (!empty($studentModel->studentCourses)) {
            foreach ($studentModel->studentCourses as $courses) {
                if (!$courses['verified']) {
                    $courseDetails = $service->getCourse($courses['id']);
                    if ($courseDetails['courseTypeCode'] == 100) {
                        $courseId = $courses['id'];
                    } else {
                        $courseId = $courses['id'];
                    }
                }
            }
        }


        $scenario = $courseType == 100 ? "PROFESSIONAL" : "DIPLOMA";
        $model = new CourseApplication(['scenario' => $scenario]);
        $model->load(Yii::$app->request->post());
        $rawItems = explode(",", $model->documentNo);
        $docNo = $req = [];
        if (count($rawItems) > 1) {
            for ($i = 0; $i < count($rawItems); $i += 2) {
                $req[] = [$rawItems[$i]];
                $docNo[] = [$rawItems[$i + 1]];
            }
        }
        $model->documentNo = $docNo;
        if ($courseType == 100) {
            $model->profReq = $req;
        } else {
            $model->dipReq = $req;
        }

        $model->studentId = $identity->student['id'];
        if ($courseType == 100) {
            $documents = $documentPath = [];
            $i = 0;
            foreach ($_FILES['CourseApplication']['type']['profDocument'] as $type) {
                $lastIndex = strripos($type, "/");
                $extension = substr($type, $lastIndex + 1, strlen($type));
                $name = $model->studentId . "_" . $model->profCourseId . "_" . $i . '.' . $extension;
                $path = Yii::getAlias('@webroot') . '/uploads/students/qualifications/' . $name;
                $documentPath[$i] = $path;
                $documents[] = ['name' => $name];
                $i++;
            }
            $j = 0;
            foreach ($_FILES['CourseApplication']['tmp_name']['profDocument'] as $temp) {
                move_uploaded_file($temp, $documentPath[$j]);
                $j++;
            }
            $model->profDocument = $documents;
        } else {
            $documents = $documentPath = [];
            $i = 0;
            foreach ($_FILES['CourseApplication']['type']['dipDocument'] as $type) {
                $lastIndex = strripos($type, "/");
                $extension = substr($type, $lastIndex + 1, strlen($type));
                $name = $model->studentId . "_" . $model->dipCourseId . "_" . $i . '.' . $extension;
                $path = Yii::getAlias('@webroot') . '/uploads/students/qualifications/' . $name;
                $documentPath[$i] = $path;
                $documents[] = ['name' => $name];
                $i++;
            }
            $j = 0;
            foreach ($_FILES['CourseApplication']['tmp_name']['dipDocument'] as $temp) {
                move_uploaded_file($temp, $documentPath[$j]);
                $j++;
            }
            $model->dipDocument = $documents;
        }
        if ($model->validate()) {
            if ($courseType == 100) {
                $qualifications = [];
                $qual = $model->profReq;
                for ($i = 0; $i < count($qual); $i++) {
                    $qualifications[] = ['id' => $qual[$i]];
                }
                $data = [
                    'id' => $courseId,
                    'student' => ['id' => $model->studentId],
                    'course' => ['id' => $model->profCourseId],
                    'documents' => $model->profDocument,
                    'studentRequirements' => $qualifications
                ];
            } else {
                $qualifications = [];
                $qual = $model->dipReq;
                for ($i = 0; $i < count($qual); $i++) {
                    $qualifications[] = ['id' => $qual[$i]];
                }
                $data = [
                    'id' => $courseId,
                    'student' => ['id' => $model->studentId],
                    'course' => ['id' => $model->dipCourseId],
                    'documents' => $model->dipDocument,
                    'studentRequirements' => $qualifications
                ];
            }
            $response = (object) $service->createCourse($data, $courseId);
            if ($response->status['code'] == 1) {
                return ['success' => 'success'];
            } else {
                $model->addError("", $response->status['message']);
            }
        }
        $errorsArry = '';
        foreach ($model->getErrors() as $error) {
            $errorsArry .= $error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

    public function actionMoreDetails() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $identity = Yii::$app->user->getIdentity();
        $service = new Service();
        $model = new Declarations();
        $declarationData = $service->getDeclarations();
        $studentModel = new Student();
        $studentModel->setAttributes($service->getStudentData($identity->student['id']));
        //print_r($studentModel); exit;
        $studentModel->studentCourses = empty($studentModel->studentCourses) ? [] : $studentModel->studentCourses;
        $studentId = $courseId = $id = 0;
        $document = "";
        $qualifications = [];
        //print_r($studentModel->studentCourses);exit;
        foreach ($studentModel->studentCourses as $courses) {
            if (!$courses['verified']) {
                $studentId = $studentModel->id;
                $document = $courses['documents'];
                $id = $courses['id'];
                $courseId = $courses['course']['id'];
                foreach ($courses['studentRequirements'] as $qualification) {
                    $qualifications[] = ['id' => $qualification['id']];
                }
            }
        }
        $declarationData1 = [];
        foreach ($declarationData as $declaration) {
            $declarationString = "declarationId" . $declaration['id'];
            $declarationAnswerString = "declaration" . $declaration['id'];
            $declarationSpecify = $_POST[$declarationAnswerString];
            if ($declaration['description'] != "How did you learn about Kasneb?") {
                $declarationAnswer1 = substr($_POST[$declarationString], 0, 1);
                $declarationAnswer = substr($_POST[$declarationString], 1);
            } else {
                $declarationAnswer1 = "Y";
                $declarationAnswer = $declaration['id'];
                if ($_POST[$declarationString] != "Others") {
                    $declarationSpecify = $_POST[$declarationString];
                }
            }
            $declarationData1[] = [
                'studentDeclarationPK' => [
                    'declarationId' => $declarationAnswer,
                    'studentCourseId' => $id,
                ],
                'response' => $declarationAnswer1 == "Y" ? true : false,
                'specification' => $declarationSpecify,
            ];
        }
        $dataToSend = [
            'id' => $id,
            'studentRequirements' => $qualifications,
            'studentCourseDeclarations' => $declarationData1,
            'documents' => $document,
            'firstSitting' => [
                'id' => $_POST['Declarations']['month']
            ]
        ];
        $response = (object) $service->createDeclarations($dataToSend);
        if ($response->status['code'] == 1) {
            $bill = new Bill();
            $studentModel = new Student();
            $identity = Yii::$app->user->getIdentity();
            $studentModel->setAttributes($service->getStudentData($identity->student['id']));
            $bill->items = [];
            foreach ($studentModel->invoices as $invoice) {
                if ($invoice['feeCode']['code'] == "REGISTRATION_FEE" && $invoice['status']['status'] == "PENDING") {
                    $bill->invoiceDate = $invoice['dateGenerated'];
                    $bill->dueDate = $invoice['dueDate'];
                    $bill->invoiceNo = $invoice['reference'];
                    $bill->localAmount = $invoice['localAmount'];
                    $bill->localCurrency = $invoice['localCurrency'];
                    $bill->id = $invoice['id'];
                    $bill->items = $invoice['invoiceDetails'];
                }
            }
            $payId = "jambopay-pay";
            $nextTab = "3";
            $feeCode = "REGISTRATION_FEE";
            return ['success' => $this->renderPartial("@common/views/_bill", ['model' => $bill, 'payId' => $payId, 'nextTabIndex' => $nextTab, 'feeCode' => $feeCode])];
        } else {
            $model->addError("", $response->status['message']);
        }
        $errorsArry = '';
        foreach ($model->getErrors() as $error) {
            $errorsArry .= $error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

    public function actionProfile() {
        return $this->render('profile');
    }

    public function actionMoreDetailsGetYear($month) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $service = new Service();
        $sittings = $service->getExaminationSittings();
        $sittingYear = "";
        foreach ($sittings as $sitting) {
            if ($sitting['id'] == $month) {
                $sittingYear = $sitting['sittingYear'];
            }
        }
        return ['success' => $sittingYear];
    }

}
