<?php

namespace studentsportal\controllers;

use common\models\Bill;
use common\models\Service;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\helpers\Html;
use common\models\Student;

/**
 * Site controller
 */
class RenewalController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionInfo() {
        return $this->render('@common/views/message');
    }

    public function actionIndex() {
        $service = new Service();
        $identity = Yii::$app->user->getIdentity();
        $studentModel = new Student();
        $studentModel->setAttributes($service->getStudentData($identity->student['id']));
        $studentModel->studentCourses = empty($studentModel->studentCourses) ? [] : $studentModel->studentCourses;
        foreach ($studentModel->studentCourses as $courses) {
            if ($courses['courseStatus'] == "REJECTED" && ($courses['remarks'] == "REJ_2" || $courses['remarks'] == "REJ_4")) {
                \Yii::$app->getSession()->setFlash('error-message', "");
                $this->redirect(['registration/update-documents', 'courseTypeCode' => $courses['course']['courseTypeCode'], 'courseId' => $courses['id'], 'studentId' => $identity->student['id']]);
                return;
            }
            if ($courses['courseStatus'] == "PENDING") {
                \Yii::$app->getSession()->setFlash('error-message', "Student course awaiting verification");
                $this->redirect(['info']);
                return;
            }
            if ($courses['courseStatus'] == "PENDING_IDENTIFICATION" || $courses['courseStatus'] == "FAILED_IDENTIFICATION") {
                \Yii::$app->getSession()->setFlash('error-message', "Student awaiting identification verification");
                $this->redirect(['info']);
                return;
            }
        }
        $invoice = $service->getInvoices($identity->student['id']);
        if (isset($invoice['status']['code']) && $invoice['status']['code'] != 1) {
            $backUrl = Html::a("Back", ['/site/index'], ['class' => 'btn btn-small btn-danger']);
            \Yii::$app->getSession()->setFlash('error-message', "Your current subscription is up to date.");
            $this->redirect(['info']);
            return;
        }
        $bill = new Bill();
        $bill->items = [];
        $bill->invoiceDate = $invoice['dateGenerated'];
        $bill->dueDate = $invoice['dueDate'];
        $bill->invoiceNo = $invoice['reference'];
        $bill->localAmount = $invoice['localAmount'];
        $bill->localCurrency = $invoice['localCurrency'];
        $bill->id = $invoice['id'];
        $bill->items = $invoice['invoiceDetails'];

        $payId = "jambopay-pay";
        $nextTab = "3";
        $feeCode = "REGISTRATION_RENEWAL_FEE";
        return $this->render("@common/views/_renewal", ['model' => $bill, 'payId' => $payId, 'nextTabIndex' => $nextTab, 'feeCode' => $feeCode]);
    }

}
