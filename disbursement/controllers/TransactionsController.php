<?php

namespace disbursement\controllers;

use common\models\Service;
use common\models\DisbursementTransaction;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\helpers\Url;
use Yii;

/**
 * Site controller
 */
class TransactionsController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex() {
        $service = new Service();
        $transactions = $service->getDisbursementTransaction();
        $transactionArray = [];
        foreach($transactions as $transaction){
            $disbursementModel = new DisbursementTransaction();
            $disbursementModel->setAttributes($transaction);
            $disbursementModel->createdAt = $service->timestampToSting1($disbursementModel->createdAt);
            $transactionArray[] = $disbursementModel;
        }
        return $this->render('index', [ 'transactions' => $transactionArray]);
    }
}
