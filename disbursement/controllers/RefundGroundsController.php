<?php

namespace disbursement\controllers;

use common\models\CourseDisplay;
use common\models\Service;
use common\models\RefundReasons;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;


class RefundGroundsController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex() {
        $service = new Service();
        $refundReasonsModel = new RefundReasons();
        $reasons = $service->getRefundReasons(1);
        $refundReasons = [];
        foreach ($reasons as $reason) {
            $reasonsModel = new RefundReasons();
            $reasonsModel->setAttributes($reason);
            $refundReasons[] = $reasonsModel;
        }
        return $this->render('index', [ 'model' => $refundReasonsModel, 'datas' => $refundReasons]);
    }
    
    public function actionCreate() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new RefundReasons();
        $service = new Service();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $response = (Object) $service->createRefundGround($model);
            if ($response->status['code'] == 100) {
                $url = Url::toRoute(['index']);
                \Yii::$app->getSession()->setFlash('success-message', "Refund ground successfully added.");
                return ['success' => $url];
            } else {
                $model->addError("", "Error Code " . $response->status['code'] . " :: " . $response->status['message']);
            }
        }
        $errorsArry = '';
        foreach ($model->getErrors() as $error) {
            $errorsArry.= $error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

    public function actionDelete($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new RefundReasons();
        $service = new Service();
        $password = $_POST['password'];
        if (!empty($password)) {
            $response = (Object) $service->deleteRefundGround($id, $password);
            if ($response->status['code'] == 100) {
                $url = Url::toRoute(['index']);
                \Yii::$app->getSession()->setFlash('success-message', "Refund ground successfully deleted.");
                return ['success' => $url];
            } else {
                $model->addError("", "Error Code " . $response->status['code'] . " :: " . $response->status['message']);
            }
        } else {
            $model->addError("", "Password cannot be blank");
        }
        $errorsArry = '';
        foreach ($model->getErrors() as $error) {
            $errorsArry.= $error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

}
