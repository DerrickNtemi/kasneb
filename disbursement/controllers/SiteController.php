<?php

namespace disbursement\controllers;

use common\models\Beneficiaries;
use common\models\Disbursements;
use common\models\LoginForm;
use common\models\Service;
use moonland\phpexcel\Excel;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\helpers\Url;
use common\models\RefundRequest;

/**
 * Site controller
 */
class SiteController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'error', 'forgot-password', 'reset-password', 'expired-password', 'change-password'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex() {
        $services = new Service();
        $disbursement = new Disbursements();
        $disbursementData = $services->disbursementsAwaitingAuthorization();
        $disbursements = [];
        foreach ($disbursementData as $disbursement) {
            $disbursementModel = new Disbursements();
            $disbursementModel->setAttributes($disbursement);
            $disbursementModel->createdAt = $services->timestampToSting1($disbursementModel->createdAt);
            $disbursements[] = $disbursementModel;
        }
        return $this->render('index', ['disbursement' => $disbursement, 'disbursements' => $disbursements]);
    }

    public function actionDisbursementHistory() {
        $services = new Service();
        $disbursement = new Disbursements();
        $disbursementData = $services->getDisbursementHistory();
        $disbursements = [];
        foreach ($disbursementData as $disbursement) {
            $disbursementModel = new Disbursements();
            $disbursementModel->setAttributes($disbursement);
            $disbursementModel->createdAt = $services->timestampToSting1($disbursementModel->createdAt);
            $disbursements[] = $disbursementModel;
        }
        return $this->render('disbursement-history', ['disbursement' => $disbursement, 'disbursements' => $disbursements]);
    }

    public function actionViewDisbursement($id) {
        $services = new Service();
        $disbursementData = $services->getDisbursement($id);
        $disbursement = new Disbursements();
        $beneficiary = new Beneficiaries();
        $disbursement->setAttributes($disbursementData['disbursement']);
        $beneficiaries = $currencies = $beneficiaryTypes = $transactionTypes = $transactionTypeAccounts = [];
        $currency = $services->getCurrency();
        foreach ($currency as $cur) {
            $currencies[$cur['id']] = $cur['code'];
        }
        $beneficiaryTypesArray = $services->getBeneficiaryTypes();
        foreach ($beneficiaryTypesArray as $bta) {
            $beneficiaryTypes[$bta['id']] = $bta['name'];
        }
        $transactionAccountTypesArray = $services->getTransactionAccountTypes(1);
        foreach ($transactionAccountTypesArray as $tata) {
            $transactionTypeAccounts[$tata['id']] = $tata['name'];
        }

        foreach ($disbursementData['beneficiaries'] as $beneficiaryData) {
            $beneficiaryModel = new Beneficiaries();
            $beneficiaryModel->setAttributes($beneficiaryData);
            $beneficiaries[] = $beneficiaryModel;
        }
        return $this->render('viewdisbursement', [
                    'model' => $disbursement,
                    'beneficiary' => $beneficiary,
                    'beneficiaries' => $beneficiaries,
                    'currencies' => $currencies,
                    'beneficiaryTypes' => $beneficiaryTypes,
                    'transactionTypes' => $transactionTypes,
                    'transactionTypeAccounts' => $transactionTypeAccounts
        ]);
    }

    public function actionCreateDisbursement() {
        $services = new Service();
        $beneficiaries = $currencies = $beneficiaryTypes = $transactionTypes = $transactionTypeAccounts = [];
        $currency = $services->getCurrency();
        foreach ($currency as $cur) {
            $currencies[$cur['id']] = $cur['code'];
        }
        $beneficiaryTypesArray = $services->getBeneficiaryTypes();
        foreach ($beneficiaryTypesArray as $bta) {
            $beneficiaryTypes[$bta['id']] = $bta['name'];
        }
        $transactionAccountTypesArray = $services->getTransactionAccountTypes(1);
        foreach ($transactionAccountTypesArray as $tata) {
            $transactionTypeAccounts[$tata['id']] = $tata['name'];
        }
        $disbursementData = $services->getPendingDisbursement();
        $disbursement = new Disbursements();
        $beneficiary = new Beneficiaries();
        if (!empty($disbursementData['disbursement'])) {
            $disbursement->setAttributes($disbursementData['disbursement']);
            foreach ($disbursementData['beneficiaries'] as $beneficiaryData) {
                $beneficiaryModel = new Beneficiaries();
                $beneficiaryModel->setAttributes($beneficiaryData);
                $beneficiaries[] = $beneficiaryModel;
            }
        }
        return $this->render('createdisbursement', [
                    'model' => $disbursement,
                    'beneficiary' => $beneficiary,
                    'beneficiaries' => $beneficiaries,
                    'currencies' => $currencies,
                    'beneficiaryTypes' => $beneficiaryTypes,
                    'transactionTypes' => $transactionTypes,
                    'transactionTypeAccounts' => $transactionTypeAccounts
        ]);
    }

    public function actionCreateDisbursementTemplate() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new Disbursements();
        $service = new Service();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $response = (object) $service->createDisbursement($model);
            if ($response->status['code'] == 100) {
                return ['success' => $response->item];
            } else {
                $model->addError("", "ERROR CODE " . $response->status['code'] . " :: " . $response->status['message']);
            }
        }
        $errorsArry = '';
        foreach ($model->getErrors() as $error) {
            $errorsArry .= '' . $error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

    public function actionCancelDisbursement($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new Disbursements();
        $service = new Service();
        $password = isset($_POST['password']) ? $_POST['password'] : "";
        if (!empty($password)) {
            $response = (Object) $service->cancelDisbursement($id);
            if ($response->status['code'] == 100) {
                \Yii::$app->getSession()->setFlash('success-message', "Disbursement record successfully cancelled.");
                $url = Url::toRoute(['index']);
                return ['success' => $url];
            } else {
                $model->addError("", "ERROR CODE " . $response->status['code'] . " :: " . $response->status['message']);
            }
        } else {
            $model->addError("", "Password cannot be blank");
        }
        $errorsArry = '';
        foreach ($model->getErrors() as $error) {
            $errorsArry .= $error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

    public function actionAuthorizeDisbursement() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new Disbursements();
        $service = new Service();
        $password = isset($_POST['password']) ? $_POST['password'] : "";
        $disbursementId = isset($_POST['disbursementsId']) ? $_POST['disbursementsId'] : "";
        if (!empty($password) && !empty($disbursementId)) {
            $response = (Object) $service->authoriseDisbursement($disbursementId);
            if ($response->status['code'] == 100) {
                \Yii::$app->getSession()->setFlash('success-message', "Disbursement record successfully authourized.");
                $url = Url::toRoute(['view-disbursement', 'id' => $disbursementId]);
                return ['success' => ['url' => $url]];
            } else {
                $model->addError("", "ERROR CODE " . $response->status['code'] . " :: " . $response->status['message']);
            }
        } else {
            $model->addError("", "Password cannot be blank");
        }
        $errorsArry = '';
        foreach ($model->getErrors() as $error) {
            $errorsArry .= $error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

    public function actionCreateBeneficiaries() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new Beneficiaries();
        $service = new Service();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $response = (object) $service->createBeneficiries($model);
            if ($response->status['code'] == 100) {
                $beneficiaryArray = $service->getBeneficiaries($model->disbursementId);
                $beneficiaries = [];
                foreach ($beneficiaryArray as $data) {
                    $beneficiaryModel = new Beneficiaries();
                    $beneficiaryModel->setAttributes($data);
                    $beneficiaries[] = $beneficiaryModel;
                }
                $message = empty($model->id) ? 'Beneficiary successfully added' : 'Beneficiary successfully updated';
                return ['success' => ['results' => $this->renderPartial('_beneficiaries', ['beneficiaries' => $beneficiaries, 'isDraft' => true], false, true), 'message' => $message]];
            } else {
                $model->addError("", "ERROR CODE " . $response->status['code'] . " :: " . $response->status['message']);
            }
        }
        $errorsArry = '';
        foreach ($model->getErrors() as $error) {
            $errorsArry .= '' . $error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

    public function actionUploadBeneficiaries() {
        $model = new Beneficiaries();
        $model->load(Yii::$app->request->post());
        $model->template = UploadedFile::getInstance($model, 'template');
        if ($model->template) {
            $time = time();
            $name = $time . '.' . $model->template->extension;
            $path = Yii::getAlias('@webroot') . '/uploads/' . $name;
            $model->template->saveAs($path);
            $data = Excel::widget([
                        'mode' => 'import',
                        'fileName' => $path,
                        'setFirstRecordAsKeys' => true,
            ]);
            $identity = Yii::$app->user->getIdentity();
            $dataArray = [];
            foreach ($data as $dt) {
                if (!empty($dt['Id Number']) && !empty($dt['Phone Number'])) {
                    $modelData = new Beneficiaries();
                    $modelData->setAttributes([
                        'name' => $dt["Name"],
                        'idNumber' => $dt["Id Number"],
                        'disbursementId' => $model->disbursementId,
                        'phoneNumber' => $dt["Phone Number"],
                        'currencyId' => $dt["Currency Code"],
                        'beneficiaryTypeId' => $dt["Beneficiary Type Code"],
                        'transactionAccountTypeId' => $dt["Platform Code"],
                        'amount' => $dt["Amount"],
                        'createdBy' => $identity->id
                    ]);
                    $dataArray[] = $modelData;
                }
            }
            unlink($path);
            $service = new Service();
            $response = (object) $service->createBatchBeneficiaries($dataArray);
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($response->status['code'] == 100) {
                $beneficiaryArray = $service->getBeneficiaries($model->disbursementId);
                $beneficiaries = [];
                foreach ($beneficiaryArray as $data) {
                    $beneficiaryModel = new Beneficiaries();
                    $beneficiaryModel->setAttributes($data);
                    $beneficiaries[] = $beneficiaryModel;
                }
                $message = 'Beneficiaries successfully uploaded';
                return ['success' => ['results' => $this->renderPartial('_beneficiaries', ['beneficiaries' => $beneficiaries, 'isDraft' => true], false, true), 'message' => $message]];
            } else {
                $model->addError("", "ERROR CODE " . $response->status['code'] . " :: " . $response->status['message']);
            }
        }
        $errorsArry = 'Error Summary<br/>';
        foreach ($model->getErrors() as $error) {
            $errorsArry .= ' - ' . $error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

    public function actionDeleteBeneficiary($id, $disbursementId) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new Beneficiaries();
        $service = new Service();
        $response = (Object) $service->deleteBeneficiary($id, $disbursementId);
        if ($response->status['code'] == 100) {
            $beneficiaryArray = $service->getBeneficiaries($disbursementId);
            $beneficiaries = [];
            foreach ($beneficiaryArray as $data) {
                $beneficiaryModel = new Beneficiaries();
                $beneficiaryModel->setAttributes($data);
                $beneficiaries[] = $beneficiaryModel;
            }
            $message = 'Beneficiary successfully deleted';
            return ['success' => ['results' => $this->renderPartial('_beneficiaries', ['beneficiaries' => $beneficiaries, 'isDraft' => true], false, true), 'message' => $message]];
        } else {
            $model->addError("", "ERROR CODE " . $response->status['code'] . " :: " . $response->status['message']);
        }
        $errorsArry = '';
        foreach ($model->getErrors() as $error) {
            $errorsArry .= $error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

    //action to download link
    public function actionDownloadBeneficiaryTemplate() {
        $path = Yii::getAlias('@webroot') . '/uploads';
        $file = $path . '/beneficiaryTemplate.xlsx';
        if (file_exists($file)) {
            Yii::$app->response->sendFile($file);
        }
    }

    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $login = $model->login()) {
            $login = explode(':', $login);
            if ($login[0] == 'force') {
                Yii::$app->session->set('currentPassword', $model->password);
                Yii::$app->session->set('forcedChange', 1);
                Yii::$app->session->set('credentialId', $login[1]);
                return $this->redirect('change-password');
            }
            return $this->goBack();
        }
        return $this->render('login', [
                    'model' => $model,
        ]);
    }

    public function actionCreateDisbursementTemplateCheckBeneficiaries($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $disbursement = new Disbursements();
        $beneficiary = new Beneficiaries();
        $services = new Service();
        $disbursementData = $services->getDisbursement($id);
        if (!empty($disbursementData['beneficiaries'])) {
            $disbursement->setAttributes($disbursementData['disbursement']);
            $beneficiaries = [];
            foreach ($disbursementData['beneficiaries'] as $beneficiaryData) {
                $beneficiaryModel = new Beneficiaries();
                $beneficiaryModel->setAttributes($beneficiaryData);
                $beneficiaries[] = $beneficiaryModel;
            }
            return ['success' => $this->renderPartial("_finish", ['model' => $disbursement, 'beneficiary' => $beneficiary, 'beneficiaries' => $beneficiaries, 'visibility' => false, 'manual' => 'false'], false, true)];
        } else {
            $disbursement->addError("", "Disbursement's beneficiaries cannot be empty");
        }
        $errorsArry = '';
        foreach ($disbursement->getErrors() as $error) {
            $errorsArry .= $error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

    public function actionCreateDisbursementTemplateFinish($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new Disbursements();
        $service = new Service();
        $response = (Object) $service->completeDisbursement($id);
        if ($response->status['code'] == 100) {
            \Yii::$app->getSession()->setFlash('success-message', "Disbursement record successfully created. Awaiting authorization");
            $url = Url::toRoute(['view-disbursement', 'id' => $id]);
            return ['success' => ['url' => $url]];
        } else {
            $model->addError("", "ERROR CODE " . $response->status['code'] . " :: " . $response->status['message']);
        }
        $errorsArry = '';
        foreach ($model->getErrors() as $error) {
            $errorsArry .= $error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

    public function actionRefundRequest() {
        $services = new Service();
        $refundRequest = new RefundRequest();
        $requestData = $services->getAdminStudentRequest(4);
        $refundRequests = [];
        foreach ($requestData as $request) {
            $refundRequestModel = new RefundRequest();
            $refundRequestModel->setAttributes($request);
            $refundRequestModel->createdAt = $services->timestampToSting1($refundRequestModel->createdAt);
            $refundRequests[] = $refundRequestModel;
        }
        return $this->render('refund-request', ['refundRequest' => $refundRequest, 'disbursements' => $refundRequests]);
    }

    public function actionAuthoriseRefundRequest($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $service = new Service();
        $model = new RefundRequest();
        $checked = isset($_POST['checked']) ? $_POST['checked'] : "";
        $remarks = isset($_POST['remarks']) ? $_POST['remarks'] : "";
        if (!empty($checked)) {
            if ($checked == 1) {
                $status = 11;
            } else {
                $status = 12;
            }
            $data = [
                'id' => $id,
                'remarks' => $remarks,
                'requestStatusId' => $status
            ];
            $response = (object) $service->authoriseRefund($data);
            if ($response->status['code'] == 100) {
                Yii::$app->session->setFlash('success-message', 'Authorization process was successful...');
                $url = Url::toRoute(['site/refund-request']);
                return ['success' => ['url' => $url]];
            } else {
                $model->addError("", "ERROR CODE " . $response->status['code'] . " :: " . $response->status['message']);
            }
        } else {
            $model->addError("", "All fields are mandatory");
        }
        $errorsArry = '';
        foreach ($model->getErrors() as $error) {
            $errorsArry .= $error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

}
