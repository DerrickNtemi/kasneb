<?php

namespace administrator\controllers;

use common\models\CourseDisplay;
use common\models\Service;
use common\models\Student;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;

/**
 * Site controller
 */
class RegistrationController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionView($id) {
        $service = new Service();
        $studentModel = new Student();
        $studentModel->setAttributes($service->getStudentData($id));
        $courses = null;
        foreach ($studentModel->studentCourses as $course) {
            if (!$course['verified']) {
                $courses = $course;
            }
        }
        $remarks = $service->rejectionReasonsRegistration();
        return $this->render('view', [ 'model' => $studentModel, 'courses' => $courses,'remarks'=>$remarks]);
    }

    public function actionIndex() {
        $service = new Service();
        $studentModel = new Student();
        $studentsCourse = $service->getStudentsPending();
        $remarks = $service->rejectionReasonsRegistration();
        $registrations = [];
        foreach ($studentsCourse as $courses) {
            $courseModel = new CourseDisplay();
            $courseModel->id = $courses['id'];
            $courseModel->created = $courses['created'];
            $courseModel->studentId = $courses['studentObj'];
            $courseModel->courseId = $courses['course'];
            $registrations[] = $courseModel;
        }
        return $this->render('pending', [ 'model' => $studentModel, 'datas' => $registrations,'remarks'=>$remarks]);
    }

    public function actionVerify($courseId, $type) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $service = new Service();
        $model = new CourseDisplay();
        $checked = isset($_POST['checked']) ? $_POST['checked'] : "";
        $remarks = isset($_POST['remarks']) ? $_POST['remarks'] : "";
        $password = isset($_POST['password']) ? $_POST['password'] : "";
        $dataToSend = isset($_POST['datatosend']) ? $_POST['datatosend'] : "";
        if (!empty($checked) && !empty($password)) {
            $remarks = $checked == "APPROVED" ? "" : $remarks;
            $courseData = [];
            if ($type == "partial") {
                //prepare partial batch verification data
                $partial = explode(',', $dataToSend);
                for ($i = 0; $i < count($partial) - 1; $i++) {
                    $courseData[] = [
                        'id' => $partial[$i]
                    ];
                }
            }
            if ($type == "all") {
                //prepare all batch verification data
                $studentsCourse = $service->getStudentsPending();
                foreach ($studentsCourse as $courses) {
                    $courseData[] = [
                        'id' => $courses['id']
                    ];
                }
            }

            if ($type == "single") {
                //prepare single verification
                $courseData = [
                    'id' => $courseId,
                ];
            }
            if (!empty($courseData)) {
                $identity = Yii::$app->user->getIdentity();
                if ($type == "single") {
                    $data = [
                        'id' => $courseData['id'],
                        'verifiedBy' => ['id'=>$identity->user['id']],
                        'remarks' => $remarks,
                        'verificationStatus' => $checked
                    ];
                } else {
                    $data = [
                        'studentCourses' => $courseData,
                        'verifiedBy' => $identity->user['id'],
                        'remarks' => $remarks,
                        'verificationStatus' => $checked
                    ];
                }
                $response = (object) $service->verifyRegistration($data,$type);
                if ($response->status['code'] == 1) {
                    Yii::$app->session->setFlash('success-message', 'Verification process was successful...');
                    $url = Url::toRoute(['registration/index']);
                    return ['success' => ['url' => $url]];
                } else {
                    $model->addError("", "ERROR CODE " . $response->status['code'] . " :: " . $response->status['message']);
                }
            }
        } else {
            $model->addError("", "All fields are mandatory");
        }
        $errorsArry = '';
        foreach ($model->getErrors() as $error) {
            $errorsArry.=$error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

}
