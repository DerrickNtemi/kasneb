<?php

namespace administrator\controllers;

use common\models\ExemptionVerification;
use common\models\Service;
use common\models\Student;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;

/**
 * Site controller
 */
class ExemptionController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionView($id) {
        $service = new Service();
        $studentModel = new Student();
        $exemptions = $service->getExemption($id);
        $studentModel->setAttributes($exemptions['student']);
        $remarks = $service->rejectionReasonsExemption();
        return $this->render('view', ['model' => $studentModel, 'exemptions' => $exemptions,'remarks'=>$remarks]);
    }

    public function actionIndex() {
        $service = new Service();
        $model = new ExemptionVerification();
        $datas = $service->getPendingExemptions();
        $remarks = $service->rejectionReasonsExemption();
        $dataArray = [];
        foreach ($datas as $data) {
            $modelData = new ExemptionVerification();
            $modelData->setAttributes($data);
            $modelData->studentId = $data['student'];
            //print_r($modelData->studentId); exit;
            $dataArray[] = $modelData;
        }
        return $this->render('pending', [ 'model' => $model, 'datas' => $dataArray,'remarks'=>$remarks]);
    }

    public function preparePapersData($data) {
        $datas = explode(',', $data);
        $papers = [];
        for ($i = 0; $i < count($datas) - 1; $i++) {
            $papers[] = ['code' => $datas[$i]];
        }
        return $papers;
    }

    public function actionVerify($id, $courseId, $studentId, $type) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new ExemptionVerification();
        $checked = isset($_POST['checked']) ? $_POST['checked'] : "";
        $remarks = isset($_POST['remarks']) ? $_POST['remarks'] : "";
        $password = isset($_POST['password']) ? $_POST['password'] : "";
        $datatosend = isset($_POST['datatosend']) ? $_POST['datatosend'] : "";
        if (!empty($checked) && !empty($password)) {
            $remarks = $checked == "APPROVED" ? "" : $remarks;
            if ($type != "all-summary" && ($type == "all-partial" || $type = "all-single") && empty($datatosend)) {                
                $model->addError("", "You must select atleast one paper");
            }else{
                $service = new Service();
                $identity = Yii::$app->user->getIdentity();
                $studentModel = new Student();
                $exemptions = $service->getExemption($id);
                $papers = [];
                if ($type == "all-partial") {
                    $papers = $this->preparePapersData($datatosend);
                }
                if ($type == "all-single") {
                    $papers[] = ['code' => $datatosend];
                }
                if ($type == "all-summary") {
                    $courseId = $studentModel->currentCourse['id'];
                    foreach ($exemptions['papers'] as $paper) {
                        $papers[] = ['code' => $paper['paper']['code']];
                    }
                }
                $papersData = [];
                foreach ($papers as $pp){
                    $papersData[] = [
                        'paper'=>$pp,
                        'verificationStatus'=>$checked,
                        'remarks'=>$remarks,
                    ];
                }

                $data = [
                    'id'=>$id,
                    'papers' => $papersData,
                    'verifiedBy' => ['id'=>$identity->user['id']],
                ];
                $response = (object) $service->verifyExemption($data);
                if ($response->status['code'] == 1) {
                    Yii::$app->session->setFlash('success-message', 'Verification process was successful...');
                    $url = Url::toRoute(['exemption/index']);
                    return ['success' => ['url' => $url]];
                } else {
                    $model->addError("", "ERROR CODE " . $response->status['code'] . " :: " . $response->status['message']);
                }
            }
        } else {
            $model->addError("", "All fields are mandatory");
        }
        $errorsArry = '';
        foreach ($model->getErrors() as $error) {
            $errorsArry.=$error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

}
