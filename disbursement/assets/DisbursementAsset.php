<?php

namespace disbursement\assets;

use yii\web\AssetBundle;

/**
 * Main disbursement application asset bundle.
 */
class DisbursementAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/custom.css',
    ];
    public $js = [
        'js/custom.js',
        'js/bootstrap-wizard.min.js',
        'js/highcharts.js',
        'js/graph.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
