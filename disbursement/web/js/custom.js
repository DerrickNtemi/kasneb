var errorMessage = 'ERROR CODE 400 :: An error occurred while processing your request, please try again or contact system administrator for assistance..';
var errorNotFound = "ERROR CODE 404 :: The requested resource was not found";
var errorInternalServer = "ERROR CODE 500 :: Internal server error, please try again or contact system administrator for assistance..";

$(document).ready(function () {

    $(window).load(function () {
        var pageHeight = $(document).height();
        var mainHeaderHeight = $('.main-header-div').height();
        var contentHeight = pageHeight - mainHeaderHeight - 60;
        $('.menu-div').css('min-height', contentHeight);
    });

    $('#rootwizard').bootstrapWizard({
        onNext: function (tab, navigation, index) {
            var total = (navigation.find('li').length) - 1;
            if (index === 1) {
                var form = $('#create-disbursement-form');
                var url = form.attr('action');
                var data = new FormData($(form)[0]);
                silentWizardCommunication(url, data, "#rootwizard", index, total, "#disbursement-template-error-summary");
                return false;
            } else if (index === 2) {
                var form = $('#create-disbursement-form');
                var disbursementId = $("#disbursements-id").val();
                var url = form.attr('action') + "-check-beneficiaries?id=" + disbursementId;
                var data = new FormData($(form)[0]);
                silentWizardCommunication(url, data, "#rootwizard", index, total, "#upload-error-summary");
                return false;
            }
        },
        onTabShow: function (tab, navigation, index) {
            var total = (navigation.find('li').length);
            var percent = ((index + 1) / total) * 100;
            $('#rootwizard').find('.bar').css({width: percent + '%'});
            $('#rootwizard').find('.bar').text(Math.round(percent) + '% Complete');
            // If it's the last tab then hide the last button and show the finish instead
            if ((index + 1) >= total) {
                $('#rootwizard').find('.pager .next').hide();
                $('#rootwizard').find('.pager .finish').show();
                $('#rootwizard').find('.pager .finish').removeClass('disabled');
            } else {
                $('#rootwizard').find('.pager .next').show();
                $('#rootwizard').find('.pager .finish').hide();
            }
        },
        onTabClick: function (tab, navigation, index) {
            return false;
        }
    });
    $('#rootwizard .finish').click(function () {
        var form = $('#create-disbursement-form');
        var disbursementId = $("#disbursements-id").val();
        var url = form.attr('action') + "-finish?id=" + disbursementId;
        var data = new FormData($(form)[0]);
        silentWizardRedirectComm(url, data, "#disbursement-preview-error-message");
    });


    $('#BeneficiaryModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var modal = $(this);
        var data = button.data('whatever');
        if (data !== "") {
            $('#beneficiary-modal-header').text("Update Beneficiary");
            $('#add-beneficiary').text("Update");
            modal.find('.modal-body #beneficiaries-id').val(data.id);
            modal.find('.modal-body #beneficiaries-disbursementid').val(data.disbursementId.id);
            modal.find('.modal-body #beneficiaries-transactiontypeid').val(1);
            modal.find('.modal-body #beneficiaries-amount').val(data.amount);
            modal.find('.modal-body #beneficiaries-name').val(data.name);
            modal.find('.modal-body #beneficiaries-idnumber').val(data.idNumber);
            modal.find('.modal-body #beneficiaries-phonenumber').val(data.phoneNumber);
            modal.find('.modal-body #beneficiaries-currencyid').val(data.currencyId.id);
            modal.find('.modal-body #beneficiaries-beneficiarytypeid').val(data.beneficiaryTypeId.id);
            modal.find('.modal-body #beneficiaries-transactionaccounttypeid').val(data.transactionAccountTypeId.id);

        } else {
            $('#beneficiary-modal-header').text("Add Beneficiary");
            $('#add-beneficiary').text("Add");
            var disbursementId = $('#disbursements-id').val();
            modal.find('.modal-body #beneficiaries-id').val(null);
            modal.find('.modal-body #beneficiaries-disbursementid').val(disbursementId);
            modal.find('.modal-body #beneficiaries-transactiontypeid').val(1);
            modal.find('.modal-body #beneficiaries-amount').val("");
            modal.find('.modal-body #beneficiaries-name').val("");
            modal.find('.modal-body #beneficiaries-idnumber').val("");
            modal.find('.modal-body #beneficiaries-phonenumber').val("");
            modal.find('.modal-body #beneficiaries-currencyid').val(1);
            modal.find('.modal-body #beneficiaries-beneficiarytypeid').val(1);
            modal.find('.modal-body #beneficiaries-transactionaccounttypeid').val(1);
        }
    });



    $('#add-beneficiary').on('click', function () {
        var form = $('#create-beneficiary-form');
        var url = form.attr('action');
        var data = new FormData(form[0]);
        var successId = "#upload-success-message";
        var modalId = "#BeneficiaryModal";
        var reloadDiv = "#all-my-beneficiaries";
        silentReloadCommunication(url, data, modalId, successId, reloadDiv);
    });


    $('#DeleteRedirectModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var modal = $(this);
        var url = button.data('whatever');
        modal.find('.modal-body #url').text(url);
        modal.find('.modal-body #error-summary').hide();
        modal.find('.modal-body #progress-spinner').hide();
    });

    $('#DeleteInnerReloadModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var modal = $(this);
        var data = button.data('whatever');
        var div = button.data('div');
        var successDiv = button.data('value');
        modal.find('.modal-body #reload-div').text(div);
        modal.find('.modal-body #success-div').text(successDiv);
        modal.find('.modal-body #delete-url').text(data);
        modal.find('.modal-body #form-processing-loader').hide();
        modal.find('.modal-body #error-summary').hide('');
    });

    $('#delete-inner-reload-btn').on('click', function () {
        var modalId = "#DeleteInnerReloadModal";
        var modal = $(modalId);
        var reloadDiv = modal.find('.modal-body #reload-div').text();
        var successDiv = modal.find('.modal-body #success-div').text();
        var url = modal.find('.modal-body #delete-url').text();
        var data = "";
        silentReloadCommunication(url, data, modalId, successDiv, reloadDiv);
    });

    $('#delete-redirect-btn').on('click', function () {
        var deleteModal = $('#DeleteRedirectModal');
        var url = deleteModal.find('.modal-body #url').text();
        var password = deleteModal.find('.modal-body #password').val();
        $.ajax({
            url: url,
            type: 'POST',
            data: {password: password},
            beforeSend: function () {
                deleteModal.find('.modal-body #progress-spinner').show();
                deleteModal.find('.modal-body #error-summary').hide();
            },
            complete: function () {
                deleteModal.find('.modal-body #progress-spinner').hide();
            },
            success: function (data) {
                if (typeof data.success !== 'undefined') {
                    deleteModal.modal('hide');
                    deleteModal.removeData('modal');
                    window.location = data.success;
                } else {
                    deleteModal.find('.modal-body #error-summary').show().html(data.error);
                }
            },
            error: function () {
                deleteModal.find('.modal-body #error-summary').show().html(errorMessage);
            },
            statusCode: {
                404: function () {
                    deleteModal.find('.modal-body #error-summary').show().html(errorNotFound);
                },
                500: function () {
                    deleteModal.find('.modal-body #error-summary').show().html(errorInternalServer);
                }
            }
        });
    });

    $("#beneficiaries-template").on('change', function () {
        var form = $('#upload-beneficiaries-form');
        $("#beneficiaries-disbursementid").val($("#disbursements-id").val());
        var formData = new FormData(form[0]);
        $.ajax({
            url: form.attr('action'),
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $('#loadingDiv').show();
                $('#upload-error-summary').hide();
            },
            complete: function () {
                $('#loadingDiv').hide();
            },
            success: function (data) {
                if (typeof data.success !== 'undefined') {
                    $('#upload-success-message').show().html(data.success.message);
                    $('#all-my-beneficiaries').html(data.success.results);
                } else {
                    $('#upload-error-summary').show().html(data.error);
                }
            },
            error: function () {
                $('#upload-error-summary').show().html(errorMessage);
            },
            statusCode: {
                404: function () {
                    $('#upload-error-summary').show().html(errorNotFound);
                },
                500: function () {
                    $('#upload-error-summary').show().html(errorInternalServer);
                }
            }
        });
    });

    $('#ValidateUser').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var modal = $(this);
        var data = button.data('whatever');
        var value = button.data('value');
        modal.find('.modal-body #url').text(data);
        modal.find('.modal-body #disbursement-totalamount').text(value.amount);
        modal.find('.modal-body #disbursement-name').text(value.templateName);
        modal.find('.modal-body #disbursementsId').text(value.id);
        modal.find('.modal-body #progress-spinner').hide();
        modal.find('.modal-body #error-summary').html('');
    });

    $('#confirm-authorization').on('click', function () {
        var url = $('#url').html();
        var disbursementsId = $('#disbursementsId').html();
        var password = $('#password').val();
        var modal = $('#ValidateUser');
        $.ajax({
            url: url,
            method: 'POST',
            data: {disbursementsId: disbursementsId, password: password},
            beforeSend: function () {
                modal.find('.modal-body #progress-spinner').show();
                modal.find('.modal-body #error-summary').hide();
            },
            complete: function () {
                modal.find('.modal-body #progress-spinner').hide();
            },
            success: function (data) {
                if (typeof data.success !== 'undefined') {
                    window.location = data.success.url;
                } else {
                    modal.find('.modal-body #error-summary').show().html(data.error);
                }
            },
            error: function () {
                modal.find('.modal-body #error-summary').show().html(errorMessage);
            },
            statusCode: {
                404: function () {
                    modal.find('.modal-body #error-summary').show().html(errorNotFound);
                },
                500: function () {
                    modal.find('.modal-body #error-summary').show().html(errorInternalServer);
                }
            }
        });

    });

    $('#AuthorizeRefundRequestModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var modal = $(this);
        var url = button.data('whatever');
        var value = button.data('value');
        var message = button.data('message');
        modal.find('.modal-body #action-url').text(url);
        modal.find('.modal-body #verify').text(value);
        modal.find('.modal-body #remarks').val();
        modal.find('.modal-body #message').text(message);
        modal.find('.modal-body #modalid').text("#AuthorizeRefundRequestModal");
        if(value === 1){
            modal.find('.modal-body #other-reasons').hide();
            $("#verify-redirect-btn").text("Accept");
            $("#model-header-authorization").text("Accept Request");
        }else{
            modal.find('.modal-body #other-reasons').show();
            $("#verify-redirect-btn").text("Reject");
            $("#model-header-authorization").text("Reject Request");
        }
    });

    $('#verify-redirect-btn').on('click', function () {
        var url = $('#action-url').text();
        var checked = $("#verify").text();
        var remarks = $('#remarks').val();
        var modal = $($('#modalid').text());
        $.ajax({
            url: url,
            method: 'POST',
            data: {checked: checked, remarks: remarks},
            beforeSend: function () {
                modal.find('.modal-body #progress-spinner').show();
                modal.find('.modal-body #error-summary').hide();
            },
            complete: function () {
                modal.find('.modal-body #progress-spinner').hide();
            },
            success: function (data) {
                if (typeof data.success !== 'undefined') {
                    window.location = data.success.url;
                } else {
                    modal.find('.modal-body #error-summary').show().html(data.error);
                }
            },
            error: function () {
                modal.find('.modal-body #error-summary').show().html(errorMessage);
            },
            statusCode: {
                404: function () {
                    modal.find('.modal-body #error-summary').show().html(errorNotFound);
                },
                500: function () {
                    modal.find('.modal-body #error-summary').show().html(errorInternalServer);
                }
            }
        });
    });

    $('#refund-grounds-btn').on('click', function () {
        var activeModalId = "#RefundGroundModal";
        var submitForm = "#refund-grounds-form";
        silentCommunicationWithRedirect(activeModalId, submitForm, $(this));
    });

    function silentReloadCommunication(url, data, modalId, successId, reloadDiv) {
        var activeModal = $(modalId);
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
                activeModal.find('#progress-spinner').show();
                activeModal.find('#error-summary').hide();
            },
            complete: function () {
                activeModal.find('#progress-spinner').hide();
            },
            success: function (data) {
                if (typeof data.success !== 'undefined') {
                    activeModal.modal('hide');
                    $(reloadDiv).html(data.success.results);
                    $(successId).show().html(data.success.message);
                } else {
                    activeModal.find('#error-summary').show().html(data.error);
                }
            },
            error: function () {
                activeModal.find('#error-summary').show().html(errorMessage);
            },
            statusCode: {
                404: function () {
                    activeModal.find('#error-summary').show().html(errorNotFound);
                },
                500: function () {
                    activeModal.find('#error-summary').show().html(errorInternalServer);
                }
            }
        });
    }

    function silentCommunicationWithRedirect(activeModalId, submitForm, button) {
        var activeModal = $(activeModalId);
        var form = $(submitForm);
        var formData = new FormData(form[0]);
        $.ajax({
            url: form.attr('action'),
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
                activeModal.find('.modal-body #progress-spinner').show();
                activeModal.find('.modal-body #error-summary').hide();
                button.attr('disabled', true);
            },
            complete: function () {
                activeModal.find('.modal-body #progress-spinner').hide();
            },
            success: function (data) {
                if (typeof data.success !== 'undefined') {
                    activeModal.modal('hide');
                    activeModal.removeData('modal');
                    window.location = data.success;
                    button.attr('disabled', false);
                } else {
                    activeModal.find('.modal-body #error-summary').show().html(data.error);
                    button.attr('disabled', false);
                }
            },
            error: function () {
                activeModal.find('.modal-body #error-summary').show().html(errorMessage);
                button.attr('disabled', false);
            },
            statusCode: {
                404: function () {
                    activeModal.find('.modal-body #error-summary').show().html(errorNotFound);
                    button.attr('disabled', false);
                },
                500: function () {
                    activeModal.find('.modal-body #error-summary').show().html(errorInternalServer);
                    button.attr('disabled', false);
                }
            }
        });
    }

    function silentWizardCommunication(url, data, wizard, index, total, errorId) {
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $('#loadingDiv').show();
                $(errorId).hide();
            },
            complete: function () {
                $('#loadingDiv').hide();
            },
            success: function (data) {
                if (typeof data.success !== 'undefined') {
                    if (wizard === "#rootwizard" && index === 1) {
                        $("#disbursements-id").val(data.success);
                    }
                    if (index === total) {
                        $('#tab' + (index + 1)).html(data.success);
                    }
                    $(wizard).bootstrapWizard('show', index);
                    $(errorId).hide();
                } else {
                    $(errorId).show().html(data.error);
                }
            },
            error: function () {
                $(errorId).show().html(errorMessage);
            },
            statusCode: {
                404: function () {
                    $(errorId).show().html(errorNotFound);
                },
                500: function () {
                    $(errorId).show().html(errorInternalServer);
                }
            }
        });
    }

    function silentWizardRedirectComm(url, data, errorId) {
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $('#loadingDiv').show();
                $(errorId).hide();
            },
            complete: function () {
                $('#loadingDiv').hide();
            },
            success: function (data) {
                if (typeof data.success !== 'undefined') {
                    window.location = data.success.url;
                } else {
                    $(errorId).show().html(data.error);
                }
            },
            error: function () {
                $(errorId).show().html(errorMessage);
            },
            statusCode: {
                404: function () {
                    $(errorId).show().html(errorNotFound);
                },
                500: function () {
                    $(errorId).show().html(errorInternalServer);
                }
            }
        });
    }

    function silentCommReturnVal(url, data, errorId, inputId) {
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $('#loadingDiv').show();
                $(errorId).hide();
            },
            complete: function () {
                $('#loadingDiv').hide();
            },
            success: function (data) {
                if (typeof data.success !== 'undefined') {
                    $(inputId).val(data.success);
                } else {
                    $(errorId).show().html(data.error);
                }
            },
            error: function () {
                $(errorId).show().html(errorMessage);
            },
            statusCode: {
                404: function () {
                    $(errorId).show().html(errorNotFound);
                },
                500: function () {
                    $(errorId).show().html(errorInternalServer);
                }
            }
        });
    }

});