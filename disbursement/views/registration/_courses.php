
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>Course Application Details</strong>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-bordered">
                            <tr><th>Course Name</th><td><?= $courses['course']['name'] ?></td></tr>
                            <tr>
                                    <th>Qualifications</th>
                                    <td>
                                        <ol>
                                        <?php
                                        foreach ($courses['studentRequirements'] as $requirement){
                                            echo "<li>".$requirement['description']."</li>";
                                        }
                                        ?>
                                        </ol>
                                    </td>
                                </tr>
                            <tr><th>Date</th><td><?= $courses['created'] ?></td></tr>
                            <?php if(!empty($courses['document'])) { ?> 
                                <tr><th>Document</th><td><a href="<?=Yii::$app->params['studentPortalUrl']?>/students/qualifications/<?=$courses['document']?>" target="_blank">click to preview</a> &nbsp;  &nbsp;  &nbsp; <a href="<?=Yii::$app->params['studentPortalUrl']?>/students/qualifications/<?=$courses['document']?>" download>Download</a></td></tr>
                            <?php } ?>
                        </table>
                    </div>
                </div>