<?php

use yii\data\ArrayDataProvider;
use yii\grid\GridView;
?>
<?=

GridView::widget([

    'dataProvider' => new ArrayDataProvider([
        'allModels' => $transactions,
            ]),
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'label' => 'Beneficiary Name',
            'value' => function($data) {
                return $data->beneficiaryId['name'];
            }
        ],
        [
            'label' => 'Beneficiary Phone',
            'value' => function($data) {
                return $data->beneficiaryId['phoneNumber'];
            }
        ],
        [
            'label' => 'Amount',
            'value' => function($data) {
                return $data->beneficiaryId['currencyId']['code']." ".$data->amount;
            }
        ],
        [
            'label' => 'Timestamp',
            'value' => function($data) {
                return $data->createdAt;
            }
        ],
        [
            'label' => 'Status',
            'value' => function($data) {
                return $data->transactionStatusId['name'];
            }
        ]
        ],
    ]);
?>