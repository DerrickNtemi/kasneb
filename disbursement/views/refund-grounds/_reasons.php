<?php

use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\helpers\Html;
?>
<?=
GridView::widget([
    'dataProvider' => new ArrayDataProvider(['allModels' => $datas]),
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'label' => 'Description',
            'value' => function($data) {
                return $data->description;
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{delete}&nbsp;&nbsp;',
            'buttons' => [
                'delete' => function($url, $model) {
                    $url = Url::toRoute(['refund-grounds/delete', 'id' => $model->id]);
                    return Html::a('delete', $url, ['data-whatever' => $url, 'data-toggle' => "modal", 'data-target' => "#DeleteRedirectModal", 'title' => Yii::t('yii', 'Delete'),]);
                },
            ],
        ],
    ],
]);
?>

<!-- Creation Model -->
<?php
Modal::begin([
    'id' => 'RefundGroundModal',
    'header' => '<h4 class="modal-title" >Add Refund Request</h4>',
    'footer' =>
    Html::button('Cancel', ['class' => 'btn btn-default', 'data-dismiss' => 'modal'])
    . PHP_EOL .
    Html::button('Add', ['id' => 'refund-grounds-btn', 'class' => 'btn btn-primary btn-modal-save']),
]);
?>
<?= $this->render('_form', ['model' => $model]) ?>
<?php Modal::end() ?>

<!-- Delete Redirect Modal -->
<?php
Modal::begin([
    'id' => 'DeleteRedirectModal',
    'header' => '<h4 class="modal-title" >Delete Record</h4>',
    'footer' =>
    Html::button('Cancel', ['class' => 'btn btn-default', 'data-dismiss' => 'modal'])
    . PHP_EOL .
    Html::button('Delete', ['id' => 'delete-redirect-btn', 'class' => 'btn btn-primary btn-modal-save']),
]);
?>
<?= $this->render('@common/views/_delete') ?>
<?php Modal::end() ?>