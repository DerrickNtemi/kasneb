<?php

use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\helpers\Html;
if (!isset($visibility)) {
    $visibility = TRUE;
}
?>
<?=

GridView::widget([
    'dataProvider' => new ArrayDataProvider(['allModels' => $beneficiaries]),
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'label' => 'Name',
            'value' => function($data) {
                return $data->name;
            }
        ],
        [
            'label' => 'ID. Number',
            'value' => function($data) {
                return $data->idNumber;
            }
        ],
        [
            'label' => 'Phone Number',
            'value' => function($data) {
                return $data->phoneNumber;
            }
        ],
        [
            'label' => 'Role',
            'value' => function($data) {
                return $data->beneficiaryTypeId['name'];
            }
        ],
        [
            'label' => 'Platform',
            'value' => function($data) {
                return $data->transactionAccountTypeId['name'];
            }
        ],
        [
            'attribute' => 'amount',
            'value' => function($data) {
                return $data->currencyId['code']." ".number_format($data->amount);
            },
            'contentOptions' => ['style' => 'text-align:right;'],
            'headerOptions' => ['style' => 'text-align:right;'],
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update}&nbsp;&nbsp;{delete}&nbsp;&nbsp;',
            'visible'=>$visibility,
            'buttons' => [
                        'update' => function($url, $model) {
                    $data = Json::encode($model->getAttributes());
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['data-whatever' => $data, 'data-toggle' => "modal", 'data-target' => "#BeneficiaryModal", 'title' => Yii::t('yii', 'Update'), 'class'=>($model->disbursementId['disbursementTypeId']['code'] == 'DT001' ? "" : 'disable-link')]);
                },
                        'delete' => function($url, $model) {
                    $url = Url::toRoute(['site/delete-beneficiary', 'id' => $model->id,'disbursementId'=>$model->disbursementId['id']]);
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, ['data-value'=>'#upload-success-message', 'data-div' => '#all-my-beneficiaries', 'data-whatever' => $url, 'data-toggle' => "modal", 'data-target' => "#DeleteInnerReloadModal", 'title' => Yii::t('yii', 'Delete'),]);
                },
            ],
        ],
    ],
]);
