<?php
use yii\helpers\Html;
$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading"><?= $this->title ?></div>
            <div class="panel-body">
                <?php if (Yii::$app->session->hasFlash('fail-message')): ?>
                    <div class="alert alert-danger"><?= Yii::$app->session->getFlash('fail-message') ?></div>
                <?php endif; ?>
                <?php if (Yii::$app->session->hasFlash('success-message')): ?>
                    <div class="alert alert-success"><?= Yii::$app->session->getFlash('success-message') ?></div>
                <?php endif; ?>
                <div class="row">
                    <div class="col-md-12">
                        <p><?=Html::a('<span class="fa fa-plus"></span> New User', "", ['style'=>'margin-right:10px;','class'=>'btn btn-primary', 'data-toggle'=>"modal",'data-whatever'=>"", 'data-target'=>"#AdminUserModal", 'title' => Yii::t('yii', 'New User'),]);?></p>
                    </div>
                </div>
                <?= $this->render('_users', ['model'=>$model,'users'=>$users,'rolesData'=>$rolesData]) ?>
            </div>
        </div>
    </div>
</div>