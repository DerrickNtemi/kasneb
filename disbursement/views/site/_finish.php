<?php

use yii\helpers\Html;
use common\models\Service;

$service = new Service();
?>

<div class="panel-body">
    <div class="alert alert-danger" style="display: none;" id="disbursement-preview-error-message"></div>
    <div  class="row">
        <div class="col-md-5"></div>
        <div class="col-md-7 text-right">   
            <h2></h2>
            <table class="table">
                <tbody>         
                    <tr>
                        <th style="text-align: right; padding-right: 30px;">Template Name</th>
                        <th>: </th>
                        <td style="text-align: right; font-style: italic;"><?= Html::encode($model->templateName) ?></td>
                    </tr> 
                    <tr>
                        <th style="text-align: right; padding-right: 30px;">Date Created</th>
                        <th>: </th>
                        <td style="text-align: right; font-style: italic;"><?= Html::encode($service->timestampToSting($model->createdAt)); ?></td>
                    </tr>  
                    <tr>
                        <th style="text-align: right; padding-right: 30px;">Status</th>
                        <th>: </th>
                        <td style="text-align: right; font-style: italic;">
                            <?= $model->disbursementStatusId['name'] ?>
                        </td>
                    </tr> 
                    <tr style="background-color: lightgrey;">
                        <th style="text-align: right; padding-right: 30px;">Amount</th>
                        <th>: </th>
                        <td style="text-align: right;"><b><?= number_format($model->amount) ?></b></td>
                    </tr>  
                </tbody>
            </table>
        </div>
    </div>
    <div class="form-group" >
        <h4>Beneficiaries</h4>
    </div>
    <div class="form-group" id="all-my-beneficiaries">
        <?= $this->render('_beneficiaries', ['model' => $beneficiary, 'beneficiaries' => $beneficiaries, 'visibility'=>FALSE]) ?>
    </div>            
</div>