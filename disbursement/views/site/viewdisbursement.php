<?php

use yii\helpers\Html;
use common\models\Service;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\helpers\Json;

$visibility = TRUE;
if ($model->disbursementStatusId['id'] != 1) {
    $visibility = FALSE;
}

$service = new Service();

$this->title = "Disbursement Record";
$index = "index";
$this->params['breadcrumbs'][] = ['label' => 'Disbursements', 'url' => [$index]];
$this->params['breadcrumbs'][] = "Disbursement Record";
$url = Url::toRoute(['site/cancel-disbursement', 'id' => $model->id]);
$authorizationUrl = Url::toRoute(['site/authorize-disbursement']);
?>
<div class="row members-index">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading"><?= $this->title ?></div>
            <div class="panel-body">
                <?php if (Yii::$app->session->hasFlash('fail-message')): ?>
                    <div class="alert alert-danger"><?= Yii::$app->session->getFlash('fail-message') ?></div>
                <?php endif; ?>
                <?php if (Yii::$app->session->hasFlash('success-message')): ?>
                    <div class="alert alert-success"><?= Yii::$app->session->getFlash('success-message') ?></div>
                <?php endif; ?>
                <div  class="row">
                    <div class="col-md-5">
                        <?php if ($model->disbursementStatusId['id'] == 1 || $model->disbursementStatusId['id'] == 8) : ?>
                            <?= Html::button('<span class="glyphicon glyphicon-check" ></span> Authorize', ['class' => 'btn btn-success btn-sm', 'data-value' => Json::encode($model->getAttributes()), 'data-toggle' => "modal", 'data-whatever' => $authorizationUrl, 'data-target' => "#ValidateUser"]) ?>
                        <?php endif; ?>
                        &nbsp;
                        <?php if ($model->disbursementTypeId['code'] == "DT001" && ($model->disbursementStatusId['id'] == 1 || $model->disbursementStatusId['id'] == 8)) : ?>
                            <?= Html::button('<span class="glyphicon glyphicon-remove" ></span> Cancel', ['class' => 'btn btn-danger btn-sm', 'data-toggle' => "modal", 'data-whatever' => $url, 'data-target' => "#DeleteRedirectModal"]) ?>
                        <?php endif; ?>
                    </div>
                    <div class="col-md-7 text-right">   
                        <h2></h2>
                        <table class="table">
                            <tbody>         
                                <tr>
                                    <th style="text-align: right; padding-right: 30px;">Template Name</th>
                                    <th>: </th>
                                    <td style="text-align: right; font-style: italic;"><?= Html::encode($model->templateName) ?></td>
                                </tr> 
                                <tr>
                                    <th style="text-align: right; padding-right: 30px;">Date Created</th>
                                    <th>: </th>
                                    <td style="text-align: right; font-style: italic;"><?= Html::encode($service->timestampToSting($model->createdAt)); ?></td>
                                </tr>  
                                <tr>
                                    <th style="text-align: right; padding-right: 30px;">Status</th>
                                    <th>: </th>
                                    <td style="text-align: right; font-style: italic;">
                                        <?= $model->disbursementStatusId['name'] ?>
                                    </td>
                                </tr> 
                                <tr style="background-color: lightgrey;">
                                    <th style="text-align: right; padding-right: 30px;">Amount</th>
                                    <th>: </th>
                                    <td style="text-align: right;"><b><?= number_format($model->amount) ?></b></td>
                                </tr>  
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="form-group" >
                    <h4>Beneficiaries</h4>
                </div>
                <div class="alert alert-danger"  id='upload-error-summary' style="display: none;"> </div>
                <div class="alert alert-success" id="upload-success-message" style="display: none;"></div>
                <div class="form-group" id="all-my-beneficiaries">
                    <?= $this->render('_beneficiaries', ['model' => $beneficiary, 'beneficiaries' => $beneficiaries, 'visibility' => $visibility]) ?>
                </div>            
            </div>
        </div>
    </div>
</div>
<!-- Beneficiary create modal -->
<?php
Modal::begin([
    'id' => 'BeneficiaryModal',
    'header' => '<h4 class="modal-title" id="beneficiary-modal-header" >Add Beneficiary</h4>',
    'footer' =>
    Html::button('Close', ['class' => 'btn btn-default', 'data-dismiss' => 'modal'])
    . PHP_EOL .
    Html::button('Add', ['id' => 'add-beneficiary', 'class' => 'btn btn-primary btn-modal-save']),
    'options' => [
        'role' => 'dialog',
        'aria-labelledby' => 'category-modal-label',
        'aria-hidden' => 'true',
    ],
]);
?>
<?=
$this->render('_beneficiaryForm', [
    'model' => $beneficiary,
    'currencies' => $currencies,
    'beneficiaryTypes' => $beneficiaryTypes,
    'transactionTypes' => $transactionTypes,
    'transactionTypeAccounts' => $transactionTypeAccounts
])
?>
<?php Modal::end() ?>


<!-- Delete Inner Reload  Modal -->
<?php
Modal::begin([
    'id' => 'DeleteInnerReloadModal',
    'header' => '<h4 class="modal-title"  id="delete-modal-header" >Delete Record</h4>',
    'footer' =>
    Html::button('Close', ['class' => 'btn btn-default', 'data-dismiss' => 'modal'])
    . PHP_EOL .
    Html::button('Delete', ['id' => 'delete-inner-reload-btn', 'class' => 'btn btn-primary btn-modal-save']),
]);
?>
<?= $this->render('@common/views/deleteInnerReload') ?>
<?php Modal::end() ?>


<!-- Delete Modal -->
<?php
Modal::begin([
    'id' => 'ValidateUser',
    'header' => '<h4 class="modal-title"  id="" >Confirm Authorization</h4>',
    'footer' =>
    Html::button('Authorise', ['id' => 'confirm-authorization', 'class' => 'btn btn-primary btn-modal-save']),
    'options' => [
        'aria-labelledby' => 'category-modal-label',
        'role' => 'dialog',
        'aria-hidden' => 'true',
    ],
]);
?>
<?= $this->render('@common/views/confirmAuthorization') ?>
<?php Modal::end() ?>


<!-- Delete Redirect Modal -->
<?php
Modal::begin([
    'id' => 'DeleteRedirectModal',
    'header' => '<h4 class="modal-title" >Cancel Record</h4>',
    'footer' =>
    Html::button('No', ['class' => 'btn btn-default', 'data-dismiss' => 'modal'])
    . PHP_EOL .
    Html::button('Yes', ['id' => 'delete-redirect-btn', 'class' => 'btn btn-primary btn-modal-save']),
]);
?>
<?= $this->render('@common/views/_cancel') ?>
<?php Modal::end() ?>