<?php

use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
?>
<?=
GridView::widget([
    'dataProvider' => new ArrayDataProvider(['allModels' => $disbursements]),
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'label' => 'Name',
            'value' => function($data) {
                return $data->name;
            }
        ],
        [
            'label' => 'Reg No.',
            'value' => function($data) {
                return $data->registrationNo;
            }
        ],
        [
            'label' => 'Grounds',
            'value' => function($data) {
                return $data->refundReasonsId['description'];
            }
        ],
        [
            'label' => 'Narrative',
            'value' => function($data) {
                return $data->description;
            }
        ],
        [
            'label' => 'Timestamp',
            'value' => function($data) {
                return $data->createdAt;
            }
        ],
        [
            'attribute' => 'amount',
            'value' => function($data) {
                return number_format($data->amount, 2);
            },
            'contentOptions' => ['style' => 'text-align:right;'],
            'headerOptions' => ['style' => 'text-align:right;'],
        ],
        [
            'label' => 'Status',
            'value' => function($data) {
                return $data->requestStatusId['name'];
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{accept}&nbsp;&nbsp;{reject}&nbsp;&nbsp;',
            'buttons' => [
                'accept' => function($url, $model) {
                    $url = Url::toRoute(['site/authorise-refund-request', 'id' => $model->id]);
                    $msg = "Accept refund request";
                    return Html::a('accept', $url, ['data-whatever' => $url, 'data-message' => $msg, 'data-value' => 1, 'data-toggle' => "modal", 'data-target' => "#AuthorizeRefundRequestModal", 'title' => Yii::t('yii', 'Accept'),]);
                },
                'reject' => function($url, $model) {
                    $url = Url::toRoute(['site/authorise-refund-request', 'id' => $model->id]);
                    $msg = "Reject refund request";
                    return Html::a('reject', $url, ['data-whatever' => $url, 'data-message' => $msg, 'data-value' => 2, 'data-toggle' => "modal", 'data-target' => "#AuthorizeRefundRequestModal", 'title' => Yii::t('yii', 'Reject'),]);
                },
            ],
        ],
    ],
]);
?>
<!-- Activate / Deactivate Redirect Modal -->
<?php
Modal::begin([
    'id' => 'AuthorizeRefundRequestModal',
    'header' => '<h4 class="modal-title" id="model-header-authorization" ></h4>',
    'footer' =>
    Html::button('Process ', ['id' => 'verify-redirect-btn', 'class' => 'btn btn-primary btn-modal-save']),
]);
?>
<?= $this->render('@common/views/confirmauthorizationrefundrequest') ?>
<?php Modal::end() ?>