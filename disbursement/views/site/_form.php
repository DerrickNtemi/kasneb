<?php
/* @var $this DefaultController */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;

$this->params['breadcrumbs'] = [
    'label' => $this->title,
];
?>
<div class="contentpanel">
    <!-- BASIC WIZARD -->
    <div id="rootwizard" class="basic-wizard placing-an-order">
        <ul class="nav nav-pills nav-justified" id="myTab">
            <li><a href="#tab1" data-toggle="tab" class="in"><span>Step 1:</span> Disbursement Details</a></li>
            <li><a href="#tab2" data-toggle="tab"><span>Step 2:</span> Beneficiaries</a></li>
            <li><a href="#tab3" data-toggle="tab"><span>Step 3:</span> Finish </a></li>
        </ul>  
        <h2></h2>
        <div id="bar" class="progress">
            <div class="bar progress-bar progress-bar-striped active" id="wizard-progress-bar"></div>
        </div>          
        <div class="tab-content">
            <div class="tab-pane" id="tab1">
                <h2></h2>
                <div class="form-group">
                    <div class="alert alert-danger"  id='disbursement-template-error-summary' role="alert" style="display: none;"> </div>
                </div>
                <?php
                $form = ActiveForm::begin([
                            'id' => 'create-disbursement-form',
                            'enableClientValidation' => true,
                            'action' => Yii::$app->urlManager->createUrl(['site/create-disbursement-template']),
                            'fieldConfig' => [
                                'template' => "{label}<div class=\"clearfix\"></div>\n{input}\n{hint}\n{error}",
                            ],
                ]);
                ?>
                <div class="row">
                    <?= Html::activeHiddenInput($model, 'id') ?> 
                    <?=
                    $form->field($model, 'templateName', [
                        'options' => ['class' => 'col-md-6'],
                        'inputOptions' => ['placeholder' => $model->getAttributeLabel('templateName'), 'class' => 'form-control'],
                    ])->label($model->getAttributeLabel('templateName'))
                    ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
            <div class="tab-pane" id="tab2">
                <div class="panel-body" style="padding: 1px;">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a data-toggle="tab" href="#manual" class=" text-center">
                                <h4 class="glyphicon glyphicon-plus"></h4><br/>MANUAL ENTRY
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#upload" class=" text-center">
                                <h4 class="glyphicon glyphicon-upload"></h4><br/>UPLOAD FILE
                            </a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div id="manual" class="tab-pane fade in active"  style="padding-left: 28px;">
                            <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                <div class='col-md-12'>
                                    Please use the below button to add beneficiary to the disbursement
                                </div>
                            </div>
                            <div class="row">
                                <div class='col-md-12'>
                                    <?= Html::button('<span class="fa fa-plus" ></span> Add Beneficiary', ['class' => 'btn btn-primary btn-sm', 'data-toggle' => "modal", 'data-whatever' => '', 'data-target' => "#BeneficiaryModal"]) ?>
                                </div>
                            </div>
                        </div>
                        <div id="upload" class="tab-pane fade" style="padding-left: 28px;">
                            <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                <div class="col-md-12">
                                    Please use the "Download Template" button to download beneficiaries template. Fill the template and click on the "Upload" button to upload beneficiaries data
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <a href='<?= Url::toRoute(['site/download-beneficiary-template']) ?>' target="_blank"><?= Html::button('<span class="glyphicon glyphicon-cloud-download" ></span> Download Template', ['class' => 'btn btn-primary btn-sm']) ?></a></p>
                                </div>
                                <div class="col-md-3">
                                    <?php
                                    $form = ActiveForm::begin([
                                                'id' => 'upload-beneficiaries-form',
                                                'action' => Yii::$app->urlManager->createUrl(['site/upload-beneficiaries']),
                                                'options' => ['enctype' => 'multipart/form-data'],
                                                'fieldConfig' => [
                                                    'template' => "{label}<div class=\"clearfix\"></div>\n{input}\n{hint}\n{error}",
                                                ],
                                    ]);
                                    ?>  
                                    <?= Html::activeHiddenInput($beneficiary, 'disbursementId') ?> 
                                    <?= $form->field($beneficiary, 'template', [])->fileInput()->label(FALSE); ?>
                                    <?php ActiveForm::end(); ?> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <h2></h2>
                <div class="alert alert-danger"  id='upload-error-summary' style="display: none;"> </div>
                <div class="alert alert-success" id="upload-success-message" style="display: none;"></div>
                <div class="row">
                    <div class="col-md-12" id="all-my-beneficiaries">
                        <?= $this->render('_beneficiaries', ['beneficiaries' => $beneficiaries, 'isDraft' => true]) ?>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="tab3">
            </div>
        </div>
        <ul class="pager wizard">
            <li class="previous"><a href="javascript:;">Previous</a></li>
            <li class="next"><a href="javascript:;">Next</a></li>
            <li class="next finish" style="display:none;"><a href="javascript:;">Finish</a></li>
        </ul>
    </div>
</div>

<!-- Beneficiary create modal -->
<?php
Modal::begin([
    'id' => 'BeneficiaryModal',
    'header' => '<h4 class="modal-title" id="beneficiary-modal-header" >Add Beneficiary</h4>',
    'footer' =>
    Html::button('Close', ['class' => 'btn btn-default', 'data-dismiss' => 'modal'])
    . PHP_EOL .
    Html::button('Add', ['id' => 'add-beneficiary', 'class' => 'btn btn-primary btn-modal-save']),
    'options' => [
        'role' => 'dialog',
        'aria-labelledby' => 'category-modal-label',
        'aria-hidden' => 'true',
    ],
]);
?>
<?=
$this->render('_beneficiaryForm', [
    'model' => $beneficiary,
    'currencies' => $currencies,
    'beneficiaryTypes' => $beneficiaryTypes,
    'transactionTypes' => $transactionTypes,
    'transactionTypeAccounts' => $transactionTypeAccounts
])
?>
<?php Modal::end() ?>


<!-- Delete Inner Reload  Modal -->
<?php
Modal::begin([
    'id' => 'DeleteInnerReloadModal',
    'header' => '<h4 class="modal-title"  id="delete-modal-header" >Delete Record</h4>',
    'footer' =>
    Html::button('Close', ['class' => 'btn btn-default', 'data-dismiss' => 'modal'])
    . PHP_EOL .
    Html::button('Delete', ['id' => 'delete-inner-reload-btn', 'class' => 'btn btn-primary btn-modal-save']),
]);
?>
<?= $this->render('@common/views/deleteInnerReload') ?>
<?php Modal::end() ?>