<?php 
    use yii\widgets\ActiveForm;
    use yii\helpers\Html;

    $form = ActiveForm::begin([
        'id' => 'create-beneficiary-form',           
        'enableClientValidation'=> true,
        'action' => Yii::$app->urlManager->createUrl(['site/create-beneficiaries']),
        'fieldConfig' => [
            'template' => "{label}<div class=\"clearfix\"></div>\n{input}\n{hint}\n{error}",
        ],
    ]);  
?>     
<?= Html::activeHiddenInput($model,'id') ?>
<?= Html::activeHiddenInput($model,'disbursementId') ?>
<?= Html::activeHiddenInput($model,'transactionTypeId') ?>
<div class="row">
    <div class="col-md-6" >
        <?= $form->field($model, 'name', [
                       'inputOptions' => ['placeholder' => $model->getAttributeLabel('name'),'class'=>'form-control chosen-select'],
            ])?> 
    </div>
    <div class="col-md-3" >
        <?= $form->field($model, 'idNumber', [
                       'inputOptions' => ['placeholder' => $model->getAttributeLabel('idNumber'),'class'=>'form-control chosen-select'],
            ])?> 
    </div>
    <div class="col-md-3" >
        <?= $form->field($model, 'phoneNumber', [
                       'inputOptions' => ['placeholder' => $model->getAttributeLabel('phoneNumber'),'class'=>'form-control chosen-select'],
            ])?> 
    </div>
</div>
<div class="row">
    <div class="col-md-3" >
        <?= $form->field($model, 'currencyId', [
                       'inputOptions' => ['data-placeholder' => $model->getAttributeLabel('currencyId'),'class'=>'form-control chosen-select'],
            ])->dropDownList($currencies) ?> 
    </div>
    <div class="col-md-3" >
        <?= $form->field($model, 'beneficiaryTypeId', [
                       'inputOptions' => ['data-placeholder' => $model->getAttributeLabel('beneficiaryTypeId'),'class'=>'form-control chosen-select'],
            ])->dropDownList($beneficiaryTypes) ?> 
    </div>
    <div class="col-md-3" >
        <?= $form->field($model, 'transactionAccountTypeId', [
                       'inputOptions' => ['data-placeholder' => $model->getAttributeLabel('transactionAccountTypeId'),'class'=>'form-control chosen-select'],
            ])->dropDownList($transactionTypeAccounts) ?> 
    </div>
    <div class="col-md-3" >
        <?= $form->field($model, 'amount', [
                'inputOptions' => ['placeholder' => $model->getAttributeLabel('amount'),'class'=>'form-control'],
            ])?>
    </div>
</div>
<?php ActiveForm::end() ?>
<h2></h2>
<div class="alert alert-danger"  id="error-summary" style="display: none;"></div>
<h2></h2>
<div id="progress-spinner" class="text-center" style="display: none;">
    <?= Html::img('@web/images/ajax-loader.gif', ['alt' => 'progress spinner']) ?>
</div>
<h2></h2>