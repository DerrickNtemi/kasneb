<?php
$this->title = 'Create Disbursement';
$this->params['breadcrumbs'][] = ['label' => 'Disbursement', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row members-index">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading"><?= $this->title ?></div>
            <div class="panel-body"> 
                <?=
                $this->render('_form', [
                    'model' => $model,
                    'beneficiaries' => $beneficiaries,
                    'beneficiary' => $beneficiary,
                    'currencies' => $currencies,
                    'beneficiaryTypes' => $beneficiaryTypes,
                    'transactionTypes' => $transactionTypes,
                    'transactionTypeAccounts' => $transactionTypeAccounts
                ])
                ?>
            </div>
        </div>
    </div>
</div>
<div id="loadingDiv"></div>