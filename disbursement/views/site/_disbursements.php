<?php

use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\Html;
?>
<?=

GridView::widget([
    'dataProvider' => new ArrayDataProvider(['allModels' => $disbursements]),
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'label' => 'Template Name',
            'value' => function($data) {
                return $data->templateName;
            }
        ],
        [
            'label' => 'Timestamp',
            'value' => function($data) {
                return $data->createdAt;
            }
        ],
           [
            'attribute' => 'amount',
            'value' => function($data) {
                return number_format($data->amount, 2);
            },
            'contentOptions' => ['style' => 'text-align:right;'],
            'headerOptions' => ['style' => 'text-align:right;'],
        ],
        [
            'label' => 'Status',
            'value' => function($data) {
                return $data->disbursementStatusId['name'];
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{receipt}&nbsp;&nbsp;',
            'buttons' => [
                'receipt' => function($url, $model) {
                    $url = Url::toRoute(['site/view-disbursement', 'id' => $model->id]);
                    return Html::a('view', $url, ['title' => Yii::t('yii', 'view disbursement')]);
                },
            ],
        ],
                
    ],
]);
?>