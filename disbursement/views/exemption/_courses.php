<?php

use yii\helpers\Html;
use yii\helpers\Url;
$url = Url::toRoute(['exemption/verify', 'id' => $courses['id'], 'courseId' => $courses['studentCourseId'], 'studentId' => $model->id, 'type' => 'all-single']);
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <strong>Current Course Details</strong>
    </div>
    <div class="panel-body">
        <table class="table table-striped table-bordered">
            <tr><th>Reg. No.</th><td><?= $courses['fullRegNo'] ?></td></tr>
        </table>
    </div>
    <div class="panel-heading">
        <strong>Application Grounds</strong>
    </div>
    <div class="panel-body">
        <table class="table table-striped table-bordered">
            <tr>
                <th>Course</th>
                <td><?=$courses['qualification']['name']?></td>
            </tr>
            <?php foreach ($courses['documents'] as $doc) : ?>
                <tr>
                    <th>Transcript</th>
                    <td><a href="<?=Yii::$app->params['studentPortalUrl']?>/students/exemptions/<?=$doc['name']?>" target="_blank">click to preview</a> &nbsp;  &nbsp;  &nbsp; <a href="<?=Yii::$app->params['studentPortalUrl']?>/students/exemptions/<?=$doc['name']?>" download>Download</a></td>
                </tr>
            <?php endforeach; ?>
        </table>
          
    </div>
    <div class="panel-heading">
        <strong>Exemption Details</strong>
    </div>
    <div class="panel-body">
        <table class="table table-striped table-bordered">
            <tr>
                <th>Paper Code</th>
                <th>Paper Name</th>
                <th><input type="checkbox" id="exemptionpapersall"></th>
                <th></th>
            </tr>
            <?php foreach ($courses['papers'] as $paper) : ?>
                <tr>
                    <td><?= $paper['paper']['code'] ?></td>
                    <td><?= $paper['paper']['name'] ?></td>
                    <td><input type="checkbox" class="exemptionpapers" name="exemptionpapers" value="<?= $paper['paper']['code'] ?>"></td>
                    <td><?= Html::a('<span class="glyphicon glyphicon-ok"></span>', $url, ['data-whatever' => $url, 'data-toggle' => "modal", 'data-target' => "#VerifyExemptionRedirectModal", 'data-type' => $paper['paper']['code'], 'title' => Yii::t('yii', 'Verify'),]) ?></td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>