<?php

use yii\data\ArrayDataProvider;
use yii\grid\GridView;
?>
<?=

GridView::widget([

    'dataProvider' => new ArrayDataProvider([ 'allModels' => $students, ]),
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'label' => 'Student',
            'value' => function($data) {
                return $data->studentId['firstName']." ".$data->studentId['middleName']." ".$data->studentId['lastName'];
            }
        ],
        [
            'label' => 'Registration Number',
            'value' => function($data) {
                return $data->fullRegNo;
            }
        ],
        [
            'attribute' => 'qualification',
            'value' => function($data) {
                return $data->type;
            }
        ],
        [
            'label' => 'Verified By',
            'value' => function($data) {
                return !empty($data->verifiedBy) ? $data->verifiedBy['firstName']." ".$data->verifiedBy['otherNames'] : "-";
            }
        ],
        ],
    ]);
?>