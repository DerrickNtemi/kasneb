<?php

use yii\data\ArrayDataProvider;
use yii\grid\GridView;

?>
<?=

GridView::widget([
    'dataProvider' => new ArrayDataProvider(['allModels' => $datas]),
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'label' => 'Student',
            'value' => function($data) {
                return $data->student['firstName']." ".$data->student['middleName']." ".$data->student['lastName'];
            }
        ],
        [
            'label' => 'Declaration',
            'value' => function($data) {
                return $data->declaration['description'];
            }
        ],
        [
            'label' => 'Response',
            'value' => function($data) {
                return $data->response ? "Yes" : "No";
            }
        ],
        [
            'label' => 'Specification',
            'value' => function($data) {
                return $data->specification;
            }
        ],
                
    ],
]);
?>