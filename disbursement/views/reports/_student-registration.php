<?php

use yii\data\ArrayDataProvider;
use yii\grid\GridView;

?>
<?=

GridView::widget([
    'dataProvider' => new ArrayDataProvider(['allModels' => $datas]),
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'firstName',
        'middleName',
        'lastName',        
        [
            'label' => 'Gender',
            'value' => function($data) {
                return $data->gender == 1 ? "Male" : "Female";
            }
        ],
                'phoneNumber',
                
    ],
]);
?>