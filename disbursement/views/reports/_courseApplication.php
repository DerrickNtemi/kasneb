<?php

use yii\data\ArrayDataProvider;
use yii\grid\GridView;
?>
<?=

GridView::widget([

    'dataProvider' => new ArrayDataProvider([
        'allModels' => $students,
            ]),
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'label' => 'Name',
            'value' => function($data) {
                return $data->studentId['firstName']." ".$data->studentId['middleName']." ".$data->studentId['lastName'];
            }
        ],
        [
            'label' => 'Reg. Date',
            'value' => function($data) {
                return $data->created;
            }
        ],
        [
            'label' => 'Course',
            'value' => function($data) {
                return $data->courseId['name'];
            }
        ],
        [
            'label' => 'Verified By',
            'value' => function($data) {
                return !empty($data->verifiedBy) ? $data->verifiedBy['firstName']." ".$data->verifiedBy['otherNames'] : "-";
            }
        ],
        ],
    ]);
?>
