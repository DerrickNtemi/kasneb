<?php

use yii\data\ArrayDataProvider;
use yii\grid\GridView;

?>
<?=

GridView::widget([
    'dataProvider' => new ArrayDataProvider(['allModels' => $datas]),
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],     
        [
            'label' => 'Name',
            'value' => function($data) {
                return $data->student['firstName']." ".$data->student['middleName']." ".$data->student['lastName'];
            }
        ],   
        [
            'label' => 'Registration Number',
            'value' => function($data) {
                return $data->fullRegNo;
            }
        ],
        [
            'label' => 'Sitting',
            'value' => function($data) {
                return $data->sitting['sittingPeriod'];
            }
        ],
        [
            'label' => 'Year',
            'value' => function($data) {
                return $data->sitting['sittingYear'];
            }
        ],
        [
            'label' => 'Center',
            'value' => function($data) {
                return $data->sittingCentre['name'];
            }
        ],
        'status',                
    ],
]);
?>