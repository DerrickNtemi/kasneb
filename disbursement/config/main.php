<?php

$params = array_merge(
        require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-disbursement',
    'name'=>'DISBURSEMENT PORTAL',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'disbursement\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'components' => [
        'request' => [
             'baseUrl'=>'/kasneb/disbursement',
            'csrfParam' => '_csrf-disbursement',
        ],
        'user' => [
            'identityClass' => 'common\models\UserIdentity',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-disbursement', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the administrator
            'name' => 'advanced-disbursement',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
             'baseUrl'=>'/kasneb/disbursement',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@app/views' => '@vendor/dmstr/yii2-adminlte-asset/example-views/yiisoft/yii2-app'
                ],
            ],
        ],
    ],
    'params' => $params,
];
