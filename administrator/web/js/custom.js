var errorMessage = 'ERROR CODE 400 :: An error occurred while processing your request, please try again or contact system administrator for assistance..';
var errorNotFound = "ERROR CODE 404 :: The requested resource was not found";
var errorInternalServer = "ERROR CODE 500 :: Internal server error, please try again or contact system administrator for assistance..";

$(document).ready(function () {

    $(window).load(function () {
        var pageHeight = $(document).height();
        var mainHeaderHeight = $('.main-header-div').height();
        var contentHeight = pageHeight - mainHeaderHeight - 60;
        $('.menu-div').css('min-height', contentHeight);
    });

    $('#loadingDiv').hide();
    $('#rootwizard').bootstrapWizard({
        onNext: function (tab, navigation, index) {
            var total = (navigation.find('li').length) - 1;
            if (index === 1) {
                var form = $('#account-details-form');
                var url = form.attr('action');
                var data = new FormData($(form)[0]);
                silentWizardCommunication(url, data, "#rootwizard", index, total, "#profile-error-summary");
                return false;
            } else if (index === 2) {
                var form = $('#contact-details-form');
                var url = form.attr('action');
                var data = new FormData($(form)[0]);
                silentWizardCommunication(url, data, "#rootwizard", index, total, "#contact-error-summary");
                return false;
            } else if (index === 3) {
                var proff = $(".course-type-proff");
                var proffClass = proff.attr('class');
                var selectedCourse = proffClass.indexOf("active");
                if (selectedCourse !== -1) {
                    courseType = 100;
                } else {
                    courseType = 200;
                }
                var form = $('#course-application-form');
                var url = form.attr('action') + "?courseType=" + courseType;
                var data = new FormData($(form)[0]);
                silentWizardCommunication(url, data, "#rootwizard", index, total, "#application-error-summary");
                return false;
            } else if (index === 4) {
                var form = $('#more-details-form');
                var declarations = new Array();
                var i = 0;
                $("input[name='Declarations[declarationId][]']:checkbox").each(function () {
                    var declarationVal = (this.checked ? true : false);
                    declarations[i] = [$(this).val(), declarationVal];
                    i++;
                });
                var url = form.attr('action');
                $('#declarations-selection').val(declarations);
                var data = new FormData($(form)[0]);
                silentWizardCommunication(url, data, "#rootwizard", index, total, "#contact-error-summary");
                return false;
            }
        },
        onTabShow: function (tab, navigation, index) {
            var total = (navigation.find('li').length);
            var percent = ((index + 1) / total) * 100;
            $('#rootwizard').find('.bar').css({width: percent + '%'});
            $('#rootwizard').find('.bar').text(Math.round(percent) + '% Complete');
            // If it's the last tab then hide the last button and show the finish instead
            if ((index + 1) >= total) {
                $('#rootwizard').find('.pager .next').hide();
                $('#rootwizard').find('.pager .finish').show();
                $('#rootwizard').find('.pager .finish').removeClass('disabled');
            } else {
                $('#rootwizard').find('.pager .next').show();
                $('#rootwizard').find('.pager .finish').hide();
            }
        },
        onTabClick: function (tab, navigation, index) {
            //return false;
        }
    });

    $('#examinationwizard').bootstrapWizard({
        onNext: function (tab, navigation, index) {
            var total = (navigation.find('li').length) - 1;
            if (index === 1) {
                var form = $('#examination-sitting-form');
                var url = form.attr('action');
                var data = new FormData($(form)[0]);
                silentWizardCommunication(url, data, "#examinationwizard", index, total, "#sitting-error-summary");
                return false;
            } else if (index === 2) {
                var form = $('#examination-paper-form');
                var url = form.attr('action');
                var data = new FormData($(form)[0]);
                silentWizardCommunication(url, data, "#examinationwizard", index, total, "#papers-error-summary");
                return false;
            } else if (index === 3) {
                var proff = $(".course-type-proff");
                var proffClass = proff.attr('class');
                var selectedCourse = proffClass.indexOf("active");
                if (selectedCourse !== -1) {
                    courseType = 100;
                } else {
                    courseType = 200;
                }
                var form = $('#course-application-form');
                var url = form.attr('action') + "?courseType=" + courseType;
                var data = new FormData($(form)[0]);
                silentWizardCommunication(url, data, "#rootwizard", index, total, "#application-error-summary");
                return false;
            } else if (index === 4) {
                var form = $('#more-details-form');
                var declarations = new Array();
                var i = 0;
                $("input[name='Declarations[declarationId][]']:checkbox").each(function () {
                    var declarationVal = (this.checked ? true : false);
                    declarations[i] = [$(this).val(), declarationVal];
                    i++;
                });
                var url = form.attr('action');
                $('#declarations-selection').val(declarations);
                var data = new FormData($(form)[0]);
                silentWizardCommunication(url, data, "#rootwizard", index, total, "#contact-error-summary");
                return false;
            }
        },
        onTabShow: function (tab, navigation, index) {
            var total = (navigation.find('li').length);
            var percent = ((index + 1) / total) * 100;
            $('#rootwizard').find('.bar').css({width: percent + '%'});
            $('#rootwizard').find('.bar').text(Math.round(percent) + '% Complete');
            // If it's the last tab then hide the last button and show the finish instead
            if ((index + 1) >= total) {
                $('#rootwizard').find('.pager .next').hide();
                $('#rootwizard').find('.pager .finish').show();
                $('#rootwizard').find('.pager .finish').removeClass('disabled');
            } else {
                $('#rootwizard').find('.pager .next').show();
                $('#rootwizard').find('.pager .finish').hide();
            }
        },
        onTabClick: function (tab, navigation, index) {
            return false;
        }
    });



    $('#AdminUserModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var modal = $(this);
        var data = button.data('whatever');
        if (data !== "" && data !== undefined) {
            modal.find('#admin-user-btn').text("Update");
            modal.find('#admin-user-modal-header').text("Update User");
            modal.find('.modal-body #adminuser-id').val(data.id);
            modal.find('.modal-body #adminuser-email').val(data.email);
            modal.find('.modal-body #adminuser-phonenumber').val(data.phoneNumber);
            modal.find('.modal-body #adminuser-firstname').val(data.firstName);
            modal.find('.modal-body #adminuser-othernames').val(data.otherNames);
            modal.find('.modal-body #adminuser-role').val(data.role.id);
        } else if (data !== undefined) {
            modal.find('#admin-user-btn').text("Create");
            modal.find('#admin-user-modal-header').text("Create User");
            modal.find('.modal-body #adminuser-id').val(0);
            modal.find('.modal-body #adminuser-email').val("");
            modal.find('.modal-body #adminuser-phonenumber').val("");
            modal.find('.modal-body #adminuser-firstname').val("");
            modal.find('.modal-body #adminuser-othernames').val("");
            modal.find('.modal-body #adminuser-role').val("");
        }
    });

    $('#NewInstitutionModal').on('show.bs.modal', function (event) {
        var modal = $(this);
        modal.find('.modal-body #institutions-coursetype').val("");
        modal.find('.modal-body #institutions-name').val("");
    });


    $('#NewExemptionCourseModal').on('show.bs.modal', function (event) {
        var modal = $(this);
        modal.find('.modal-body exemptioncourse-coursename').val("");
    });
    
    $('#DeclarationModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var modal = $(this);
        var data = button.data('whatever');
        if (data !== "") {
            modal.find('#register-declaration-btn').text("Update");
            modal.find('#create-declaration-modal-header').text("Update Declaration");
            modal.find('.modal-body #admindeclarations-id').val(data.id);
            modal.find('.modal-body #admindeclarations-description').val(data.description);
        } else {
            modal.find('#register-declaration-btn').text("Create");
            modal.find('#create-declaration-modal-header').text("Create Declaration");
            modal.find('.modal-body #admindeclarations-id').val(0);
            modal.find('.modal-body #admindeclarations-description').val("");
        }
    });


    $('#DeleteRedirectModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var modal = $(this);
        var url = button.data('whatever');
        modal.find('.modal-body #url').text(url);
        modal.find('.modal-body #error-summary').hide();
        modal.find('.modal-body #progress-spinner').hide();
    });

    $('#delete-redirect-btn').on('click', function () {
        var deleteModal = $('#DeleteRedirectModal');
        var url = deleteModal.find('.modal-body #url').text();
        var password = deleteModal.find('.modal-body #password').val();
        $.ajax({
            url: url,
            type: 'POST',
            data: {password: password},
            beforeSend: function () {
                deleteModal.find('.modal-body #progress-spinner').show();
                deleteModal.find('.modal-body #error-summary').hide();
            },
            complete: function () {
                deleteModal.find('.modal-body #progress-spinner').hide();
            },
            success: function (data) {
                if (typeof data.success !== 'undefined') {
                    deleteModal.modal('hide');
                    deleteModal.removeData('modal');
                    window.location = data.success;
                } else {
                    deleteModal.find('.modal-body #error-summary').show().html(data.error);
                }
            },
            error: function () {
                deleteModal.find('.modal-body #error-summary').show().html(errorMessage);
            },
            statusCode: {
                404: function () {
                    deleteModal.find('.modal-body #error-summary').show().html(errorNotFound);
                },
                500: function () {
                    deleteModal.find('.modal-body #error-summary').show().html(errorInternalServer);
                }
            }
        });
    });

    $('#admin-user-btn').on('click', function () {
        var activeModalId = "#AdminUserModal";
        var submitForm = "#admin-user-form";
        silentCommunicationWithRedirect(activeModalId, submitForm, $(this));
    });

    $('#register-declaration-btn').on('click', function () {
        var activeModalId = "#DeclarationModal";
        var submitForm = "#declaration-model-form";
        silentCommunicationWithRedirect(activeModalId, submitForm, $(this));
    });

    $('#new-institution-btn').on('click', function () {
        var activeModalId = "#NewInstitutionModal";
        var submitForm = "#exemption-institution-form";
        silentCommunicationWithRedirect(activeModalId, submitForm, $(this));
    });

    $('#new-course-btn').on('click', function () {
        var activeModalId = "#NewExemptionCourseModal";
        var submitForm = "#exemption-course-papers-form";
        silentCommunicationWithRedirect(activeModalId, submitForm, $(this));
    });

    $('#new-institution-course-paper-btn').on('click', function () {
        var activeModalId = "#NewExemptionCoursePaperModal";
        var submitForm = "#exemption-institution-course-papers-form";
        silentCommunicationWithRedirect(activeModalId, submitForm, $(this));
    });

    function silentCommunicationWithRedirect(activeModalId, submitForm, button) {
        var activeModal = $(activeModalId);
        var form = $(submitForm);
        var formData = new FormData(form[0]);
        $.ajax({
            url: form.attr('action'),
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
                activeModal.find('.modal-body #progress-spinner').show();
                activeModal.find('.modal-body #error-summary').hide();
                button.attr('disabled', true);
            },
            complete: function () {
                activeModal.find('.modal-body #progress-spinner').hide();
            },
            success: function (data) {
                if (typeof data.success !== 'undefined') {
                    activeModal.modal('hide');
                    activeModal.removeData('modal');
                    window.location = data.success;
                    button.attr('disabled', false);
                } else {
                    activeModal.find('.modal-body #error-summary').show().html(data.error);
                    button.attr('disabled', false);
                }
            },
            error: function () {
                activeModal.find('.modal-body #error-summary').show().html(errorMessage);
                button.attr('disabled', false);
            },
            statusCode: {
                404: function () {
                    activeModal.find('.modal-body #error-summary').show().html(errorNotFound);
                    button.attr('disabled', false);
                },
                500: function () {
                    activeModal.find('.modal-body #error-summary').show().html(errorInternalServer);
                    button.attr('disabled', false);
                }
            }
        });
    }

    function silentWizardCommunication(url, data, wizard, index, total, errorId) {
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $('#loadingDiv').show();
                $(errorId).hide();
            },
            complete: function () {
                $('#loadingDiv').hide();
            },
            success: function (data) {
                if (typeof data.success !== 'undefined') {
                    if (index === total) {
                        $('#tab' + (index + 1)).html(data.success);
                    }
                    $(wizard).bootstrapWizard('show', index);
                    $(errorId).hide();
                } else {
                    $(errorId).show().html(data.error);
                }
            },
            error: function () {
                $(errorId).show().html(errorMessage);
            },
            statusCode: {
                404: function () {
                    $(errorId).show().html(errorNotFound);
                },
                500: function () {
                    $(errorId).show().html(errorInternalServer);
                }
            }
        });
    }

    function silentWizardRedirectComm(url, data, errorDiv) {
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            beforeSend: function () {
                $('#loadingDiv').show();
                $(errorDiv).hide();
            },
            complete: function () {
                $('#loadingDiv').hide();
            },
            success: function (data) {
                if (typeof data.success !== 'undefined') {
                    window.location = data.success.url;
                } else {
                    $(errorDiv).show().html(data.error);
                }
            },
            error: function () {
                $(errorDiv).show().html(errorMessage);
            },
            statusCode: {
                404: function () {
                    $(errorDiv).show().html(errorNotFound);
                },
                500: function () {
                    $(errorDiv).show().html(errorInternalServer);
                }
            }
        });
    }

    function silentCommReturnVal(url, data, errorId, inputId) {
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $('#loadingDiv').show();
                $(errorId).hide();
            },
            complete: function () {
                $('#loadingDiv').hide();
            },
            success: function (data) {
                if (typeof data.success !== 'undefined') {
                    $(inputId).val(data.success);
                } else {
                    $(errorId).show().html(data.error);
                }
            },
            error: function () {
                $(errorId).show().html(errorMessage);
            },
            statusCode: {
                404: function () {
                    $(errorId).show().html(errorNotFound);
                },
                500: function () {
                    $(errorId).show().html(errorInternalServer);
                }
            }
        });
    }

    $(document).on('click', "#jambopay-pay", function () {
        var pin = $("#wallet-pin-pay").val();
        var form = $('#bill-details-form');
        var url = form.attr('action');
        var data = {pin: pin};
        silentWizardRedirectComm(url, data, "#bill-error-summary");
    });

    $("#remarks").change(function () {
        if ($(this).val() === "REJ_6") {
            $("#other-reasons").show();
        }else{
            $("#other-reasons").hide();
        }
    });

    $("#sitting-month").change(function () {
        var form = $('#examination-sitting-form');
        var url = form.attr('action') + "-get-year?month=" + $(this).val();
        var data = "";
        silentCommReturnVal(url, data, "#sitting-error-summary", '#sitting-year');
    });

    $("#declarations-month").change(function () {
        var form = $('#more-details-form');
        var url = form.attr('action') + "-get-year?month=" + $(this).val();
        var data = "";
        silentCommReturnVal(url, data, "#declaration-error-summary", '#declarations-year');
    });


    $(document).on('change', "#bill-methods", function () {
        if ($(this).val() === "1") {
            $('.jp').show();
            $('.paybill').hide();
            $('.bankbill').hide();
        } else if ($(this).val() === "2") {
            $('.jp').hide();
            $('.paybill').show();
            $('.bankbill').hide();
        } else if ($(this).val() === "3") {
            $('.jp').hide();
            $('.paybill').hide();
            $('.bankbill').show();
        }
    });

    $('#VerifyRedirectModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var modal = $(this);
        var data = button.data('whatever');
        var type = button.data('type');
        var datatosend = "";
        var dataType = type;
        //if  batch verification button is clicked
        if (type === "all") {
            if (!$("input[name='id_all']:checkbox").is(':checked')) {
                $("input[name='id[]']:checkbox").each(function () {
                    if ($(this).is(':checked')) {
                        console.log($(this).val());
                        datatosend += this.value + ",";
                    }
                });
                dataType = "partial";
            }
        }
        data = data + "&type=" + dataType;
        modal.find('.modal-body #action-url').text(data);
        modal.find('.modal-body #div-datatosend').text(datatosend);
        modal.find('.modal-body #modalid').text("#VerifyRedirectModal");
    });

    $('#VerifyExemptionRedirectModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var modal = $(this);
        var data = button.data('whatever');
        var type = button.data('type');
        var datatosend = "";
        //if all partial is clicked
        if (type === "all-partial") {
            $("input[name='exemptionpapers']:checkbox").each(function () {
                if (this.checked) {
                    datatosend += this.value + ",";
                }
            });
        }
        //if neither all summary nor all partial is clicked
        if (type !== "all-summary" && type !== "all-partial") {
            datatosend = type;
        }
        modal.find('.modal-body #action-url').text(data);
        modal.find('.modal-body #div-datatosend').text(datatosend);
        modal.find('.modal-body #modalid').text("#VerifyExemptionRedirectModal");
    });

    $('#exemptionpapersall').change(function () {
        if ($(this).prop('checked')) {
            $('.exemptionpapers').prop('checked', true);
        } else {
            $('.exemptionpapers').prop('checked', false);
        }
    });


    $('#verify-redirect-btn').on('click', function () {
        var url = $('#action-url').text();
        var datatosend = $('#div-datatosend').text();
        var checked = $("input[name=verify]:checked").val();
        var remarks = $('#remarks').val();
        var otherremarks = $('#otherremarks').val();
        var password = $('#password').val();
        var modal = $($('#modalid').text());
        $.ajax({
            url: url,
            method: 'POST',
            data: {checked: checked, remarks: remarks, password: password, datatosend: datatosend, otherremarks: otherremarks},
            beforeSend: function () {
                modal.find('.modal-body #progress-spinner').show();
                modal.find('.modal-body #error-summary').hide();
            },
            complete: function () {
                modal.find('.modal-body #progress-spinner').hide();
            },
            success: function (data) {
                if (typeof data.success !== 'undefined') {
                    window.location = data.success.url;
                } else {
                    modal.find('.modal-body #error-summary').show().html(data.error);
                }
            },
            error: function () {
                modal.find('.modal-body #error-summary').show().html(errorMessage);
            },
            statusCode: {
                404: function () {
                    modal.find('.modal-body #error-summary').show().html(errorNotFound);
                },
                500: function () {
                    modal.find('.modal-body #error-summary').show().html(errorInternalServer);
                }
            }
        });
    });

    $("input[name='verify']").click(function () {
        if ($(this).val() === "REJECTED") {
            $("#rejection-div").show();
        } else {
            $("#rejection-div").hide();
        }
    });

    $('#filter-students').on('click', function () {
        var form = $('#filter-details-form');
        var url = form.attr('action');
        var startdate = $("#filter-startdate").val();
        var enddate = $("#filter-enddate").val();
        window.location = url + '?k=' + startdate + "&k1=" + enddate;
    });

    $('#excel-students').on('click', function () {
        var form = $('#filter-details-form');
        var url = form.attr('action');
        var startdate = $("#filter-startdate").val();
        var enddate = $("#filter-enddate").val();
        window.location = url + '-export?k=' + startdate + "&k1=" + enddate;
    });

    $('#filter-audit-trail').on('click', function () {
        var form = $('#filter-audit-trail-form');
        var url = form.attr('action');
        var startdate = $("#filter-startdate").val();
        var enddate = $("#filter-enddate").val();
        var user = $("#filter-user").val();
        window.location = url + '?f=' + user + '&f1=' + startdate + "&f2=" + enddate;
    });

    $('#filter-exam-booking').on('click', function () {
        var form = $('#filter-details-form');
        var url = form.attr('action');
        var coursetype = $("#filter-coursetype").val();
        var course = $("#filter-course").val();
        var year = $("#filter-year").val();
        var month = $("#filter-month").val();
        var center = $("#filter-center").val();
        var zone = $("#filter-zone").val();
        var status = $("#filter-status").val();
        window.location = url + '?k=' + month + "&k1=" + year + "&k2=" + coursetype + "&k3=" + course + "&k4=" + zone + "&k5=" + center + "&k6=" + status;
    });

    $('#excel-examination').on('click', function () {
        var form = $('#filter-details-form');
        var url = form.attr('action');
        var coursetype = $("#filter-coursetype").val();
        var course = $("#filter-course").val();
        var year = $("#filter-year").val();
        var month = $("#filter-month").val();
        var center = $("#filter-center").val();
        var zone = $("#filter-zone").val();
        var status = $("#filter-status").val();
        window.location = url + '-export?k=' + month + "&k1=" + year + "&k2=" + coursetype + "&k3=" + course + "&k4=" + zone + "&k5=" + center + "&k6=" + status;
    });

    $('#filter-payments').on('click', function () {
        var form = $('#filter-details-form');
        var url = form.attr('action');
        var filter = $("#filter-stream").val();
        var startdate = $("#filter-startdate").val();
        var enddate = $("#filter-enddate").val();
        var mode = $("#filter-mode").val();
        window.location = url + '?k=' + startdate + "&k1=" + enddate + "&k2=" + filter + "&k3=" + mode;
    });

    $('#excel-payments').on('click', function () {
        var form = $('#filter-details-form');
        var url = form.attr('action');
        var filter = $("#filter-stream").val();
        var startdate = $("#filter-startdate").val();
        var enddate = $("#filter-enddate").val();
        var mode = $("#filter-mode").val();
        window.location = url + '-export?k=' + startdate + "&k1=" + enddate + "&k2=" + filter + "&k3=" + mode;
    });

    $('#filter-course-application').on('click', function () {
        var form = $('#filter-details-form');
        var url = form.attr('action');
        var startdate = $("#filter-startdate").val();
        var enddate = $("#filter-enddate").val();
        var status = $("#filter-coursestatus").val();
        var user = $("#filter-user").val();
        window.location = url + '?k=' + startdate + "&k1=" + enddate + "&k2=" + status + "&k3=" + user;
    });

    $('#excel-course-application').on('click', function () {
        var form = $('#filter-details-form');
        var url = form.attr('action');
        var startdate = $("#filter-startdate").val();
        var enddate = $("#filter-enddate").val();
        //var status = $("#filter-coursestatus").val();
        var user = $("#filter-user").val();
        window.location = url + '-export?k=' + startdate + "&k1=" + enddate + "&k2=" + user;
    });

    $('#filter-dashboard').on('click', function () {
        var form = $('#filter-details-form');
        var url = form.attr('action');
        var filter = $("#filter-stream").val();
        var startdate = $("#filter-startdate").val();
        var enddate = $("#filter-enddate").val();
        window.location = url + '?k=' + startdate + "&k1=" + enddate + "&k2=" + filter;
    });
    $('#filter-exemption').on('click', function () {
        var form = $('#filter-details-form');
        var url = form.attr('action');
        var startdate = $("#filter-startdate").val();
        var enddate = $("#filter-enddate").val();
        var status = $("#filter-coursestatus").val();
        var user = $("#filter-user").val();
        window.location = url + '?k=' + startdate + "&k1=" + enddate + "&k2=" + status + "&k3=" + user;
    });
    $('#filter-declarations').on('click', function () {
        var form = $('#filter-details-form');
        var url = form.attr('action');
        var declrations = $("#filter-declrations").val();
        var response = $("#filter-response").val();
        window.location = url + '?k=' + declrations + "&k1=" + response;
    });
    $('#excel-exemption').on('click', function () {
        var form = $('#filter-details-form');
        var url = form.attr('action');
        var startdate = $("#filter-startdate").val();
        var enddate = $("#filter-enddate").val();
        //var status = $("#filter-coursestatus").val();
        var user = $("#filter-user").val();
        window.location = url + '-export?k=' + startdate + "&k1=" + enddate + "&k2=" + user;
    });


    $('#batch-verify-redirect-btn').on('click', function () {
        var url = $('#action-url').text();
        var datatosend = $('#div-datatosend').text();
        var checked = $("input[name=verify]:checked").val();
        var remarks = $('#remarks').val();
        var password = $('#password').val();
        var modal = $($('#modalid').text());
        $.ajax({
            url: url,
            method: 'POST',
            data: {checked: checked, remarks: remarks, password: password, datatosend: datatosend},
            beforeSend: function () {
                modal.find('.modal-body #progress-spinner').show();
                modal.find('.modal-body #error-summary').hide();
            },
            complete: function () {
                modal.find('.modal-body #progress-spinner').hide();
            },
            success: function (data) {
                if (typeof data.success !== 'undefined') {
                    window.location = data.success.url;
                } else {
                    modal.find('.modal-body #error-summary').show().html(data.error);
                }
            },
            error: function () {
                modal.find('.modal-body #error-summary').show().html(errorMessage);
            },
            statusCode: {
                404: function () {
                    modal.find('.modal-body #error-summary').show().html(errorNotFound);
                },
                500: function () {
                    modal.find('.modal-body #error-summary').show().html(errorInternalServer);
                }
            }
        });
    });


    $("#filter-zone").change(function () {
        var form = $('#filter-details-form');
        var data = "";
        var url = form.attr('action') + "-filter?zone=" + $(this).val();
        silentCommReturnDropdownList(url, data, "#center-selection-error-summary", '#filter-center');
    });

    $("#exemptioncoursepapers-papers").change(function () {
        var form = $('#exemption-institution-course-papers-form');
        var data = "";
        var url = form.attr('action') + "-filter?courseId=" + $(this).val();
        silentCommReturnDropdownList(url, data, "#error-summary", '#exemptioncoursepapers-name');
    });

    function silentCommReturnDropdownList(url, data, errorId, inputId) {
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
                if (errorId === "#error-summary") {
                    $('#progress-spinner').show();
                    $('#error-summary').hide();
                } else {
                    $('#loadingDiv').show();
                    $(errorId).hide();
                }
            },
            complete: function () {
                if (errorId === "#error-summary") {
                    $('#progress-spinner').hide();
                } else {
                    $('#loadingDiv').hide();
                }
            },
            success: function (data) {
                if (typeof data.success !== 'undefined') {
                    $(inputId).find('option').remove().end();
                    $.each(data.success, function (i, value) {
                        $(inputId).append($('<option>').text(value).attr('value', i));
                    });

                } else {
                    $(errorId).show().html(data.error);
                }
            },
            error: function () {
                $(errorId).show().html(errorMessage);
            },
            statusCode: {
                404: function () {
                    $(errorId).show().html(errorNotFound);
                },
                500: function () {
                    $(errorId).show().html(errorInternalServer);
                }
            }
        });
    }

});