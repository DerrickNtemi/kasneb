/*
 * $(window).load(function () {
 $('.card-1').trigger('click');
 });
 
 $('.card-1').click(function(){ //clicking card one for loan value
 loanGraph(1);
 });
 
 $('.card-2').click(function(){ //clicking card one for loan value
 loanGraph(2);
 });
 
 $('.card-3').click(function(){ //clicking card one for loan value
 loanGraph(3);
 });
 
 
 function loanGraph(item){
 url = baseUrl + '/site/graph-data?item='+item;
 $.getJSON(url, function (result) {  
 var options = {
 chart: {
 zoomType: 'xy',
 renderTo: 'graph-container',
 type: 'column',
 },
 title: {
 text: result.title
 },
 yAxis: {
 title: {
 text: result.yAxis
 },
 },
 tooltip: {
 shared: true
 },
 xAxis: {
 categories: result.axis,
 crosshair: true
 },
 labels: {
 items: [{
 html: '',
 style: {
 left: '50px',
 top: '18px',
 color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
 }
 }]
 },
 series: result.data
 }
 
 var hiChart = new Highcharts.Chart(options);
 });
 }
 
 */

function loanGraph(result) {
    var options = {
        chart: {
            zoomType: 'xy',
            renderTo: 'graph-container',
            type: 'column'
        },
        title: {
            text: result.title
        },
        yAxis: {
            title: {
                text: result.yAxis
            },
            crosshair: true
        },
        tooltip: {
            shared: true
        },
        xAxis: {
            categories: result.axis,
            crosshair: true
        },
        labels: {
            items: [{
                    html: '',
                    style: {
                        left: '50px',
                        top: '18px',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                    }
                }]
        },
        series: result.data
    };
    new Highcharts.Chart(options);
}


function pieChartOne(result) {
    
    Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
        return {
            radialGradient: {
                cx: 0.5,
                cy: 0.3,
                r: 0.7
            },
            stops: [
                [0, color],
                [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
            ]
        };
    });
    
    var options = {
        chart: {
            renderTo : "total-pie-chart",
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Total Revenue'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y:.2f}',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    },
                    connectorColor: 'silver'
                }
            }
        },
        series: [{
            name: 'Revenue',
            data: [
                { name: 'Reg', y: result.registration },
                {
                    name: 'Renewal',
                    y: result.renewal 
                },
                { 
                    name: 'Exam', 
                    y: result.exam,  
                    sliced: true,
                    selected: true
                },
                { name: 'Exemption', y: result.exemption  }
            ]
        }]
    };
    new Highcharts.Chart(options);
}


function pieChartTwo(result) {    
    var options = {
    chart: {
            type: 'pie',
            renderTo : "visitors-pie-chart",
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        title: {
            text: 'Visitors'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 50,
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Browser share',
            data: [
                ['Mobile', 45.0],
                {
                    name: 'Web Portal',
                    y: 55.0,
                    sliced: true,
                    selected: true
                },
            ]
        }]
    };
    new Highcharts.Chart(options);
}