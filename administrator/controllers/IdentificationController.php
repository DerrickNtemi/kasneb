<?php

namespace administrator\controllers;

use common\models\CourseDisplay;
use common\models\Service;
use common\models\Student;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

/**
 * Site controller
 */
class IdentificationController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionView($id) {
        $service = new Service();
        $studentModel = new Student();
        $studentModel->setAttributes($service->getStudentData($id));
        $courses = null;
        foreach ($studentModel->studentCourses as $course) {
            if (!$course['verified']) {
                $courses = $course;
            }
        }
        $remarks = $service->rejectionReasonsRegistration();
        return $this->render('view', ['model' => $studentModel, 'courses' => $courses, 'remarks' => $remarks]);
    }

    public function actionIndex() {
        $service = new Service();
        $studentModel = new Student();
        $studentsIdentifications = $service->getStudentsPendingIdentifications();

        $registrations = [];
        foreach ($studentsIdentifications as $courses) {
            $courseModel = new CourseDisplay();
            $courseModel->registrationNumber = $courses['fullRegistrationNumber'];
            $courseModel->id = $courses['id'];
            $courseModel->created = $courses['created'];
            $courseModel->studentId = $courses['studentObj'];
            $courseModel->courseId = $courses['course'];
            $registrations[] = $courseModel;
        }
        return $this->render('pending', ['model' => $studentModel, 'datas' => $registrations]);
    }

    public function actionVerify($courseId, $courseStatus) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $service = new Service();
        $model = new CourseDisplay();
        $identity = Yii::$app->user->getIdentity();
        $data = ['id' => $courseId, 'courseStatus' => $courseStatus,'verifiedBy'=>['id'=>$identity->user['id']]];
        $response = (object) $service->verifySignupIdentification($data);
        if ($response->status['code'] == 1) {
            Yii::$app->session->setFlash('success-message', 'Verification process was successful...');
            return $this->redirect(['identification/index']);
        } else {
            $model->addError("", "ERROR CODE " . $response->status['code'] . " :: " . $response->status['message']);
        }
        $errorsArry = '';
        foreach ($model->getErrors() as $error) {
            $errorsArry .= $error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

}
