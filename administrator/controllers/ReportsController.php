<?php

namespace administrator\controllers;

use common\models\Service;
use common\models\Student;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\Filter;
use common\models\Payments;
use common\models\CourseDisplay;
use common\models\ExemptionVerification;
use common\models\ExaminationBooking;
use common\models\DeclarationReport;
use yii\web\Response;
use Yii;

/**
 * Site controller
 */
class ReportsController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionExaminationBooking($k = "", $k1 = "", $k2 = "", $k3 = "", $k4 = "", $k5 = "", $k6 = "") {
        $filterModel = new Filter();
        $filterModel->month = empty($k) ? "" : $k;
        $filterModel->year = empty($k1) ? "" : $k1;
        $filterModel->courseType = empty($k2) ? "" : $k2;
        $filterModel->course = empty($k3) ? "" : $k3;
        $filterModel->zone = empty($k4) ? "" : $k4;
        $filterModel->center = empty($k5) ? "" : $k5;
        $filterModel->status = empty($k6) ? "" : $k6;

        $service = new Service();
        $courseTypes = $service->courseTypes();
        $status = $service->getStatus();
        $sittingYears = $service->sittingYears();
        $sittingMonths = $service->sittingMonths();
        $coursesData = $service->getCourseType();
        $courses[0] = "All";
        foreach ($coursesData as $requirement) {
            foreach ($requirement['courseCollection'] as $course) {
                $courses[$course['id']] = $course['name'];
            }
        }
        $exams = $service->getExamBookings($filterModel->courseType, $filterModel->course, $filterModel->year, $filterModel->month, $filterModel->center, $filterModel->status);
        $datas = [];
        foreach ($exams as $data) {
            $model = new ExaminationBooking();
            $model->setAttributes($data);
            $datas[] = $model;
        }

        $zoneData = $service->getZones();
        $zones = $centersArray = [];
        $i = $j = 0;
        foreach ($zoneData as $dt) {
            if ($i == 0 && empty($k4)) {
                $k4 = $dt['id'];
                $zones[''] = "All";
            }
            $i++;
            $zones[$dt['id']] = $dt['name'];
        }
        $sittingCenters = $service->getCenters($k4);

        foreach ($sittingCenters as $center) {
            if ($j == 0 && empty($k5)) {
                $centersArray[''] = "All";
            }
            $j++;
            $centersArray[$center['code']] = $center['name'];
        }

        return $this->render('examination-booking', ['status' => $status, 'zones' => $zones, 'centers' => $centersArray, 'datas' => $datas, 'filterModel' => $filterModel, 'courseTypes' => $courseTypes, 'sittingYears' => $sittingYears, 'sittingMonths' => $sittingMonths, 'courses' => $courses]);
    }

    public function actionExaminationBookingFilter($zone) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $service = new Service();
        $centersArray[] = "All";

        $sittingCenters = $service->getCenters($zone, "");
        foreach ($sittingCenters as $center) {
            $centersArray[$center['code']] = $center['name'];
        }
        return ['success' => $centersArray];
    }

    public function actionStudentRegistration($k = "", $k1 = "") {
        $currentMonth = date("m");
        $currentYear = date("Y");
        $startDate = empty($k) ? ($currentYear . "-" . $currentMonth . "-01") : $k;
        $endDate = empty($k1) ? date("Y-m-t", strtotime($startDate)) : $k1;
        $filterModel = new Filter();
        $filterModel->startDate = $startDate;
        $filterModel->endDate = $endDate;
        $service = new Service();
        $students = $service->getRegisteredStudents($startDate, $endDate);
        $datas = [];
        foreach ($students as $data) {
            $studentModel = new Student();
            $studentModel->setAttributes($data);
            $datas[] = $studentModel;
        }
        return $this->render('student-registration', ['datas' => $datas, 'filterModel' => $filterModel]);
    }

    public function actionPayments($k = "", $k1 = "", $k2 = "", $k3 = "") {
        $currentMonth = date("m");
        $currentYear = date("Y");
        $startDate = empty($k) ? ($currentYear . "-" . $currentMonth . "-01") : $k;
        $endDate = empty($k1) ? date("Y-m-t", strtotime($startDate)) : $k1;
        $filterModel = new Filter();
        $filterModel->startDate = $startDate;
        $filterModel->endDate = $endDate;

        $service = new Service();
        $revenueStreams = $service->revenueStream();
        $modes = $service->paymentMode();
        $dashboardData = $service->getDashboardTransactions($startDate, $endDate, $k2);
        $paymentData = [];
        foreach ($dashboardData as $payment) {
            if ($payment['feeCode'] != $k2 && !empty($k2)) {
                continue;
            }
            if ($k3 != $payment['channel'] && !empty($k3)) {
                continue;
            }
            $paymentModel = new Payments();
            $paymentModel->setAttributes($payment);
            $paymentModel->student = $payment['student'];
            $paymentData[] = $paymentModel;
        }
        return $this->render('payments', ['filterModel' => $filterModel, 'paymentData' => $paymentData, 'modes' => $modes, 'revenueStreams' => $revenueStreams]);
    }

    public function actionCourseApplications($k = "", $k1 = "", $k2 = "", $k3 = "") {
        $identity = Yii::$app->user->getIdentity();
        $currentMonth = date("m");
        $currentYear = date("Y");
        $filterModel = new Filter();
        $filterModel->startDate = empty($k) ? $currentYear . "-" . $currentMonth . "-01" : $k;
        $filterModel->endDate = empty($k1) ? date("Y-m-t", strtotime($filterModel->startDate)) : $k1;
        $filterModel->courseStatus = empty($k2) ? 2 : $k2;
        $filterModel->user = empty($k3) ? $identity->user['role']['description'] == 'superadmin' ? "" : $identity->id : $k3;

        $service = new Service();
        $applicationStatus = $service->courseApplicationStatus();
        $users = $service->getVerificationUsers();
        $studentsCourse = $service->getStudentsCourseApplication($filterModel->startDate, $filterModel->endDate, $filterModel->user);
        $registrations = [];
        $userData[] = 'All';
        foreach ($users as $user) {
            $userData[$user['id']] = $user['firstName'] . " " . $user['otherNames'];
        }

        foreach ($studentsCourse as $courses) {
            //print_r($courses); exit;
            if ($filterModel->courseStatus <= 1 && $filterModel->courseStatus != $courses['verified']) {
                continue;
            }
            $courseModel = new CourseDisplay();
            $courseModel->id = $courses['id'];
            $courseModel->verifiedBy = $courses['verifiedBy'];
            $courseModel->created = $courses['created'];
            $courseModel->dateVerified = $courses['dateVerified'];
            $courseModel->studentId = $courses['studentObj'];
            $courseModel->courseId = $courses['course'];
            $courseModel->verificationStatus = $courses['verificationStatus'];
            $registrations[] = $courseModel;
        }
        return $this->render('course-applications', ['userData' => $userData, 'filterModel' => $filterModel, 'registrations' => $registrations, 'applicationStatus' => $applicationStatus]);
    }

    public function actionDeclarations($k = "", $k1 = "") {
        $filterModel = new Filter();
        $filterModel->declrations = empty($k) ? "" : $k;
        $filterModel->response = empty($k1) ? "" : $k1;
        $service = new Service();
        $declarations = $service->getDeclarations();
        $datas = $service->getDeclarationReport($filterModel->declrations, $filterModel->response);
        $dataArray = [];
        $declarationsData[] = 'All';
        foreach ($declarations as $declaration) {
            $declarationsData[$declaration['id']] = $declaration['description'];
        }
        foreach ($datas as $data) {
            $modelData = new DeclarationReport();
            $modelData->setAttributes($data);
            $dataArray[] = $modelData;
        }
        return $this->render('declarations', ['filterModel' => $filterModel, 'dataArray' => $dataArray, 'declarationsData' => $declarationsData]);
    }

    public function actionExemptionApplications($k = "", $k1 = "", $k2 = 2, $k3 = "") {
        $identity = Yii::$app->user->getIdentity();
        $currentMonth = date("m");
        $currentYear = date("Y");
        $filterModel = new Filter();
        $filterModel->startDate = empty($k) ? $currentYear . "-" . $currentMonth . "-01" : $k;
        $filterModel->endDate = empty($k1) ? date("Y-m-t", strtotime($filterModel->startDate)) : $k1;
        $filterModel->courseStatus = $k2;
        $filterModel->user = empty($k3) ? $identity->user['role']['description'] == 'superadmin' ? "" : $identity->id : $k3;
        $service = new Service();
        $applicationStatus = $service->courseApplicationStatus();
        $users = $service->getVerificationUsers();
        $datas = $service->getExemptionsBookings($filterModel->startDate, $filterModel->endDate, $filterModel->user);
        //print_r($datas); exit;
        $dataArray = [];
        $userData[] = 'All';
        foreach ($users as $user) {
            $userData[$user['id']] = $user['firstName'] . " " . $user['otherNames'];
        }
        foreach ($datas as $data) {
            if ($k2 != 2 && $k2 != $data['verified']) {
                continue;
            }
            //print_r($data); exit;
            $modelData = new ExemptionVerification();
            $modelData->setAttributes($data);
            $modelData->studentId = $modelData->student;
            $dataArray[] = $modelData;
        }
        return $this->render('exemption-applications', ['userData' => $userData, 'filterModel' => $filterModel, 'dataArray' => $dataArray, 'applicationStatus' => $applicationStatus]);
    }

    public function actionPaymentsExport($k, $k1, $k2, $k3) {

        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=payments.csv');

        // create a file pointer connected to the output stream
        $output = fopen('php://output', 'w');

        // output the column headings
        fputcsv($output, array('', 'Student', 'Registration Number', 'Channel', 'Phone Number', 'Revenue Stream', 'Timestamp', 'Amount'));
        $service = new Service();
        $dashboardData = $service->getDashboardTransactions($k, $k1, $k2);
        $i = 1;
        $totalAmount = $total = 0;
        foreach ($dashboardData as $payment) {
            if ($payment['feeCode'] != $k2 && !empty($k2)) {
                continue;
            }
            if ($k3 != $payment['channel'] && !empty($k3)) {
                continue;
            }
            $paymentModel = new Payments();
            $paymentModel->setAttributes($payment);
            $paymentModel->student = $payment['student'];
            $amount = number_format($paymentModel->amount, 2);
            $totalAmount += $paymentModel->amount;
            $name = $paymentModel->student['firstName'] . " " . $paymentModel->student['middleName'] . " " . $paymentModel->student['lastName'];
            fputcsv($output, array(
                $i,
                $name,
                $paymentModel->fullRegNo,
                $paymentModel->channel,
                ' ' . $paymentModel->phoneNumber,
                str_replace("_", " ", $paymentModel->feeCode),
                $paymentModel->paymentTimestamp,
                $paymentModel->currency . " " . $amount
            ));
            $i++;
        }
        fputcsv($output, array('', '', '', '', '', '', 'TOTAL', number_format($totalAmount, 2)));
    }

    public function actionCourseApplicationsExport($k, $k1, $k2) {
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=course-application.csv');

        // create a file pointer connected to the output stream
        $output = fopen('php://output', 'w');

        // output the column headings
        fputcsv($output, array('', 'Name', 'Reg. Date', 'Course', 'Verified By'));

        $service = new Service();
        $studentsCourse = $service->getStudentsCourseApplication($k, $k1, $k2);
        $i = 1;
        foreach ($studentsCourse as $courses) {
            if ($k2 <= 1 && $k2 != $courses['verified']) {
                continue;
            }
            $courseModel = new CourseDisplay();
            $courseModel->id = $courses['id'];
            $courseModel->verifiedBy = $courses['verifiedBy'];
            $courseModel->created = $courses['created'];
            $courseModel->studentId = $courses['studentObj'];
            $courseModel->courseId = $courses['course'];
            $name = $courseModel->studentId['firstName'] . " " . $courseModel->studentId['middleName'] . " " . $courseModel->studentId['lastName'];
            $verifiedBy = !empty($courseModel->verifiedBy) ? $courseModel->verifiedBy['firstName'] . " " . $courseModel->verifiedBy['otherNames'] : "-";
            fputcsv($output, array($i, $name, $courseModel->created, $courseModel->courseId['name'], $verifiedBy));
            $i++;
        }
    }

    public function actionExaminationBookingExport($k = "", $k1 = "", $k2 = "", $k3 = "", $k4 = "", $k5 = "", $k6 = "") {
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=examination-booking.csv');

        // create a file pointer connected to the output stream
        $output = fopen('php://output', 'w');

        // output the column headings
        fputcsv($output, array('', 'Student', 'Registartion Number', 'Course', 'Sitting', 'Year', 'Center', 'Status', 'Timestamp'));
        $service = new Service();
        $exams = $service->getExamBookings($k2, $k3, $k1, $k, $k5, $k6);
        $i = 1;
        foreach ($exams as $data) {
            $model = new ExaminationBooking();
            $model->setAttributes($data);
            $name = $model->student['firstName'] . " " . $model->student['middleName'] . " " . $model->student['lastName'];
            fputcsv($output, array($i, $name, $model->fullRegNo, $model->course, $model->sitting['sittingPeriod'], $model->sitting['sittingYear'], $model->sittingCentre['name'], $model->status, $model->created));
            $i++;
        }
    }

    public function actionStudentRegistrationExport($k = "", $k1 = "") {
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=students.csv');

        // create a file pointer connected to the output stream
        $output = fopen('php://output', 'w');

        // output the column headings
        fputcsv($output, array('', 'First Name', 'Middle Name', 'Last Name', 'Gender', 'Phone Number', 'County', 'Country', 'Timestamp'));
        $service = new Service();
        $students = $service->getRegisteredStudents($k, $k1);
        $i = 1;
        foreach ($students as $data) {
            $studentModel = new Student();
            $studentModel->setAttributes($data);
            $gender = $studentModel->gender == 1 ? "Male" : "Female";
            fputcsv($output, array($i, $studentModel->firstName, $studentModel->middleName, $studentModel->lastName, $gender, $studentModel->phoneNumber, $studentModel->countyId['name'], $studentModel->countryId['name'], $studentModel->created));
            $i++;
        }
    }

    public function actionExemptionApplicationsExport($k = "", $k1 = "", $k2 = "") {
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=exemption.csv');

        // create a file pointer connected to the output stream
        $output = fopen('php://output', 'w');

        // output the column headings
        fputcsv($output, array('', 'Student', 'Registration Number', 'Qualification', 'Verified By', 'Verified At'));
        $service = new Service();
        $datas = $service->getExemptionsBookings($k, $k1, $k2);
        $i = 1;
        foreach ($datas as $data) {
            $modelData = new ExemptionVerification();
            $modelData->setAttributes($data);
            $modelData->studentId = $service->getStudentData($modelData->studentId);
            $name = $modelData->studentId['firstName'] . " " . $modelData->studentId['middleName'] . " " . $modelData->studentId['lastName'];
            $verifiedBy = !empty($data->verifiedBy) ? $data->verifiedBy['firstName'] . " " . $data->verifiedBy['otherNames'] : "-";
            fputcsv($output, array($i, $name, $modelData->fullRegNo, $modelData->qualification['type'], $verifiedBy, $modelData->dateVerified));
            $i++;
        }
    }

}
