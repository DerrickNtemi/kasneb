<?php

namespace administrator\controllers;

use common\models\Service;
use common\models\Student;
use common\models\CourseDisplay;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\helpers\Url;
use Yii;

/**
 * Site controller
 */
class TransactionsController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionView($id) {
        $service = new Service();
        $studentModel = new Student();
        $studentModel->setAttributes($service->getStudentData($id));
        $courses=null;
        foreach ($studentModel->studentCourses as $course) {
            if (!$course['verified']) {
                $courses = $course;
            }
        }
        return $this->render('view', [ 'model' => $studentModel,'courses'=>$courses]);
    }

    public function actionIndex() {
        $service = new Service();
        $studentModel = new Student();
        $studentsData = $service->getStudents();
        $students = [];
        foreach ($studentsData as $student) {
            $model = new Student();
            $model->setAttributes($student);
            $students[] = $model;
        }
        return $this->render('index', [ 'model' => $studentModel, 'datas' => $students]);
    }

    public function actionPending() {
        $service = new Service();
        $studentModel = new Student();
        $studentsData = $service->getStudents();
        $registrations = [];
        foreach ($studentsData as $student) {
            foreach ($student['studentCourses'] as $courses) {
                if (!empty($courses['verified'])) {
                    continue;
                }
                $courseModel = new CourseDisplay();
                $courseModel->id = $courses['id'];
                $courseModel->created = $courses['created'];
                $courseModel->studentId = $student;
                $courseModel->courseId = $courses['course'];
                $registrations[] = $courseModel;
            }
        }
        return $this->render('pending', [ 'model' => $studentModel, 'datas' => $registrations]);
    }

    public function actionVerify($courseId, $studentId) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new CourseDisplay();
        $checked = isset($_POST['checked']) ? $_POST['checked'] : "";
        $remarks = isset($_POST['remarks']) ? $_POST['remarks'] : "";
        $password = isset($_POST['password']) ? $_POST['password'] : "";
        if (!empty($checked) && !empty($remarks) && !empty($password)) {

            $service = new Service();
            $identity = Yii::$app->user->getIdentity();
            $studentModel = new Student();
            $studentModel->setAttributes($service->getStudentData($studentId));
            $id = 0;
            foreach ($studentModel->studentCourses as $courses) {
                if (!$courses['verified']) {
                    $courseDetails = $service->getCourse($courses['id']);
                    if ($courseDetails['courseType'] == 100) {
                        $id = $courses['id'];
                    } else {
                        $id = $courses['id'];
                    }
                }
            }
            $data = [
                'id' => $id,
                'verifiedBy' => [
                    'id' => $identity->user['id'],
                ],
                'remarks' => $remarks
            ];
            $response = (object) $service->verifyRegistration($data);
            if ($response->status['code'] == 1) {
                Yii::$app->session->setFlash('success-message', 'Verification process was successful...');
                $url = Url::toRoute(['registration/pending']);
                return ['success' => ['url' => $url]];
            } else {
                $model->addError("", "ERROR CODE " . $response->status['code'] . " :: " . $response->status['message']);
            }
        } else {
            $model->addError("", "All fields are mandatory");
        }
        $errorsArry = '';
        foreach ($model->getErrors() as $error) {
            $errorsArry.=$error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

}
