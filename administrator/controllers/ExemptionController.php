<?php

namespace administrator\controllers;

use common\models\ExemptionVerification;
use common\models\Service;
use common\models\Student;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;
use common\models\Institutions;
use common\models\ExemptionCourse;
use common\models\ExemptionCoursePapers;
use common\models\InstitutionCourses;

/**
 * Site controller
 */
class ExemptionController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionView($id) {
        $service = new Service();
        $studentModel = new Student();
        $exemptions = $service->getExemption($id);
        $studentModel->setAttributes($exemptions['student']);
        $remarks = $service->rejectionReasonsExemption();
        return $this->render('view', ['model' => $studentModel, 'exemptions' => $exemptions, 'remarks' => $remarks]);
    }

    public function actionIndex() {
        $service = new Service();
        $model = new ExemptionVerification();
        $datas = $service->getPendingExemptions();
        $remarks = $service->rejectionReasonsExemption();
        $dataArray = [];
        foreach ($datas as $data) {
            $modelData = new ExemptionVerification();
            $modelData->setAttributes($data);
            $modelData->studentId = $data['student'];
            //print_r($modelData->studentId); exit;
            $dataArray[] = $modelData;
        }
        return $this->render('pending', ['model' => $model, 'datas' => $dataArray, 'remarks' => $remarks]);
    }

    public function preparePapersData($data) {
        $datas = explode(',', $data);
        $papers = [];
        for ($i = 0; $i < count($datas) - 1; $i++) {
            $papers[] = ['code' => $datas[$i]];
        }
        return $papers;
    }

    public function actionVerify($id, $courseId, $studentId, $type) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new ExemptionVerification();
        $checked = isset($_POST['checked']) ? $_POST['checked'] : "";
        $remarks = isset($_POST['remarks']) ? $_POST['remarks'] : "";
        $password = isset($_POST['password']) ? $_POST['password'] : "";
        $datatosend = isset($_POST['datatosend']) ? $_POST['datatosend'] : "";
        if (!empty($checked) && !empty($password)) {
            $remarks = $checked == "APPROVED" ? "" : $remarks;
            if ($type != "all-summary" && ($type == "all-partial" || $type = "all-single") && empty($datatosend)) {
                $model->addError("", "You must select atleast one paper");
            } else {
                $service = new Service();
                $identity = Yii::$app->user->getIdentity();
                $studentModel = new Student();
                $exemptions = $service->getExemption($id);
                $papers = [];
                if ($type == "all-partial") {
                    $papers = $this->preparePapersData($datatosend);
                }
                if ($type == "all-single") {
                    $papers[] = ['code' => $datatosend];
                }
                if ($type == "all-summary") {
                    $courseId = $studentModel->currentCourse['id'];
                    foreach ($exemptions['papers'] as $paper) {
                        $papers[] = ['code' => $paper['paper']['code']];
                    }
                }
                $papersData = [];
                foreach ($papers as $pp) {
                    $papersData[] = [
                        'paper' => $pp,
                        'verificationStatus' => $checked,
                        'remarks' => $remarks,
                    ];
                }

                $data = [
                    'id' => $id,
                    'papers' => $papersData,
                    'verifiedBy' => ['id' => $identity->user['id']],
                ];
                $response = (object) $service->verifyExemption($data);
                if ($response->status['code'] == 1) {
                    Yii::$app->session->setFlash('success-message', 'Verification process was successful...');
                    $url = Url::toRoute(['exemption/index']);
                    return ['success' => ['url' => $url]];
                } else {
                    $model->addError("", "ERROR CODE " . $response->status['code'] . " :: " . $response->status['message']);
                }
            }
        } else {
            $model->addError("", "All fields are mandatory");
        }
        $errorsArry = '';
        foreach ($model->getErrors() as $error) {
            $errorsArry .= $error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

    public function actionInstitution() {
        $service = new Service();
        $model = new Institutions();
        $institutionData = $service->getInstitutions();
        $institutions = $qualificationTypes = [];
        foreach ($institutionData as $data) {
            $institutionModel = new Institutions();
            $institutionModel->setAttributes($data);
            $institutionModel->courseCount = count($institutionModel->courses);
            $institutions[] = $institutionModel;
        }
        $qualificationType = $service->getCourseTypeOthers();
        foreach ($qualificationType as $data) {
            $qualificationTypes[$data['code']] = $data['name'];
        }
        return $this->render('institutions', ['model' => $model, 'datas' => $institutions, 'qualificationTypes' => $qualificationTypes]);
    }

    public function actionViewInstitution($id) {
        $service = new Service();
        $model = new Institutions();
        $exemptionCourseModel = new ExemptionCourse();
        //$model->setAttributes($service->getInstitution($id));
        $institutionData = $service->getInstitutions();
        $courses = $coursesArray = $coursePapers = [];
        foreach ($institutionData as $data) {
            if ($id == $data['id']) {
                $model->setAttributes($data);
                foreach ($data['courses'] as $course) {
                    $institutionCourseModel = new InstitutionCourses();
                    $institutionCourseModel->setAttributes($course);
                    $institutionCourseModel->institutionId = $id;
                    $courses[] = $institutionCourseModel;
                }
                $model->courseCount = count($courses);
            }
        }
        $exemptionCourseModel->institutionId = $id;
        $exemptionCourseModel->courseType = $model->courseType['code'];
        return $this->render('view-exemption', ['exemptionCourseModel' => $exemptionCourseModel, 'model' => $model, 'courses' => $courses, 'coursesArray' => $coursesArray, 'coursePapers' => $coursePapers]);
    }

    public function actionViewInstitutionCourse($institutionId, $id) {
        $service = new Service();
        $exemptionCourseModel = new ExemptionCourse();
        $model = new ExemptionCoursePapers();
        $model->courseId = $id;
        $model->institutionId = $institutionId;
        $exemptionCourse = $service->getInstitutionCourse($id);
        $exemptionCourseModel->id = $exemptionCourse['id'];
        $exemptionCourseModel->courseName = $exemptionCourse['name'];
        $exemptionCourseModel->institutionId = $exemptionCourse['institutionName'];
        $exemptionCourseModel->institution = $exemptionCourse['institutionId'];
        $coursesArray = $coursePapers = $papers = [];
        $coursesData = $service->getAllCourses();
        $i = 0;
        foreach ($coursesData as $course) {
            $coursesArray[$course['id']] = $course['name'];
            if ($i == 0) {
                foreach ($course['papers'] as $paper) {
                    $coursePapers[$paper['code']] = $paper['name'];
                }
            }
            $i++;
        }

        foreach ($exemptionCourse['exemptionCourses'] as $courses) {
            foreach ($courses['papers'] as $course) {
                $exemptionCoursePapers = new ExemptionCoursePapers();
                $exemptionCoursePapers->setAttributes($course);
                $exemptionCoursePapers->courseId = ['id' => $courses['id'], 'name' => $courses['name']];
                $papers[] = $exemptionCoursePapers;
                //print_r($exemptionCoursePapers); exit;
            }
        }
        $exemptionCourseModel->papers = count($papers);
        return $this->render('view-institution-course', ['model' => $model, 'exemptionCourseModel' => $exemptionCourseModel, 'coursesArray' => $coursesArray, 'coursePapers' => $coursePapers, 'papers' => $papers]);
    }

    public function actionCreateInstitutionCoursePaperFilter($courseId) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $coursePapers = [];
        $service = new Service();
        $coursesData = $service->getAllCourses();
        foreach ($coursesData as $course) {
            if ($course['id'] == $courseId) {
                foreach ($course['papers'] as $paper) {
                    $coursePapers[$paper['code']] = $paper['name'];
                }
            }
        }
        return ['success' => $coursePapers];
    }

    public function actionCreateInstitutionCoursePaper() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new ExemptionCoursePapers();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $service = new Service();
            //$data = ['name'=>$model->courseName,'institution'=>['id'=>$model->institutionId]];
            $data = ['paper' => ['code' => $model->name], 'qualification' => ['id' => $model->courseId], 'course' => ['id' => $model->papers]];
            $response = (object) $service->createInstitutionCoursePaper($data);
            if ($response->status['code'] == 1) {
                Yii::$app->session->setFlash('success-message', 'Institution course papers record successfully created...');
                $url = Url::toRoute(['exemption/view-institution-course', 'institutionId' => $model->institutionId, 'id' => $model->courseId]);
                return ['success' => $url];
            } else {
                $model->addError("", "ERROR CODE " . $response->status['code'] . " :: " . $response->status['message']);
            }
        }
        $errorsArry = '';
        foreach ($model->getErrors() as $error) {
            $errorsArry .= $error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

    public function actionCreateInstitutionCourse() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new ExemptionCourse();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $service = new Service();
            //$data = ['name'=>$model->courseName,'institution'=>['id'=>$model->institutionId]];
            $data = ['name' => $model->courseName, 'institution' => ['id' => $model->institutionId], 'courseType' => ['code' => $model->courseType]];
            $response = (object) $service->createInstitutionCourse($data);
            if ($response->status['code'] == 1) {
                Yii::$app->session->setFlash('success-message', 'Institution course record successfully created...');
                $url = Url::toRoute(['exemption/view-institution', 'id' => $model->institutionId]);
                return ['success' => $url];
            } else {
                $model->addError("", "ERROR CODE " . $response->status['code'] . " :: " . $response->status['message']);
            }
        }
        $errorsArry = '';
        foreach ($model->getErrors() as $error) {
            $errorsArry .= $error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

    public function actionDeleteInstitution($id) {
        $service = new Service();
        $response = (object) $service->deleteInstitution($id);
        if ($response->status['code'] == 1) {
            Yii::$app->session->setFlash('success-message', "Institution #$id Successfully deleted...");
        } else {
            Yii::$app->session->setFlash('failed-message', "ERROR CODE " . $response->status['code'] . " :: " . $response->status['message']);
        }
        $url = Url::toRoute(['exemption/institution']);
        return $this->redirect($url);
    }

    public function actionDeleteInstitutionCourse($institutionId, $id) {
        $service = new Service();
        $response = (object) $service->deleteInstitutionCourse($id);
        if ($response->status['code'] == 1) {
            Yii::$app->session->setFlash('success-message', "Institution course #$id successfully deleted...");
        } else {
            Yii::$app->session->setFlash('failed-message', "ERROR CODE " . $response->status['code'] . " :: " . $response->status['message']);
        }
        $url = Url::toRoute(['exemption/view-institution', 'id' => $institutionId]);
        return $this->redirect($url);
    }

    public function actionDeleteInstitutionCoursePaper($qualification, $paper, $institutionId) {
        $service = new Service();
        $response = (object) $service->deleteInstitutionCoursePaper($qualification, $paper);
        if ($response->status['code'] == 1) {
            Yii::$app->session->setFlash('success-message', "Institution course paper $paper successfully deleted...");
        } else {
            Yii::$app->session->setFlash('failed-message', "ERROR CODE " . $response->status['code'] . " :: " . $response->status['message']);
        }
        $url = Url::toRoute(['exemption/view-institution-course', 'institutionId' => $institutionId, 'id' => $qualification]);
        return $this->redirect($url);
    }

    public function actionCreateInstitution() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new Institutions();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $service = new Service();
            $data = ['name' => $model->name, 'courseType' => ["code" => $model->courseType]];
            $response = (object) $service->createInstitution($data);
            if ($response->status['code'] == 1) {
                Yii::$app->session->setFlash('success-message', 'Institution record successfully created...');
                $url = Url::toRoute(['exemption/institution']);
                return ['success' => $url];
            } else {
                $model->addError("", "ERROR CODE " . $response->status['code'] . " :: " . $response->status['message']);
            }
        }
        $errorsArry = '';
        foreach ($model->getErrors() as $error) {
            $errorsArry .= $error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

}
