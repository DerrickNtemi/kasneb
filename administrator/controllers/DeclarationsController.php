<?php

namespace administrator\controllers;

use common\models\Service;
use common\models\AdminDeclarations;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\helpers\Url;
use Yii;

/**
 * Site controller
 */
class DeclarationsController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex() {
        $service = new Service();
        $model = new AdminDeclarations();
        $declarations = $service->getDeclarations();
        $items = [];
        foreach ($declarations as $data) {
            $modelData = new AdminDeclarations();
            $modelData->setAttributes($data);
            $items[] = $modelData;
        }
        return $this->render('index', [ 'model' => $model, 'datas' => $items]);
    }

    public function actionCreate() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new AdminDeclarations();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $service = new Service();
            $response = (object) $service->createAdminDeclaration($model);
            if ($response->status['code'] == 1) {
                Yii::$app->session->setFlash('success-message', 'Declaration record successfully created...');
                $url = Url::toRoute(['index']);
                return ['success' => $url];
            } else {
                $model->addError("", "ERROR CODE " . $response->status['code'] . " :: " . $response->status['message']);
            }
        }
        $errorsArry = '';
        foreach ($model->getErrors() as $error) {
            $errorsArry.=$error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

    public function actionDelete($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new AdminDeclarations();
        $service = new Service();
        $password = $_POST['password'];
        if (!empty($password)) {
            $response = (Object) $service->deleteDeclaration($id, $password);
            if ($response->status['code'] == 200) {
                $url = Url::toRoute(['index']);
                \Yii::$app->getSession()->setFlash('success-message', "Record #" . $id . " successfully deleted.");
                return ['success' => $url];
            } else {
                $model->addError("", "Error Code " . $response->status['code'] . " :: " . $response->status['message']);
            }
        } else {
            $model->addError("", "Password cannot be blank");
        }
        $errorsArry = '';
        foreach ($model->getErrors() as $error) {
            $errorsArry.= $error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

}
