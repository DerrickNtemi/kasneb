<?php

namespace administrator\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use common\models\LoginForm;
use yii\web\Response;
use common\models\Filter;
use common\models\Service;
use common\models\ChangePassword;
use common\models\AdminUser;
use common\models\AuditTrail;
use yii\helpers\Url;

/**
 * Site controller
 */
class SiteController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'error', 'forgot-password', 'reset-password', 'expired-password','change-password'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function getGraphTitle($k) {
        $titleArray = [];
        if ($k == "") {
            $titleArray = [
                'tranVol' => "ALL TRANSACTION VOLUME",
                'tranVal' => "ALL TRANSACTION VALUE",
                'student' => "ALL STUDENTS",
                'graphTitle' => "ALL STREAMS DATA",
            ];
        } elseif ($k == "REGISTRATION_RENEWAL_FEE") {
            $titleArray = [
                'tranVol' => "RENEWAL TRANSACTION VOLUME",
                'tranVal' => "RENEWAL TRANSACTION VALUE",
                'student' => "RENEWAL STUDENTS",
                'graphTitle' => "RENEWAL GRAPH DATA",
            ];
        } elseif ($k == "EXEMPTION_FEE") {
            $titleArray = [
                'tranVol' => "EXEMPTION TRANSACTION VOLUME",
                'tranVal' => "EXEMPTION TRANSACTION VALUE",
                'student' => "EXEMPTION STUDENTS",
                'graphTitle' => "EXEMPTION GRAPH DATA",
            ];
        } elseif ($k == "EXAM_ENTRY_FEE") {
            $titleArray = [
                'tranVol' => "EXAM TRANSACTION VOLUME",
                'tranVal' => "EXAM TRANSACTION VALUE",
                'student' => "EXAM STUDENTS",
                'graphTitle' => "EXAMINATION GRAPH DATA",
            ];
        } elseif ($k == "REGISTRATION_FEE") {
            $titleArray = [
                'tranVol' => "REGISTRATION TRANSACTION VOLUME",
                'tranVal' => "REGISTRATION TRANSACTION VALUE",
                'student' => "REGISTRATION STUDENTS",
                'graphTitle' => "REGISTRATION GRAPH DATA",
            ];
        }
        return $titleArray;
    }

    public function actionIndex($k = "", $k1 = "", $k2 = "REGISTRATION_RENEWAL_FEE") {
        $startDate = empty($k) ? date("Y-m-d") : $k;
        $endDate = empty($k1) ?  date("Y-m-d")  : $k1;
        $filterModel = new Filter();
        $filterModel->startDate = $startDate;
        $filterModel->endDate = $endDate;

        $filterModel->stream = empty($k2) ? "" : $k2;
        $titleArray = $this->getGraphTitle($k2);
        $service = new Service();
        $revenueStreams = $service->revenueStream();
        $dashboardData = $service->getDashboardTransactions($startDate, $endDate, $k2);
        //echo count($dashboardData); exit;
        $graphData = $this->graphData($titleArray, $dashboardData, $filterModel);
        $cardData = $this->cardsData($dashboardData);
        return $this->render('index', ['cardData' => $cardData, 'graphData' => $graphData, 'filterModel' => $filterModel, 'revenueStreams' => $revenueStreams, 'titleArray' => $titleArray]);
    }

    public function cardsData($dashboardData) {
        $students = count($dashboardData);
        $trnxVolume = $trnxValue = $examFee = $exemptionFee = $regFee = $renewalFee = 0;
        //$i=0;
        foreach ($dashboardData as $data) {
            $trnxVolume += count($data['payments']);
            foreach ($data['payments'] as $transaction) {
                $trnxValue += $transaction['amount'];
                $examFee += $transaction['feeCode'] == "EXAM_ENTRY_FEE" ? $transaction['amount'] : 0;
                $exemptionFee += $transaction['feeCode'] == "EXEMPTION_FEE" ? $transaction['amount'] : 0;
                $regFee += $transaction['feeCode'] == "REGISTRATION_FEE" ? $transaction['amount'] : 0;
                $renewalFee += $transaction['feeCode'] == "REGISTRATION_RENEWAL_FEE" ? $transaction['amount'] : 0;
            }
        }
        $loanData = [
            'student' => number_format($students),
            'value' => number_format($trnxValue, 2),
            'volume' => number_format($trnxVolume),
            'chartData' => [
                'exam' => $examFee,
                'exemption' => $exemptionFee,
                'registration' => $regFee,
                'renewal' => $renewalFee,
            ]
        ];
        return $loanData;
    }

    public function graphData($titleArray, $dashboardData, $filterModel) {
        $service = new Service();
        $filterBounds = $service->getStartEndDates($filterModel->startDate, $filterModel->endDate);
        $loanData = [];
        $loanData['yAxis'][] = "Value";
        $loanData['title'][] = $titleArray['graphTitle'];
        $loanData['data'][0] = ['name' => 'Students', 'data' => []];
        $loanData['data'][1] = ['name' => 'Volume', 'data' => []];
        $loanData['data'][2] = ['name' => 'Value', 'data' => []];
        foreach ($filterBounds as $bounds) {
            $loanData['axis'][] = $bounds['label'];
            $student = $volume = $value = 0;
            foreach ($dashboardData as $data) {
                $stdHit = 0;
                foreach ($data['payments'] as $transaction) {
                    $trnxDate = $service->timestampConversionWithoutJava($transaction['paymentTimestamp']);
                    //echo $trnxDate."  ".$bounds['value']['start']." ".$bounds['value']['end']."<br/>";
                    if ($trnxDate >= $bounds['value']['start'] && $trnxDate <= $bounds['value']['end']) {
                        $value += $transaction['amount'];
                        $volume++;
                        $stdHit++;
                    }
                }
                if ($stdHit > 0) {
                    $student++;
                }
            }
            $loanData['data'][0]['data'][] = $student;
            $loanData['data'][1]['data'][] = $volume;
            $loanData['data'][2]['data'][] = $value;
        }
        return $loanData;
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $login = $model->login()) {
            $login = explode(':', $login);
            if ($login[0] == 'force') {
                Yii::$app->session->set('currentPassword', $model->password);
                Yii::$app->session->set('forcedChange', 1);
                Yii::$app->session->set('credentialId', $login[1]);
                return $this->redirect('change-password');
            }
            return $this->redirect(['registration/index']);
            //print_r($login);exit;
            return $this->goBack();
        }
        return $this->render('login', [
                    'model' => $model,
        ]);
    }
    /**
     * Change Password
     */
    public function actionChangePassword() {
        $model = new ChangePassword();
        if ($model->load(Yii::$app->request->post())) {
            $service = new Service();
            $data = [
                'id'=>Yii::$app->session->get('credentialId'),
                'currentPassword'=>Yii::$app->session->get('currentPassword'),
                'newPassword'=>$model->password
            ];
            $response = (Object)$service->resetPassword($data);
            if ($response->status['code'] == 1) {
                return $this->redirect('login');
            } else {
                $model->addError("", $response->status['message']);
            }
            return $this->goBack();
        }
        return $this->render('changePassword', [
                    'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionGraphData($item) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $loanData = [];
        $title = $this->getTitle($item);
        $yAxis = $this->getYAxis($item);
        $loanData['yAxis'][] = $yAxis;
        $loanData['title'][] = $title;
        $loanData['data'][0] = ['name' => 'Current Period', 'data' => []];
        $loanData['data'][1] = ['name' => 'Previous Period', 'data' => []];
        return $loanData;
    }

    public function getTitle($item) {
        $title = "";
        if ($item == 1) {
            $title = "TRANSACTIONS VOLUME";
        } else if ($item == 2) {
            $title = "TRANSACTIONS VALUE";
        } else if ($item == 3) {
            $title = "STUDENT REGISTRATION";
        }
        return $title;
    }

    public function getYAxis($item) {
        $title = "";
        if ($item == 1) {
            $title = "Volume";
        } else if ($item == 2) {
            $title = "Amount (KES)";
        } else if ($item == 3) {
            $title = "Value";
        }
        return $title;
    }

    public function actionUsers() {
        
        //$identity = Yii::$app->user->getIdentity();
        //print_r($identity->user['role']); exit;
        $model = new AdminUser();
        $service = new Service();
        $users = $service->getUsers();
        $roles = $service->getUsersRoles();
        $rolesData = [];
        foreach ($roles as $role) {
            $rolesData[$role['id']] = $role['description'];
        }
        $usersData = [];
        foreach ($users as $user) {
            $userModel = new AdminUser();
            $userModel->setAttributes($user);
            $usersData[] = $userModel;
        }
        return $this->render('users', [
                    'model' => $model,
                    'users' => $usersData,
                    'rolesData' => $rolesData,
        ]);
    }

    public function actionCreateUser() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new AdminUser();
        $service = new Service();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->id = empty($model->id) ? null : $model->id;
            $model->role = ['id' => $model->role];
            $response = (Object) $service->adminUserCreate($model);
            if ($response->status['code'] == 1) {
                $url = Url::toRoute(['users']);
                \Yii::$app->getSession()->setFlash('success-message', "Process was successfully.");
                return ['success' => $url];
            } else {
                $model->addError("", "Error Code " . $response->status['code'] . " :: " . $response->status['message']);
            }
        }
        $errorsArry = '';
        foreach ($model->getErrors() as $error) {
            $errorsArry.= $error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

    public function actionDeleteUser($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new AdminUser();
        $service = new Service();
        $password = $_POST['password'];
        if (!empty($password)) {
            $response = (Object) $service->deleteAdminUser($id, $password);
            if ($response->status['code'] == 1) {
                $url = Url::toRoute(['users']);
                \Yii::$app->getSession()->setFlash('success-message', "Record successfully deleted.");
                return ['success' => $url];
            } else {
                $model->addError("", "Error Code " . $response->status['code'] . " :: " . $response->status['message']);
            }
        } else {
            $model->addError("", "Password cannot be blank");
        }
        $errorsArry = '';
        foreach ($model->getErrors() as $error) {
            $errorsArry.= $error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

    public function actionAuditTrail($f="",$f1="",$f2="") {
        $service = new Service();
        
        $startDate = empty($f1) ? date("Y-m-d") : $f1;
        $endDate = empty($f2) ? date("Y-m-d") : $f2;
        
        $filterModel = new Filter();
        $filterModel->startDate = $startDate;
        $filterModel->endDate = $endDate;
        $filterModel->user = empty($f) ? "" : $f;
        
        $trails = $service->getAuditTrails($filterModel->user, $filterModel->startDate,$filterModel->endDate);
        $audits = [];
        foreach($trails as $trail){
            $auditModel = new AuditTrail();
            $auditModel->setAttributes($trail);
            $audits[] = $auditModel;
        }
        $users = $service->getUsers();
        $usersData[] = "Choose user ...";
        foreach ($users as $user) {
            $usersData[$user['id']] = $user['email'];
        }
        
        return $this->render('audit-trails', [
                    'filterModel' => $filterModel,
                    'audits' => $audits,
            'users'=>$usersData,
        ]);
    }

}
