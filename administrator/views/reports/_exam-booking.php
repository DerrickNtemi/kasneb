<?php

use yii\data\ArrayDataProvider;
use yii\grid\GridView;

?>
<?=

GridView::widget([
    'dataProvider' => new ArrayDataProvider(['allModels' => $datas]),
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],     
        [
            'label' => 'Name',
            'value' => function($data) {
                return $data->student['firstName']." ".$data->student['middleName']." ".$data->student['lastName'];
            }
        ],   
        [
            'label' => 'Phone No.',
            'value' => function($data) {
                return $data->student['phoneNumber'];
            }
        ], 
        [
            'label' => 'Reg. No.',
            'value' => function($data) {
                return $data->fullRegNo;
            }
        ],
        [
            'label' => 'Sitting',
            'value' => function($data) {
                return $data->sitting['sittingPeriod']." - ".$data->sitting['sittingYear'];
            }
        ],
        [
            'label' => 'Center',
            'value' => function($data) {
                return $data->sittingCentre['name'];
            },
            'headerOptions' => ['style' => 'max-width: 250px'],
            'contentOptions' => ['style' => 'max-width: 250px; word-wrap: break-word; overflow: auto;'],
        ],
        'status',
        [
            'label' => 'Timestamp',
            'value' => function($data) {
                return $data->created;
            }
        ],                
    ],
]);
?>