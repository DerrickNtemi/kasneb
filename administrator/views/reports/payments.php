<?php
$this->title = Yii::t('app', 'Payments');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row members-index">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?=$this->title?>
            </div>
            <div class="panel-body">
                <?php if(Yii::$app->session->hasFlash('fail-message')): ?>
                    <div class="alert alert-danger"><?=Yii::$app->session->getFlash('fail-message')?></div>
                <?php endif; ?>
                <?php if(Yii::$app->session->hasFlash('success-message')): ?>
                    <div class="alert alert-success"><?=Yii::$app->session->getFlash('success-message')?></div>
                <?php endif; ?>
                <div class="alert alert-danger" style="display: none;" id="dashboard-filter-error"></div>

                <div class="row" style="background-color: #f5f5f0; margin-right: 0px; margin-left: 0px; margin-bottom:10px;">
                    <div class="col-md-11">
                        <?= $this->render('_filterAdvanced', ['model' => $filterModel, 'revenueStreams' => $revenueStreams,'modes'=>$modes]) ?>
                    </div>
                    <div class="col-md-1" style="padding-top: 25px; padding-left: 0px;">
                        <button class = "btn btn-sm btn-primary" id= "filter-payments" ><span class="fa fa-search"></span>Search</button>
                    </div>
                </div> 
                <div class="row text-right" style="margin-right: 0px; margin-left: 0px; margin-bottom: 20px;" >
                    <!--<button class = "btn btn-sm btn-success"  id= "pdf-students" ><span class="fa fa-file"></span> Export PDF</button> -->
                    <button class = "btn btn-sm btn-primary"  id= "excel-payments" ><span class="fa fa-download"></span> Export Excel</button>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?= $this->render('_payments', ['payments' => $paymentData]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
