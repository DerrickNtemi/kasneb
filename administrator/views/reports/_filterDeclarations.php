<?php
use kartik\form\ActiveForm;

$form = ActiveForm::begin([
            'id' => 'filter-details-form',
            'action' => Yii::$app->urlManager->createUrl(['reports/declarations']),
            'options' => ['enctype' => 'multipart/form-data',],
            'fieldConfig' => [
                'template' => "{label}{input}\n{hint}\n{error}",
            ],
        ]);
?>
<div class="row">
    <div class="col-md-12">
        <div class="row form-row-below">   
            <div class="col-md-6"></div>             
            <div class="col-md-3">                            
                            <?= $form->field($model, 'declrations', [
                                    'addon' => ['prepend' => ['content' => '<i class="fa fa-user"></i>']],
                                'inputOptions' => ['class' => 'form-control  form-control-custom'],
                            ])->dropDownList($declarationsData) ?>
                        </div>
            <div class="col-md-3">                            
                            <?= $form->field($model, 'response', [
                                    'addon' => ['prepend' => ['content' => '<i class="fa fa-exchange"></i>']],
                                'inputOptions' => ['class' => 'form-control  form-control-custom'],
                            ])->dropDownList([''=>'All','true'=>'Yes','false'=>'No']) ?>
                        </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>