<?php

use kartik\form\ActiveForm;

$form = ActiveForm::begin([
            'id' => 'filter-details-form',
            'action' => Yii::$app->urlManager->createUrl(['reports/examination-booking']),
            'options' => ['enctype' => 'multipart/form-data',],
            'fieldConfig' => [
                'template' => "{label}{input}\n{hint}\n{error}",
            ],
        ]);
?>
<div class="row">
    <div class="col-md-12">
        <div class="row form-row-below">    
            <div class="col-md-3">                            
                            <?= $form->field($model, 'courseType', [
                                    'addon' => ['prepend' => ['content' => '<i class="fa fa-exchange"></i>']],
                                'inputOptions' => ['class' => 'form-control  form-control-custom'],
                            ])->dropDownList($courseTypes) ?>
                        </div>
            <div class="col-md-3">                            
                            <?= $form->field($model, 'course', [
                                    'addon' => ['prepend' => ['content' => '<i class="fa fa-exchange"></i>']],
                                'inputOptions' => ['class' => 'form-control  form-control-custom'],
                            ])->dropDownList($courses) ?>
                        </div>
            <div class="col-md-3">                            
                            <?= $form->field($model, 'year', [
                                    'addon' => ['prepend' => ['content' => '<i class="fa fa-calendar-o"></i>']],
                                'inputOptions' => ['class' => 'form-control  form-control-custom'],
                            ])->dropDownList($sittingYears) ?>
                        </div>
            <div class="col-md-3">                            
                            <?= $form->field($model, 'month', [
                                    'addon' => ['prepend' => ['content' => '<i class="fa fa-calendar"></i>']],
                                'inputOptions' => ['class' => 'form-control  form-control-custom'],
                            ])->dropDownList($sittingMonths) ?>
                        </div>
            <div class="col-md-3">                            
                            <?= $form->field($model, 'zone', [
                                    'addon' => ['prepend' => ['content' => '<i class="fa fa-building"></i>']],
                                'inputOptions' => ['class' => 'form-control  form-control-custom'],
                            ])->dropDownList($zones) ?>
                        </div>
            <div class="col-md-3">                            
                            <?= $form->field($model, 'center', [
                                    'addon' => ['prepend' => ['content' => '<i class="fa fa-building"></i>']],
                                'inputOptions' => ['class' => 'form-control  form-control-custom'],
                            ])->dropDownList($centers) ?>
                        </div>
            <div class="col-md-3">                            
                            <?= $form->field($model, 'status', [
                                    'addon' => ['prepend' => ['content' => '<i class="fa fa-building"></i>']],
                                'inputOptions' => ['class' => 'form-control  form-control-custom'],
                            ])->dropDownList($status) ?>
                        </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>