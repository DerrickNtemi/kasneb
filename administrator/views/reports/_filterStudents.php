<?php
use kartik\date\DatePicker;
use kartik\form\ActiveForm;

$form = ActiveForm::begin([
            'id' => 'filter-details-form',
            'action' => Yii::$app->urlManager->createUrl(['reports/student-registration']),
            'options' => ['enctype' => 'multipart/form-data',],
            'fieldConfig' => [
                'template' => "{label}{input}\n{hint}\n{error}",
            ],
        ]);
?>
<div class="row">
    <div class="col-md-12">
        <div class="row form-row-below">      
            <div class="col-md-6"> </div> 
            <div class="col-md-3"> 

                <?=
                $form->field($model, 'startDate', [
                    'inputOptions' => ['class' => 'form-control  form-control-custom'],
                ])->widget(DatePicker::classname(), [
                    'value' => date("Y-m-d"),
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ])
                ?>
            </div>
            <div class="col-md-3"> 

                <?=
                $form->field($model, 'endDate', [
                    'inputOptions' => ['class' => 'form-control  form-control-custom'],
                ])->widget(DatePicker::classname(), [
                    'value' => date("Y-m-d"),
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ])
                ?>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>