<?php
$this->title = Yii::t('app', 'Student Registration');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row members-index">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading"><?=$this->title?></div>
            <div class="panel-body">
                <?php if(Yii::$app->session->hasFlash('fail-message')): ?>
                    <div class="alert alert-danger"><?=Yii::$app->session->getFlash('fail-message')?></div>
                <?php endif; ?>
                <?php if(Yii::$app->session->hasFlash('success-message')): ?>
                    <div class="alert alert-success"><?=Yii::$app->session->getFlash('success-message')?></div>
                <?php endif; ?>
                <div class="row" style="background-color: #f5f5f0; margin-right: 0px; margin-left: 0px; margin-bottom:30px;">
                    <div class="col-md-11">
                        <?= $this->render('_filterStudents', ['model' => $filterModel]) ?>
                    </div>
                    <div class="col-md-1" style="padding-top: 25px; padding-left: 0px;">
                        <button class = "btn btn-sm btn-primary" id= "filter-students" ><span class="fa fa-search"></span>Search</button>
                    </div>
                </div>
                <div class="row text-right" style="margin-right: 0px; margin-left: 0px; margin-bottom: 20px;" >
                    <button class = "btn btn-sm btn-primary"  id= "excel-students" ><span class="fa fa-download"></span> Export Excel</button>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?= $this->render('_student-registration', ['datas' => $datas]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
