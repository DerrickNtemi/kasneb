<?php

use yii\data\ArrayDataProvider;
use yii\grid\GridView;

?>
<?=

GridView::widget([
    'dataProvider' => new ArrayDataProvider(['allModels' => $datas]),
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'firstName',
        'middleName',
        'lastName',
        [
            'label' => 'Gender',
            'value' => function($data) {
                return $data->gender == 1 ? "Male" : "Female";
            }
        ],
        'phoneNumber',      
        [
            'label' => 'County',
            'value' => function($data) {
                return $data->countyId['name'];
            }
        ],    
        [
            'label' => 'Country',
            'value' => function($data) {
                return $data->countryId['name'];
            }
        ], 
        [
            'label' => 'Timestamp',
            'value' => function($data) {
                return $data->created;
            }
        ],
    ],
]);
?>