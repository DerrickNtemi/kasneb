<?php

use yii\data\ArrayDataProvider;
use yii\grid\GridView;

?>
<?=

GridView::widget([
    'dataProvider' => new ArrayDataProvider(['allModels' => $payments]),
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'label' => 'Student',
            'value' => function($data) {
                return $data->student['firstName']." ".$data->student['middleName']." ".$data->student['lastName'];
            }
        ],
        [
            'label' => 'Registration Number',
            'value' => function($data) {
                return $data->fullRegNo;
            }
        ],
        [
            'label' => 'Channel',
            'value' => function($data) {
                return $data->channel;
            }
        ],
        'phoneNumber',
        [
            'label' => 'Revenue Stream',
            'value' => function($data) {
                return str_replace("_", " ", $data->feeCode) ;
            }
        ],
        [
            'label' => 'Timestamp',
            'value' => function($data) {
                return $data->paymentTimestamp;
            }
        ],
        [
            'attribute' => 'amount',
            'value' => function($data) {
                return $data->currency . " " . number_format($data->amount, 2);
            },
            'contentOptions' => ['style' => 'text-align:right;'],
            'headerOptions' => ['style' => 'text-align:right;'],
        ],
                
    ],
]);
?>