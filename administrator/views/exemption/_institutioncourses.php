<?php

use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\Html;
?>
<?=

GridView::widget([

    'dataProvider' => new ArrayDataProvider([ 'allModels' => $courses, ]),
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'label' => 'Name',
            'value' => function($data) {
                return $data->name;
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view}&nbsp;&nbsp;{delete}&nbsp;&nbsp;',
            'buttons' => [
                    'view' => function($url, $model) {
                        $url = Url::toRoute(['exemption/view-institution-course', 'institutionId' => $model->institutionId, 'id' => $model->id]);
                        return Html::a('view', $url, ['title' => 'View']);
                    },
                    'delete' => function($url,$model){
                        $url = Url::toRoute(['exemption/delete-institution-course', 'institutionId' => $model->institutionId, 'id' => $model->id]);
                        return Html::a('delete', $url, ['title' => 'Delete']);
                    },
                ],
            ],
        ],
    ]);
?>