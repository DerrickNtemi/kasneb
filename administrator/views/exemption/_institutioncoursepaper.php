<?php

use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\Html;
?>
<?=

GridView::widget([

    'dataProvider' => new ArrayDataProvider([ 'allModels' => $papers, ]),
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'label' => 'Exemption Examination',
            'value' => function($data) {
                return $data->courseId['name'];
            }
        ],
        [
            'label' => 'Paper',
            'value' => function($data) {
                return $data->name;
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{delete}&nbsp;&nbsp;',
            'buttons' => [
                    'delete' => function($url,$model) use ($courseId, $institution){
                        $url = Url::toRoute(['exemption/delete-institution-course-paper', 'qualification' => $courseId, 'paper' => $model->code, 'institutionId'=>$institution]);
                        return Html::a('delete', $url, ['title' => 'Delete']);
                    },
                ],
            ],
        ],
    ]);
?>