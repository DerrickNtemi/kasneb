<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-md-8 personal-details-div">Details</div>
         </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <div class="row profile-info-row">  
                    <div class="col-md-4"><strong><?= $model->getAttributeLabel("Institution Name") ?></strong></div>
                    <div class="col-md-1"><strong>:</strong></div>
                    <div class="col-md-7"><?= $model->name ?></div>
                </div>
                <div class="row profile-info-row" style="margin-bottom: 10px; margin-top: 10px;">  
                    <div class="col-md-4"><strong><?= $model->getAttributeLabel("Course Type") ?></strong></div>
                    <div class="col-md-1"><strong>:</strong></div>
                    <div class="col-md-7"><?= $model->courseType['name'] ?></div>
                </div>
                <div class="row profile-info-row">  
                    <div class="col-md-4"><strong><?= $model->getAttributeLabel("Course Count") ?></strong></div>
                    <div class="col-md-1"><strong>:</strong></div>
                    <div class="col-md-7"><?= $model->courseCount ?></div>
                </div>
            </div>
        </div>
    </div>
</div>