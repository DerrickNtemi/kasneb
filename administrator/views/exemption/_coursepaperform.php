<?php
use kartik\form\ActiveForm;
use yii\helpers\Html;

$form = ActiveForm::begin([
            'id' => 'exemption-institution-course-papers-form',
            'action' => Yii::$app->urlManager->createUrl(['exemption/create-institution-course-paper']),
            'options' => ['enctype' => 'multipart/form-data',],
            'fieldConfig' => [
                'template' => "{label}{input}\n{hint}\n{error}",
            ],
        ]);
?>
<?= Html::activeHiddenInput($model, 'courseId') ?>
<?= Html::activeHiddenInput($model, 'institutionId') ?>

<div class="row">
    <div class="col-md-12">
        <div class="row form-row-below">    
            <div class="col-md-6">                               
                <?=
                $form->field($model, 'papers', [
                    'addon' => ['prepend' => ['content' => '<i class="fa fa-tasks"></i>']],
                    'inputOptions' => ['class' => 'form-control  form-control-custom'],
                ])->dropDownList($coursesArray)
                ?>
            </div>   
            <div class="col-md-6">                               
                <?=
                $form->field($model, 'name', [
                    'addon' => ['prepend' => ['content' => '<i class="fa fa-tasks"></i>']],
                    'inputOptions' => ['class' => 'form-control  form-control-custom'],
                ])->dropDownList($coursePapers)
                ?>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
<h2></h2>
<div class="alert alert-danger"  id="error-summary" style="display: none;"></div>
<h2></h2>
<div id="progress-spinner" class="text-center" style="display: none;">
    <?= Html::img('@web/images/ajax-loader.gif', ['alt' => 'progress spinner']) ?>
</div>
<h2></h2>