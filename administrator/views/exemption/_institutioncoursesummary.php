<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-md-8 personal-details-div">Details</div>
         </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <div class="row profile-info-row">  
                    <div class="col-md-4"><strong><?= $model->getAttributeLabel("Course Name") ?></strong></div>
                    <div class="col-md-1"><strong>:</strong></div>
                    <div class="col-md-7"><?= $model->courseName ?></div>
                </div>
                <div class="row profile-info-row">  
                    <div class="col-md-4"><strong><?= $model->getAttributeLabel("Institution") ?></strong></div>
                    <div class="col-md-1"><strong>:</strong></div>
                    <div class="col-md-7"><?= $model->institutionId ?></div>
                </div>
                <div class="row profile-info-row">  
                    <div class="col-md-4"><strong><?= $model->getAttributeLabel("Papers Count") ?></strong></div>
                    <div class="col-md-1"><strong>:</strong></div>
                    <div class="col-md-7"><?= $model->papers ?></div>
                </div>
            </div>
        </div>
    </div>
</div>