<?php
use kartik\form\ActiveForm;
use yii\helpers\Html;

$form = ActiveForm::begin([
            'id' => 'exemption-course-papers-form',
            'action' => Yii::$app->urlManager->createUrl(['exemption/create-institution-course']),
            'options' => ['enctype' => 'multipart/form-data',],
            'fieldConfig' => [
                'template' => "{label}{input}\n{hint}\n{error}",
            ],
        ]);
?>
<?= Html::activeHiddenInput($model, 'institutionId') ?>
<?= Html::activeHiddenInput($model, 'courseType') ?>
<div class="row">
    <div class="col-md-12">
        <div class="row form-row-below">    
            <div class="col-md-12">                      
                <?=
                $form->field($model, 'courseName', [
                    'addon' => ['prepend' => ['content' => '<i class="fa fa-tasks"></i>']],
                    'inputOptions' => ['class' => 'form-control  form-control-custom'],
                ])->textInput()
                ?>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
<h2></h2>
<div class="alert alert-danger"  id="error-summary" style="display: none;"></div>
<h2></h2>
<div id="progress-spinner" class="text-center" style="display: none;">
    <?= Html::img('@web/images/ajax-loader.gif', ['alt' => 'progress spinner']) ?>
</div>
<h2></h2>