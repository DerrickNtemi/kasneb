<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ArrayDataProvider;
use yii\helpers\Url;
use yii\bootstrap\Modal;
?>
<?=

GridView::widget([

    'dataProvider' => new ArrayDataProvider([ 'allModels' => $students, ]),
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'label' => 'Student',
            'value' => function($data) {
                return $data->studentId['firstName']." ".$data->studentId['middleName']." ".$data->studentId['lastName'];
            }
        ],
        [
            'label' => 'Registration Number',
            'value' => function($data) {
                return $data->fullRegNo;
            }
        ],
        [
            'attribute' => 'qualification',
            'value' => function($data) {
                return $data->type;
            }
        ],
        
        [
            'class' => 'yii\grid\CheckboxColumn',
            'checkboxOptions' => function ($model, $key, $index, $column) {
                return ['value' => $model->id];
            },
            'name' => 'id',
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view}&nbsp;&nbsp;{verify}&nbsp;&nbsp;',
            'buttons' => [
                    'view' => function($url, $model) {
                        $url = Url::toRoute(['exemption/view', 'id' => $model->id]);
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => 'View']);
                    },
                    'verify' => function($url,$model){
                        $url = Url::toRoute(['exemption/verify','id'=>$model->id,'courseId'=>$model->studentCourseId,'studentId'=>$model->studentId['id'],'type'=>'all-summary']);
                        return Html::a('<span class="glyphicon glyphicon-ok"></span>', $url, ['data-whatever'=>$url, 'data-toggle'=>"modal", 'data-type'=>'all-summary', 'data-target'=>"#VerifyExemptionRedirectModal",'title' => Yii::t('yii', 'Verify'),]);
                    },
                ],
            ],
        ],
    ]);
?>

<!-- Verification Modal -->
<?php
    Modal::begin([
        'id' => 'VerifyExemptionRedirectModal',
        'header' => '<h4 class="modal-title" >Record Verification</h4>',
        'footer' =>
            Html::button('Cancel', ['class' => 'btn btn-default', 'data-dismiss' => 'modal'])
            . PHP_EOL .
            Html::button('Verify', ['id' => 'verify-redirect-btn', 'class' => 'btn btn-primary btn-modal-save']),
    ]);
?>
<?= $this->render('@common/views/_verify',['remarks'=>$remarks]) ?>
<?php Modal::end() ?>