<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ArrayDataProvider;
use yii\helpers\Url;
?>
<?=

GridView::widget([

    'dataProvider' => new ArrayDataProvider([ 'allModels' => $institutions, ]),
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'label' => 'name',
            'value' => function($data) {
                return $data->name;
            }
        ],
        [
            'label' => 'Course Type',
            'value' => function($data) {
                return $data->courseType['name'];
            }
        ],
        [
            'label' => 'Number of Course',
            'value' => function($data) {
                return $data->courseCount;
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view}&nbsp;&nbsp;{delete}&nbsp;&nbsp;',
            'buttons' => [
                    'view' => function($url, $model) {
                        $url = Url::toRoute(['exemption/view-institution', 'id' => $model->id]);
                        return Html::a('view', $url, ['title' => 'View']);
                    },
                    'delete' => function($url,$model){
                        $url = Url::toRoute(['exemption/delete-institution', 'id' => $model->id]);
                        return Html::a('delete', $url, ['title' => 'Delete']);
                    },
                ],
            ],
        ],
    ]);
?>