<?php
use yii\bootstrap\Modal;
use yii\helpers\Html;
$this->title = Yii::t('app', 'Exemption :: Institution');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row members-index">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading"><span class="fa fa-user"></span> <?= $this->title ?></div>
            <div class="panel-body">
                <?php if (Yii::$app->session->hasFlash('fail-message')): ?>
                    <div class="alert alert-danger"><?= Yii::$app->session->getFlash('fail-message') ?></div>
                <?php endif; ?>
                <?php if (Yii::$app->session->hasFlash('success-message')): ?>
                    <div class="alert alert-success"><?= Yii::$app->session->getFlash('success-message') ?></div>
                <?php endif; ?>
                <div class="row">
                    <div class="col-md-6">
                        <p class="text-left"><button class="btn btn-sm btn-primary" data-whatever="" data-toggle="modal" data-target="#NewExemptionCourseModal"><span class="glyphicon glyphicon-plus"></span> New Course</button></p>
                    </div>
                    <div class="col-md-6">
                        <?= $this->render('_institutionsummary', ['model' => $model]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?= $this->render('_institutioncourses', ['courses' => $courses]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Course Modal -->
<?php
Modal::begin([
    'id' => 'NewExemptionCourseModal',
    'header' => '<h4 class="modal-title" >New Course</h4>',
    'footer' =>
    Html::button('Cancel', ['class' => 'btn btn-default', 'data-dismiss' => 'modal'])
    . PHP_EOL .
    Html::button('Add Course', ['id' => 'new-course-btn', 'class' => 'btn btn-primary btn-modal-save']),
]);
?>
<?= $this->render('_courseform', ['model' => $exemptionCourseModel]) ?>
<?php Modal::end() ?>