<?php
use yii\helpers\Html;
use yii\bootstrap\Modal;
$this->title = Yii::t('app', 'Students\' Exemption Verification');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row members-index">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading"><?=$this->title?></div>
            <div class="panel-body">
                <?php if(Yii::$app->session->hasFlash('fail-message')): ?>
                    <div class="alert alert-danger"><?=Yii::$app->session->getFlash('fail-message')?></div>
                <?php endif; ?>
                <?php if(Yii::$app->session->hasFlash('success-message')): ?>
                    <div class="alert alert-success"><?=Yii::$app->session->getFlash('success-message')?></div>
                <?php endif; ?>
                <!--<p class="text-right"><button class="btn btn-sm btn-primary" data-whatever="" data-toggle="modal" data-target="#BatchVerificationModal">Batch Verification</button></p>-->
                <?= $this->render('_data', [ 'model' => $model,'students'=>$datas,'remarks'=>$remarks]) ?>
            </div>
        </div>
    </div>
</div>


<!-- Verification Modal -->
<?php
    Modal::begin([
        'id' => 'VerifyRedirectModal',
        'header' => '<h4 class="modal-title" >Record Verification</h4>',
        'footer' =>
            Html::button('Cancel', ['class' => 'btn btn-default', 'data-dismiss' => 'modal'])
            . PHP_EOL .
            Html::button('Verify', ['id' => 'verify-redirect-btn', 'class' => 'btn btn-primary btn-modal-save']),
    ]);
?>
<?= $this->render('@common/views/_verify',['remarks'=>$remarks]) ?>
<?php Modal::end() ?>