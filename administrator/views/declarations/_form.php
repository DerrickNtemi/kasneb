<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<?php $form = ActiveForm::begin([
        'action'=>Yii::$app->urlManager->createUrl(['declarations/create']),
        'id' => 'declaration-model-form',
        'enableClientValidation' => true,
    ]); ?>
<?= Html::activeHiddenInput($model,'id') ?>
<?= $form->field($model, 'description')->textarea() ?>

<?php ActiveForm::end(); ?>
<h2></h2>
<div class="alert alert-danger"  id="error-summary" style="display: none;"></div>
<h2></h2>
<div id="progress-spinner" class="text-center" style="display: none;">
    <?= Html::img('@web/images/ajax-loader.gif', ['alt' => 'progress spinner']) ?>
</div>
<h2></h2>