<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ArrayDataProvider;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\helpers\Json;
?>
<?=

GridView::widget([

    'dataProvider' => new ArrayDataProvider([
        'allModels' => $datas,
            ]),
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'description',
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update}&nbsp;&nbsp;{delete}&nbsp;&nbsp;',
            'buttons' => [
                'update' => function($url, $model) {
                    $data = Json::encode($model->getAttributes());
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['data-whatever' => $data, 'data-toggle' => "modal", 'data-target' => "#DeclarationModal", 'title' => Yii::t('yii', 'Update'),]);
                },
                'delete' => function($url, $model) {
                    $url = Url::toRoute(['declarations/delete', 'id' => $model->id]);
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, ['data-whatever' => $url, 'data-toggle' => "modal", 'data-target' => "#DeleteRedirectModal", 'title' => Yii::t('yii', 'Delete'),]);
                },
                ],
            ],
        ],
    ]);
?>
        <!-- Creation Model -->
        <?php
        Modal::begin([
            'id' => 'DeclarationModal',
            'header' => '<h4 class="modal-title" id="create-declaration-modal-header" >Create Declaration</h4>',
            'footer' =>
            Html::button('Cancel', ['class' => 'btn btn-default', 'data-dismiss' => 'modal'])
            . PHP_EOL .
            Html::button('Create', ['id' => 'register-declaration-btn', 'class' => 'btn btn-primary btn-modal-save']),
        ]);
        ?>
        <?= $this->render('_form', ['model' => $model]) ?>
        <?php Modal::end() ?>


        <!-- Delete Redirect Modal -->
        <?php
        Modal::begin([
            'id' => 'DeleteRedirectModal',
            'header' => '<h4 class="modal-title" >Delete Record</h4>',
            'footer' =>
            Html::button('Cancel', ['class' => 'btn btn-default', 'data-dismiss' => 'modal'])
            . PHP_EOL .
            Html::button('Delete', ['id' => 'delete-redirect-btn', 'class' => 'btn btn-primary btn-modal-save']),
        ]);
        ?>
        <?= $this->render('@common/views/_delete') ?>
        <?php Modal::end() ?>