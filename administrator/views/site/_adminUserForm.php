<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<?php $form = ActiveForm::begin([
        'action'=>Yii::$app->urlManager->createUrl(['site/create-user']),
        'id' => 'admin-user-form',
        'enableClientValidation' => true,
    ]); ?>
<?= Html::activeHiddenInput($model,'id') ?>
<div class="row">
    <div class="col-md-6">
        <?= $form->field($model, 'firstName')->textInput() ?>        
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'otherNames')->textInput() ?>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <?= $form->field($model, 'email')->textInput() ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'phoneNumber')->textInput() ?>        
    </div>
</div>
<div class="row">
    <div class="col-md-6">        
        <?= $form->field($model, 'role', [
                    'inputOptions' => ['class' => 'form-control  form-control-custom'],
                ])->dropDownList($rolesData,['prompt'=>'Select role ...']) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
<h2></h2>
<div class="alert alert-danger"  id="error-summary" style="display: none;"></div>
<h2></h2>
<div id="progress-spinner" class="text-center" style="display: none;">
    <?= Html::img('@web/images/ajax-loader.gif', ['alt' => 'progress spinner']) ?>
</div>
<h2></h2>