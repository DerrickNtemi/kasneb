<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ArrayDataProvider;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\helpers\Json;
?>
<?=
GridView::widget([

    'dataProvider' => new ArrayDataProvider([
        'allModels' => $users,
            ]),
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'label' => 'Name',
            'value' => function($data) {
                return $data->firstName . " " . $data->otherNames;
            }
        ],
        [
            'label' => 'Username',
            'value' => function($data) {
                return $data->email;
            }
        ],
        [
            'label' => 'Phone',
            'value' => function($data) {
                return $data->phoneNumber;
            }
        ],
        [
            'label' => 'Date Created',
            'value' => function($data) {
                return $data->created;
            }
        ],
        [
            'label' => 'Role',
            'value' => function($data) {
                return $data->role['description'];
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update}&nbsp;&nbsp;{delete}&nbsp;&nbsp;',
            'buttons' => [
                'update' => function($url, $model) {
                    $data = Json::encode($model->getAttributes());
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['data-whatever' => $data, 'data-toggle' => "modal", 'data-target' => "#AdminUserModal", 'title' => Yii::t('yii', 'Update'),]);
                },
                'delete' => function($url, $model) {
                    $url = Url::toRoute(['site/delete-user', 'id' => $model->id]);
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, ['data-whatever' => $url, 'data-toggle' => "modal", 'data-target' => "#DeleteRedirectModal", 'title' => Yii::t('yii', 'Delete'),]);
                },
                    ],
                ],
            ],
        ]);
        ?>

        <!-- Creation Model -->
        <?php
        Modal::begin([
            'id' => 'AdminUserModal',
            'header' => '<h4 class="modal-title" id="admin-user-modal-header" >Create User</h4>',
            'footer' =>
            Html::button('Cancel', ['class' => 'btn btn-default', 'data-dismiss' => 'modal'])
            . PHP_EOL .
            Html::button('Create', ['id' => 'admin-user-btn', 'class' => 'btn btn-primary btn-modal-save']),
        ]);
        ?>
        <?= $this->render('_adminUserForm', ['model' => $model,'rolesData'=>$rolesData]) ?>
        <?php Modal::end() ?>


        <!-- Delete Redirect Modal -->
        <?php
        Modal::begin([
            'id' => 'DeleteRedirectModal',
            'header' => '<h4 class="modal-title" >Delete Record</h4>',
            'footer' =>
            Html::button('Cancel', ['class' => 'btn btn-default', 'data-dismiss' => 'modal'])
            . PHP_EOL .
            Html::button('Delete', ['id' => 'delete-redirect-btn', 'class' => 'btn btn-primary btn-modal-save']),
        ]);
        ?>
        <?= $this->render('@common/views/_delete') ?>
        <?php Modal::end() ?>