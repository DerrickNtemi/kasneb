<?php
$this->title = Yii::t('app', 'Audit Trail');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading"><?= $this->title ?></div>
            <div class="panel-body">
                <div class="row" style="background-color: #f5f5f0; margin-right: 0px; margin-left: 0px; margin-bottom:10px;">
                    <div class="col-md-11">
                        <?= $this->render('_filterAdvanced', ['model' => $filterModel , 'users' => $users]) ?>
                    </div>
                    <div class="col-md-1" style="padding-top: 25px; padding-left: 0px;">
                        <button class = "btn btn-sm btn-primary" id= "filter-audit-trail" ><span class="fa fa-search"></span>Search</button>
                    </div>
                </div> 
                <div class="row">
                    <div class="col-md-12">
                        <?= $this->render('_trails', ['data' => $audits]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>