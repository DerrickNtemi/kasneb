<div class="card-holder-row">
    <div class="cards card-1">
        <div class="row card-title card-title-1">
            <div class="col-md-12">
                <p class="card-header"><?=$titleArray['student']?></p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4"><span class="fa fa-area-chart" id="dashboard-span"></span></div>
            <div class="col-md-8">
                <p class="card-value"><?=$cardData['student']?></p>
            </div>
        </div>
    </div>
    <div class="cards card-2">
        <div class="row card-title card-title-2">
            <div class="col-md-12">
                <p class="card-header"><?=$titleArray['tranVol']?></p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4"><span class="fa fa-bar-chart-o" id="dashboard-span"></span></div>
            <div class="col-md-8">
                <p class="card-value"><?=$cardData['volume']?></p>
            </div>
        </div>
    </div>
    <div class="cards card-3">
        <div class="row card-title card-title-3">
            <div class="col-md-12">
                <p class="card-header"><?=$titleArray['tranVal']?></p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4"><span class="fa fa-line-chart" id="dashboard-span"></span></div>
            <div class="col-md-8">
                <p class="card-value"><?=$cardData['value']?></p>
            </div>
        </div>
    </div>
</div>