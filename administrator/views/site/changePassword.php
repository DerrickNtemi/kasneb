<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Change Password';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading"><?= $this->title ?></div>
            <div class="panel-body">

                <div class="row">
                    <div class="col-lg-5">
                        <?php $form = ActiveForm::begin(['id' => 'change-password-form']); ?>

                        <?= $form->field($model, 'password')->passwordInput() ?>

<?= $form->field($model, 'passwordConfirm')->passwordInput() ?>

                        <div class="form-group">
<?= Html::submitButton('Change Password', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                        </div>

<?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
