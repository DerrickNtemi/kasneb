<script>
window.onload = function() {
  loanGraph(<?=json_encode($graphData)?>);
  pieChartOne(<?=json_encode($chartData)?>);
  pieChartTwo(<?=json_encode($chartData)?>);
};
</script>
<div class="row" style="margin-top: 10px; margin-left: 1px;">
    <div class="col-md-8">
        <div id="graph-container" style="min-width: 310px; height: 400px; margin: 0 auto;"></div>
    </div>
    <div class="col-md-4" style="background-color: lightgrey;">
        <div class="row pie-charts">
            <div class="col-md-12">                
                <div id="total-pie-chart" style="min-width: 310px; height: 200px; margin: 0 auto;  "></div>
            </div>
        </div>
        <div class="row pie-charts">
            <div class="col-md-12">                
                <div id="visitors-pie-chart" style="min-width: 310px; height: 200px; margin: 0 auto;  "></div>
            </div>
        </div>
    </div>
</div>