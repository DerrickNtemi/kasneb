<?php

use yii\data\ArrayDataProvider;
use yii\grid\GridView;

?>
<?=

GridView::widget([

    'dataProvider' => new ArrayDataProvider([
        'allModels' => $data,
            ]),
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'label' => 'Timestamp',
            'value' => function($data) {
                return $data->created;
            }
        ],
        [
            'label' => 'User',
            'value' => function($data) {
                return $data->user['email'];
            }
        ],
        [
            'label' => 'Description',
            'value' => function($data) {
                return $data->description;
            }
        ],
    ],
]);
?>
