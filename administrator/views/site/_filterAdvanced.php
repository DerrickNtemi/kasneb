<?php
use kartik\date\DatePicker;
use kartik\form\ActiveForm;

$form = ActiveForm::begin([
            'id' => 'filter-audit-trail-form',
            'action' => Yii::$app->urlManager->createUrl(['site/audit-trail']),
            'options' => ['enctype' => 'multipart/form-data',],
            'fieldConfig' => [
                'template' => "{label}{input}\n{hint}\n{error}",
            ],
        ]);
?>
<div class="row">
    <div class="col-md-12">
        <div class="row form-row-below">    
            <div class="col-md-3"></div>       
            <div class="col-md-3">                            
                            <?= $form->field($model, 'user', [
                                    'addon' => ['prepend' => ['content' => '<i class="fa fa-user"></i>']],
                                'inputOptions' => ['class' => 'form-control  form-control-custom'],
                            ])->dropDownList($users) ?>
                        </div>
            <div class="col-md-3"> 

                <?=
                $form->field($model, 'startDate', [
                    'inputOptions' => ['class' => 'form-control  form-control-custom'],
                ])->widget(DatePicker::classname(), [
                    'value' => date("Y-m-d"),
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ])
                ?>
            </div>
            <div class="col-md-3"> 

                <?=
                $form->field($model, 'endDate', [
                    'inputOptions' => ['class' => 'form-control  form-control-custom'],
                ])->widget(DatePicker::classname(), [
                    'value' => date("Y-m-d"),
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ])
                ?>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>