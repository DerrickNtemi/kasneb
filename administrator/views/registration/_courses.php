
<div class="panel panel-default">
    <div class="panel-heading">
        <strong>Course Application Details</strong>
    </div>
    <div class="panel-body">
        <table class="table table-striped table-bordered">
            <tr><th>Course Name</th><td><?= $courses['course']['name'] ?></td></tr>
            <tr>
                <th>Qualifications</th>
                <td>
                    <ol>
                        <?php
                        foreach ($courses['studentRequirements'] as $requirement) {
                            echo "<li>" . $requirement['description'] . "</li>";
                        }
                        ?>
                    </ol>
                </td>
            </tr>
            <tr><th>Date</th><td><?= $courses['created'] ?></td></tr>
            <?php if (!empty($courses['documents'])) { ?> 
                <tr>
                    <th>Documents</th>
                    <td>
                        <?php
                        foreach ($courses['documents'] as $document) {
                            ?>
                            <a href="<?= Yii::$app->params['studentPortalUrl'] ?>/students/qualifications/<?= $document['name'] ?>" target="_blank">preview</a>/<a href="<?= Yii::$app->params['studentPortalUrl'] ?>/students/qualifications/<?= $document['name'] ?>" download>Download</a> , &nbsp;  &nbsp; 
                            <?php
                        }
                        ?>
                    </td>
                </tr>
            <?php } ?>
        </table>
    </div>
</div>