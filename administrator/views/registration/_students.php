<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ArrayDataProvider;
use yii\helpers\Url;
use yii\bootstrap\Modal;
?>
<?=

GridView::widget([

    'dataProvider' => new ArrayDataProvider([
        'allModels' => $students,
            ]),
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'label' => 'Name',
            'value' => function($data) {
                return $data->studentId['firstName']." ".$data->studentId['middleName']." ".$data->studentId['lastName'];
            }
        ],
        [
            'label' => 'Reg. Date',
            'value' => function($data) {
                return $data->created;
            }
        ],
        [
            'label' => 'Course',
            'value' => function($data) {
                return $data->courseId['name'];
            }
        ],
        [
            'class' => 'yii\grid\CheckboxColumn',
            'checkboxOptions' => function ($model, $key, $index, $column) {
                return ['value' => $model->id];
            },
            'name' => 'id',
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view}&nbsp;&nbsp;{verify}&nbsp;&nbsp;',
            'buttons' => [
                    'view' => function($url, $model) {
                        $url = Url::toRoute(['registration/view', 'id' => $model->studentId['id']]);
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => 'View']);
                    },
                    'verify' => function($url,$model){
                        $url = Url::toRoute(['registration/verify','courseId'=>$model->id]);
                        return Html::a('<span class="glyphicon glyphicon-ok"></span>', $url, ['data-whatever'=>$url, 'data-toggle'=>"modal", 'data-type'=>'single', 'data-target'=>"#VerifyRedirectModal",'title' => Yii::t('yii', 'Verify'),]);
                    },
                ],
            ],
        ],
    ]);
?>

<!-- Verification Modal -->
<?php
    Modal::begin([
        'id' => 'VerifyRedirectModal',
        'header' => '<h4 class="modal-title" >Record Verification</h4>',
        'footer' =>
            Html::button('Cancel', ['class' => 'btn btn-default', 'data-dismiss' => 'modal'])
            . PHP_EOL .
            Html::button('Verify', ['id' => 'verify-redirect-btn', 'class' => 'btn btn-primary btn-modal-save']),
    ]);
?>
<?= $this->render('@common/views/_verify',['remarks'=>$remarks]) ?>
<?php Modal::end() ?>