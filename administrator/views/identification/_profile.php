<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-md-8 personal-details-div">Personal Details</div>
        </div>
    </div>
    <div class="panel-body">
        <div class="row" style="margin-bottom: 10px;">
            <div class="col-md-5">
                <img src="<?=Yii::$app->params['studentPortalUrl']?>/students/documents/<?= $passport ?>" style="height:120px;width:120px;" > 
             </div>
            <div class="col-md-4"></div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="row profile-info-row">  
                    <div class="col-md-4"><strong><?= $model->getAttributeLabel("firstName") ?></strong></div>
                    <div class="col-md-1"><strong>:</strong></div>
                    <div class="col-md-7"><?= $model->firstName ?></div>
                </div>
                <div class="row profile-info-row">  
                    <div class="col-md-4"><strong><?= $model->getAttributeLabel("middleName") ?></strong></div>
                    <div class="col-md-1"><strong>:</strong></div>
                    <div class="col-md-7"><?= $model->middleName ?></div>
                </div>
                <div class="row profile-info-row">  
                    <div class="col-md-4"><strong><?= $model->getAttributeLabel("lastName") ?></strong></div>
                    <div class="col-md-1"><strong>:</strong></div>
                    <div class="col-md-7"><?= $model->lastName ?></div>
                </div>
                <div class="row profile-info-row">  
                    <div class="col-md-4"><strong><?= $model->getAttributeLabel("nationality") ?></strong></div>
                    <div class="col-md-1"><strong>:</strong></div>
                    <div class="col-md-7"><?= $model->nationality['nationality'] ?></div>
                </div>
                <div class="row profile-info-row">  
                    <div class="col-md-4"><strong><?= $model->getAttributeLabel("dob") ?></strong></div>
                    <div class="col-md-1"><strong>:</strong></div>
                    <div class="col-md-7"><?= $model->dob ?></div>
                </div>
                <div class="row profile-info-row">  
                    <div class="col-md-4"><strong><?= $model->getAttributeLabel("gender") ?></strong></div>
                    <div class="col-md-1"><strong>:</strong></div>
                    <div class="col-md-7"><?= $gender ?></div>
                </div>
                <div class="row profile-info-row">  
                    <div class="col-md-4"><strong><?= $model->getAttributeLabel("email") ?></strong></div>
                    <div class="col-md-1"><strong>:</strong></div>
                    <div class="col-md-7"><?= $model->email ?></div>
                </div>
                <div class="row profile-info-row">  
                    <div class="col-md-4"><strong><?= $model->getAttributeLabel("phoneNumber") ?></strong></div>
                    <div class="col-md-1"><strong>:</strong></div>
                    <div class="col-md-7"><?= $model->phoneNumber ?></div>
                </div>
                <div class="row profile-info-row">  
                    <div class="col-md-4"><strong><?= $model->getAttributeLabel("contact") ?></strong></div>
                    <div class="col-md-1"><strong>:</strong></div>
                    <div class="col-md-7"><?= $model->contact['postalAddress'] . " - " . $model->contact['postalCode'] . " " . $model->contact['town'] ?></div>
                </div>
                <div class="row profile-info-row">  
                    <div class="col-md-4"><strong><?= $model->getAttributeLabel("documentNo") ?></strong></div>
                    <div class="col-md-1"><strong>:</strong></div>
                    <div class="col-md-7"><?= $model->documentNo ?></div>
                </div>
                <div class="row profile-info-row">  
                    <div class="col-md-4"><strong><?= $model->getAttributeLabel("documentType") ?></strong></div>
                    <div class="col-md-1"><strong>:</strong></div>
                    <div class="col-md-7"><?= $model->documentType['name'] ?></div>
                </div>
                <div class="row profile-info-row">  
                    <div class="col-md-4"><strong>Document</strong></div>
                    <div class="col-md-1"><strong>:</strong></div>
                    <div class="col-md-7"><a href="<?=Yii::$app->params['studentPortalUrl']?>/students/documents/<?=$model->documentScan?>" target="_blank">click to preview</a> &nbsp;  &nbsp;  &nbsp; <a href="<?=Yii::$app->params['studentPortalUrl']?>/students/documents/<?=$model->documentScan?>"  download > Download </a></div>
                </div>
            </div>
        </div>
    </div>
</div>