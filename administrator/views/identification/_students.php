<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ArrayDataProvider;
use yii\helpers\Url;
?>
<?=

GridView::widget([

    'dataProvider' => new ArrayDataProvider([
        'allModels' => $students,
            ]),
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'label' => 'Name',
            'value' => function($data) {
                return $data->studentId['firstName']." ".$data->studentId['middleName']." ".$data->studentId['lastName'];
            }
        ],
        [
            'label' => 'Reg. No',
            'value' => function($data) {
                return $data->registrationNumber;
            }
        ],
        [
            'label' => 'Phone Number',
            'value' => function($data) {
                return $data->studentId['phoneNumber'];
            }
        ],
        [
            'label' => 'Reg. Date',
            'value' => function($data) {
                return $data->created;
            }
        ],
        [
            'label' => 'Course',
            'value' => function($data) {
                return $data->courseId['name'];
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view}&nbsp;&nbsp;{approve}&nbsp;&nbsp;{reject}&nbsp;&nbsp;',
            'buttons' => [
                    'view' => function($url, $model) {
                        $url = Url::toRoute(['identification/view', 'id' => $model->studentId['id']]);
                        return Html::a('view', $url, ['title' => 'View']);
                    },
                    'approve' => function($url,$model){
                        $url = Url::toRoute(['identification/verify','courseId'=>$model->id, 'courseStatus'=>"ACTIVE"]);
                        return Html::a('approve', $url, ['style' => 'color:#00a65a;', 'title' => Yii::t('yii', 'Approve'),]);
                    },
                    'reject' => function($url,$model){
                        $url = Url::toRoute(['identification/verify','courseId'=>$model->id, 'courseStatus'=>"FAILED_IDENTIFICATION"]);
                        return Html::a('reject', $url, ['style' => 'color:#dd4b39;', 'title' => Yii::t('yii', 'Reject'),]);
                    },
                ],
            ],
        ],
    ]);
?>