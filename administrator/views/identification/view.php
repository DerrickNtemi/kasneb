<?php
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
$url = Url::toRoute(['identification/verify','courseId'=>$courses['id'],"courseStatus"=>"ACTIVE"]);
$rejectUrl = Url::toRoute(['identification/verify','courseId'=>$courses['id'],"courseStatus"=>"FAILED_IDENTIFICATION"]);
$this->title = Yii::t('app', 'Student Profile');
$this->params['breadcrumbs'][] = $this->title;
$passport = empty($model->passportPhoto) ? "placeholder.jpg" : $model->passportPhoto;
$gender = empty($model->gender) ? "" : ($model->gender == 1 ? "Male" : "Female");
$documentType = empty($model->documentType) ? "ID No." : ($model->documentType == 1 ? "ID No." : ($model->documentType == 2 ? "Birth Cert. No." : "Passport No."));
?>
<div class="row members-index">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading"><span class="fa fa-user"></span> <?=$this->title?></div>
            <div class="panel-body">
                <?php if(Yii::$app->session->hasFlash('fail-message')): ?>
                    <div class="alert alert-danger"><?=Yii::$app->session->getFlash('fail-message')?></div>
                <?php endif; ?>
                <?php if(Yii::$app->session->hasFlash('success-message')): ?>
                    <div class="alert alert-success"><?=Yii::$app->session->getFlash('success-message')?></div>
                <?php endif; ?>
                <p class="text-right">
                    <a class="btn btn-sm btn-success" href="<?=$url?>" >approve</a>
                    <a class="btn btn-sm btn-danger" href="<?=$rejectUrl?>" >reject</a>
                </p>
                 <div class="row">
                    <div class="col-md-5">
                        <?= $this->render('_profile', [ 'model' => $model,'passport'=>$passport,'gender'=>$gender,"documentType"=>$documentType]) ?>
                    </div>
                    <div class="col-md-7">
                        <?= $this->render('_courses', [ 'courses' => $courses]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Verification Modal -->
<?php
    Modal::begin([
        'id' => 'VerifyRedirectModal',
        'header' => '<h4 class="modal-title" >Record Verification</h4>',
        'footer' =>
            Html::button('Cancel', ['class' => 'btn btn-default', 'data-dismiss' => 'modal'])
            . PHP_EOL .
            Html::button('Verify', ['id' => 'verify-redirect-btn', 'class' => 'btn btn-primary btn-modal-save']),
    ]);
?>
<?= $this->render('@common/views/_verify',['remarks'=>$remarks]) ?>
<?php Modal::end() ?>
