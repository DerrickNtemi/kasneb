<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "COUNTRY".
 *
 * @property string $CODE
 * @property string $NAME
 *
 * @property STUDENT[] $sTUDENTs
 */
class COUNTRY extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'COUNTRY';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CODE'], 'required'],
            [['CODE', 'NAME'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'CODE' => 'Code',
            'NAME' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSTUDENTs()
    {
        return $this->hasMany(STUDENT::className(), ['COUNTRY_CODE' => 'CODE']);
    }
}
