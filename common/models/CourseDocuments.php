<?php

namespace common\models;

class CourseDocuments extends \yii\base\Model {

    public $document;
    public $studentId;
    public $courseId;
    public $courseTypeCode;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['document','studentId','courseTypeCode', 'courseId'], 'safe'],
       ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'document' => 'Attach Certificate(s) *can attach multiple'
        ];
    }
}

