<?php

namespace common\models;


class Sitting extends \yii\base\Model
{
    public $id;
    public $studentId;
    public $registrationNumber;
    public $month;
    public $year;
    public $courseId;


    public function rules() {
        return [
            [['month','year'], 'required'],
            [['id','studentId','courseId'], 'safe'],
       ];
    }
    
    public function attributeLabels() {
        return [
            'registrationNumber' => 'Registration Number',
            'month' => 'Sitting',
            'year' => 'Year',
        ];
    }
}
