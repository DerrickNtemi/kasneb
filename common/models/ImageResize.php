<?php

namespace common\models;

class ImageResize extends \yii\base\Model {

    public function resizeImage($filename) {
        $percent = 0.5;
        // Get new sizes
        list($width, $height) = getimagesize($filename);
        $newwidth = $width * $percent;
        $newheight = $height * $percent;
        // Load
        $thumb = imagecreatetruecolor($newwidth, $newheight);
        $source = imagecreatefromjpeg($filename);
        // Resize
        imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
        // Output
        imagejpeg($thumb);
    }

    function resizeImage2($file, $w, $h, $crop = FALSE) {
        list($width, $height) = getimagesize($file);
        $r = $width / $height;
        if ($crop) {
            if ($width > $height) {
                $width = ceil($width - ($width * abs($r - $w / $h)));
            } else {
                $height = ceil($height - ($height * abs($r - $w / $h)));
            }
            $newwidth = $w;
            $newheight = $h;
        } else {
            if ($w / $h > $r) {
                $newwidth = $h * $r;
                $newheight = $h;
            } else {
                $newheight = $w / $r;
                $newwidth = $w;
            }
        }
        $src = imagecreatefromjpeg($file);
        $dst = imagecreatetruecolor($newwidth, $newheight);
        imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

        return $dst;
    }

    function store_uploaded_image($html_element_name, $new_img_width, $new_img_height) {

        $target_dir = "your-uploaded-images-folder/";
        $target_file = $target_dir . basename($_FILES[$html_element_name]["name"]);

        $image = new SimpleImage();
        $image->load($_FILES[$html_element_name]['tmp_name']);
        $image->resize($new_img_width, $new_img_height);
        $image->save($target_file);
        return $target_file; //return name of saved file in case you want to store it in you database or show confirmation message to user
    }

}
