<?php

namespace common\models;

class AdminDeclarations extends \yii\base\Model {

    public $id;
    public $description;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['description'], 'required'],
            [['id'], 'safe'],
       ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'Id',
            'description' => 'Description',
        ];
    }
}

