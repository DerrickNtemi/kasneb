<?php

namespace common\models;


class WalletBalance extends \yii\base\Model
{
    public $id;
    public $phoneNumber;
    public $jpPin;


    public function rules() {
        return [
            [['jpPin','phoneNumber'], 'required'],
            [['id'], 'safe'],
       ];
    }
    
    public function attributeLabels() {
        return [
            'jpPin' => 'Pin',
            'phoneNumber'=>'Phone Number',
        ];
    }
}
