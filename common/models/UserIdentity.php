<?php

namespace common\models;

class UserIdentity extends \yii\base\Object implements \yii\web\IdentityInterface
{
    public $id;
    public $email;
    public $loginAttempts;
    public $verificationToken;
    public $activated;
    public $banned;
    public $lastLogin;
    public $student;
    public $authKey;
    public $password;    
    public $user;
    public $status;
    public $phoneNumber;
    public $emailActivated;
    public $phoneNumberActivated;


    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        $service = new Service;
        $result = $service->getUserById($id);
        return new static($result); 
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByUsername($username, $password)
    {
        $service = new Service;
        $result = $service->login(['email'=>$username, 'password'=>$password]);
        return new static($result);
    }
    
      

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }

 

    public function getId() {
        return $this->id;
    }

}
