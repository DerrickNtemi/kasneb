<?php

namespace common\models;

class Contact extends \yii\base\Model {

    public $postalAddress;
    public $countyId;
    public $countryId;
    public $town;
    public $postalCode;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['postalAddress', 'countryId', 'town','postalCode'], 'required'],
            [['postalAddress', 'town', 'postalCode','countryId' ], 'string'],
            [['countyId'], 'integer'],
       ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'postalAddress' => 'Postal Address',
            'postalCode' => 'Postal Code',
            'town' => 'City / Town',
            'countryId' => 'Country',
            'countyId' => 'County of Residence',
        ];
    }

}
