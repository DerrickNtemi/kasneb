<?php

namespace common\models;


class Notifications extends \yii\base\Model
{
    public $id;
    public $status;
    public $type;
    public $details;


    public function rules() {
        return [
            [['status','type','details'], 'required'],
            [['id'], 'safe'],
       ];
    }
    
    public function attributeLabels() {
        return [
            'id' => 'Ref.',
            'status' => 'Status',
            'type'=>'Type',
            'details'=>'Details',
        ];
    }
}
