<?php

namespace common\models;


class Course extends \yii\base\Model
{
    public $id;
    public $name;
    public $levelCollection;
    public $feeTypeCollection;
    
    public function rules() {
        return [
            [['name', 'levelCollection', 'feeTypeCollection'], 'required'],
            [['id'], 'safe'],
       ];
    }
}
