<?php

namespace common\models;


class ExemptionVerification extends \yii\base\Model
{
    public $id;
    public $studentId;
    public $paper;
    public $qualification;
    public $verified;
    public $status;
    public $dateVerified;
    public $verifyRemarks;
    public $fullRegNo;
    public $type;
    public $courseId;
    public $student;
    public $studentCourseId;
    public $verifiedBy;


    public function rules() {
        return [
            [['id','studentId','paper','qualification','verified','status','dateVerified','verifyRemarks','fullRegNo','type','courseId','student','studentCourseId','verifiedBy'], 'safe'],
       ];
    }
    
    public function attributeLabels() {
        return [
            'studentId' => 'Student',
            'paper' => 'Papers',
            'qualification'=>'Exemption Type',
            'verified'=>"Verified",
            'status'=>'Status',
            'dateVerified'=>'Date Verified',
            'verifyRemarks'=>'Verify Remarks',
        ];
    }
}
