<?php

namespace common\models;

use yii\base\Model;

/**
 * Login form
 */
class ChangePassword extends Model {

    public $id;
    public $password;
    public $passwordConfirm;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id','password', 'passwordConfirm'], 'required'],     
            ['passwordConfirm', 'compare', 'compareAttribute' => 'password', 'message' => "Passwords don't match"],
         ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'password' => 'Password',
            'passwordConfirm' => 'Confirm Password'
        ];
    }

}
