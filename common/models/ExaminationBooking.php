<?php

namespace common\models;

class ExaminationBooking extends \yii\base\Model {

    public $id;
    public $sitting;
    public $invoice;
    public $sittingCentre;
    public $papers;
    public $status;
    public $student;
    public $fullRegNo;
    public $created;
    public $course;
    public $phoneNumber;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id','status','papers','sittingCentre','invoice','sitting','student','fullRegNo','created','course','phoneNumber'], 'safe'],
       ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'status' => 'Status',
            'papers' => 'Papers',
            'sittingCenter' => 'Sitting Center',
            'invoice' => 'Invoice',
            'sitting' => 'Sitting',
            'course' => 'Course',
            'phoneNumber'=>'Phone Number'
        ];
    }

}
