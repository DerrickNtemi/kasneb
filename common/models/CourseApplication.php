<?php

namespace common\models;

class CourseApplication extends \yii\base\Model {

    public $profCourseId;
    public $dipCourseId;
    public $studentId;
    public $profDocument;
    public $dipDocument;
    public $dipReq;
    public $profReq;
    public $documentNo;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['profCourseId','dipCourseId', 'dipReq','profReq','documentNo'], 'required'],
            [['profCourseId','dipCourseId'], 'integer'],
            [['studentId'], 'safe'],
       ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'profCourseId' => 'Course',
            'dipCourseId' => 'Course',
            'studentId' => 'Student',
            'dipDocument' => 'Attach Certified Certificate(s) *can attach multiple',
            'profDocument' => 'Attach Certified Certificate(s) *can attach multiple',
            'profReq' => 'Qualification',
            'dipReq' => 'Qualification',
            'documentNo' => 'Qualification Document No.',
        ];
    }

   public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['DIPLOMA'] = ['dipCourseId', 'studentId', 'dipDocument', 'dipReq','documentNo'];
        $scenarios['PROFESSIONAL'] = ['profCourseId','studentId','profDocument', 'profReq','documentNo'];
        return $scenarios;
    }
}

