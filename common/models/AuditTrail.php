<?php

namespace common\models;

class AuditTrail extends \yii\base\Model
{
    public $id;
    public $created;
    public $description;
    public $user;
    public $tableAffected;
    public $rowAffected;
    
   public function rules(){
        return [
            [['id','created','description','user','tableAffected','rowAffected'], 'required']
        ];
    }
    
}
