<?php

namespace common\models;

class ExemptionCourse extends \yii\base\Model
{

     public $id;
     public $institutionId;
     public $institution;
     public $courseName;
     public $courseType;
     public $papers;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['courseName','institutionId','courseType'], 'required'],
            [['id','papers','institution'], 'safe'],
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'courseName' => 'Course Name',                     
            'institutionId' => 'Institution',          

        ];
    }
}
