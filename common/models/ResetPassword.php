<?php

namespace common\models;

class ResetPassword extends \yii\base\Model {

    public $password;
    public $passwordRepeat;
    public $token;

    public function rules() {
        return [
            [['password', 'passwordRepeat'], 'required'],
            [['token'], 'safe'],
            ['passwordRepeat', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match" ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'password' => 'New Password',
            'passwordRepeat' => 'Confirm Password'
        ];
    }

}
