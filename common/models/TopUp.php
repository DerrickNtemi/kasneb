<?php

namespace common\models;


class TopUp extends \yii\base\Model
{
    public $id;
    public $phoneNumber;
    public $jpPin;
    public $amount;


    public function rules() {
        return [
            [['phoneNumber','amount'], 'required'],
            [['jpPin','id'], 'safe'],
       ];
    }
    
    public function attributeLabels() {
        return [
            'jpPin' => 'Pin',
            'phoneNumber'=>'Phone Number',
            'amount'=>'Amount',
        ];
    }
}
