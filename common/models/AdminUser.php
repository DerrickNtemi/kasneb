<?php

namespace common\models;

class AdminUser extends \yii\base\Model
{
    public $id;
    public $phoneNumber;
    public $firstName;
    public $otherNames;
    public $created;
    public $role;
    public $email;
   public function rules(){
        return [
            [['phoneNumber','firstName','otherNames','role','email'], 'required'],
            [['id','created'], 'safe'],
        ];
    }
    
}
