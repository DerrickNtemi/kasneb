<?php

namespace common\models;


class Filter extends \yii\base\Model
{
    public $startDate;
    public $endDate;
    public $stream;
    public $mode;
    public $courseStatus;
    public $month;
    public $year;
    public $courseType;
    public $course;
    public $user;
    public $declrations;
    public $response;
    public $center;
    public $zone;
    public $status;


    public function rules() {
        return [
            [['startDate','endDate'], 'required'],
            [['status','stream','mode','courseStatus','month','year','courseType','course','user','declrations','response','zone','center'], 'safe'],
       ];
    }
    
    public function attributeLabels() {
        return [
            'startDate' => 'Start Date',
            'endDate' => 'End Date',
            'stream'=>'Revenue Stream',
            'mode'=>'Payment Mode',
            'courseStatus'=>'Application Status',
            'year' => 'Year',
            'month' => 'Month',
            'course' => 'Course',
            'courseType' => 'Course Type',
            'user' => 'User',
            'declrations'=>'Declarations',
            'response'=>'Response',
            'center'=>'Center',
            'zone'=>'Zone',
            'status'=>'Status',
        ];
    }
}
