<?php

namespace common\models;

class InstitutionCourses extends \yii\base\Model
{

     public $id;
     public $name;
     public $institutionId;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['id','institutionId'], 'safe'],
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name'        

        ];
    }
}
