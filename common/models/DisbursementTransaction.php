<?php

namespace common\models;


class DisbursementTransaction extends \yii\base\Model
{
    public $id;
    public $disbursementId;
    public $amount;
    public $beneficiaryId;
    public $transactionRef;
    public $createdAt;
    public $confirmedAt;
    public $createdBy;
    public $transactionStatusId;
    public $statusId;


    public function rules() {
        return [
            [['disbursementId','amount','beneficiaryId','transactionRef'], 'required'],
            [['id','createdAt','confirmedAt','createdBy','transactionStatusId','statusId'], 'safe'],
       ];
    }
    
    public function attributeLabels() {
        return [
            'amount' => 'Amount',
            'disbursementId' => 'Disbursement',
            'beneficiaryId'=>'Beneficiary',
            'transactionRef'=>'Transaction Ref.',
        ];
    }
}
