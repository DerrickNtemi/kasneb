<?php
namespace common\models;

class Disbursements extends \yii\base\Model
{
    public $id;
    public $templateName;
    public $reference;
    public $amount;
    public $disbursementTypeId;
    public $createdBy;
    public $createdAt;
    public $disbursementStatusId;
    public $statusId;
    public $disbursementId;


    public function rules() {
        return [
                [['templateName'], 'required'],
                [['id','reference','amount','disbursementTypeId','createdBy','createdAt','disbursementStatusId'], 'safe'],
       ];
    }
    
    public function attributeLabels() {
        return [
            'templateName' => 'Template Name'
        ];
    }
}
