<?php

namespace common\models;


class ExemptionGroundsOthers extends \yii\base\Model
{
    public $id;
    public $registrationNumber;
    public $institutionId;
    public $institutionName;
    public $qualificationId;
    public $qualificationTypesId;
    public $document;
    public $courseId;
    public $paperId;
    public $courseName;


    public function rules() {
        return [
            [['document','qualificationTypesId','paperId'], 'required'],
            [['registrationNumber','institutionId','qualificationId','id','institutionName','courseName','courseId'], 'safe'],
       ];
    }
    
    public function attributeLabels() {
        return [
            'registrationNumber' => 'Registration Number',
            'institutionId' => 'Institution',
            'institutionName'=>'Institution Name',
            'qualificationId'=>'Qualification',
            'document'=>"Attach Letter of Completion / Transcript / Certified Certificate",
            'qualificationTypesId'=>'Qualification Type',
            'courseId'=>'Course',
        ];
    }
}
