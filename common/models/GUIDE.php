<?php

namespace common\models;

class GUIDE extends \yii\base\Model
{

    public $DESCRIPTION;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['DESCRIPTION'], 'required'],
            [['DESCRIPTION'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'DESCRIPTION' => 'Description',
        ];
    }
}
