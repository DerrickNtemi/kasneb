<?php

namespace common\models;

class Bill extends \yii\base\Model
{

     public $id;
     public $invoiceNo;
     public $invoiceDate;
     public $dueDate;
     public $items;
     public $pin;
     public $transRef;
     public $status;
     public $timestamp;
     public $methods;
     public $startDate;
     public $endDate;
     public $revenueStream;
     public $course;
     public $total;
     public $kesTotal;
     public $usdTotal;
     public $gbpTotal;
     public $localAmount;
     public $localCurrency;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['invoiceNo','invoiceDate','dueDate','items','localCurrency','localAmount'], 'required'],
            [['id',], 'integer'],
            [['invoiceNo',], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'invoiceNo' => 'Invoice Number',
            'invoiceDate' => 'Invoice Date',
            'dueDate' => 'Due Date',
            'items' => 'Items',
            'pin' => 'Wallet Pin',
            'startDate' => 'Start Date',
            'endDate' => 'End Date',
            'revenueStream' => 'Revenue Stream',
            'course' => 'Courses',
        ];
    }
}