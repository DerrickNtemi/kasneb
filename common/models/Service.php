<?php

namespace common\models;

use Yii;
use linslin\yii2\curl;
use yii\helpers\Json;
use yii\web\HttpException;

class Service {

    public $serviceUrl;
    public $adminServiceUrl;
    public $disbursementUrl;
    public $appKey;
    public $userId;
    public $clientId;
    public $singleDay;
    public $systemStartDate;

    public function __construct() {
        $identity = Yii::$app->user->getIdentity();
        if ($identity) {
            $this->userId = $identity->id;
        }

        $this->appKey = Yii::$app->params['AppKey'];
        $this->serviceUrl = Yii::$app->params['serviceUrl'];
        $this->adminServiceUrl = Yii::$app->params['adminServiceUrl'];
        $this->disbursementUrl = Yii::$app->params['disbursementUrl'];
    }

    public function revenueStream() {
        return [
            '' => 'All',
            'REGISTRATION_RENEWAL_FEE' => 'Renewal',
            'EXEMPTION_FEE' => 'Exemption',
            'EXAM_ENTRY_FEE' => 'Examination ',
            'REGISTRATION_FEE' => 'Registration',
        ];
    }

    public function rejectionReasonsRegistration() {
        return [
            'REJ_1' => 'Not Qualified for course Applied For',
            'REJ_2' => 'Mismatch of names on supporting Document and Identification Document',
            'REJ_3' => 'Already an Existing KASNEB student(Allow Typing  of Registration Number and Amount',
            'REJ_4' => 'Lack of Supporting Documents( Allow type of Missing Document',
            'REJ_5' => 'Not Qualified for course Applied For',
            'REJ_6' => 'Others',
        ];
    }

    public function rejectionReasonsExemption() {
        return [
            'Lack of Supporting Documents( Allow type of Missing Document' => 'Lack of Supporting Documents( Allow type of Missing Document',
        ];
    }

    public function sittingYears() {
        $i = 2014;
        $years[] = "All";
        while ($i <= date("Y")) {
            $years[$i] = $i;
            $i++;
        }
        return $years;
    }

    public function sittingMonths() {
        return [
            "" => 'All',
            "MAY" => 'MAY',
            "NOVEMBER" => 'NOVEMBER',
        ];
    }

    public function getStatus() {
        return [
            "" => 'All',
            100 => 'Confirmed',
            200 => 'Pending',
        ];
    }

    public function courseTypes() {
        return [
            "" => 'All',
            100 => 'Professional',
            200 => 'Diploma',
        ];
    }

    public function courseApplicationStatus() {
        return [
            2 => 'All',
            1 => 'Verified',
            0 => 'Pending Verification',
        ];
    }

    public function paymentMode() {
        return [
            '' => 'All',
            'JAMBOPAY E_WALLET' => 'Wallet',
            'BANK' => 'Bank',
        ];
    }

    function getInstitutionsMockup($id = 0) {
        $data = [];
        $data[0] = 'Select Institution';
        if ($id == 1) {
            $data[1] = "UoN";
            $data[2] = "KU";
            $data[3] = "JKUAT";
        } else if ($id == 2) {
            $data[4] = "KIM";
            $data[5] = "KMTC";
        } else if ($id == 3) {
            $data[1] = "UoN";
            $data[2] = "KU";
            $data[3] = "JKUAT";
        } else if ($id == 4) {
            $data[1] = "UoN";
            $data[2] = "KU";
            $data[3] = "JKUAT";
        }
        $data[6] = "Others";
        return $data;
    }

    function getTestCourseOthers($type) {
        $courses = [];
        $courses[0] = 'Select Course';
        if ($type == 1) {
            $courses[1] = 'Degree in B. Com';
        } else if ($type == 2) {
            $courses[2] = 'Degree in Business Administration';
        } else if ($type == 2) {
            $courses[3] = 'Degree in Business Administration';
        } else if ($type == 3) {
            $courses[4] = 'Degree in BA & Mathematics';
        } else if ($type == 4) {
            $courses[5] = 'Diploma Borehole Digging';
        } else if ($type == 5) {
            $courses[6] = 'Diploma Medicine';
        } else {
            $courses[1] = 'Degree in B. Com';
            $courses[2] = 'Degree in Business Administration';
            $courses[3] = 'Degree in Business Administration';
            $courses[4] = 'Degree in BA & Mathematics';
            $courses[5] = 'Diploma Borehole Digging';
            $courses[6] = 'Diploma Medicine';
        }
        $courses[7] = 'Others';
        return $courses;
    }

    function getQualificationMockup() {
        $data[0] = 'Select Qualification Type';
        $data[1] = 'Degree';
        $data[2] = 'Diploma';
        $data[3] = 'Masters';
        $data[4] = 'PHD';
        $data[5] = 'Others';
        return $data;
    }

    function getTestCourse($type) {
        $courses = [];
        if ($type == 1) {
            $courses[0] = 'Select Professional Course';
            $courses[1] = 'CPA';
            $courses[2] = 'CS';
            $courses[3] = 'CCIT';
        } else if ($type == 2) {
            $courses[0] = 'Select Diploma Course';
            $courses[5] = 'DCIT';
        } else {
            $courses[0] = 'Select Course';
            $courses[1] = 'CPA';
            $courses[2] = 'CS';
            $courses[3] = 'CCIT';
            $courses[5] = 'DCIT';
        }
        return $courses;
    }

    function getTestPapers($course) {

        $papers = [];
        if ($course == 1) {
            $papers[1] = 'CA11 - Credit Management';
            $papers[2] = 'CA12 - Commercial Law';
        } else if ($course == 2) {
            $papers[3] = 'CS11 - Organizational Behaviour';
            $papers[4] = 'CS12 - Commercial Law';
        } else if ($course == 3) {
            $papers[5] = 'CT11 - Introduction to Computing';
            $papers[6] = 'CT12 - Computer Application';
        } else if ($course == 5) {
            $papers[7] = 'TD11 - Introduction to Computing';
            $papers[8] = 'TD12 - Computer Mathematics';
        } else {
            $papers[1] = 'CA11 - Credit Management';
            $papers[2] = 'CA12 - Commercial Law';
            $papers[3] = 'CS11 - Organizational Behaviour';
            $papers[4] = 'CS12 - Commercial Law';
            $papers[5] = 'CT11 - Introduction to Computing';
            $papers[6] = 'CT12 - Computer Application';
            $papers[7] = 'TD11 - Introduction to Computing';
            $papers[8] = 'TD12 - Computer Mathematics';
        }
        return $papers;
    }

    //Get Qualification types
    function getDashboardTransactions($startDate, $endDate, $stream) {
        $sDate = date("d-m-Y", strtotime($startDate));
        $eDate = date("d-m-Y", strtotime($endDate));
        //echo $this->serviceUrl . "/administrator/payment/summary?startDate=$sDate&endDate=$eDate&feeCode=$stream"; exit;
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/administrator/payment/summary?startDate=$sDate&endDate=$eDate&stream=$stream");
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //Get Examination Booking
    function getExamBookings($courseType, $courseId, $year, $month, $center, $status) {
        //echo ($this->serviceUrl . "/administrator/studentcoursesitting?centre=$center&courseTypeCode=$courseType&courseId=$courseId&year=$year&month=$month&status=$status"); exit;
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/administrator/studentcoursesitting?centre=$center&courseTypeCode=$courseType&courseId=$courseId&year=$year&month=$month&status=$status");
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //Get Audit Trails
    function getAuditTrails($userId, $from, $to) {
        $sDate = date("d-m-Y", strtotime($from));
        $eDate = date("d-m-Y", strtotime($to));
        $curl = new curl\Curl();
        //echo ($this->serviceUrl . "/administrator/audittrail?userId=$userId&from=$sDate&to=$eDate");; exit;
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/administrator/audittrail?userId=$userId&from=$sDate&to=$eDate");
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //Get disbursements
    function getDisbursements($disbursementStatusId = 1, $statusId = 1) {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/disbursements/$statusId/$disbursementStatusId");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //Get disbursements awaiting authorization
    function disbursementsAwaitingAuthorization() {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/disbursements/summary/all/pending/authorise");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //Get disbursement - comprehensive
    function getDisbursement($id) {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/disbursements/$id");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //Get student timetable
    function getStudentTimetable($id) {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/resources/timetable?studentId=$id");
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //Get student timetable
    function getStudentExemptionLetter($id) {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/resources/exemption_letter?studentId=$id");
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //Get refund request
    function getRefundRequest($id) {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->disbursementUrl . "/refundrequest/$id");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //Get refund request for admin
    function getAdminStudentRequest($requestStatusId = 1) {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->disbursementUrl . "/refundrequest?requestStatusId=$requestStatusId");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //Get student refund request
    function getStudentRequest($studentId, $requestStatusId = "") {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->disbursementUrl . "/refundrequest/student/$studentId?requestStatusId=$requestStatusId");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //Get disbursement history
    function getDisbursementHistory() {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/disbursements");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //Get pending disbursement - comprehensive
    function getPendingDisbursement() {
        //echo $this->serviceUrl . "/disbursements/comprehensive/pending/$this->userId"; exit;
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/disbursements/comprehensive/pending/$this->userId");
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //creating / updating disbursement
    function createDisbursement($model) {
        $curl = new curl\Curl();
        $model->createdBy = $this->userId;
        $model->id = empty($model->id) ? null : $model->id;
        $modelAttributes = $model->getAttributes();
        unset($modelAttributes['disbursementId']);
        $data = Json::encode($modelAttributes, JSON_NUMERIC_CHECK);
        //echo $data; exit;
        $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data);
        $response = empty($model->id) ? $curl->post($this->serviceUrl . '/disbursements') : $curl->put($this->serviceUrl . '/disbursements');
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode);
        }
    }

    //completing disbursement
    function completeDisbursement($id) {
        $curl = new curl\Curl();
        $data = Json::encode(['id' => $id, 'createdBy' => $this->userId], JSON_NUMERIC_CHECK);
        $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data);
        $response = $curl->put($this->serviceUrl . '/disbursements/complete');
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode);
        }
    }

    //authorise disbursement
    function authoriseDisbursement($id) {
        $curl = new curl\Curl();
        $data = Json::encode(['id' => $id, 'createdBy' => $this->userId], JSON_NUMERIC_CHECK);
        $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data);
        $response = $curl->put($this->serviceUrl . '/disbursements/authorise');
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode);
        }
    }

    //authorise disbursement
    function authoriseRefund($datas) {
        $curl = new curl\Curl();
        $data = Json::encode($datas, JSON_NUMERIC_CHECK);
        //echo $data; exit;
        $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data);
        $response = $curl->put($this->serviceUrl . '/refundrequest/authorize');
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode);
        }
    }

    //creating/updating beneficiries
    function createBeneficiries($model) {
        $curl = new curl\Curl();
        $model->createdBy = $this->userId;
        $model->id = empty($model->id) ? null : $model->id;
        $modelAttributes = $model->getAttributes();
        unset($modelAttributes['template']);
        $data = Json::encode($modelAttributes, JSON_NUMERIC_CHECK);
        //echo $data; exit;
        $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data);
        $response = empty($model->id) ? $curl->post($this->serviceUrl . '/beneficiaries') : $response = $curl->put($this->serviceUrl . '/beneficiaries');
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode);
        }
    }

    //creating batch beneficiries
    function createBatchBeneficiaries($model) {
        $curl = new curl\Curl();
        $datas = [];
        foreach ($model as $modelData) {
            $modelAttributes = $modelData->getAttributes();
            unset($modelAttributes['transactionTypeId']);
            unset($modelAttributes['statusId']);
            unset($modelAttributes['template']);
            unset($modelAttributes['createdAt']);
            unset($modelAttributes['refundRequestId']);
            $datas[] = $modelAttributes;
        }
        $data = Json::encode($datas, JSON_NUMERIC_CHECK);
        //echo $data; exit;
        $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data);
        $response = $curl->post($this->serviceUrl . '/beneficiaries/batch');
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode);
        }
    }

    //delete beneficiary
    function deleteBeneficiary($id, $disbursementId) {
        $curl = new curl\Curl();
        $data = Json::encode(['id' => $id, 'disbursementId' => $disbursementId], JSON_NUMERIC_CHECK);
        //echo $data; exit;
        $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data);
        $response = $curl->delete($this->serviceUrl . '/beneficiaries');
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode);
        }
    }

    //cancel disbursemnt
    function cancelDisbursement($id) {
        $curl = new curl\Curl();
        $data = Json::encode(['id' => $id, 'createdBy' => $this->userId], JSON_NUMERIC_CHECK);
        $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data);
        $response = $curl->delete($this->serviceUrl . '/disbursements');
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode);
        }
    }

    //Get currency
    function getCurrency() {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/currency");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //Get beneficiary types
    function getBeneficiaryTypes() {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/beneficiarytype");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //Get transaction account types
    function getTransactionAccountTypes($transactionTypeId) {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->disbursementUrl . "/transactionaccounttype/$transactionTypeId");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //Get beneficiaries
    function getBeneficiaries($disbursementId) {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/beneficiaries/$disbursementId");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //Get transaction types
    function getTransactionTypes() {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/transactiontype");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //Receipt pdf
    function getReceiptPdf($id) {
        return $this->serviceUrl . "/export/receipt/$id";
    }

    //Invoice pdf
    function getInvoicePdf($id) {
        return $this->serviceUrl . "/export/invoice/$id";
    }

    //Timetable pdf
    function getTimetable($id) {
        return $this->serviceUrl . "/export/timetable/$id";
    }

    //exemption letter pdf
    function getExemptionLetter($id) {
        return $this->serviceUrl . "/export/exemptionletter/$id";
    }

    //Get Users Who Can Verify
    function getVerificationUsers() {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/user/verify");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //Get declarations report
    function getDeclarationReport($declaration, $responseId) {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/studentdeclaration?id=$declaration&response=$responseId");
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //Get Exemptions Booking
    function getExemptionsBookings($from, $to, $user) {
        $sDate = date("d-m-Y", strtotime($from));
        $eDate = date("d-m-Y", strtotime($to));
        $curl = new curl\Curl();
        //echo $this->serviceUrl . "/administrator/exemption?from=$sDate&to=$eDate&userId=$user"; exit;
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/administrator/exemption?from=$sDate&to=$eDate&userId=$user");
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //Get Students Registered
    function getRegisteredStudents($startDate, $endDate) {
        $sDate = date("d-m-Y", strtotime($startDate));
        $eDate = date("d-m-Y", strtotime($endDate));
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/administrator/student?from=$sDate&to=$eDate");
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //Get Qualification types
    function getQualificationTypes() {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/qualificationtype");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //Get Qualifications
    function getQualifications($type = "") {
        $curl = new curl\Curl();
        $curl->setOption(
                CURLOPT_HTTPHEADER, [
            'Content-Type:application/json',
            'Accept:application/json',
            "App-Key: $this->appKey",
        ]);
        $response = empty($type) ? $curl->get($this->serviceUrl . "/qualification") : $curl->get($this->serviceUrl . "/qualification?type_id=$type");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //Get KASNEB Courses
    function getKasnebCourses($type) {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/course/type/$type");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //Get exemption papaers for multiple qualifications
    function getMultipleExemptionPapers($courses,$studentCourseId) {
        $curl = new curl\Curl();
        $data = Json::encode($courses, JSON_NUMERIC_CHECK);
        $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data);
        $response = $curl->post($this->serviceUrl . "/studentcourse/eligible_exemptions/$studentCourseId");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode);
        }
    }

    //Get exemption papaers
    function getExemptionPapers($studentCourseId, $course, $codeType) {
        //echo $this->serviceUrl . "/studentcourse/eligible_exemptions/$studentCourseId/$course/$codeType"; exit;
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/studentcourse/eligible_exemptions/$studentCourseId/$course/$codeType");
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //Get Institutions
    function getInstitutions($type = "") {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/institution?courseTypeId=$type");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //Get Institutions Course
    function getInstitutionCourse($id) {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/courseexemption/$id");
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //delete Institution
    function deleteInstitutionCoursePaper($qualification, $paper) {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->delete($this->serviceUrl . "/courseexemption/$qualification/$paper");
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return $this->getSuccessResponse();
        } else {
            $response = Json::decode($response);
            return ['status' => ['code' => $response['code'], 'message' => $response['message']]];
        }
    }

    //delete Institution
    function deleteInstitution($id) {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->delete($this->serviceUrl . "/institution/$id");
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return $this->getSuccessResponse();
        } else {
            $response = Json::decode($response);
            return ['status' => ['code' => $response['code'], 'message' => $response['message']]];
        }
    }

    //delete Institution
    function deleteInstitutionCourse($id) {
        //echo $this->serviceUrl . "/course/$id"; exit;
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->delete($this->serviceUrl . "/course/$id");
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return $this->getSuccessResponse();
        } else {
            $response = Json::decode($response);
            return ['status' => ['code' => $response['code'], 'message' => $response['message']]];
        }
    }

    //create Institution
    function createInstitution($model) {
        $curl = new curl\Curl();
        $data = Json::encode($model, JSON_NUMERIC_CHECK);
        //echo $data; exit;
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->post($this->serviceUrl . "/institution");
        if ($curl->responseCode == 200) {
            return $this->getSuccessResponse();
        } else {
            $response = Json::decode($response);
            return ['status' => ['code' => $response['code'], 'message' => $response['message']]];
        }
    }

    //create Institution Course
    function createInstitutionCourse($model) {
        $curl = new curl\Curl();
        $data = Json::encode($model, JSON_NUMERIC_CHECK);
        //echo $data; exit;
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->post($this->serviceUrl . "/course");
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return $this->getSuccessResponse();
        } else {
            $response = Json::decode($response);
            return ['status' => ['code' => $response['code'], 'message' => $response['message']]];
        }
    }

    //create Institution Course Paper
    function createInstitutionCoursePaper($model) {
        $curl = new curl\Curl();
        $data = Json::encode($model, JSON_NUMERIC_CHECK);
        //echo $data; exit;
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->post($this->serviceUrl . "/courseexemption");
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return $this->getSuccessResponse();
        } else {
            $response = Json::decode($response);
            return ['status' => ['code' => $response['code'], 'message' => $response['message']]];
        }
    }

    //Get Institutions
    function getInstitution($id) {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/institution?$id");
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //Get Student Data
    function getStudentData($id) {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/student/$id");
        //print_r($response);exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //Get invoice
    function getInvoice($id) {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/invoice/$id");
        //print_r($response);exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //Get Student Transactions
    function getStudentTransactions($id, $feeCode) {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/transaction/student/$id?feeCode=$feeCode");
        //print_r($curl);exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //Get Student invoices
    function getInvoices($id) {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/renewal/invoice/$id");
        //print_r($curl);exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            $response = Json::decode($response);
            return ['status' => ['code' => $response['code'], 'message' => $response['message']]];
        }
    }

    //Get Student Data
    function getEligiblePapers($id) {
        //echo $this->serviceUrl . "/studentcourse/active/$id"; exit;
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/studentcourse/active/$id");
        //print_r($response);exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //Get Student Data
    function getStudents() {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/student");
        //print_r($response);exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    function getStudentsCourseApplication($from, $to, $user) {
        $sDate = date("d-m-Y", strtotime($from));
        $eDate = date("d-m-Y", strtotime($to));
        //echo $this->serviceUrl . "/administrator/studentcourse?from=$sDate&to=$eDate&userId=$user"; exit;
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/administrator/studentcourse?from=$sDate&to=$eDate&userId=$user");
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    function getStudentsPending() {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/studentcourse/pending");
        //print_r($response);exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    function getStudentsPendingIdentifications() {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/studentcourse/pending/identification");
        //print_r($response);exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    function getRefundReasons($purpose = "") {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->disbursementUrl . "/refundreasons?purpose=$purpose");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    function getDisbursementTransaction($transactionStatusId = "") {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->disbursementUrl . "/transactions?transactionStatusId=$transactionStatusId");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //delete refund model
    function deleteRefundGround($id, $password) {
        $curl = new curl\Curl();
        $data = Json::encode(['id' => $id, 'createdBy' => $this->userId], JSON_NUMERIC_CHECK);
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->delete($this->disbursementUrl . "/refundreasons");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //create refunds model
    function createRefundGround($model) {
        $curl = new curl\Curl();
        $model->createdBy = $this->userId;
        $data = Json::encode($model, JSON_NUMERIC_CHECK);
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->post($this->disbursementUrl . "/refundreasons");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //create refund request model
    function createRefundRequest($model) {
        $curl = new curl\Curl();
        $data = Json::encode($model, JSON_NUMERIC_CHECK);
        //echo $data; exit;
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->post($this->disbursementUrl . "/refundrequest");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //delete refund request model
    function deleteRefundRequest($datas) {
        $curl = new curl\Curl();
        $data = Json::encode($datas, JSON_NUMERIC_CHECK);
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->delete($this->disbursementUrl . "/refundrequest");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //Get exemptions Data
    function getExemptions() {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/exemption");
        //print_r($response);exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //Get pending exemptions Data
    function getPendingExemptions() {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/exemption/pending");
        //print_r($response);exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //Get exemption
    function getExemption($id) {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/exemption/$id");
        //print_r($response);exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //Get Student Zones
    function getZones($studentId = "") {
        if (empty($studentId)) {
            $url = $this->serviceUrl . "/centrezone";
        } else {
            $url = $this->serviceUrl . "/centrezone?studentId=$studentId";
        }
        //echo $url; exit;
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($url);
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //Get Student Regions
    function getRegions() {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/centreregion");
        //print_r($response);exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //Get Student Clusters
    function getClusters() {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/centrecluster");
        //print_r($response);exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //Get Student Centers
    function getCenters($zone, $studentId = "") {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/examcentre?zoneCode=$zone&studentId=$studentId");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    function getStudentGuide() {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/guide");
        //print_r($response);exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    function getCounties() {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/county");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    function getCountries() {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/country");
        //print_r($response);exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    function getNotifications($id) {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/notification/student/$id");
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    function getCourseTypeOthers() {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/coursetype/other");
        //print_r($response);exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    function getCourseType() {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/coursetype");
        //print_r($response);exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    function getDeclarations() {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/declaration");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    function getCourse($id) {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/course/" . $id);
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    function getCourses($courseType) {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/course/type/$courseType");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    function getAllCourses() {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/course/kasneb");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    function getCoursesOthers($institutionId) {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/course/other?institutionId=$institutionId");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    function signUp($model) {
        $curl = new curl\Curl();
        $modelAttributes = $model->getAttributes();
        unset($modelAttributes['passwordConfirm']);
        unset($modelAttributes['password']);
        unset($modelAttributes['created']);
        unset($modelAttributes['nationality']);
        unset($modelAttributes['countyId']);
        unset($modelAttributes['passportPhoto']);
        unset($modelAttributes['documentType']);
        unset($modelAttributes['documentScan']);
        unset($modelAttributes['contact']);
        unset($modelAttributes['studentCourses']);
        unset($modelAttributes['studentDeclarations']);
        unset($modelAttributes['invoices']);
        unset($modelAttributes['currentCourse']);
        //unset($modelAttributes['studentStatus']);


        $data = Json::encode($modelAttributes, JSON_NUMERIC_CHECK);
        //echo $data; exit;
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->post($this->serviceUrl . "/student");
        //print_r($response);exit;
        if ($curl->responseCode == 200) {
            return $this->getSuccessResponse();
        } else {
            $response = Json::decode($response);
            return ['status' => ['code' => $response['code'], 'message' => $response['message']]];
        }
    }

    function adminUserCreate($model) {
        $curl = new curl\Curl();
        $data = Json::encode($model, JSON_NUMERIC_CHECK);
        //echo $data; exit;
        $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data);
        $response = empty($model->id) ? $curl->post($this->serviceUrl . "/user") : $curl->put($this->serviceUrl . "/user");
        //print_r($response);exit;
        if ($curl->responseCode == 200) {
            return $this->getSuccessResponse();
        } else {
            $response = Json::decode($response);
            return ['status' => ['code' => $response['code'], 'message' => $response['message']]];
        }
    }

    function deleteAdminUser($id) {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->delete($this->serviceUrl . "/user/$id");
        if ($curl->responseCode == 200) {
            return $this->getSuccessResponse();
        } else {
            $response = Json::decode($response);
            return ['status' => ['code' => $response['code'], 'message' => $response['message']]];
        }
    }

    function createStudent($model) {
        $curl = new curl\Curl();
        $modelAttributes = $model->getAttributes();
        unset($modelAttributes['passwordConfirm']);
        unset($modelAttributes['password']);
        unset($modelAttributes['countyId']);
        unset($modelAttributes['created']);
        unset($modelAttributes['invoices']);
        unset($modelAttributes['studentDeclarations']);
        unset($modelAttributes['studentCourses']);
        unset($modelAttributes['currentCourse']);
        $data = Json::encode($modelAttributes, JSON_NUMERIC_CHECK);
        //print_r($data);exit;
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->put($this->serviceUrl . "/student");
        //print_r($response);exit;
        if ($curl->responseCode == 200) {
            return $this->getSuccessResponse();
        } else {
            $response = Json::decode($response);
            return ['status' => ['code' => $response['code'], 'message' => $response['message']]];
        }
    }

    function createExemptionGrounds($model) {
        //echo $this->serviceUrl . "/exemption"; exit;
        $curl = new curl\Curl();
        $data = Json::encode($model, JSON_NUMERIC_CHECK);
        //echo($data);exit;
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->post($this->serviceUrl . "/exemption");
        //echo($response);exit;
        if ($curl->responseCode == 200) {
            return $this->getSuccessResponse();
        } else {
            $response = Json::decode($response);
            return ['status' => ['code' => $response['code'], 'message' => $response['message']]];
        }
    }

    function getWalletBalance($model) {
        $curl = new curl\Curl();
        $data = Json::encode($model, JSON_NUMERIC_CHECK);
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->post($this->serviceUrl . "/student/balance");
        if ($curl->responseCode == 200) {
            $response = Json::decode($response);
            return $this->getSuccessResponse("", $response['message']);
        } else {
            $response = Json::decode($response);
            return ['status' => ['code' => $response['code'], 'message' => $response['message']]];
        }
    }

    function createAdminDeclaration($model) {
        $curl = new curl\Curl();
        $data = Json::encode($model, JSON_NUMERIC_CHECK);
        $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data);
        $response = empty($model->id) ? $curl->post($this->serviceUrl . "/administrator/declaration") : $curl->put($this->serviceUrl . "/administrator/declaration");
        if ($curl->responseCode == 200) {
            return $this->getSuccessResponse();
        } else {
            $response = Json::decode($response);
            return ['status' => ['code' => $response['code'], 'message' => $response['message']]];
        }
    }

    //delete vehicle model
    function deleteDeclaration($id, $password) {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->delete($this->serviceUrl . "/administrator/declaration/$id");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    function createDeclarations($model) {
        $curl = new curl\Curl();
        $data = Json::encode($model, JSON_NUMERIC_CHECK);
        //echo($data);exit;
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->put($this->serviceUrl . "/studentcourse");
        //print_r($response);exit;
        if ($curl->responseCode == 200) {
            return $this->getSuccessResponse();
        } else {
            $response = Json::decode($response);
            return ['status' => ['code' => $response['code'], 'message' => $response['message']]];
        }
    }

    function makePayments1($model) {
        $curl = new curl\Curl();
        return $this->getSuccessResponse();
    }

    function makePayments($model) {
        $curl = new curl\Curl();
        //$phone = $model['phoneNumber'];
        //$model['phoneNumber'] = (string)$phone;
        $data = Json::encode($model); //, JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
        //print_r($data);exit;
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->put($this->serviceUrl . "/payment");
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return $this->getSuccessResponse();
        } else {
            $response = Json::decode($response);
            return ['status' => ['code' => $response['code'], 'message' => $response['message']]];
        }
    }

    function createCourse($model, $courseId) {
        $curl = new curl\Curl();
        $data = Json::encode($model, JSON_NUMERIC_CHECK);
        //echo($data); exit;
        $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data);
        $response = empty($courseId) ? $curl->post($this->serviceUrl . "/studentcourse") : $curl->put($this->serviceUrl . "/studentcourse");
        //echo($response);exit;
        if ($curl->responseCode == 200) {
            return $this->getSuccessResponse();
        } else {
            $response = Json::decode($response);
            return ['status' => ['code' => $response['code'], 'message' => $response['message']]];
        }
    }

    function updateDocuments($model) {
        $curl = new curl\Curl();
        $data = Json::encode($model, JSON_NUMERIC_CHECK);
        //echo($data); exit;
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->put($this->serviceUrl . "/studentcourse/documents");
        //echo($response);exit;
        if ($curl->responseCode == 200) {
            return $this->getSuccessResponse();
        } else {
            $response = Json::decode($response);
            return ['status' => ['code' => $response['code'], 'message' => $response['message']]];
        }
    }

    function createSitting($model, $id) {
        $curl = new curl\Curl();
        $data = Json::encode($model, JSON_NUMERIC_CHECK);
        //echo($data);exit;
        $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data);
        $response = empty($id) ? $curl->post($this->serviceUrl . "/studentcoursesitting") : $curl->put($this->serviceUrl . "/studentcoursesitting");
        //print_r($response);exit;
        if ($curl->responseCode == 200) {
            return $this->getSuccessResponse();
        } else {
            $response = Json::decode($response);
            return ['status' => ['code' => $response['code'], 'message' => $response['message']]];
        }
    }

    function activateAccount($token) {
        $curl = new curl\Curl();
        $data = Json::encode(['verificationToken' => $token], JSON_NUMERIC_CHECK);
        //echo $data;exit;
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->put($this->serviceUrl . "/student/verify");
        //print_r($response);exit;
        if ($curl->responseCode == 200) {
            return $this->getSuccessResponse();
        } else {
            $response = Json::decode($response);
            return ['status' => ['code' => $response['code'], 'message' => $response['message']]];
        }
    }

    //Login User
    function login($params = []) {
        //echo $this->serviceUrl . '/login';exit;
        $curl = new curl\Curl();
        $data = Json::encode($params, JSON_NUMERIC_CHECK);
        //echo $data; exit;
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->post($this->adminServiceUrl . '/login');
        //echo($response);exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            $response = Json::decode($response);
            return ['status' => ['code' => $response['code'], 'message' => $response['message']]];
        }
    }

    //Login User
    function getUserById($id) {
        //echo $this->serviceUrl . "/login/$id";exit;
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->adminServiceUrl . "/login/$id");
        //print_r($response);exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            $response = Json::decode($response);
            return ['status' => ['code' => $response['code'], 'message' => $response['message']]];
        }
    }

    //Get Users
    function getUsers() {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/user");
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            $response = Json::decode($response);
            return ['status' => ['code' => $response['code'], 'message' => $response['message']]];
        }
    }

    //Get Users Roles
    function getUsersRoles() {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/role");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            $response = Json::decode($response);
            return ['status' => ['code' => $response['code'], 'message' => $response['message']]];
        }
    }

    //Secret Quiz
    function getSecretQuizes() {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/users/quiz");

        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //RESET OR Change Password
    function resetPassword($params) {
        $curl = new curl\Curl();
        $data = Json::encode($params, JSON_NUMERIC_CHECK);
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->put($this->serviceUrl . "/user/changepassword");
        if ($curl->responseCode == 200) {
            return $this->getSuccessResponse();
        } else {
            $response = Json::decode($response);
            return ['status' => ['code' => $response['code'], 'message' => $response['message']]];
        }
    }

    function requestPasswordReset($params) {
        $curl = new curl\Curl();
        $data = Json::encode($params, JSON_NUMERIC_CHECK);
        //echo $this->serviceUrl . "/login/send_token"; exit;
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->post($this->serviceUrl . "/login/send_token");
        if ($curl->responseCode == 200) {
            return $this->getSuccessResponse();
        } else {
            $response = Json::decode($response);
            return ['status' => ['code' => $response['code'], 'message' => $response['message']]];
        }
    }

    //reset student password
    function resetStudentPassword($params) {
        $curl = new curl\Curl();
        $data = Json::encode($params, JSON_NUMERIC_CHECK);
        //echo $this->serviceUrl . "/login/reset_password"; exit;
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->post($this->serviceUrl . "/login/reset_password");
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return $this->getSuccessResponse();
        } else {
            $response = Json::decode($response);
            return ['status' => ['code' => $response['code'], 'message' => $response['message']]];
        }
    }

    //verify registration
    function verifyRegistration($params, $type) {
        $curl = new curl\Curl();
        $data = Json::encode($params, JSON_NUMERIC_CHECK);
        //echo $data;exit;
        $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data);
        $response = $type == "single" ? $curl->put($this->serviceUrl . "/studentcourse/verify") : $curl->put($this->serviceUrl . "/studentcourse/verify/batch");
        //print_r($response);
        //exit;
        if ($curl->responseCode == 200) {
            return $this->getSuccessResponse();
        } else {
            $response = Json::decode($response);
            return ['status' => ['code' => $response['code'], 'message' => $response['message']]];
        }
    }
    
    //verify signup identification
    function verifySignupIdentification($params) {
        $curl = new curl\Curl();
        $data = Json::encode($params, JSON_NUMERIC_CHECK);
        //echo $data;exit;
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->put($this->serviceUrl . "/studentcourse/verify/identification");
        //print_r($response);exit;
        if ($curl->responseCode == 200) {
            return $this->getSuccessResponse();
        } else {
            $response = Json::decode($response);
            return ['status' => ['code' => $response['code'], 'message' => $response['message']]];
        }
    }

    //verify exemption
    function verifyExemption($params) {
        $curl = new curl\Curl();
        $data = Json::encode($params, JSON_NUMERIC_CHECK);
        //echo $data; exit;
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->put($this->serviceUrl . "/exemption");
        //print_r($response);exit;
        if ($curl->responseCode == 200) {
            return $this->getSuccessResponse();
        } else {
            $response = Json::decode($response);
            return ['status' => ['code' => $response['code'], 'message' => $response['message']]];
        }
    }

    //create center
    function createCenter($params) {
        $curl = new curl\Curl();
        $data = Json::encode($params, JSON_NUMERIC_CHECK);
        //echo $data;exit;
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->put($this->serviceUrl . "/studentcoursesitting/centre");
        //echo($response);exit;
        if ($curl->responseCode == 200) {
            return $this->getSuccessResponse();
        } else {
            $response = Json::decode($response);
            return ['status' => ['code' => $response['code'], 'message' => $response['message']]];
        }
    }

    //get the sittings
    function getExaminationSittings() {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                ])
                ->get($this->serviceUrl . "/sitting");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    function getSuccessResponse($itemId = "", $message = "Operation successful") {
        return ['status' => ['code' => 1, 'message' => $message], 'itemId' => $itemId];
    }

    public function javaToPhpTimestampConversion($timestamp) {
        return intval($timestamp / 1000);
    }

    public function timestampConversionWithoutJava($date) {
        return strtotime(date("Y-m-d", strtotime($date)));
    }

    public function timestampConversion($timestamp) {
        return strtotime(date("Y-m-d", $this->javaToPhpTimestampConversion($timestamp)));
    }

    public function timestampToSting($timestamp) {
        return date("Y-m-d", $this->javaToPhpTimestampConversion($timestamp));
    }

    public function timestampToSting1($timestamp) {
        return date("Y-m-d H:i:s", $this->javaToPhpTimestampConversion($timestamp));
    }

    //function to calculate the start and end date of a specified period (X-Axis Bounds)
    function getStartEndDates($sDate, $eDate) {
        $bounds = [];
        $startDate = strtotime($sDate);
        $endDate = strtotime($eDate);
        $days = (($endDate - $startDate) / 86400) + 1;
        if ($days <= 31) {
            while ($startDate <= $endDate) {
                $bounds[] = [
                    'value' => [
                        'start' => $startDate,
                        'end' => $startDate,
                    ],
                    'label' => date("d", $startDate),
                ];
                $startDate += 86400;
            }
        } elseif ($days <= 92) {
            $i = 1;
            while ($startDate <= $endDate) {

                $nextDate = $startDate + (86400 * 7);
                $bounds[] = [
                    'value' => [
                        'start' => $startDate,
                        'end' => $nextDate,
                    ],
                    'label' => date("m/d", $startDate) . "-" . date("m/d", $nextDate),
                ];
                $startDate = ($nextDate + 86400);
            }
        }
        return $bounds;
    }

}
