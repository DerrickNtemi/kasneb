<?php

namespace common\models;


class Invoice extends \yii\base\Model
{
    public $id;
    public $kesTotal;
    public $usdTotal;
    public $reference;
    public $dateGenerated;
    public $status;
    public $invoiceDetails;
    public $localAmount;
    public $localCurrency;

    public function rules() {
        return [
            [['kesTotal','usdTotal','reference','dateGenerated','status','invoiceDetails','localAmount','localCurrency'], 'required'],
            [['id'], 'safe'],
       ];
    }
    
    public function attributeLabels() {
        return [
            'kesTotal' => 'Amount',
            'usdTotal' => 'Amount',
            'reference'=>'Invoice No.',
            'dateGenerated'=>'Timestamp',
            'status'=>"Status",
            'invoiceDetails'=>"Invoice Details",
        ];
    }
}
