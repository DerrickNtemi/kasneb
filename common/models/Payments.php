<?php

namespace common\models;


class Payments extends \yii\base\Model
{
    public $id;
    public $student;
    public $amount;
    public $currency;
    public $channel;
    public $phoneNumber;
    public $paymentTimestamp;
    public $fullRegNo;
    public $feeCode;


    public function rules() {
        return [
            [['amount','currency','channel','phoneNumber','paymentTimestamp','student','fullRegNo','feeCode'], 'required'],
            [['id'], 'safe'],
       ];
    }
    
    public function attributeLabels() {
        return [
            'amount' => 'Amount',
            'currency' => 'Currency',
            'channel'=>'Channel',
            'phoneNumber'=>'Phone Number',
            'paymentTimestamp'=>"Timestamp",
            'student'=>"Student",
        ];
    }
}
