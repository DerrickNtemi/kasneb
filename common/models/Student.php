<?php

namespace common\models;
use kartik\password\StrengthValidator;

class Student extends \yii\base\Model {

    public $id;
    public $created;
    public $dob;
    public $documentNo;
    public $documentScan;
    public $documentType;
    public $email;
    public $firstName;
    public $gender;
    public $lastName;
    public $middleName;
    public $passportPhoto;
    public $phoneNumber;
    public $countryId;
    public $countyId;
    public $nationality;
    public $loginId;
    public $password;
    public $passwordConfirm;
    public $contact;
    public $studentCourses;
    public $studentDeclarations;
    public $invoices;
    public $currentCourse;
    public $previousRegistrationNo;
    public $studentStatus;
    public $previousCourseCode;
    
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['password','created', 'dob', 'email', 'loginId','documentNo','passportPhoto','documentType','documentScan','nationality','countyId','previousRegistrationNo','previousCourseCode','studentStatus'], 'required'],
            [['created', 'id', 'gender' ,'contact','invoices','studentDeclarations','studentCourses','countryId','currentCourse'], 'safe'],
            [['documentNo', 'documentScan', 'email', 'firstName', 'gender', 'lastName', 'middleName', 'passportPhoto', 'phoneNumber', 'countryId', 'previousRegistrationNo','previousCourseCode'], 'string'],
            [['previousRegistrationNo'], 'integer','message' => "Reg. must be a number"],
            ['passwordConfirm', 'compare', 'compareAttribute' => 'password', 'message' => "Passwords don't match"],
            [['passportPhoto'], 'file' , 'extensions' => 'jpeg, jpg, png'],
            [['documentScan'], 'file' , 'extensions' => 'jpeg, jpg, png, pdf'],
            [['password'], StrengthValidator::className(), 'preset' => 'normal', 'userAttribute' => 'email']
       ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'created' => 'Created',
            'dob' => 'Date of Birth',
            'documentNo' => 'Identification No',
            'documentScan' => 'Attach Document',
            'documentType' => 'Identification Type',
            'email' => 'Active Email',
            'firstName' => 'First Name',
            'gender' => 'Gender',
            'lastName' => 'Last Name',
            'middleName' => 'Middle Name',
            'passportPhoto' => 'Passport Photo',
            'phoneNumber' => 'Phone Number',
            'countryId' => 'Country',
            'countyId' => 'County of Residence',
            'nationality' => 'Nationality',
            'loginId' => 'Login ID',
            'passwordConfirm' => 'Confirm Password',
            'password' => 'Password',
            'contact'=>'Address',
            'previousRegistrationNo'=>'Registration Number e.g 12345',
            'previousCourseCode'=>'Examination'
        ];
    }

   public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['PARTIAL_REGISTRATION'] = ['dob','gender','email', 'countryId', 'firstName', 'middleName', 'lastName', 'password', 'passwordConfirm', 'phoneNumber','studentStatus'];
        $scenarios['EXISTING_SIGNUP'] = ['dob','gender','email', 'phoneNumber', 'password', 'passwordConfirm', 'phoneNumber','previousRegistrationNo','previousCourseCode','studentStatus','documentType','documentNo'];
        $scenarios['REGISTRATION'] = ['id','firstName', 'middleName', 'lastName', 'countryId', 'email', 'phoneNumber','nationality','gender','passportPhoto','documentNo','documentType','documentScan','gender','dob','contact'];
        return $scenarios;
    }

}
