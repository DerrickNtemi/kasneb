<?php

namespace common\models;

class Centers extends \yii\base\Model
{

     public $id;
     public $name;
     public $zone;
     public $maxCapacity;
     public $currCapacity;
     public $status;
     public $examCenter;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['zone','examCenter'], 'required'],
            [['id'], 'safe'],
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'maxCapacity' => 'Maximum Capacity',            
            'currCapacity' => 'Current Capacity',          
            'zone' => 'Choose Zone',          
            'examCenter' => 'Choose your preferred center',

        ];
    }
}
