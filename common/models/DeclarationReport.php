<?php

namespace common\models;


class DeclarationReport extends \yii\base\Model
{
    public $studentDeclarationPK;
    public $response;
    public $specification;
    public $declaration;
    public $student;


    public function rules() {
        return [
            [['studentDeclarationPK','response','specification','declaration','student'], 'required'],
       ];
    }
}
