<?php

namespace common\models;


class Transactions extends \yii\base\Model
{
    public $id;
    public $amount;
    public $currency;
    public $channel;
    public $phoneNumber;
    public $paymentTimestamp;
    public $totalAmount;


    public function rules() {
        return [
            [['amount','currency','channel','phoneNumber','paymentTimestamp','totalAmount'], 'required'],
            [['id'], 'safe'],
       ];
    }
    
    public function attributeLabels() {
        return [
            'amount' => 'Amount',
            'currency' => 'Currency',
            'channel'=>'Channel',
            'phoneNumber'=>'Phone Number',
            'paymentTimestamp'=>"Timestamp",
        ];
    }
}
