<?php

namespace common\models;


class Requirements extends \yii\base\Model
{
    public $id;
    public $documentName;
    public $description;
    public $type;


    public function rules() {
        return [
            [['description'], 'required'],
            [['id','documentName','type'], 'safe'],
       ];
    }
}
