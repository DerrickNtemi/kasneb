<?php

namespace common\models;


class RefundReasons extends \yii\base\Model
{
    public $id;
    public $description;
    public $code;
    public $createdBy;
    public $createdAt;
    public $statusId;


    public function rules() {
        return [
            [['description'], 'required'],
            [['id','createdBy','createdAt','code','statusId'], 'safe'],
       ];
    }
}
