<?php
namespace common\models;

class Beneficiaries extends \yii\base\Model
{
    public $id;
    public $idNumber;
    public $name;
    public $phoneNumber;
    public $disbursementId;
    public $amount;
    public $beneficiaryTypeId;
    public $transactionTypeId;
    public $transactionAccountTypeId;
    public $currencyId;
    public $createdBy;
    public $createdAt;
    public $statusId;
    public $template;
    public $refundRequestId;


    public function rules() {
        return [
                [['idNumber','name','phoneNumber','disbursementId','amount','beneficiaryTypeId','transactionTypeId','transactionAccountTypeId','currencyId'], 'required'],
                [['id','createdBy','createdAt','template','refundRequestId'], 'safe'],
       ];
    }
    
    public function attributeLabels() {
        return [
            'idNumber' => 'ID. Number',
            'name' => 'Names',
            'phoneNumber' => 'Phone Number',
            'amount' => 'Amount',
            'beneficiaryTypeId' => 'Beneficiary Type',
            'transactionTypeId' => 'Transaction Type',
            'transactionAccountTypeId' => 'Platform',
            'currencyId' => 'Currency',
        ];
    }
}
