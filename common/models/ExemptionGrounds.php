<?php

namespace common\models;


class ExemptionGrounds extends \yii\base\Model
{
    public $id;
    public $registrationNumber;
    public $institutionId;
    public $institutionName;
    public $qualificationId;
    public $qualificationTypesId;
    public $document;
    public $courseId;
    public $paperId;


    public function rules() {
        return [
            [['paperId'], 'required'],
            [['registrationNumber','institutionId','qualificationId','id','institutionName'], 'safe'],
       ];
    }
    
    public function attributeLabels() {
        return [
            'registrationNumber' => 'Registration Number',
            'institutionId' => 'Institution',
            'institutionName'=>'Institution Name',
            'qualificationId'=>'Qualification',
            'document'=>"Attach Letter of Completion / Transcript / Certified Certificate",
            'qualificationTypesId'=>'Qualification Type',
            'courseId'=>'Course',
        ];
    }
}
