<?php

namespace common\models;

class Institutions extends \yii\base\Model
{

     public $id;
     public $name;
     public $courseType;
     public $courses;
     public $courseCount;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name','courseType'], 'required'],
            [['id','courses','courseCount'], 'safe'],
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',            
            'courseType' => 'Course Type',          
            'course' => 'Course',          

        ];
    }
}
