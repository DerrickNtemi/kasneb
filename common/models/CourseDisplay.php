<?php

namespace common\models;


class CourseDisplay extends \yii\base\Model
{
    public $id;
    public $studentId;
    public $registrationNumber;
    public $created;
    public $document;
    public $active;
    public $verified;
    public $dateVerified;
    public $verifiedBy;
    public $remarks;
    public $nextRenewal;
    public $courseId;
    public $verificationStatus;




    public function rules() {
        return [
            [['studentId', 'registrationNumber', 'created','document','active','verified','dateVerified','verifiedBy','remarks','nextRenewal','courseId'], 'required'],
            [['id','verificationStatus'], 'safe'],
       ];
    }
}
