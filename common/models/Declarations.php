<?php

namespace common\models;


class Declarations extends \yii\base\Model
{
    public $id;
    public $declarationId;
    public $selection;
    public $specify;
    public $month;
    public $year;


    public function rules() {
        return [
            [['requirement','month'], 'required'],
            [['id','specify','selection','year'], 'safe'],
       ];
    }
}
