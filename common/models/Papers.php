<?php

namespace common\models;


class Papers extends \yii\base\Model
{
    public $paperCode;
    public $paperName;
    public $courseId;


    public function rules() {
        return [
            [['paperCode'], 'required'],
            [['paperName','paperCode','courseId'], 'safe'],
       ];
    }
    
    public function attributeLabels() {
        return [
            'paperCode' => 'Examination Papers',
            'paperName' => 'Paper Name',
        ];
    }
}
