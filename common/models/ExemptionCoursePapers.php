<?php

namespace common\models;

class ExemptionCoursePapers extends \yii\base\Model
{

     public $id;
     public $courseId;
     public $name;
     public $papers;
     public $code;
     public $institutionId;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name','papers','courseId','institutionId'], 'required'],
            [['id', 'code'], 'safe'],
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',                     
            'papers' => 'Papers',          
            'courseId'=>'Course'
        ];
    }
}
