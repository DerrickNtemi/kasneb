<?php
namespace common\models;

class RefundRequest extends \yii\base\Model
{
    public $id;
    public $studentId;
    public $registrationNo;
    public $name;
    public $course;
    public $amount;
    public $refundReasonsId;
    public $description;
    public $phoneNumber;
    public $currency;
    public $transactionTypeId;
    public $transactionAccountTypeId;
    public $createdAt;
    public $statusId;
    public $requestStatusId;
    public $remarks;


    public function rules() {
        return [
                [['studentId','registrationNo','name','course','amount','phoneNumber','currency','transactionTypeId','transactionAccountTypeId','refundReasonsId'], 'required'],
                [['id','createdAt', 'statusId', 'requestStatusId','description','remarks'], 'safe'],
       ];
    }
    
    public function attributeLabels() {
        return [
            'name' => 'Name',
            'registrationNo' => 'Reg No.',
            'course' => 'Course',
            'amount' => 'Amount',
            'description' => 'Description',
            'phoneNumber' => 'Phone Number',
            'currency'=>'Currency',
            'transactionTypeId'=>'Transaction Type',
            'transactionAccountTypeId'=>'Transaction Account Type',
            'refundReasonsId'=>'Refund Reasons',
        ];
    }
}
