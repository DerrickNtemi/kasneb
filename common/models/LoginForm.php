<?php

namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class LoginForm extends Model {

    public $username;
    public $password;
    public $rememberMe = true;
    private $_user = false;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            // username and password are both required
                [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
        ];
    }
    
    public function attributeLabels() {
        return [
            'username' => 'Email'
        ];
    }

    public function login() {
        if ($this->validate()) {
            $user = $this->getUser();
            if ($user->status == 200) {
                //print_r($user); exit;
                return Yii::$app->user->login($user, $this->rememberMe ? 60 * 3 : 0);
            } elseif ($user->status == 500) {
                return "force: $user->id";
            } else {
                $this->addError('password', Yii::t('app', 'Incorrect username or password.'));
            }
        }
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser() {
        if ($this->_user === false) {
            $this->_user = UserIdentity::findByUsername($this->username, $this->password);
        }
        return $this->_user;
    }

}
