<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'serviceUrl'=> 'http://192.168.11.15:8080/kasneb/api',
    'adminServiceUrl'=> 'http://192.168.11.15:8080/kasneb/api',
    'disbursementUrl'=> 'http://192.168.11.15:8081/kasnebdisbursement/services',
    'studentPortalUrl'=>'http://kasneb/uploads',
];
