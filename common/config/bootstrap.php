<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@studentsportal', dirname(dirname(__DIR__)) . '/studentsportal');
Yii::setAlias('@administrator', dirname(dirname(__DIR__)) . '/administrator');
Yii::setAlias('@disbursement', dirname(dirname(__DIR__)) . '/disbursement');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');