<?php
use kartik\form\ActiveForm;
$form = ActiveForm::begin([
            'id' => 'filter-details-form',
            'action' => Yii::$app->urlManager->createUrl(['registration/filter']),
            'options' => ['enctype' => 'multipart/form-data',],
            'fieldConfig' => [
                'template' => "{label}{input}\n{hint}\n{error}",
            ],
        ]);
?>

<div class="row" style="background-color: #f5f5f0; margin-right: 0px; margin-left: 0px; margin-bottom:30px;">
    <div class="col-md-12">
        <div class="row form-row-below">   
            <div class="col-md-6"></div>
            <div class="col-md-3 text-right text-bold text-capitalize" style="padding-top: 20px;"> FILTER BY </div> 
            <div class="col-md-3" style="padding-top: 10px;">                            
                            <?= $form->field($model, 'stream', [
                                    'addon' => ['prepend' => ['content' => '<i class="fa fa-exchange"></i>']],
                                'inputOptions' => ['class' => 'form-control  form-control-custom'],
                            ])->dropDownList($revenueStreams, ['prompt'=>'Choose ...'])->label(FALSE) ?>
                        </div>
            
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>