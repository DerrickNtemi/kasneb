<?php
    use yii\helpers\Html;
?>
<p id="url" style="display: none;"></p>
<div class="row">
    <div class="col-md-12">Enter Wallet Pin to get wallet balance: </div>
</div>
<div class="row">
    <div class="col-md-6">
        <input type="password" name="walletpin" id="walletpin" placeholder="Wallet Pin" class="reverse-password form-control" />
    </div>
</div>
<h2></h2>
<div id="progress-spinner" class="text-center" style="display: none;">
    <?= Html::img('@web/images/ajax-loader.gif', ['alt' => 'progress spinner']) ?>
</div>
<h2></h2>
<div class="alert alert-danger"  id="error-summary" style="display: none;"></div>