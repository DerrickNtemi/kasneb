<?php
    use yii\helpers\Html;
?>
<p>Do you want to delete this record ? </p>
<p id="url" style="display: none;"></p>
<p id="reload-div" style="display: none;"></p>
Enter Password : <input type="password" name="password" id="password" placeholder="password" class="form-control reverse-password" />
<h2></h2>
<div id="progress-spinner" class="text-center" style="display: none;">
    <?= Html::img('@web/images/ajax-loader.gif', ['alt' => 'progress spinner']) ?>
</div>
<h2></h2>
<div class="alert alert-danger"  id="error-summary" style="display: none;"></div>