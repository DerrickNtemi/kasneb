<?php 
use yii\helpers\Html;
?>
<div class="row">
    <div class="col-md-4 text-right"><b>Disbursement :</b></div>
    <div class="col-md-8" id='disbursement-name'></div>
</div>
<div class="row">
    <div class="col-md-4 text-right"><b>Total Amount :</b></div>
    <div class="col-md-8" id='disbursement-totalamount'></div>
</div>
<div id="url" class="hidden" ></div>
<div id="disbursementsId" class="hidden" ></div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-4 text-right"><b>Enter Password</b></div> 
    <div class="col-md-8">
        <input type="password" name="password" id="password" class="form-control"> 
    </div>
</div>
<h2></h2>
<div id="progress-spinner" class="text-center" style="display: none;">
    <?= Html::img('@web/images/ajax-loader.gif', ['alt' => 'progress spinner']) ?>
</div>
<h2></h2>
<div class="alert alert-danger"  id="error-summary" style="display: none;"></div>