<?php
use yii\helpers\Html;
?>
<p id="modalid" style="display: none;"></p>
<p id="action-url" style="display: none;"></p>
<p id="reload-div" style="display: none;"></p>
<p id="div-datatosend" style="display: none;"></p>


<div class="row">
    <div class="col-md-2"><?= Html::input('radio', 'verify', "APPROVED", []) ?>Approve</div>
    <div class="col-md-2"><?= Html::input('radio', 'verify', "REJECTED", []) ?>Reject</div>
</div>
<div id="rejection-div"   style="display: none;">
    <div class="row" style="margin-top: 10px;">
        <div class="col-md-12">
            Rejection Reason :
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <select id="remarks" class="form-control"> 
                <?php foreach ($remarks as $key => $value) : ?>
                    <option value="<?= $key ?>" ><?= $value ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
</div>
<div class="row"  style="margin-top: 10px; display: none;" id="other-reasons">
    <div class="col-md-8">
        <textarea  name="otherremarks" id="otherremarks" placeholder="Remarks" class="reverse-password form-control" ></textarea>
    </div>
</div>
<div class="row"  style="margin-top: 10px;">
    <div class="col-md-12">
        Enter Password :
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <input type="password" name="password" id="password" placeholder="password" class="reverse-password form-control" />
    </div>
</div>
<h2></h2>
<div id="progress-spinner" class="text-center" style="display: none;">
    <?= Html::img('@web/images/ajax-loader.gif', ['alt' => 'progress spinner']) ?>
</div>
<h2></h2>
<div class="alert alert-danger"  id="error-summary" style="display: none;"></div>