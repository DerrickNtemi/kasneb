<?php

use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('app', 'Renewal Fee');
$methods[1] = "KASNEB Wallet";
$methods[3] = "Bank";
$url = Url::toRoute(['invoices/print', 'id' => $model->id]);
?>
<h1 class='header-h1'><?= $this->title ?></h1>
<div class="row header-underline"></div>
<h2></h2>
<div class="alert alert-danger"  id="bill-error-summary" role="alert" style="display: none;"></div>

<div class="panel-group mt10">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10 invoice-div" >
                    <p class="text-right" style="display:none;"><button class="btn btn-sm">Download Invoice</button></p>
                    <div class="row bill-header">
                        <div class="col-md-8">
                            <p><strong>INVOICE No. <?= $model->invoiceNo ?></strong></p>
                            <p>Invoice Date: <?= $model->invoiceDate ?></p>
                            <p style="display:none;" id="common-invoice-id"><?= $model->id ?></p>
                        </div>
                        <div class="col-md-4 text-right" style="padding-top:5px;">
                            <?= Html::a("Download Invoice", $url, ['class' => 'btn btn-sm btn-default', 'target' => '_blank']) ?>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 30px;">
                        <div class="col-md-1"></div>
                        <div class="col-md-10" style="padding-left: 0px; padding-right: 0px;">
                            <table class="table table-bordered table-striped">
                                <tr class="table-rows">
                                    <th></th>
                                    <th>Description</th>
                                    <th class="text-right"><?= $model->localCurrency ?></th>
                                </tr>
                                <?php
                                $total = 0;
                                $i = 1;
                                foreach ($model->items as $items) {
                                    ?>
                                    <tr>
                                        <td><?= $i ?></td>
                                        <td><?= $items['description'] ?></td>
                                        <td class="text-right"><?= number_format($items['localAmount']) ?></td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                                ?>
                                <tr class="table-rows">
                                    <th colspan="2" class="text-right">Total</th>
                                    <th class="text-right"><?= number_format($model->localAmount) ?></th>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-2" style="padding-left: 0;"><button class="btn btn-sm btn-primary pay-bill-btn">MAKE PAYMENT</button></div>
                        <div class="col-md-5">
                            <?php
                            $form = ActiveForm::begin([
                                        'id' => 'bill-details-form',
                                        'action' => Yii::$app->urlManager->createUrl(['site/bill-payment']),
                                        'options' => ['enctype' => 'multipart/form-data',],
                                        'fieldConfig' => [
                                            'template' => "{label}{input}\n{hint}\n{error}",
                                        ],
                            ]);
                            ?>

                            <?=
                            $form->field($model, 'methods', [
                                'options' => ['class' => 'col-md-12'],
                                'inputOptions' => ['class' => 'form-control'],
                            ])->dropDownList($methods, ['prompt' => 'Choose payment method...'])->label(FALSE)
                            ?>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                    <div class="row jp"  style="display: none; background-color: lightgrey; padding-bottom: 30px;">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12"><h4>KASNEB WALLET</h4></div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">Phone : </div>
                                <div class="col-md-3"><input name="phone-number" class="wallet-pin" id="phone-number-pay" type="text" placeholder="Phone Number" /></div>
                                <div class="col-md-2">Wallet Pin : </div>
                                <div class="col-md-3"><input name="wallet-pin" class="wallet-pin" id="wallet-pin-pay" type="password" placeholder="Wallet Pin" /></div>
                                <div class="col-md-2">
                                    <button 
                                        class="btn btn-primary btn-sm" 
                                        data-whatever="<?= isset($payId) ? $payId : 'jambopay-pay' ?>" 
                                        data-feecode="<?= isset($feeCode) ? $feeCode : "REGISTRATION_FEE" ?>" 
                                        data-wizard="<?= isset($payId) == "examination-pay" ? '#examinationwizard' : '' ?>" 
                                        data-invoiceid= "" 
                                        data-tabindex="<?= isset($nextTabIndex) ? $nextTabIndex : "" ?>" 
                                        data-toggle="modal" 
                                        data-target="#VerifyPayment"                                        
                                        >Pay</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row paybill"  style="display: none; background-color: lightgrey;">
                        <h4>&nbsp;&nbsp;PAYBILL</h4>
                        <ol>
                            <li>Go to the M-PESA menu on your phone </li>
                            <li>Select Pay Bill Option </li>
                            <li>Enter the following business number: 832222</li>
                            <li>Enter <b><?= $model->invoiceNo ?></b> as the Account Number</li>
                            <li>Enter the total amount (KES <b><?= ceil($model->total) ?></b>) </li>
                            <li>You will receive a transaction confirmation SMS from M-PESA </li>
                        </ol>
                    </div>
                    <div class="row bankbill"  style="display: none; background-color: lightgrey;">
                        <h4>&nbsp;&nbsp;BANK</h4>
                        <ol>
                            <li>Download the invoice </li>
                            <li>Go to KASNEB collecting bank and pay using invoice as reference </li>
                        </ol>
                    </div>
                </div>
                <div class="col-md-1"></div>
            </div>
        </div>
    </div>
</div>  
<div id="loadingDiv"></div>
<!-- Verification Modal -->
<?php
Modal::begin([
    'id' => 'VerifyPayment',
    'header' => '<h4 class="modal-title" >Confirm Payment</h4>',
    'footer' =>
    Html::button('Cancel', ['class' => 'btn btn-default', 'data-dismiss' => 'modal'])
    . PHP_EOL .
    Html::button('Pay', ['id' => 'verify-payment-btn', 'class' => 'btn btn-primary btn-modal-save']),
]);
?>
<?= $this->render('@common/views/_verify-payment') ?>
<?php Modal::end() ?>