<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\date\DatePicker;

$form = ActiveForm::begin([
            'id' => 'existing-signup',
            'enableClientValidation' => true]);
?>
<h1></h1>
<div class="row">
    <div class="col-md-6 zero-padding-div-right">
        <?= $form->field($model, 'previousRegistrationNo', ['addon' => ['prepend' => ['content' => '<i class="glyphicon glyphicon-user"></i>']]])->textInput(['placeholder' => $model->getAttributeLabel('previousRegistrationNo')]) ?>
    </div>
    <div class="col-md-6 zero-padding-div-left">
        <?=
        $form->field($model, 'dob', [
            'inputOptions' => ['class' => 'form-control  form-control-custom'],
        ])->widget(DatePicker::classname(), [
            'value' => date("d-m-Y"),
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'dd-mm-yyyy'
            ]
        ])
        ?>
    </div>
</div>
<div class="row">
    <div class="col-md-6 zero-padding-div-right">
        <?=
        $form->field($model, 'email', [
            'addon' => ['prepend' => ['content' => '@']]
        ])->textInput(['placeholder' => $model->getAttributeLabel('email')])
        ?>
    </div>
    <div class="col-md-6 zero-padding-div-left">
        <?=
        $form->field($model, 'phoneNumber', [
            'addon' => ['prepend' => ['content' => '<i class="glyphicon glyphicon-phone"></i>']]
        ])->textInput(['placeholder' => $model->getAttributeLabel('phoneNumber')])
        ?>
    </div>
</div>
<div class="row">
    <div class="col-md-6 zero-padding-div-right">
        <?=
        $form->field($model, 'password', [
            'addon' => ['prepend' => ['content' => '<i class="glyphicon glyphicon-lock"></i>']]
        ])->passwordInput(['placeholder' => $model->getAttributeLabel('password')])
        ?>
    </div>
    <div class="col-md-6 zero-padding-div-left">
        <?=
        $form->field($model, 'passwordConfirm', [
            'addon' => ['prepend' => ['content' => '<i class="glyphicon glyphicon-lock"></i>']]
        ])->passwordInput(['placeholder' => $model->getAttributeLabel('passwordConfirm')])
        ?>
    </div>
</div>
<div>
    <?php
    if ($model->hasErrors()) {
        $errors = $model->getErrors();
        ?><div class="row alert alert-danger">
        <?php
        foreach ($errors as $key => $value) {
            foreach ($value as $key => $value) {
                echo $value . "<br/>";
            }
        }
        ?>
        </div><?php
}
    ?>
</div>
<div class="row">
    <div class="col-md-12 zero-padding-div">
<?= Html::submitButton('Sign Up', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>