<?php

use kartik\date\DatePicker;
use kartik\form\ActiveForm;

$form = ActiveForm::begin([
            'id' => 'filter-details-form',
            'action' => Yii::$app->urlManager->createUrl(['registration/filter']),
            'options' => ['enctype' => 'multipart/form-data',],
            'fieldConfig' => [
                'template' => "{label}{input}\n{hint}\n{error}",
            ],
        ]);
?>

<div class="row" style="background-color: #f5f5f0; margin-right: 0px; margin-left: 0px; margin-bottom:30px;">
    <div class="col-md-12">
        <div class="row form-row-below">   
            <div class="col-md-6 text-right text-bold text-capitalize" style="padding-top: 30px;"> FILTER BY </div> 
            <div class="col-md-3"> 

                <?=
                $form->field($model, 'startDate', [
                    'inputOptions' => ['class' => 'form-control  form-control-custom'],
                ])->widget(DatePicker::classname(), [
                    'value' => date("Y-m-d"),
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ])
                ?>
            </div>
            <div class="col-md-3"> 

                <?=
                $form->field($model, 'endDate', [
                    'inputOptions' => ['class' => 'form-control  form-control-custom'],
                ])->widget(DatePicker::classname(), [
                    'value' => date("Y-m-d"),
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ])
                ?>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>