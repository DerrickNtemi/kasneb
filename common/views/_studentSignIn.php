<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use yii\helpers\Url;

$url = Yii::$app->getUrlManager();
?>
<div class="row account-access-main-row">
    <div class="col-md-4">
        <div class=" row login-main-div login-div">
            <div class="col-md-12"  style="margin-left: 30px;">
                <?php if (Yii::$app->session->hasFlash('fail-message')): ?>
                    <div class="alert alert-danger"><?= Yii::$app->session->getFlash('fail-message') ?></div>
                <?php endif; ?>
                <?php if (Yii::$app->session->hasFlash('success-message')): ?>
                    <div class="alert alert-success"><?= Yii::$app->session->getFlash('success-message') ?></div>
                <?php endif; ?>
                <!-- /.login-logo -->
                <div class="row login-box-body">
                    <p class="create-account-link"><?= Html::a("Don't have an account ? Click here to register", $url->createUrl(['/site/signup']), []) ?></p>
                <p class="login-box-msg" style="margin-top:10px;">Sign in to start your session</p>

                    <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => true]); ?>
                    <?=
                    $form->field($model, 'username', [
                        'addon' => ['prepend' => ['content' => '<i class="glyphicon glyphicon-user"></i>']]
                    ])->textInput(['placeholder' => $model->getAttributeLabel('username')])
                    ?>
                    <?=
                    $form->field($model, 'password', [
                        'addon' => ['prepend' => ['content' => '<i class="glyphicon glyphicon-lock"></i>']]
                    ])->passwordInput(['placeholder' => $model->getAttributeLabel('password')])
                    ?>

                    <div>
                        <?php
                        if ($model->hasErrors()) {
                            $errors = $model->getErrors();
                            ?><div class="alert alert-danger"><?= $errors['password'][0] ?></div><?php
                        }
                        ?>
                    </div>
                    <div class="row">
                        <div class="col-xs-8">
                            <?= $form->field($model, 'rememberMe')->checkbox() ?>
                        </div>
                        <div class="col-xs-4">
                            <?= Html::submitButton('Sign In', ['class' => 'btn btn-primary btn-block btn-flat btn-sm', 'name' => 'login-button']) ?>
                        </div>
                    </div>


                    <?php ActiveForm::end(); ?>
                    <p class='text-right'><?= Html::a('Forgot Password', Url::toRoute(['site/forgot-password']), ['class' => 'text-right']) ?></p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-1"></div>
    <div class="col-md-7">
        <div class="row signup-info">
            <div class="col-md-12">
                <?= $this->render('_instructions', ['instructions' => $studentGuide]) ?>
            </div>
        </div>

    </div>
</div>