<div class="panel-group mt10">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10 invoice-div" >
                    <p class="text-right" style="display:none;"><button class="btn btn-sm">Download Receipt</button></p>
                    <div class="row bill-header">
                        <div class="col-md-12">
                            <p><strong>Transaction Ref: <?= $model->transRef ?></strong></p>
                            <p><strong>INVOICE No. <?= $model->invoiceNo ?></strong></p>
                            <p>Payment Date: <?= $model->dueDate ?></p>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 30px;">
                        <div class="col-md-1"></div>
                        <div class="col-md-10" style="padding-left: 0px; padding-right: 0px;">
                            <table class="table table-bordered">
                                <tr class="table-rows">
                                    <th>Description</th>
                                    <th>Amount</th>
                                </tr>
                                <?php
                                $total = 0;
                                foreach ($model->items as $items) {
                                    $total+=$items['amount']['kesAmount'];
                                    ?>
                                    <tr>
                                        <th><?= $items['description'] ?></th>
                                        <th><?= number_format($items['amount']['kesAmount'], 2) ?> KES</th>
                                    </tr>
                                    <?php
                                }
                                ?>
                                <tr class="table-rows">
                                    <th>Total</th>
                                    <th><?= number_format($total, 2) ?> KES</th>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                </div>
                <div class="col-md-1"></div>
            </div>
        </div>
    </div>
</div>  