<?php

use yii\helpers\Html;
?>
<p id="modalid" style="display: none;"></p>
<p id="action-url" style="display: none;"></p>
<p id="verify" style="display: none;"></p>
<p id="message"></p>
<div class="row"  style="margin-top: 10px;" id="other-reasons">
    <div class="col-md-8">
        <textarea  name="otherremarks" id="remarks" placeholder="Remarks" class="reverse-password form-control" ></textarea>
    </div>
</div>
<h2></h2>
<div id="progress-spinner" class="text-center" style="display: none;">
    <?= Html::img('@web/images/ajax-loader.gif', ['alt' => 'progress spinner']) ?>
</div>
<h2></h2>
<div class="alert alert-danger"  id="error-summary" style="display: none;"></div>