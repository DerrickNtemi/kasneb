<?php

use yii\helpers\Html;

$identity = Yii::$app->user->getIdentity();
if ($identity) {
    ?>
    <div class="row" style="margin-left: -15px; margin-right: 0px;" >
        <div class="col-md-3 powered-by-row"></div>
        <div class="col-md-9">                                   
    <?= Html::img('@web/images/JamboPaylogo.png', ['alt' => 'Powered by Jambopay', 'class' => 'img-responsive powered-by', 'align' => 'right']) ?>
        </div>
    </div>
    <?php
}
?>
<footer class="footer navbar-default">
    <div class="row" style="margin-left: 0px; margin-right: 0px;">
        <div class="col-md-12">
            <?= Html::a("Website", "http://www.kasneb.or.ke/", ['class' => 'footer-links', 'target' => '_blank']) ?><span class="stroke-separator"> | </span>  
<?= Html::a("Contact Us", "http://www.kasneb.or.ke/index.php?option=com_content&view=article&id=14&Itemid=8", ['class' => 'footer-links', 'target' => '_blank']) ?><span class="stroke-separator"> | </span> 
<?= Html::a("Feedback", "http://www.kasneb.or.ke/index.php?option=com_ckforms&view=ckforms&id=1&Itemid=85", ['class' => 'footer-links', 'target' => '_blank']) ?>
        </div>
    </div>
</footer>