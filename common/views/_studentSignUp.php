<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\Url;

$elligibleYear = 473040000;
$elligibleDate = date("d-m-Y", (time() - $elligibleYear));
$url = Yii::$app->getUrlManager();
?>
<div class="row account-access-main-row">
    <div class="col-md-5">
        <div class=" row login-main-div login-div">
            <div class="col-md-12"  style="margin-left: 30px;">
                <!-- /.login-logo -->
                <div class="row login-box-body">
                    <div class="col-md-12" style="padding-bottom: 40px;">
                        <div class="row">
                            <div class="col-md-12 create-account-link"><?= Html::a("Have an account ? Click here to login", $url->createUrl(['/site/login']), []) ?></div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 login-box-msg">Sign Up to access our services</div>
                        </div>      
                        <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => true]); ?>
                        <h1></h1>                  
                        <div class="row" style="margin-top: 10px; margin-bottom: 20px;">
                            <div class="col-md-12 zero-padding-div-right">
                                <?= $form->field($model, 'studentStatus', ['options' => ['class' => 'signup-status']])->radioList(['1' => 'New Registration', '2' => 'Students with Registration Number'])->label(FALSE) ?>
                            </div>
                        </div>
                        <div id="signup-div" style="display: none">

                            <div class="row for-new-students">
                                <div class="col-md-4 zero-padding-div-right">
                                    <?= $form->field($model, 'firstName', ['addon' => ['prepend' => ['content' => '<i class="glyphicon glyphicon-user"></i>']]])->textInput(['placeholder' => $model->getAttributeLabel('firstName')]) ?>
                                </div>
                                <div class="col-md-4 zero-padding-div-right">
                                    <?=
                                    $form->field($model, 'middleName', [
                                        'addon' => ['prepend' => ['content' => '<i class="glyphicon glyphicon-user"></i>']]
                                    ])->textInput(['placeholder' => $model->getAttributeLabel('middleName')])
                                    ?>
                                </div>
                                <div class="col-md-4 zero-padding-div-left" id="last-name-div">
                                    <?=
                                    $form->field($model, 'lastName', [
                                        'addon' => ['prepend' => ['content' => '<i class="glyphicon glyphicon-user"></i>']]
                                    ])->textInput(['placeholder' => $model->getAttributeLabel('lastName')])
                                    ?>
                                </div>
                            </div>
                            <div class="row for-existing-students" style="display: none;">
                                <div class="col-md-6 zero-padding-div-right">
                                    <?=
                                    $form->field($model, 'previousCourseCode', [
                                        'addon' => ['prepend' => ['content' => '<i class="glyphicon glyphicon-book"></i>']]
                                    ])->dropDownList($courseData, ['placeholder' => $model->getAttributeLabel('previousCourseCode')])
                                    ?>
                                </div>
                                <div class="col-md-6 zero-padding-div-left">
                                    <?= $form->field($model, 'previousRegistrationNo', ['addon' => ['prepend' => ['content' => '<i class="glyphicon glyphicon-user"></i>']]])->textInput(['placeholder' => $model->getAttributeLabel('previousRegistrationNo')]) ?>
                                </div>
                            </div>
                            <div class="row">                                
                                <div class="col-md-6 zero-padding-div-right">
                                    <?=
                                    $form->field($model, 'dob', [
                                        'inputOptions' => ['class' => 'form-control  form-control-custom'],
                                    ])->widget(DatePicker::classname(), [
                                        'value' => date("d-m-Y"),
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'format' => 'dd-mm-yyyy',
                                            'endDate' => $elligibleDate,
                                        ]
                                    ])
                                    ?>
                                </div>
                                <div class="col-md-6 zero-padding-div-left for-existing-students">
                                    <?= $form->field($model, 'documentNo', ['addon' => ['prepend' => ['content' => '<i class="glyphicon glyphicon-user"></i>']]])->textInput(['placeholder' => $model->getAttributeLabel('documentNo')]) ?>
                                </div>
                                <div class="col-md-6 zero-padding-div-left for-new-students">
                                    <?= $form->field($model, 'gender')->radioList(['1' => 'Male', '2' => 'Female'], ['inline' => true]) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 zero-padding-div-right">
                                    <?=
                                    $form->field($model, 'email', [
                                        'addon' => ['prepend' => ['content' => '<i class="glyphicon glyphicon-envelope"></i>']]
                                    ])->textInput(['placeholder' => $model->getAttributeLabel('email')])
                                    ?>
                                </div>
                                <div class="col-md-4 zero-padding-div-right">
                                    <?=
                                    $form->field($model, 'phoneNumber', [
                                        'addon' => ['prepend' => ['content' => '<i class="glyphicon glyphicon-phone"></i>']]
                                    ])->textInput(['placeholder' => $model->getAttributeLabel('phoneNumber')])
                                    ?>
                                </div>
                                <div class="col-md-4 zero-padding-div-right">
                                    <?=
                                    $form->field($model, 'countryId', [
                                        'addon' => ['prepend' => ['content' => '<i class="glyphicon glyphicon-flag"></i>']]
                                    ])->dropDownList($countryCodes, ['placeholder' => $model->getAttributeLabel('countryId')])
                                    ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 zero-padding-div-right">
                                    <?=
                                    $form->field($model, 'password', [
                                        'addon' => ['prepend' => ['content' => '<i class="glyphicon glyphicon-lock"></i>']]
                                    ])->passwordInput(['placeholder' => $model->getAttributeLabel('password')])
                                    ?>
                                </div>
                                <div class="col-md-6 zero-padding-div-left">
                                    <?=
                                    $form->field($model, 'passwordConfirm', [
                                        'addon' => ['prepend' => ['content' => '<i class="glyphicon glyphicon-lock"></i>']]
                                    ])->passwordInput(['placeholder' => $model->getAttributeLabel('passwordConfirm')])
                                    ?>
                                </div>
                            </div>
                            <?php
                            if ($model->hasErrors()) {
                                $errors = $model->getErrors();
                                ?><div class="row alert alert-danger">
                                <?php
                                foreach ($errors as $key => $value) {
                                    foreach ($value as $key => $value) {
                                        echo $value . "<br/>";
                                    }
                                }
                                ?>
                                </div><?php
                            }
                            ?>
                            <p class="text-right"><?= Html::a("code of ethic", $url->createUrl(['/site/code-of-ethics']), ['class' => 'text-right', 'style' => 'color:black;']) ?></p>
                            <div class="row">
                                <div class="col-md-12 zero-padding-div">
                                    <?= Html::submitButton('Sign Up', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
                                </div>
                            </div>
                        </div>

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-1"></div>
    <div class="col-md-6">
        <div class="row signup-info">
            <div class="col-md-12">
                <?= $this->render('_instructions', ['instructions' => $studentGuide]) ?>
            </div>
        </div>

    </div>
</div>