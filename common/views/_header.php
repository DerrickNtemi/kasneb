<?php 
use yii\helpers\Html;
?>
<div class="row header-row">
    <div class="col-md-5" >
        <?=Html::img('@web/images/logo.png',['alt'=>'logo', 'class'=>'img-responsive logo'])?>
    </div>
    <div class="col-md-4"></div>
    <div class="col-md-2 facebook-div">    
        <a href="https://www.facebook.com/KASNEBOfficial-730456930399562/" target="_blank" style="text-decoration: none; color:#000;">
        <?=Html::img('@web/images/facebook.png',['alt'=>'logo', 'class'=>'img-responsive facebook', 'align'=>'left'])?>
        </a>
        <a href="https://twitter.com/KASNEBOfficial" target="_blank" style="text-decoration: none; color:#000;">
        <?=Html::img('@web/images/twitter.png',['alt'=>'logo', 'class'=>'img-responsive facebook', 'align'=>'left'])?>
        </a>
    </div>
</div>
<div class="row header-below-row">
    <div class="col-md-12"></div>
</div>