<div class="alert alert-danger"  id="bill-error-summary" role="alert" style="display: none;"></div>
<h1></h1>
<?php if(Yii::$app->session->hasFlash('success-message')): ?>
    <div class="alert alert-success"><?=Yii::$app->session->getFlash('success-message')?></div>
<?php endif; ?>

<?php if(Yii::$app->session->hasFlash('error-message')): ?>
    <div class="alert alert-danger"><?=Yii::$app->session->getFlash('error-message')?></div>
<?php endif; ?>