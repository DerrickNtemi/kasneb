<?php

use yii\helpers\Html;
use common\assets\AppAsset;
use common\models\Service;
use yii\helpers\Url;

AppAsset::register($this);
$url = Yii::$app->getUrlManager();
$identity = Yii::$app->user->getIdentity();
$service = new Service();
if ($identity) {
    $notificationsData = $service->getNotifications($identity->student['id']);
}

$regNo = "";
if (isset($identity->student['studentCourses'])) {
    foreach ($identity->student['studentCourses'] as $course) {
        if ($course['verified'] && $course['active']) {
            $regNo = $course['fullRegistrationNumber'];
        }
    }
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>

    <body>
        <?php $this->beginBody() ?>
        <div class="container">
            <div class="row main-row">
                <div class="col-md-12 main-div">
                    <div class="row main-header-div">
                        <div class="col-md-12">
                            <?= $this->render('../_header') ?>
                        </div>
                    </div>
                    <div class="row notification-bar">
                        <div class="col-md-12 text-right">
                            <?php if ($identity) : ?>
                                <?php $notificationsCount = count($notificationsData); ?>
                                <label>
                                    [ <span class="regNo"><?= $regNo ?></span> ] <?= $identity->student['firstName'] . " " . $identity->student['middleName'] ?> <span class="fa fa-user"></span>
                                </label>
                                <label>
                                    <?= Html::a("<span class='fa fa-sign-out logout-icon'></span>", Url::toRoute(['site/logout']), ['title' => 'Logout']) ?>
                                </label>
                                <label class="<?= $notificationsCount > 0 ? 'has-notifications' : 'has-not-notifications' ?>">
                                    <?= Html::a("<span class='fa fa-bell-o span-black'></span><sup class='not'>$notificationsCount</sup>", Url::toRoute(['site/notifications']), ['title' => 'Notifications']) ?>
                                </label>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="row" id="content-div">
                        <div class="col-md-3 menu-div">
                            <?= $this->blocks['menu'] ?>
                        </div>
                        <div class="col-md-9">
                                    <?= $content ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <?= $this->render('../_footer') ?>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>