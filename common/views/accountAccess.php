<?php

use yii\helpers\Html;
use common\assets\AppAsset;

$this->title = Yii::$app->name . ' - ' . $portalAction;

AppAsset::register($this);

$url = Yii::$app->getUrlManager();
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>

    <body>
        <?php $this->beginBody() ?>
        <div class="container">
            <div class="row main-row">
                <div class="col-md-12 main-div">
                    <?= $this->render('_header') ?>
                    <?php
                        if($portalAction == "Login"){
                            echo $this->render('_studentSignIn',['model'=>$model,'studentGuide'=>$studentGuide]); 
                        }elseif($portalAction == "Sign Up"){
                            echo $this->render('_studentSignUp',['model'=>$model,'courseData'=>$courseData,'studentGuide'=>$studentGuide,'countryCodes'=>$countries]); 
                        }
                    ?>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <?= $this->render('_footer') ?>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>