<?php

use yii\helpers\Html;
?>
<p id="url" style="display: none;"></p>
<p id="tabindex" style="display: none;"></p>
<p id="feecode" style="display: none;"></p>
<p id="wizard" style="display: none;"></p>
<p id="pin" style="display: none;"></p>
<p id="phone" style="display: none;"></p>
<p id="payid" style="display: none;"></p>
<p id="invoiceid" style="display: none;"></p>


<div class="row">
    <div class="col-md-12">
        Do you wish to proceed with the payment ?
    </div>
</div>
<h2></h2>
<div id="progress-spinner" class="text-center" style="display: none;">
    <?= Html::img('@web/images/ajax-loader.gif', ['alt' => 'progress spinner']) ?>
</div>
<h2></h2>
<div class="alert alert-danger"  id="error-summary" style="display: none;"></div>